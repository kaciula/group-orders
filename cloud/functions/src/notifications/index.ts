import * as admin from 'firebase-admin'
import {firestoreInstance} from "../index";

export async function notifyStatusChange(orderId: string) {
    const orderDoc = await firestoreInstance.collection('orders').doc(orderId).get();
    const order = orderDoc.data()!;
    const status = order.status;

    const ownerDoc = await firestoreInstance.collection('users').doc(order.ownerId).get();
    const owner = ownerDoc.data()!;
    const isRo = owner.languageCode === 'ro';

    const title = order.title + (isRo ? ' de ' : ' by ') + owner.displayName;
    let body = '';
    if (status === 'inProgress') {
        body = isRo ? 'Comanda este în curs' : 'Order is in progress';
    } else if (status === 'blocked') {
        body = isRo ? 'Comanda este înghețată' : 'Order is freezed';
    } else if (status === 'madeUpstream') {
        body = isRo ? 'Comanda este plasată' : 'Order is placed';
    } else if (status === 'arrived') {
        body = isRo ? 'Comanda a sosit' : 'Order has arrived';
    } else if (status === 'completed') {
        body = isRo ? 'Comanda este finalizată' : 'Order is completed';
    } else if (status === 'forcefullyClosed') {
        body = isRo ? 'Comanda este anulată' : 'Order is cancelled.';
    }

    order.participantIds.forEach(async (participantId: string, index: number) => {
        const userId = order.participantIds[index];
        if (userId !== owner.id) {
            await sendPush(userId, orderId, 'statusChange', title, body);
        }
    });
    return 0;
}

export async function sendPush(userId: string, orderId: string, type: string, title: string, body: string) {
    const userDoc = await firestoreInstance.collection('users').doc(userId).get();
    const user = userDoc.data()!;

    try {
        const token = user.messagingToken;
        const payload = buildMessage(title, body,
                {
                    orderId: orderId,
                    type: type,
                    click_action: 'FLUTTER_NOTIFICATION_CLICK'
                }
        );
        const options = {
          priority: 'high',
          timeToLive: 60 * 60 * 24
        };
        const response = await admin.messaging().sendToDevice(token, payload, options);
        console.log('Notification sent result: ', response.results[0]);
    } catch (error) {
        console.error('Notification sent failed: ', error);
    }
}

const buildMessage = (title: string, body: string, data: any) => {
    return {
        notification: {
            title: title,
            body: body
        },
        data: data,
    };
}
