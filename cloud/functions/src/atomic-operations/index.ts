//import * as admin from 'firebase-admin'
import {firestoreInstance} from "../index";

export async function updateProfileFields(userId: string, displayName: string) {
    console.log('Called update profile fields function for user ', userId);

    const orders = await getOrdersAsOwner(userId);
    const updateOrdersBatch = firestoreInstance.batch();
    orders.forEach((order: FirebaseFirestore.DocumentSnapshot) => {
        updateOrdersBatch.update(order.ref, {'ownerName': displayName});
    });

    const orders2 = await getOrdersAsParticipant(userId);
    const updateOrdersBatch2 = firestoreInstance.batch();
    orders2.forEach((order: FirebaseFirestore.DocumentSnapshot) => {
        updateOrdersBatch2.update(order.ref.collection('participants').doc(userId), {'displayName': displayName});
    });

    console.log('Committing the orders updates...');
    return Promise.all([updateOrdersBatch.commit(), updateOrdersBatch2.commit()]);
}

async function getOrdersAsOwner(userId: string): Promise<FirebaseFirestore.DocumentSnapshot[]> {
    const orders = await firestoreInstance.collection('orders').where('ownerId', '==', userId).get();
    return orders.docs;
}

async function getOrdersAsParticipant(userId: string): Promise<FirebaseFirestore.DocumentSnapshot[]> {
    const orders = await firestoreInstance.collection('orders').where('participantIds', 'array-contains', userId).get();
    return orders.docs;
}

export async function updateProductStock(orderId: string, productId: string, numItemsToAdd: number) {
    console.log('Called update product stock function for order ', orderId, ' and product ', productId);

    const productPath = firestoreInstance.collection('orders').doc(orderId).collection('products').doc(productId);
    const productDoc = await productPath.get();
    const product = productDoc.data()!;
    if (product.numItemsAvailable !== null && product.numItemsAvailable >= 0) {
        const numItemsAvailable = product.numItemsAvailable + numItemsToAdd;
        return productPath.update({'numItemsAvailable': numItemsAvailable});
    }
    return 0;
}