import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
admin.initializeApp(functions.config().firebase);

import * as atomicFunctions from './atomic-operations/index';
import * as notificationFunctions from './notifications/index';

export const firestoreInstance = admin.firestore();

export const onUserUpdate = functions.firestore
    .document('users/{userId}')
    .onUpdate((change, context) => {
        const newValue = change.after.data();
        const previousValue = change.before.data();

        console.log('User ', newValue.id, ' updated and is now ', newValue);

        if (newValue.displayName !== previousValue.displayName) {
            return atomicFunctions.updateProfileFields(newValue.id, newValue.displayName);
        }
        return 0;
    });

export const onStatusOrderUpdate = functions.firestore
    .document('orders/{orderId}')
    .onUpdate((change, context) => {
        const newValue = change.after.data();
        const previousValue = change.before.data();

        console.log('Order ', newValue.id, ' updated and is now ', newValue);

        if (newValue.status !== previousValue.status) {
            return notificationFunctions.notifyStatusChange(newValue.id);
        }
        return 0;
    });

export const onCartEntryCreate = functions.firestore
    .document('orders/{orderId}/cart/{cartEntryId}')
    .onCreate((snap, context) => {
        const newValue = snap.data();

        console.log('Order ', context.params.orderId, ' has cart entry created ', newValue.id);

        if (newValue.type === 'Product') {
            return atomicFunctions.updateProductStock(context.params.orderId, newValue.productId, 0 - newValue.numItems);
        }
        return 0;
    });

export const onCartEntryUpdate = functions.firestore
    .document('orders/{orderId}/cart/{cartEntryId}')
    .onUpdate((change, context) => {
        const newValue = change.after.data();
        const previousValue = change.before.data();

        console.log('Order ', context.params.orderId, ' has cart entry updated ', newValue.id);

        if (newValue.type === 'Product' && newValue.numItems !== previousValue.numItems) {
            return atomicFunctions.updateProductStock(context.params.orderId, newValue.productId, previousValue.numItems - newValue.numItems);
        }
        return 0;
    });

export const onCartEntryDelete = functions.firestore
    .document('orders/{orderId}/cart/{cartEntryId}')
    .onDelete((snap, context) => {
        const deletedValue = snap.data();

        console.log('Order ', context.params.orderId, ' has cart entry deleted ', deletedValue.id);

        if (deletedValue.type === 'Product') {
            return atomicFunctions.updateProductStock(context.params.orderId, deletedValue.productId, deletedValue.numItems);
        }
        return 0;
    });

export const deleteAccount = functions.https.onRequest(async (req, res) => {
    try {
        const userId = req.body.data.userId;
        await admin.auth().deleteUser(userId);
        console.log("Deleted account with userId: ", userId);
        res.send('{"data": {"success": true}}');
    } catch (error) {
        console.error("Error deleting account: ", error);
        res.send('{"data": {"success": false}}');
    }
});