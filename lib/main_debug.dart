import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:orders/app/app.dart';
import 'package:orders/main_utils.dart';
import 'package:orders/service_locator.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  registerInstances();

  await init();

  return runApp(ThisApp());
}
