import 'package:flutter/material.dart';

// ignore: avoid_classes_with_only_static_members
class AppColors {
  static Color primary = Colors.purple[300]!;
  static const Color primaryDark = Color(0xFF883997);
  static const Color primaryLight = Color(0xFFEE98FB);
  static const Color accent = Color(0xFF81C784);

  static const Color textWhite = Color(0xFFFAFAFA);
  static const Color textGreyLight = Color(0xFFDDDDDD);
  static const Color textGreyDark = Color(0xFF888888);

  static const Color snackBarError = Colors.red;
  static const Color blue = Color(0xFF4267B2);
  static const Color red = Colors.red;
  static const Color orange = Colors.orange;
  static const Color lightGrey = Color(0xFFF5F5F5);
  static const Color mediumGrey = Color(0xFFBBBBBB);

  static const Color connectEmail = Color(0xFF41AAF3);
  static const Color connectFacebook = Color(0xFF4267B2);
  static const Color connectGoogle = Color(0xFF4184F3);
}
