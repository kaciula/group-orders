import 'package:flutter/material.dart';

import 'app_colors.dart';

const TextStyle styleDirections = TextStyle(fontSize: 16, height: 1.5);

const TextStyle styleEmpty = TextStyle(fontSize: 16, height: 1.5);

const TextStyle styleAppBarSubtitle = TextStyle(
  color: AppColors.textGreyLight,
  fontSize: 15,
  fontStyle: FontStyle.italic,
);

const TextStyle styleProductAndPrice = TextStyle(fontSize: 16);

const TextStyle styleTab = TextStyle(fontSize: 15);
