import 'package:orders/app/app_locale.dart';
import 'package:orders/utils/app_constants.dart';

// ignore: avoid_classes_with_only_static_members
class Strings {
  static String get appName {
    return isProduction
        ? (AppLocale.isRo ? 'Comenzi Comune' : 'Group Orders')
        : (AppLocale.isRo ? 'Comenzi Comune Debug' : 'Group Orders Debug');
  }

  /* Home screen */
  static String get homeTitle {
    return AppLocale.isRo ? 'Comenzi' : 'Orders';
  }

  static String get homeEmpty {
    return AppLocale.isRo
        ? 'Nu ai nicio comandă.\nCreează una folosind iconița +.\nSau fă rost de o invitație pentru o comandă deja existentă.'
        : 'No orders.\nCreate one using the + icon.\nOr get an invitation for an already existing order.';
  }

  static String get homeErrorMsg {
    return AppLocale.isRo
        ? 'Eroare actualizare comenzi'
        : 'Failure to refresh orders';
  }

  static String get homeActiveOrders {
    return AppLocale.isRo ? 'COMENZI ACTIVE' : 'ACTIVE ORDERS';
  }

  static String get homePastOrders {
    return AppLocale.isRo ? 'COMENZI VECHI' : 'PAST ORDERS';
  }

  /* CreateOrder screen */
  static String get createOrderTitle {
    return AppLocale.isRo ? 'Creare comandă' : 'Create Order';
  }

  static String get createOrderContinue {
    return AppLocale.isRo ? 'Continuă' : 'Continue';
  }

  static String get createOrderDuplicate {
    return AppLocale.isRo
        ? 'Alege comanda pentru duplicare'
        : 'Choose order to duplicate';
  }

  static String get createOrderProviderTitle {
    return AppLocale.isRo ? 'Comercializare produse' : 'Merchant';
  }

  static String get createOrderProviderDesc {
    return AppLocale.isRo
        ? 'Sunt comerciant și am produse de vânzare.'
        : 'I have products for sale.';
  }

  static String get createOrderProxyTitle {
    return AppLocale.isRo ? 'Intermediere comandă' : 'Intermediary';
  }

  static String get createOrderProxyDesc {
    return AppLocale.isRo
        ? 'Sunt persoană fizică care vrea să intermedieze o comandă pentru mai mulți cunoscuți.'
        : 'I am a person who wants to make a group order for several people.';
  }

  static String get createOrderStep1 {
    return AppLocale.isRo ? 'PAS 1. TIPUL COMENZII' : 'STEP 1. ORDER TYPE';
  }

  static String get createOrderStep2 {
    return AppLocale.isRo
        ? 'PAS 2. SETĂRI INIȚIALE'
        : 'STEP 2. INITIAL SETTINGS';
  }

  static String get createOrderBlankTitle {
    return AppLocale.isRo ? 'Creare de la zero' : 'Start from scratch';
  }

  static String get createOrderBlankDesc {
    return AppLocale.isRo
        ? 'Introdu toate detaliile comenzii.'
        : 'Enter all order details.';
  }

  static String get createOrderDuplicateTitle {
    return AppLocale.isRo
        ? 'Duplicare comandă existentă'
        : 'Duplicate existing order';
  }

  static String get createOrderDuplicateDesc {
    return AppLocale.isRo
        ? 'Pornește de la o comandă care deja există. Poți modifica tot ce vrei diferit.'
        : 'Choose an existing order to start from. You can edit everything you want different.';
  }

  /* EditOrder screen */
  static String get editOrderCreateTitle {
    return AppLocale.isRo ? 'Creare comandă' : 'Create Order';
  }

  static String get editOrderEditTitle {
    return AppLocale.isRo ? 'Editare comandă' : 'Edit Order';
  }

  static String get editOrderTitleField {
    return AppLocale.isRo ? 'Titlu' : 'Title';
  }

  static String get editOrderDetails {
    return AppLocale.isRo
        ? 'Detalii opționale precum site-ul de ecommerce și motivul pentru care vrei să faci o comanda comună.'
        : 'Optional details such as the ecommerce site and the reason you want to make a group order.';
  }

  static String get editOrderCreate {
    return AppLocale.isRo ? 'CREARE' : 'CREATE';
  }

  static String get editOrderErrorMsg {
    return AppLocale.isRo ? 'Eroare salvare comandă' : 'Failure to save order';
  }

  static String get editOrderCurrency {
    return AppLocale.isRo ? 'Moneda:' : 'Currency:';
  }

  static String get editOrderCurrencyUnknown {
    return AppLocale.isRo ? 'Monedă necunoscută' : 'Unknown currency';
  }

  /* Invite screen */
  static String get inviteTitle {
    return AppLocale.isRo ? 'Invită oameni' : 'Invite people';
  }

  static String get inviteDesc {
    return AppLocale.isRo
        ? 'Partajează acest link cu persoanele pe care vrei să le inviți la această comandă comună.'
        : 'Share this link with people you want to invite to this group order.';
  }

  static String get inviteShare {
    return AppLocale.isRo ? 'Partajează' : 'Share';
  }

  static String get inviteShareErrorMsg {
    return AppLocale.isRo ? 'Eroare de partajare' : 'Share failure';
  }

  static String get inviteCopyLinkSuccess {
    return AppLocale.isRo
        ? 'Link copiat cu succes'
        : 'Link copied successfully';
  }

  static String inviteShareTxt({
    required String userDisplayName,
    required String link,
    required String orderTitle,
  }) {
    return AppLocale.isRo
        ? '$userDisplayName te invită la o comandă comună numită "$orderTitle". Apasă pe următorul link.\n\n$link'
        : '$userDisplayName is inviting you to \'$orderTitle\' group order. Click on the following link.\n\n$link';
  }

  static String inviteShareSubject({required String displayName}) {
    return AppLocale.isRo
        ? 'Invitație pentru o comandă comună de la $displayName'
        : 'Group order invitation from $displayName';
  }

  /* Order screen */
  static String get orderCreateProduct {
    return AppLocale.isRo ? 'CREARE PRODUS' : 'CREATE PRODUCT';
  }

  static String get orderProducts {
    return AppLocale.isRo ? 'PRODUSE' : 'PRODUCTS';
  }

  static String get orderEdit {
    return AppLocale.isRo ? 'Editează' : 'Edit';
  }

  static String get orderDelete {
    return AppLocale.isRo ? 'Șterge' : 'Delete';
  }

  static String get orderNumItems {
    return AppLocale.isRo ? 'Nr. produse:' : 'No. products:';
  }

  static String orderNumItemsAvailable(int numItems) {
    return AppLocale.isRo ? 'au mai rămas $numItems' : '$numItems remaining';
  }

  static String get orderOutOfStock {
    return AppLocale.isRo ? 'stoc zero' : 'out of stock';
  }

  static String get orderEntryDiscount {
    return AppLocale.isRo ? 'Reducere' : 'Discount';
  }

  static String get orderEntryTransport {
    return AppLocale.isRo ? 'Transport' : 'Transport';
  }

  static String get orderInvite {
    return AppLocale.isRo ? 'Invită persoane' : 'Invite people';
  }

  static String get orderViewParticipants {
    return AppLocale.isRo ? 'Vizualizare participanți' : 'View participants';
  }

  static String get orderAddToCart {
    return AppLocale.isRo ? 'ADAUGĂ ÎN COȘ' : 'ADD TO CART';
  }

  static String get orderNoProducts {
    return AppLocale.isRo
        ? 'Niciun produs creat încă.\n\nMai întâi creează produsul și apoi tu sau orice alt participant îl poate adăuga în coș.'
        : 'No products created yet.\n\nFirst create a product and then you or any other participant can add it to the cart.';
  }

  static String get orderAddProductToCartDetails {
    return AppLocale.isRo
        ? 'Detalii opționale precum mărimea și culoarea pe care o dorești'
        : 'Optional details such as the size and color you want';
  }

  static String get orderAddDiscountToCartDetails {
    return AppLocale.isRo
        ? 'Detalii opționale precum motivul pentru care se aplică reducerea'
        : 'Optional details such as the reason there is a discount';
  }

  static String get orderAddTransportToCartDetails {
    return AppLocale.isRo
        ? 'Detalii opționale precum motivul pentru care există taxă de transport'
        : 'Optional details such as reason for a transport tax';
  }

  static String get orderAddProductToCartSuccess {
    return AppLocale.isRo
        ? 'Produsul a fost adăugat cu succes în coș'
        : 'Product successfully added to cart';
  }

  static String get orderAddProductToCartErrorMsg {
    return AppLocale.isRo
        ? 'Eroare adăugare produs în coș'
        : 'Failure adding product to cart';
  }

  static String get orderAddEntryToCartSuccess {
    return AppLocale.isRo
        ? 'Adăugat cu succes în coș'
        : 'Successfully added to cart';
  }

  static String get orderAddEntryToCartErrorMsg {
    return AppLocale.isRo ? 'Eroare adăugare în coș' : 'Failure adding to cart';
  }

  static String get orderStatusOrder {
    return AppLocale.isRo ? 'COMANDĂ' : 'ORDER';
  }

  static String get orderStatusInProgress {
    return AppLocale.isRo ? 'ÎN CURS' : 'IN PROGRESS';
  }

  static String get orderStatusBlocked {
    return AppLocale.isRo ? 'ÎNGHEȚATĂ' : 'FREEZED';
  }

  static String get orderStatusMadeUpstream {
    return AppLocale.isRo ? 'PLASATĂ' : 'PLACED';
  }

  static String get orderStatusArrived {
    return AppLocale.isRo ? 'SOSITĂ' : 'ARRIVED';
  }

  static String get orderStatusCompleted {
    return AppLocale.isRo ? 'FINALIZATĂ' : 'COMPLETED';
  }

  static String get orderStatusForcefullyClosed {
    return AppLocale.isRo ? 'ANULATĂ' : 'CANCELLED';
  }

  static List<String> orderStatusInProgressDesc(
      {required bool hasFeatureEditProducts}) {
    return AppLocale.isRo
        ? <String>[
            'Comanda este în curs.',
            hasFeatureEditProducts
                ? 'Poți crea și adăuga produse în coș.'
                : 'Poți adăuga produsele dorite în coș.'
          ]
        : <String>[
            'Order is in progress.',
            hasFeatureEditProducts
                ? 'You can create and add products to cart.'
                : 'You can add desired products to cart.'
          ];
  }

  static List<String> get orderStatusBlockedDesc {
    return AppLocale.isRo
        ? <String>[
            'Comanda este înghețată.',
            'Nu mai poți adăuga sau șterge produse din coș.'
          ]
        : <String>[
            'Order is freezed.',
            'You can no longer add or remove products from cart.'
          ];
  }

  static List<String> orderStatusMadeUpstreamDesc(String ownerName) {
    return AppLocale.isRo
        ? <String>[
            'Comanda este plasată.',
            '$ownerName te va anunța când va ajunge.'
          ]
        : <String>[
            'Order is placed.',
            '$ownerName will notify you when it arrives.'
          ];
  }

  static List<String> orderStatusArrivedDesc(String ownerName) {
    return AppLocale.isRo
        ? <String>[
            'Comanda a sosit.',
            'Vorbește cu $ownerName să îți primești produsele.'
          ]
        : <String>[
            'Order has arrived.',
            'Talk to $ownerName to receive your products.'
          ];
  }

  static List<String> get orderStatusCompletedDesc {
    return AppLocale.isRo
        ? <String>['Comanda este finalizată.', 'Petrecereeee!']
        : <String>['Order is completed.', 'Enjoy!'];
  }

  static List<String> get orderStatusForcefullyClosedDesc {
    return AppLocale.isRo
        ? <String>['Comanda este anulată.', 'Nasol!']
        : <String>['Order is cancelled.', 'Sorry!'];
  }

  static List<String> orderStatusOwnerInProgressDesc(
      {required bool hasFeatureEditProducts}) {
    return AppLocale.isRo
        ? <String>[
            'Comanda este în curs.',
            hasFeatureEditProducts
                ? 'Orice participant poate crea și adăuga produse în coș.'
                : 'Orice participant poate adăuga produse în coș.'
          ]
        : <String>[
            'Order is in progress.',
            hasFeatureEditProducts
                ? 'Any participant can create and add products to cart.'
                : 'Any participant can add products to cart.'
          ];
  }

  static List<String> get orderStatusOwnerBlockedDesc {
    return AppLocale.isRo
        ? <String>[
            'Comanda este înghețată.',
            'Ceilalți participanți nu mai pot adăuga sau șterge produse din coș.'
          ]
        : <String>[
            'Order is freezed.',
            'The other participants can no longer add or remove products from cart.'
          ];
  }

  static List<String> orderStatusOwnerMadeUpstreamDesc() {
    return AppLocale.isRo
        ? <String>[
            'Comanda este plasată.',
            'Nu uita să schimbi statusul atunci când aceasta va ajunge.'
          ]
        : <String>[
            'Order is placed.',
            'Don\'t forget to change status when it will arrive.'
          ];
  }

  static List<String> orderStatusOwnerArrivedDesc({bool isProxyType = true}) {
    return AppLocale.isRo
        ? <String>[
            'Comanda a sosit.',
            isProxyType
                ? 'Vorbește cu ceilalți ca să le dai produsele.'
                : 'Vorbește cu clienții ca să le dai produsele.'
          ]
        : <String>[
            'Order has arrived.',
            'Talk to the others to give them their products.'
          ];
  }

  static List<String> get orderStatusOwnerCompletedDesc {
    return AppLocale.isRo
        ? <String>['Comanda este finalizată.', 'Petrecereeee!']
        : <String>['Order is completed.', 'Enjoy!'];
  }

  static List<String> get orderStatusOwnerForcefullyClosedDesc {
    return AppLocale.isRo
        ? <String>['Comanda este anulată.', 'Nasol!']
        : <String>['Order is cancelled.', 'Sorry!'];
  }

  static String get orderAreYouSureDelete {
    return AppLocale.isRo
        ? 'Ești sigur că vrei să ștergi acest produs?'
        : 'Are you sure you want to delete this product?';
  }

  static String get orderAreYouSureDeleteOrder {
    return AppLocale.isRo
        ? 'Ești sigur că vrei să ștergi această comandă?'
        : 'Are you sure you want to delete this order?';
  }

  static String get orderProductRemoveSuccess {
    return AppLocale.isRo
        ? 'Produsul a fost șters cu succes'
        : 'Product successfully deleted';
  }

  static String get orderProductRemoveErrorMsg {
    return AppLocale.isRo
        ? 'Eroare ștergere produs'
        : 'Failure deleting product';
  }

  static String get orderChangeStatus {
    return AppLocale.isRo ? 'Modifică status comandă' : 'Change order status';
  }

  static String get orderStatusSuccess {
    return AppLocale.isRo
        ? 'Status schimbat cu succes'
        : 'Status successfully changed';
  }

  static String get orderStatusErrorMsg {
    return AppLocale.isRo
        ? 'Eroare schimbare status'
        : 'Failure changing status';
  }

  static String get orderDeletionErrorMsg {
    return AppLocale.isRo
        ? 'Eroare ștergere comandă'
        : 'Failure deleting order';
  }

  static String get orderPercentInvalid {
    return AppLocale.isRo ? 'Procent invalid' : 'Invalid percent';
  }

  static String get orderTransportInvalid {
    return AppLocale.isRo ? 'Cost invalid' : 'Invalid cost';
  }

  static String get orderCart {
    return AppLocale.isRo ? 'Coș' : 'Cart';
  }

  static String get orderDoNotForget {
    return AppLocale.isRo
        ? 'Nu uita să adaugi în coș produsul pe care tocmai l-ai creat.'
        : 'Do not forget to add to cart the product you have just created.';
  }

  /* Cart screen */
  static String get cartTitle {
    return AppLocale.isRo ? 'Coș' : 'Cart';
  }

  static String get cartTotal {
    return AppLocale.isRo ? 'TOTAL' : 'TOTAL';
  }

  static String get cartAllParticipants {
    return AppLocale.isRo ? 'Toți participanții' : 'All Participants';
  }

  static String get cartAreYouSureProduct {
    return AppLocale.isRo
        ? 'Ești sigur că vrei să elimini acest produs din coș?'
        : 'Are you sure you want to remove this product from the cart?';
  }

  static String get cartAreYouSureEntry {
    return AppLocale.isRo
        ? 'Ești sigur că vrei să elimini această intrare din coș?'
        : 'Are you sure you want to remove this entry from the cart?';
  }

  static String get cartRemoveSuccess {
    return AppLocale.isRo
        ? 'Produsul a fost eliminat cu succes din coș'
        : 'Product successfully removed from cart';
  }

  static String get cartRemoveErrorMsg {
    return AppLocale.isRo
        ? 'Eroare eliminare produs din coș'
        : 'Failure removing product from cart';
  }

  static String get cartEditErrorMsg {
    return AppLocale.isRo
        ? 'Eroare modificare intrare'
        : 'Failure editing item entry';
  }

  static String get cartEditSuccess {
    return AppLocale.isRo
        ? 'Intrarea a fost modificată cu succes'
        : 'Item entry successfully modified';
  }

  static String get cartEmpty {
    return AppLocale.isRo ? 'Niciun produs în coș.' : 'No products in cart.';
  }

  static String get cartEmptyOwner {
    return AppLocale.isRo
        ? 'Niciun produs în coș. Adaugă unul din ecranul cu produse.'
        : 'No products in cart. Add some from the products screen.';
  }

  static String get cartCall {
    return AppLocale.isRo ? 'Sună' : 'Call';
  }

  /* Participants screen */
  static String get participantsTitle {
    return AppLocale.isRo ? 'Participanți' : 'Participants';
  }

  static String get participantsActive {
    return AppLocale.isRo ? 'PARTICIPANȚI ACTIVI' : 'ACTIVE PARTICIPANTS';
  }

  static String get participantsShy {
    return AppLocale.isRo ? 'PARTICIPANȚI TIMIZI' : 'SHY PARTICIPANTS';
  }

  static String get participantsMarkDelivered {
    return AppLocale.isRo ? 'Bifează livrarea' : 'Mark as delivered';
  }

  static String get participantsConfirmDelivered {
    return AppLocale.isRo ? 'Confirmă livrarea' : 'Confirm delivery';
  }

  static String get participantsMarkPaid {
    return AppLocale.isRo ? 'Bifează plata' : 'Mark as paid';
  }

  static String get participantsConfirmPaid {
    return AppLocale.isRo ? 'Confirmă plata' : 'Confirm payment';
  }

  static String get participantsDelivery {
    return AppLocale.isRo ? 'LIVRAT' : 'DELIVERED';
  }

  static String get participantsPayment {
    return AppLocale.isRo ? 'PLĂTIT' : 'PAID';
  }

  static String get participantsPaymentMethodTitle {
    return AppLocale.isRo ? 'Introdu metoda de plată' : 'Enter payment method';
  }

  static String get participantsPaymentMethodHint {
    return AppLocale.isRo
        ? 'cash, transfer bancar, Revolut etc.'
        : 'cash, bank transfer, Revolut etc.';
  }

  /* Edit product screen */
  static String get editProductCreateTitle {
    return AppLocale.isRo ? 'Creare produs' : 'Create product';
  }

  static String get editProductEditTitle {
    return AppLocale.isRo ? 'Editare produs' : 'Edit product';
  }

  static String get editProductTitleField {
    return AppLocale.isRo ? 'Titlu' : 'Title';
  }

  static String get editProductDetails {
    return AppLocale.isRo
        ? 'Detalii opționale precum link-ul către produs'
        : 'Optional details such as product link';
  }

  static String get editProductPrice {
    return AppLocale.isRo ? 'Preț' : 'Price';
  }

  static String get editProductNumItemsAvailable {
    return AppLocale.isRo ? 'bucăți disponibile' : 'items available';
  }

  static String get editProductStock {
    return AppLocale.isRo ? 'Stoc' : 'Stock';
  }

  static String get editProductUnlimited {
    return AppLocale.isRo ? 'Nelimitat' : 'Unlimited';
  }

  static String get editProductNum {
    return AppLocale.isRo ? 'nr.' : 'no.';
  }

  static String get editProductCreate {
    return AppLocale.isRo ? 'Creare' : 'Create';
  }

  static String get editProductInvalidPrice {
    return AppLocale.isRo ? 'Preț invalid' : 'Invalid price';
  }

  static String get editProductInvalidStock {
    return AppLocale.isRo ? 'Stoc invalid' : 'Invalid stock';
  }

  static String get editProductErrorMsg {
    return AppLocale.isRo ? 'Eroare salvare produs' : 'Failure to save product';
  }

  /* Invite screen */
  static String get acceptInviteTitle {
    return AppLocale.isRo ? 'Invitație comandă' : 'Order Invitation';
  }

  static String get acceptInviteAction {
    return AppLocale.isRo ? 'Acceptă invitație' : 'Accept invitation';
  }

  static String get acceptInviteErrorGet {
    return AppLocale.isRo
        ? 'Eroare preluare comandă comună'
        : 'Error fetching the group order';
  }

  static String acceptInviteWelcome(String userName) {
    return AppLocale.isRo
        ? '$userName te-a invitat să participi la următoarea comandă comună.'
        : '$userName invited you to the following group order.';
  }

  /* Profile screen */
  static String get profileTitle {
    return AppLocale.isRo ? 'Profil' : 'Profile';
  }

  static String get profileSignOut {
    return AppLocale.isRo ? 'DECONECTARE' : 'SIGN OUT';
  }

  static String get profileContact {
    return AppLocale.isRo ? 'CONTACTEAZĂ DEZVOLTATORUL' : 'CONTACT DEVELOPER';
  }

  static String profileSignedInAs(
      {required String displayName, required String emailAddress}) {
    return AppLocale.isRo
        ? 'Conectat ca $displayName ($emailAddress)'
        : 'Signed in as $displayName ($emailAddress)';
  }

  static String get profileDeleteAccount {
    return AppLocale.isRo ? 'ȘTERGE CONT' : 'DELETE ACCOUNT';
  }

  static String get profileSureDeleteAccount {
    return AppLocale.isRo
        ? 'Ești sigur că vrei să ștergi contul? Acțiunea este ireversibilă.'
        : 'Are you sure you want to delete your account? This is irreversible.';
  }

  /* Edit Profile screen */
  static String get editProfileTitle {
    return AppLocale.isRo ? 'Editare Profil' : 'Edit Profile';
  }

  /* VerifyPhone screen */
  static String get verifyPhoneTitle {
    return AppLocale.isRo ? 'Adaugă număr de telefon' : 'Add phone number';
  }

  static String get verifyPhoneIntro {
    return AppLocale.isRo
        ? 'Persoanele cunoscute te pot găsi în aplicație pe baza numărului tău de telefon.'
        : 'Acquaintances can find you inside the app based on your phone number.';
  }

  static String get verifyPhoneContinue {
    return AppLocale.isRo ? 'Continuă' : 'Continue';
  }

  static String get verifyPhoneReenter {
    return AppLocale.isRo ? 'Reîncearcă' : 'Retry';
  }

  static String get verifyPhoneHint {
    return AppLocale.isRo ? 'Număr de telefon' : 'Phone number';
  }

  static String get verifyPhoneInputErr {
    return AppLocale.isRo ? 'Invalid' : 'Invalid';
  }

  static String get verifyPhoneCodeErr {
    return AppLocale.isRo
        ? 'Eroare verificare număr de telefon'
        : 'Failure to verify phone number';
  }

  static String verifyPhoneCodeText(String phoneNumber) {
    return AppLocale.isRo
        ? 'Introdu codul SMS de verificare trimis către $phoneNumber'
        : 'Enter the SMS verification code sent to $phoneNumber';
  }

  static String get verifyPhoneSkip {
    return AppLocale.isRo ? 'MAI TÂRZIU' : 'SKIP';
  }

  /* Connect screen */
  static String get connectTitle {
    return AppLocale.isRo ? 'Creare cont' : 'Create account';
  }

  static String get connectIntro1 {
    return AppLocale.isRo ? 'Salut!\n\n' : 'Welcome!\n\n';
  }

  static String get connectIntro2 {
    return AppLocale.isRo
        ? 'Ai nevoie de un cont pentru a putea participa la comenzi comune cu alte persoane.'
        : 'You need an account to be able to make group orders with other people.';
  }

  static String get connectEmail {
    return AppLocale.isRo ? 'CONECTARE CU EMAIL' : 'CONNECT WITH EMAIL';
  }

  static String get connectGoogle {
    return AppLocale.isRo ? 'CONECTARE CU GOOGLE' : 'CONNECT WITH GOOGLE';
  }

  static String get connectFacebook {
    return AppLocale.isRo ? 'CONECTARE CU FACEBOOK' : 'CONNECT WITH FACEBOOK';
  }

  static String get connectSignInErrorMsg {
    return AppLocale.isRo ? 'Eroare conectare' : 'Sign in failure';
  }

  /* Sign in email screen */
  static String get signInEmailTitle {
    return AppLocale.isRo ? 'Conectare' : 'Sign In';
  }

  static String get signInEmailAction {
    return AppLocale.isRo ? 'CONECTARE' : 'SIGN IN';
  }

  static String get signInEmailFailure {
    return AppLocale.isRo ? 'Eroare conectare' : 'Sign in failure';
  }

  static String get signInEmailInvalidEmail {
    return AppLocale.isRo ? 'Email invalid' : 'Invalid email';
  }

  static String get signInEmailInvalidPassword {
    return AppLocale.isRo ? 'Parolă prea scurtă' : 'Password too short';
  }

  /* Sign up screen */
  static String get signUpTitle {
    return AppLocale.isRo ? 'Înregistrare cont' : 'Sign Up';
  }

  static String get signUpDisplayName {
    return AppLocale.isRo ? 'Nume' : 'Name';
  }

  static String get signUpInput {
    return AppLocale.isRo ? 'Email' : 'Email';
  }

  static String get signUpPassword {
    return AppLocale.isRo ? 'Parolă' : 'Password';
  }

  static String get signUpConfirmPassword {
    return AppLocale.isRo ? 'Confirmă parola' : 'Confirm Password';
  }

  static String get signUpAction {
    return AppLocale.isRo ? 'ÎNREGISTRARE' : 'SIGN UP';
  }

  static String get signUpPasswordsMismatch {
    return AppLocale.isRo
        ? 'Parolele nu sunt identice'
        : 'Passwords don\'t match';
  }

  static String get signUpInvalidPassword {
    return AppLocale.isRo ? 'Parolă prea scurtă' : 'Password too short';
  }

  static String get signUpFailure {
    return AppLocale.isRo ? 'Eroare înregistrare' : 'Sign up failure';
  }

  static String get signUpExisting {
    return AppLocale.isRo
        ? 'Ai deja un cont? CONECTARE'
        : 'Have an existing account? SIGN IN';
  }

  static String get signUpSignIn {
    return AppLocale.isRo ? 'CONECTARE?' : 'SIGN IN?';
  }

  /* Generic */
  static String get genericNoInternetTitle {
    return AppLocale.isRo
        ? 'Conectează-te la Internet'
        : 'Connect to the Internet';
  }

  static String get genericNoInternetMsg {
    return AppLocale.isRo
        ? 'Nu ai conexiune la Internet. Verifică-ți conexiunea.'
        : 'You have no Internet connection. Check your connection.';
  }

  static String get genericNoInternetSnackBarMsg {
    return AppLocale.isRo
        ? 'Nu ai conexiune la Internet'
        : 'No Internet connection.';
  }

  static String get genericErrorSnackBarMsg {
    return AppLocale.isRo
        ? 'Oops! Încearcă din nou.'
        : 'Oops! Please try again.';
  }

  static String get genericErrorTitle {
    return AppLocale.isRo ? 'Oops!' : 'Oops!';
  }

  static String get genericErrorMsg {
    return AppLocale.isRo
        ? 'Ceva neașteptat s-a întâmplat. Încearcă din nou.'
        : 'Something unexpected happened. Please try again.';
  }

  static String get genericRetry {
    return AppLocale.isRo ? 'Reîncearcă' : 'Retry';
  }

  static String get genericOk {
    return AppLocale.isRo ? 'OK' : 'OK';
  }

  static String get genericYes {
    return AppLocale.isRo ? 'DA' : 'YES';
  }

  static String get genericNo {
    return AppLocale.isRo ? 'NU' : 'NO';
  }

  static String get genericRequired {
    return AppLocale.isRo ? 'Obligatoriu' : 'Required';
  }

  static String get genericInvalidEmail {
    return AppLocale.isRo ? 'Email invalid' : 'Invalid email';
  }

  static String get genericSave {
    return AppLocale.isRo ? 'SALVARE' : 'SAVE';
  }

  static String get genericTalkWhatsApp {
    return AppLocale.isRo ? 'Vorbește pe WhatsApp' : 'Talk on WhatsApp';
  }

  static String get genericDueDate {
    return AppLocale.isRo ? 'Dată scadentă:' : 'Due Date:';
  }

  static String get genericDeliveryDate {
    return AppLocale.isRo ? 'Dată livrare:' : 'Delivery Date:';
  }
}
