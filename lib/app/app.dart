import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orders/app/app_routes.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/app/app_theme.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/analytics.dart';
import 'package:orders/screens/splash/bloc/splash_bloc.dart';
import 'package:orders/screens/splash/bloc/splash_event.dart';
import 'package:orders/screens/splash/splash_screen.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/utils/app_constants.dart';

class ThisApp extends StatefulWidget {
  @override
  _ThisAppState createState() => _ThisAppState();
}

class _ThisAppState extends State<ThisApp> {
  final DataBloc _dataBloc = getIt<DataBloc>();
  final Analytics _analytics = getIt<Analytics>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: Strings.appName,
        home: BlocProvider<SplashBloc>(
          create: (BuildContext context) =>
              SplashBloc(_dataBloc)..add(AppStarted()),
          child: SplashScreen(),
        ),
        theme: appTheme(),
        navigatorObservers: isProduction
            ? _analytics.navigatorObservers()
            : const <NavigatorObserver>[],
        onGenerateRoute: (RouteSettings settings) {
          return appRoutes(settings, _dataBloc);
        },
      ),
    );
  }

  @override
  void dispose() {
    _dataBloc.close();
    super.dispose();
  }
}
