import 'package:flutter/material.dart';
import 'package:orders/app/app_colors.dart';

ThemeData appTheme() {
  return ThemeData(
    typography: Typography.material2018(),
    primaryColor: AppColors.primary,
    primaryColorDark: AppColors.primaryDark,
    primaryColorLight: AppColors.primaryLight,
    appBarTheme: AppBarTheme(color: AppColors.primary),
    dividerTheme: const DividerThemeData(space: 3),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(foregroundColor: AppColors.accent),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: TextButton.styleFrom(foregroundColor: AppColors.accent),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(backgroundColor: AppColors.accent)),
    inputDecorationTheme: InputDecorationTheme(
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: AppColors.primary),
      ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: AppColors.mediumGrey),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: AppColors.mediumGrey),
      ),
    ),
    floatingActionButtonTheme:
        FloatingActionButtonThemeData(backgroundColor: AppColors.accent),
  );
}
