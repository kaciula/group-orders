import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/accept_invite/accept_invite_screen.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_bloc.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_event.dart'
    as accept_invite;
import 'package:orders/screens/cart/bloc/cart_bloc.dart';
import 'package:orders/screens/cart/cart_screen.dart';
import 'package:orders/screens/connect/bloc/connect_bloc.dart';
import 'package:orders/screens/connect/connect_screen.dart';
import 'package:orders/screens/create_order/bloc/create_order_bloc.dart';
import 'package:orders/screens/create_order/create_order_screen.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_bloc.dart';
import 'package:orders/screens/edit_order/edit_order_screen.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_bloc.dart';
import 'package:orders/screens/edit_product/edit_product_screen.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_bloc.dart';
import 'package:orders/screens/edit_profile/edit_profile_screen.dart';
import 'package:orders/screens/home/bloc/home_bloc.dart';
import 'package:orders/screens/home/bloc/home_event.dart' as home;
import 'package:orders/screens/home/home_screen.dart';
import 'package:orders/screens/invite/bloc/invite_bloc.dart';
import 'package:orders/screens/invite/bloc/invite_event.dart' as invite;
import 'package:orders/screens/invite/invite_screen.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart' as order_event;
import 'package:orders/screens/order/order_screen.dart';
import 'package:orders/screens/participants/bloc/participants_bloc.dart';
import 'package:orders/screens/participants/participants_screen.dart';
import 'package:orders/screens/profile/bloc/profile_bloc.dart';
import 'package:orders/screens/profile/profile_screen.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_bloc.dart';
import 'package:orders/screens/sign_in_email/sign_in_email_screen.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_bloc.dart';
import 'package:orders/screens/sign_up/sign_up_screen.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_bloc.dart';
import 'package:orders/screens/verify_phone/verify_phone_screen.dart';

Route<dynamic> appRoutes(RouteSettings settings, DataBloc dataBloc) {
  if (settings.name == HomeScreen.routeName) {
    return MaterialPageRoute<Widget>(
      builder: (BuildContext context) {
        return BlocProvider<HomeBloc>(
          create: (BuildContext context) =>
              HomeBloc(dataBloc)..add(home.ScreenStarted()),
          child: HomeScreen(),
        );
      },
      settings: const RouteSettings(name: HomeScreen.routeName),
    );
  }
  if (settings.name == ConnectScreen.routeName) {
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<ConnectBloc>(
        create: (BuildContext context) => ConnectBloc(),
        child: ConnectScreen(),
      );
    });
  }
  if (settings.name == SignUpScreen.routeName) {
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<SignUpBloc>(
        create: (BuildContext context) => SignUpBloc(),
        child: SignUpScreen(),
      );
    });
  }
  if (settings.name == SignInEmailScreen.routeName) {
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<SignInEmailBloc>(
        create: (BuildContext context) => SignInEmailBloc(),
        child: SignInEmailScreen(),
      );
    });
  }
  if (settings.name == VerifyPhoneScreen.routeName) {
    final User user = settings.arguments as User;
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<VerifyPhoneBloc>(
        create: (BuildContext context) => VerifyPhoneBloc(user),
        child: VerifyPhoneScreen(),
      );
    });
  }
  if (settings.name == CreateOrderScreen.routeName) {
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<CreateOrderBloc>(
        create: (BuildContext context) => CreateOrderBloc(dataBloc),
        child: CreateOrderScreen(),
      );
    });
  }
  if (settings.name == EditOrderScreen.routeName) {
    final EditOrderArgs args = settings.arguments as EditOrderArgs;
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<EditOrderBloc>(
        create: (BuildContext context) => EditOrderBloc(
          dataBloc,
          orderBloc: args.orderBloc,
          initialOrder: args.order,
          orderType: args.orderType,
          duplicateOrder: args.duplicateOrder,
        ),
        child: EditOrderScreen(),
      );
    });
  }
  if (settings.name == OrderScreen.routeName) {
    final Order order = settings.arguments as Order;
    return MaterialPageRoute<OrderScreenPopArgs>(
        builder: (BuildContext context) {
      return BlocProvider<OrderBloc>(
        create: (BuildContext context) =>
            OrderBloc(dataBloc, order)..add(order_event.ScreenStarted()),
        child: OrderScreen(),
      );
    });
  }
  if (settings.name == EditProductScreen.routeName) {
    final EditProductArgs args = settings.arguments as EditProductArgs;
    final OrderBloc orderBloc = args.orderBloc;
    return MaterialPageRoute<bool>(builder: (BuildContext context) {
      return BlocProvider<EditProductBloc>(
        create: (BuildContext context) =>
            EditProductBloc(dataBloc, orderBloc, initialProduct: args.product),
        child: EditProductScreen(),
      );
    });
  }
  if (settings.name == CartScreen.routeName) {
    final OrderBloc orderBloc = settings.arguments as OrderBloc;
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<CartBloc>(
        create: (BuildContext context) => CartBloc(dataBloc, orderBloc),
        child: CartScreen(),
      );
    });
  }
  if (settings.name == ParticipantsScreen.routeName) {
    final ParticipantsArgs args = settings.arguments as ParticipantsArgs;
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<ParticipantsBloc>(
        create: (BuildContext context) => ParticipantsBloc(
          orderBloc: args.orderBloc,
          order: args.order,
          participants: args.participants,
          cart: args.cart,
          isOwner: args.isOwner,
          owner: args.owner,
        ),
        child: ParticipantsScreen(),
      );
    });
  }
  if (settings.name == ProfileScreen.routeName) {
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<ProfileBloc>(
        create: (BuildContext context) => ProfileBloc(dataBloc),
        child: ProfileScreen(),
      );
    });
  }
  if (settings.name == EditProfileScreen.routeName) {
    final User user = settings.arguments as User;
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<EditProfileBloc>(
        create: (BuildContext context) => EditProfileBloc(dataBloc, user),
        child: EditProfileScreen(),
      );
    });
  }
  if (settings.name == InviteScreen.routeName) {
    final Order order = settings.arguments as Order;
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<InviteBloc>(
        create: (BuildContext context) =>
            InviteBloc(dataBloc, order)..add(invite.ScreenStarted()),
        child: InviteScreen(),
      );
    });
  }
  if (settings.name == AcceptInviteScreen.routeName) {
    final String orderId = settings.arguments as String;
    return MaterialPageRoute<Widget>(builder: (BuildContext context) {
      return BlocProvider<AcceptInviteBloc>(
        create: (BuildContext context) => AcceptInviteBloc(dataBloc, orderId)
          ..add(accept_invite.ScreenStarted()),
        child: AcceptInviteScreen(),
      );
    });
  }
  throw Exception('Unknown route $settings');
}
