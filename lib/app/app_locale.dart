import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:intl/intl_standalone.dart';
import 'package:logging/logging.dart';
import 'package:timeago/timeago.dart' as timeago;

// ignore: avoid_classes_with_only_static_members
class AppLocale {
  static String _languageCode = EN;

  static String get languageCode => _languageCode;

  static Locale get currentLocale => Locale(_languageCode);

  static bool get isRo => _languageCode == RO;

  static Future<void> init() async {
    await findSystemLocale();
    await initializeDateFormatting();

    final List<String> splits = Intl.systemLocale.split('_');
    if (splits.isNotEmpty && splits[0] == RO) {
      timeago.setLocaleMessages(RO, timeago.RoMessages());
      _languageCode = RO;
    } else {
      timeago.setLocaleMessages(EN, timeago.EnMessages());
      _languageCode = EN;
    }
    _logger.fine('Current locale is $_languageCode');
  }
}

const String RO = 'ro';
const String EN = 'en';

// ignore: unused_element
final Logger _logger = Logger('AppLocale');
