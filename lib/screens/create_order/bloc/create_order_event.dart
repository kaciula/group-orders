import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/order.dart';

part 'create_order_event.freezed.dart';

@freezed
class CreateOrderEvent with _$CreateOrderEvent {
  factory CreateOrderEvent.createRequested({
    required OrderType orderType,
    Order? duplicateOrder,
  }) = CreateRequested;
}
