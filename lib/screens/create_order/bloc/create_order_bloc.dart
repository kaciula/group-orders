import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/screens/create_order/bloc/create_order_effect.dart';
import 'package:orders/screens/create_order/bloc/create_order_event.dart';
import 'package:orders/screens/create_order/bloc/create_order_state.dart';

class CreateOrderBloc extends Bloc<CreateOrderEvent, CreateOrderState> {
  CreateOrderBloc(this.dataBloc)
      : super(CreateOrderState.initial(dataBloc.orders));

  final DataBloc dataBloc;

  @override
  Stream<CreateOrderState> mapEventToState(CreateOrderEvent event) async* {
    if (event is CreateRequested) {
      yield* _withEffect(
        state,
        GoToEditOrder(
          orderType: event.orderType,
          duplicateOrder: event.duplicateOrder,
        ),
      );
    }
  }

  Stream<CreateOrderState> _withEffect(
      CreateOrderState state, CreateOrderEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('CreateOrderBloc');
