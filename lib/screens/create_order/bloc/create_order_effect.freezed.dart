// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'create_order_effect.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CreateOrderEffect {
  OrderType get orderType => throw _privateConstructorUsedError;
  Order? get duplicateOrder => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrderType orderType, Order? duplicateOrder)
        goToEditOrder,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrderType orderType, Order? duplicateOrder)? goToEditOrder,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrderType orderType, Order? duplicateOrder)? goToEditOrder,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToEditOrder value) goToEditOrder,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToEditOrder value)? goToEditOrder,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToEditOrder value)? goToEditOrder,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateOrderEffectCopyWith<CreateOrderEffect> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateOrderEffectCopyWith<$Res> {
  factory $CreateOrderEffectCopyWith(
          CreateOrderEffect value, $Res Function(CreateOrderEffect) then) =
      _$CreateOrderEffectCopyWithImpl<$Res>;
  $Res call({OrderType orderType, Order? duplicateOrder});

  $OrderCopyWith<$Res>? get duplicateOrder;
}

/// @nodoc
class _$CreateOrderEffectCopyWithImpl<$Res>
    implements $CreateOrderEffectCopyWith<$Res> {
  _$CreateOrderEffectCopyWithImpl(this._value, this._then);

  final CreateOrderEffect _value;
  // ignore: unused_field
  final $Res Function(CreateOrderEffect) _then;

  @override
  $Res call({
    Object? orderType = freezed,
    Object? duplicateOrder = freezed,
  }) {
    return _then(_value.copyWith(
      orderType: orderType == freezed
          ? _value.orderType
          : orderType // ignore: cast_nullable_to_non_nullable
              as OrderType,
      duplicateOrder: duplicateOrder == freezed
          ? _value.duplicateOrder
          : duplicateOrder // ignore: cast_nullable_to_non_nullable
              as Order?,
    ));
  }

  @override
  $OrderCopyWith<$Res>? get duplicateOrder {
    if (_value.duplicateOrder == null) {
      return null;
    }

    return $OrderCopyWith<$Res>(_value.duplicateOrder!, (value) {
      return _then(_value.copyWith(duplicateOrder: value));
    });
  }
}

/// @nodoc
abstract class _$$GoToEditOrderCopyWith<$Res>
    implements $CreateOrderEffectCopyWith<$Res> {
  factory _$$GoToEditOrderCopyWith(
          _$GoToEditOrder value, $Res Function(_$GoToEditOrder) then) =
      __$$GoToEditOrderCopyWithImpl<$Res>;
  @override
  $Res call({OrderType orderType, Order? duplicateOrder});

  @override
  $OrderCopyWith<$Res>? get duplicateOrder;
}

/// @nodoc
class __$$GoToEditOrderCopyWithImpl<$Res>
    extends _$CreateOrderEffectCopyWithImpl<$Res>
    implements _$$GoToEditOrderCopyWith<$Res> {
  __$$GoToEditOrderCopyWithImpl(
      _$GoToEditOrder _value, $Res Function(_$GoToEditOrder) _then)
      : super(_value, (v) => _then(v as _$GoToEditOrder));

  @override
  _$GoToEditOrder get _value => super._value as _$GoToEditOrder;

  @override
  $Res call({
    Object? orderType = freezed,
    Object? duplicateOrder = freezed,
  }) {
    return _then(_$GoToEditOrder(
      orderType: orderType == freezed
          ? _value.orderType
          : orderType // ignore: cast_nullable_to_non_nullable
              as OrderType,
      duplicateOrder: duplicateOrder == freezed
          ? _value.duplicateOrder
          : duplicateOrder // ignore: cast_nullable_to_non_nullable
              as Order?,
    ));
  }
}

/// @nodoc

class _$GoToEditOrder implements GoToEditOrder {
  _$GoToEditOrder({required this.orderType, this.duplicateOrder});

  @override
  final OrderType orderType;
  @override
  final Order? duplicateOrder;

  @override
  String toString() {
    return 'CreateOrderEffect.goToEditOrder(orderType: $orderType, duplicateOrder: $duplicateOrder)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GoToEditOrder &&
            const DeepCollectionEquality().equals(other.orderType, orderType) &&
            const DeepCollectionEquality()
                .equals(other.duplicateOrder, duplicateOrder));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(orderType),
      const DeepCollectionEquality().hash(duplicateOrder));

  @JsonKey(ignore: true)
  @override
  _$$GoToEditOrderCopyWith<_$GoToEditOrder> get copyWith =>
      __$$GoToEditOrderCopyWithImpl<_$GoToEditOrder>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrderType orderType, Order? duplicateOrder)
        goToEditOrder,
  }) {
    return goToEditOrder(orderType, duplicateOrder);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrderType orderType, Order? duplicateOrder)? goToEditOrder,
  }) {
    return goToEditOrder?.call(orderType, duplicateOrder);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrderType orderType, Order? duplicateOrder)? goToEditOrder,
    required TResult orElse(),
  }) {
    if (goToEditOrder != null) {
      return goToEditOrder(orderType, duplicateOrder);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToEditOrder value) goToEditOrder,
  }) {
    return goToEditOrder(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToEditOrder value)? goToEditOrder,
  }) {
    return goToEditOrder?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToEditOrder value)? goToEditOrder,
    required TResult orElse(),
  }) {
    if (goToEditOrder != null) {
      return goToEditOrder(this);
    }
    return orElse();
  }
}

abstract class GoToEditOrder implements CreateOrderEffect {
  factory GoToEditOrder(
      {required final OrderType orderType,
      final Order? duplicateOrder}) = _$GoToEditOrder;

  @override
  OrderType get orderType;
  @override
  Order? get duplicateOrder;
  @override
  @JsonKey(ignore: true)
  _$$GoToEditOrderCopyWith<_$GoToEditOrder> get copyWith =>
      throw _privateConstructorUsedError;
}
