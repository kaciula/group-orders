// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'create_order_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CreateOrderEvent {
  OrderType get orderType => throw _privateConstructorUsedError;
  Order? get duplicateOrder => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrderType orderType, Order? duplicateOrder)
        createRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrderType orderType, Order? duplicateOrder)?
        createRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrderType orderType, Order? duplicateOrder)?
        createRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateRequested value) createRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CreateRequested value)? createRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateRequested value)? createRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateOrderEventCopyWith<CreateOrderEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateOrderEventCopyWith<$Res> {
  factory $CreateOrderEventCopyWith(
          CreateOrderEvent value, $Res Function(CreateOrderEvent) then) =
      _$CreateOrderEventCopyWithImpl<$Res>;
  $Res call({OrderType orderType, Order? duplicateOrder});

  $OrderCopyWith<$Res>? get duplicateOrder;
}

/// @nodoc
class _$CreateOrderEventCopyWithImpl<$Res>
    implements $CreateOrderEventCopyWith<$Res> {
  _$CreateOrderEventCopyWithImpl(this._value, this._then);

  final CreateOrderEvent _value;
  // ignore: unused_field
  final $Res Function(CreateOrderEvent) _then;

  @override
  $Res call({
    Object? orderType = freezed,
    Object? duplicateOrder = freezed,
  }) {
    return _then(_value.copyWith(
      orderType: orderType == freezed
          ? _value.orderType
          : orderType // ignore: cast_nullable_to_non_nullable
              as OrderType,
      duplicateOrder: duplicateOrder == freezed
          ? _value.duplicateOrder
          : duplicateOrder // ignore: cast_nullable_to_non_nullable
              as Order?,
    ));
  }

  @override
  $OrderCopyWith<$Res>? get duplicateOrder {
    if (_value.duplicateOrder == null) {
      return null;
    }

    return $OrderCopyWith<$Res>(_value.duplicateOrder!, (value) {
      return _then(_value.copyWith(duplicateOrder: value));
    });
  }
}

/// @nodoc
abstract class _$$CreateRequestedCopyWith<$Res>
    implements $CreateOrderEventCopyWith<$Res> {
  factory _$$CreateRequestedCopyWith(
          _$CreateRequested value, $Res Function(_$CreateRequested) then) =
      __$$CreateRequestedCopyWithImpl<$Res>;
  @override
  $Res call({OrderType orderType, Order? duplicateOrder});

  @override
  $OrderCopyWith<$Res>? get duplicateOrder;
}

/// @nodoc
class __$$CreateRequestedCopyWithImpl<$Res>
    extends _$CreateOrderEventCopyWithImpl<$Res>
    implements _$$CreateRequestedCopyWith<$Res> {
  __$$CreateRequestedCopyWithImpl(
      _$CreateRequested _value, $Res Function(_$CreateRequested) _then)
      : super(_value, (v) => _then(v as _$CreateRequested));

  @override
  _$CreateRequested get _value => super._value as _$CreateRequested;

  @override
  $Res call({
    Object? orderType = freezed,
    Object? duplicateOrder = freezed,
  }) {
    return _then(_$CreateRequested(
      orderType: orderType == freezed
          ? _value.orderType
          : orderType // ignore: cast_nullable_to_non_nullable
              as OrderType,
      duplicateOrder: duplicateOrder == freezed
          ? _value.duplicateOrder
          : duplicateOrder // ignore: cast_nullable_to_non_nullable
              as Order?,
    ));
  }
}

/// @nodoc

class _$CreateRequested implements CreateRequested {
  _$CreateRequested({required this.orderType, this.duplicateOrder});

  @override
  final OrderType orderType;
  @override
  final Order? duplicateOrder;

  @override
  String toString() {
    return 'CreateOrderEvent.createRequested(orderType: $orderType, duplicateOrder: $duplicateOrder)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateRequested &&
            const DeepCollectionEquality().equals(other.orderType, orderType) &&
            const DeepCollectionEquality()
                .equals(other.duplicateOrder, duplicateOrder));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(orderType),
      const DeepCollectionEquality().hash(duplicateOrder));

  @JsonKey(ignore: true)
  @override
  _$$CreateRequestedCopyWith<_$CreateRequested> get copyWith =>
      __$$CreateRequestedCopyWithImpl<_$CreateRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrderType orderType, Order? duplicateOrder)
        createRequested,
  }) {
    return createRequested(orderType, duplicateOrder);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrderType orderType, Order? duplicateOrder)?
        createRequested,
  }) {
    return createRequested?.call(orderType, duplicateOrder);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrderType orderType, Order? duplicateOrder)?
        createRequested,
    required TResult orElse(),
  }) {
    if (createRequested != null) {
      return createRequested(orderType, duplicateOrder);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateRequested value) createRequested,
  }) {
    return createRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CreateRequested value)? createRequested,
  }) {
    return createRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateRequested value)? createRequested,
    required TResult orElse(),
  }) {
    if (createRequested != null) {
      return createRequested(this);
    }
    return orElse();
  }
}

abstract class CreateRequested implements CreateOrderEvent {
  factory CreateRequested(
      {required final OrderType orderType,
      final Order? duplicateOrder}) = _$CreateRequested;

  @override
  OrderType get orderType;
  @override
  Order? get duplicateOrder;
  @override
  @JsonKey(ignore: true)
  _$$CreateRequestedCopyWith<_$CreateRequested> get copyWith =>
      throw _privateConstructorUsedError;
}
