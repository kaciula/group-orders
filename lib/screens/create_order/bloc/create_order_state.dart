import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/create_order/bloc/create_order_effect.dart';

part 'create_order_state.freezed.dart';

@freezed
class CreateOrderState with _$CreateOrderState {
  factory CreateOrderState({
    required KtList<Order> nonDeletedOrders,
    CreateOrderEffect? effect,
  }) = _CreateOrderState;

  CreateOrderState._();

  factory CreateOrderState.initial(KtList<Order> orders) {
    return CreateOrderState(
        nonDeletedOrders: orders
            .filter((Order it) => !it.isDeleted)
            .sortedByDescending((Order it) => it.creationDate));
  }

  static bool buildWhen(CreateOrderState previous, CreateOrderState current) {
    return previous.nonDeletedOrders != current.nonDeletedOrders;
  }

  static bool listenWhen(CreateOrderState previous, CreateOrderState current) {
    return current.effect != null;
  }

  bool get hasAtLeastOneNonDeletedOrder => nonDeletedOrders.size > 0;
}
