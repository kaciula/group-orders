import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/order.dart';

part 'create_order_effect.freezed.dart';

@freezed
class CreateOrderEffect with _$CreateOrderEffect {
  factory CreateOrderEffect.goToEditOrder({
    required OrderType orderType,
    Order? duplicateOrder,
  }) = GoToEditOrder;
}
