// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'create_order_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CreateOrderState {
  KtList<Order> get nonDeletedOrders => throw _privateConstructorUsedError;
  CreateOrderEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CreateOrderStateCopyWith<CreateOrderState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateOrderStateCopyWith<$Res> {
  factory $CreateOrderStateCopyWith(
          CreateOrderState value, $Res Function(CreateOrderState) then) =
      _$CreateOrderStateCopyWithImpl<$Res>;
  $Res call({KtList<Order> nonDeletedOrders, CreateOrderEffect? effect});

  $CreateOrderEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$CreateOrderStateCopyWithImpl<$Res>
    implements $CreateOrderStateCopyWith<$Res> {
  _$CreateOrderStateCopyWithImpl(this._value, this._then);

  final CreateOrderState _value;
  // ignore: unused_field
  final $Res Function(CreateOrderState) _then;

  @override
  $Res call({
    Object? nonDeletedOrders = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      nonDeletedOrders: nonDeletedOrders == freezed
          ? _value.nonDeletedOrders
          : nonDeletedOrders // ignore: cast_nullable_to_non_nullable
              as KtList<Order>,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as CreateOrderEffect?,
    ));
  }

  @override
  $CreateOrderEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $CreateOrderEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_CreateOrderStateCopyWith<$Res>
    implements $CreateOrderStateCopyWith<$Res> {
  factory _$$_CreateOrderStateCopyWith(
          _$_CreateOrderState value, $Res Function(_$_CreateOrderState) then) =
      __$$_CreateOrderStateCopyWithImpl<$Res>;
  @override
  $Res call({KtList<Order> nonDeletedOrders, CreateOrderEffect? effect});

  @override
  $CreateOrderEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_CreateOrderStateCopyWithImpl<$Res>
    extends _$CreateOrderStateCopyWithImpl<$Res>
    implements _$$_CreateOrderStateCopyWith<$Res> {
  __$$_CreateOrderStateCopyWithImpl(
      _$_CreateOrderState _value, $Res Function(_$_CreateOrderState) _then)
      : super(_value, (v) => _then(v as _$_CreateOrderState));

  @override
  _$_CreateOrderState get _value => super._value as _$_CreateOrderState;

  @override
  $Res call({
    Object? nonDeletedOrders = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_CreateOrderState(
      nonDeletedOrders: nonDeletedOrders == freezed
          ? _value.nonDeletedOrders
          : nonDeletedOrders // ignore: cast_nullable_to_non_nullable
              as KtList<Order>,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as CreateOrderEffect?,
    ));
  }
}

/// @nodoc

class _$_CreateOrderState extends _CreateOrderState {
  _$_CreateOrderState({required this.nonDeletedOrders, this.effect})
      : super._();

  @override
  final KtList<Order> nonDeletedOrders;
  @override
  final CreateOrderEffect? effect;

  @override
  String toString() {
    return 'CreateOrderState(nonDeletedOrders: $nonDeletedOrders, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CreateOrderState &&
            const DeepCollectionEquality()
                .equals(other.nonDeletedOrders, nonDeletedOrders) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(nonDeletedOrders),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_CreateOrderStateCopyWith<_$_CreateOrderState> get copyWith =>
      __$$_CreateOrderStateCopyWithImpl<_$_CreateOrderState>(this, _$identity);
}

abstract class _CreateOrderState extends CreateOrderState {
  factory _CreateOrderState(
      {required final KtList<Order> nonDeletedOrders,
      final CreateOrderEffect? effect}) = _$_CreateOrderState;
  _CreateOrderState._() : super._();

  @override
  KtList<Order> get nonDeletedOrders;
  @override
  CreateOrderEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_CreateOrderStateCopyWith<_$_CreateOrderState> get copyWith =>
      throw _privateConstructorUsedError;
}
