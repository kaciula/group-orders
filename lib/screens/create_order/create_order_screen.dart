import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kt_dart/collection.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/create_order/bloc/create_order_bloc.dart';
import 'package:orders/screens/create_order/bloc/create_order_effect.dart';
import 'package:orders/screens/create_order/bloc/create_order_event.dart';
import 'package:orders/screens/create_order/bloc/create_order_state.dart';
import 'package:orders/screens/edit_order/edit_order_screen.dart';

class CreateOrderScreen extends StatefulWidget {
  static const String routeName = 'create_order';

  @override
  State<CreateOrderScreen> createState() => _CreateOrderScreenState();
}

class _CreateOrderScreenState extends State<CreateOrderScreen> {
  OrderType? _selectedOrderType;
  InitialSettings? _selectedInitialSettings;
  Order? _duplicateOrder;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateOrderBloc, CreateOrderState>(
      bloc: BlocProvider.of<CreateOrderBloc>(context),
      buildWhen: CreateOrderState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, CreateOrderState state) {
    final CreateOrderEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToEditOrder) {
      Navigator.pushNamed(
        context,
        EditOrderScreen.routeName,
        arguments: EditOrderArgs(
          orderType: effect.orderType,
          duplicateOrder: effect.duplicateOrder,
        ),
      );
    }
  }

  Widget _builder(BuildContext context, CreateOrderState state) {
    final CreateOrderBloc bloc = BlocProvider.of<CreateOrderBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(title: Text(Strings.createOrderTitle)),
      body: BlocListener<CreateOrderBloc, CreateOrderState>(
        bloc: bloc,
        listenWhen: CreateOrderState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, CreateOrderState state) {
    final bool showInitialSettingsStep = state.hasAtLeastOneNonDeletedOrder;
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              Strings.createOrderStep1,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 16),
            Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                border: Border.all(color: AppColors.accent),
              ),
              child: Column(
                children: <Widget>[
                  _optionOrderType(
                    OrderType.provider,
                    Strings.createOrderProviderTitle,
                    Strings.createOrderProviderDesc,
                  ),
                  const Divider(indent: 16, endIndent: 16),
                  _optionOrderType(
                    OrderType.proxy,
                    Strings.createOrderProxyTitle,
                    Strings.createOrderProxyDesc,
                  ),
                ],
              ),
            ),
            if (showInitialSettingsStep) ...<Widget>[
              const SizedBox(height: 16),
              Text(
                Strings.createOrderStep2,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 16),
              Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                  border: Border.all(color: AppColors.accent),
                ),
                child: Column(
                  children: <Widget>[
                    _optionInitialSettings(
                      InitialSettings.blank,
                      Strings.createOrderBlankTitle,
                      Strings.createOrderBlankDesc,
                      () {
                        setState(() {
                          _selectedInitialSettings = InitialSettings.blank;
                          _duplicateOrder = null;
                        });
                      },
                    ),
                    const Divider(indent: 16, endIndent: 16),
                    _optionInitialSettings(
                      InitialSettings.duplicate,
                      Strings.createOrderDuplicateTitle,
                      Strings.createOrderDuplicateDesc,
                      () => showDialog(
                        context: context,
                        builder: (BuildContext innerContext) {
                          return _ordersDialog(innerContext, state);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
            const SizedBox(height: 16),
            ElevatedButton(
              child: Text(Strings.createOrderContinue),
              onPressed: _selectedOrderType != null &&
                      (_selectedInitialSettings != null ||
                          !showInitialSettingsStep)
                  ? () {
                      _bloc(context).add(CreateRequested(
                        orderType: _selectedOrderType!,
                        duplicateOrder: _duplicateOrder,
                      ));
                    }
                  : null,
            ),
          ],
        ),
      ),
    );
  }

  Widget _optionOrderType(
      OrderType orderType, String title, String description) {
    final VoidCallback callback = () {
      setState(() {
        _selectedOrderType = orderType;
      });
    };
    return InkWell(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: Row(
          children: <Widget>[
            Radio<OrderType>(
              value: orderType,
              groupValue: _selectedOrderType,
              activeColor: AppColors.accent,
              onChanged: (OrderType? it) => callback(),
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  Text(
                    title,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    description,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _optionInitialSettings(InitialSettings initialSettings, String title,
      String description, VoidCallback callback) {
    return InkWell(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: Row(
          children: <Widget>[
            Radio<InitialSettings>(
              value: initialSettings,
              groupValue: _selectedInitialSettings,
              activeColor: AppColors.accent,
              onChanged: (InitialSettings? it) => callback(),
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  Text(
                    title,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    description,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _ordersDialog(BuildContext context, CreateOrderState state) {
    final List<Widget> options = state.nonDeletedOrders
        .map(
          (Order it) => SimpleDialogOption(
            onPressed: () {
              setState(() {
                _selectedInitialSettings = InitialSettings.duplicate;
                _duplicateOrder = it;
              });
              Navigator.pop(context);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  it.title,
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                const SizedBox(height: 8),
                Text(
                  it.ownerName,
                  style: Theme.of(context).textTheme.subtitle2!.copyWith(
                        color: AppColors.textGreyDark,
                        fontWeight: FontWeight.w400,
                      ),
                )
              ],
            ),
          ),
        )
        .asList();
    return SimpleDialog(
      title: Text(Strings.createOrderDuplicate),
      children: options,
    );
  }

  CreateOrderBloc _bloc(BuildContext context) {
    return BlocProvider.of<CreateOrderBloc>(context);
  }
}

enum InitialSettings { blank, duplicate }

// ignore: unused_element
final Logger _logger = Logger('CreateOrderScreen');
