import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:orders/app/app_locale.dart';
import 'package:orders/utils/misc_utils.dart';
import 'package:universal_io/io.dart';

typedef DatePickerCallback = void Function(DateTime pickedDate);
typedef TimePickerCallback = void Function(TimeOfDay pickedTime);

Future<void> pickDate(
  BuildContext context, {
  required DateTime? initialDate,
  required DatePickerCallback callback,
}) async {
  initialDate ??= DateTime.now();
  if (Platform.isAndroid) {
    await _pickAndroidDate(context,
        initialDate: initialDate, callback: callback);
  } else {
    await _pickIOSDateTime(context,
        initialDate: initialDate, dateCallback: callback);
  }
}

Future<void> pickTime(
  BuildContext context, {
  required DateTime? initialDate,
  required TimePickerCallback callback,
}) async {
  initialDate ??= DateTime.now();
  if (Platform.isAndroid) {
    await _pickAndroidTime(context,
        initialDate: initialDate, callback: callback);
  } else {
    await _pickIOSDateTime(context,
        initialDate: initialDate, timeCallback: callback);
  }
}

Future<void> _pickAndroidDate(
  BuildContext context, {
  required DateTime initialDate,
  required DatePickerCallback callback,
}) async {
  // Set first date from start of day because of weird behavior of picker that goes to the next day
  final DateTime firstDate = MiscUtils.mergeDateTime(
      DateTime.now(), const TimeOfDay(hour: 0, minute: 0));

  final DateTime? pickedDate = await showDatePicker(
    context: context,
    initialDate: initialDate,
    firstDate: firstDate,
    lastDate: initialDate.add(const Duration(days: 90)),
    locale: AppLocale.currentLocale,
  );
  if (pickedDate != null) {
    callback(pickedDate);
  }
}

Future<void> _pickAndroidTime(
  BuildContext context, {
  required DateTime initialDate,
  required TimePickerCallback callback,
}) async {
  final TimeOfDay? pickedTime = await showTimePicker(
    context: context,
    initialTime: TimeOfDay.fromDateTime(initialDate),
  );
  if (pickedTime != null) {
    callback(pickedTime);
  }
}

Future<void> _pickIOSDateTime(
  BuildContext context, {
  required DateTime initialDate,
  DatePickerCallback? dateCallback,
  TimePickerCallback? timeCallback,
}) async {
  // Set first date from start of day because of weird behavior of picker that goes to the next day
  final DateTime firstDate = MiscUtils.mergeDateTime(
      DateTime.now(), const TimeOfDay(hour: 0, minute: 0));

  // Adjust initialDate to minute interval
  final TimeOfDay initialTime = TimeOfDay.fromDateTime(initialDate);
  const int minuteInterval = 15;
  if (initialTime.minute % minuteInterval != 0) {
    final TimeOfDay adjustedTime = TimeOfDay(
      hour: initialTime.hour,
      minute: initialTime.minute - (initialTime.minute % minuteInterval),
    );
    initialDate = MiscUtils.mergeDateTime(initialDate, adjustedTime);
  }

  await showCupertinoModalPopup(
    context: context,
    builder: (BuildContext context) {
      return Container(
        height: MediaQuery.of(context).size.height / 3,
        child: CupertinoDatePicker(
          mode: CupertinoDatePickerMode.dateAndTime,
          initialDateTime: initialDate,
          minuteInterval: minuteInterval,
          minimumDate: firstDate,
          maximumDate: initialDate.add(const Duration(days: 28)),
          onDateTimeChanged: dateCallback != null
              ? dateCallback
              : (DateTime dateTime) =>
                  timeCallback?.call(TimeOfDay.fromDateTime(dateTime)),
        ),
      );
    },
  );
}
