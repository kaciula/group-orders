import 'package:bloc/bloc.dart';
import 'package:kt_dart/collection.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart' as data_event;
import 'package:orders/model/currency.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_effect.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_event.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_state.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart' as order_event;
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/create_order.dart';
import 'package:orders/usecases/duplicate_order.dart';
import 'package:orders/usecases/update_order.dart';
import 'package:orders/usecases/use_case_results.dart';
import 'package:orders/utils/available_currencies.dart';

class EditOrderBloc extends Bloc<EditOrderEvent, EditOrderState> {
  EditOrderBloc(this.dataBloc,
      {required this.orderBloc,
      Order? initialOrder,
      required OrderType orderType,
      Order? duplicateOrder})
      : super(EditOrderState.initial(
          initialOrder: initialOrder,
          orderType: orderType,
          duplicateOrder: duplicateOrder,
        ));

  final DataBloc dataBloc;
  final OrderBloc? orderBloc;

  final CreateOrder _createOrder = getIt<CreateOrder>();
  final DuplicateOrder _duplicateOrder = getIt<DuplicateOrder>();
  final UpdateOrder _updateOrder = getIt<UpdateOrder>();

  @override
  Stream<EditOrderState> mapEventToState(EditOrderEvent event) async* {
    if (event is SaveOrderRequested) {
      yield state.copyWith(inProgressAction: true);
      if (state.isEdit) {
        final UseCaseResult result = await _updateOrder(
          orderId: state.initialOrder!.id,
          title: event.title,
          details: event.details,
          deliveryDate: event.deliveryDate,
          currency: event.currency,
          dueDate: event.dueDate,
        );
        if (result is UseCaseSuccess) {
          final Order modifiedOrder = state.initialOrder!.copyWith(
            title: event.title,
            details: event.details,
            currency: event.currency,
            deliveryDate: event.deliveryDate,
            dueDate: event.dueDate,
          );
          orderBloc?.add(order_event.OrderUpdated(modifiedOrder));
          dataBloc.add(data_event.OrderUpdated(order: modifiedOrder));

          yield* _withEffect(state, Close());
        } else {
          yield* _withEffect(state.copyWith(inProgressAction: false),
              ShowErrorSnackBar(errorMsg: Strings.editOrderErrorMsg));
        }
      } else if (state.isDuplication) {
        final DuplicateOrderResult result = await _duplicateOrder(
          title: event.title,
          details: event.details,
          currency: event.currency,
          deliveryDate: event.deliveryDate,
          dueDate: event.dueDate,
          duplicateOrderId: state.duplicateOrder!.id,
          orderType: state.orderType,
        );
        if (result is DuplicateOrderSuccess) {
          yield* _withEffect(state, GoToOrder(result.order));
        } else {
          yield* _withEffect(state.copyWith(inProgressAction: false),
              ShowErrorSnackBar(errorMsg: Strings.editOrderErrorMsg));
        }
      } else {
        final CreateOrderResult result = await _createOrder(
          title: event.title,
          details: event.details,
          currency: event.currency,
          deliveryDate: event.deliveryDate,
          dueDate: event.dueDate,
          orderType: state.orderType,
        );
        if (result is CreateOrderSuccess) {
          yield* _withEffect(state, GoToOrder(result.order));
        } else {
          yield* _withEffect(state.copyWith(inProgressAction: false),
              ShowErrorSnackBar(errorMsg: Strings.editOrderErrorMsg));
        }
      }
    }
  }

  String? titleValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    return null;
  }

  String? currencyValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    if (availableCurrencies.firstOrNull(
            (Currency it) => it.code == value!.trim().toUpperCase()) ==
        null) {
      return Strings.editOrderCurrencyUnknown;
    }
    return null;
  }

  Stream<EditOrderState> _withEffect(
      EditOrderState state, EditOrderEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('EditOrderBloc');
