import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_effect.dart';

part 'edit_order_state.freezed.dart';

@freezed
class EditOrderState with _$EditOrderState {
  factory EditOrderState({
    required bool inProgressAction,
    Order? initialOrder,
    required OrderType orderType,
    Order? duplicateOrder,
    EditOrderEffect? effect,
  }) = _EditOrderState;

  EditOrderState._();

  factory EditOrderState.initial(
      {Order? initialOrder,
      required OrderType orderType,
      Order? duplicateOrder}) {
    return EditOrderState(
      inProgressAction: false,
      initialOrder: initialOrder,
      orderType: orderType,
      duplicateOrder: duplicateOrder,
    );
  }

  static bool buildWhen(EditOrderState previous, EditOrderState current) {
    return previous.inProgressAction != current.inProgressAction ||
        previous.initialOrder != current.initialOrder ||
        previous.orderType != current.orderType ||
        previous.duplicateOrder != current.duplicateOrder;
  }

  static bool listenWhen(EditOrderState previous, EditOrderState current) {
    return current.effect != null;
  }

  bool get isNew => !isEdit && !isDuplication;

  bool get isEdit => initialOrder != null;

  bool get isDuplication => duplicateOrder != null;
}
