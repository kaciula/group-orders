// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'edit_order_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$EditOrderState {
  bool get inProgressAction => throw _privateConstructorUsedError;
  Order? get initialOrder => throw _privateConstructorUsedError;
  OrderType get orderType => throw _privateConstructorUsedError;
  Order? get duplicateOrder => throw _privateConstructorUsedError;
  EditOrderEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $EditOrderStateCopyWith<EditOrderState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EditOrderStateCopyWith<$Res> {
  factory $EditOrderStateCopyWith(
          EditOrderState value, $Res Function(EditOrderState) then) =
      _$EditOrderStateCopyWithImpl<$Res>;
  $Res call(
      {bool inProgressAction,
      Order? initialOrder,
      OrderType orderType,
      Order? duplicateOrder,
      EditOrderEffect? effect});

  $OrderCopyWith<$Res>? get initialOrder;
  $OrderCopyWith<$Res>? get duplicateOrder;
  $EditOrderEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$EditOrderStateCopyWithImpl<$Res>
    implements $EditOrderStateCopyWith<$Res> {
  _$EditOrderStateCopyWithImpl(this._value, this._then);

  final EditOrderState _value;
  // ignore: unused_field
  final $Res Function(EditOrderState) _then;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? initialOrder = freezed,
    Object? orderType = freezed,
    Object? duplicateOrder = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      initialOrder: initialOrder == freezed
          ? _value.initialOrder
          : initialOrder // ignore: cast_nullable_to_non_nullable
              as Order?,
      orderType: orderType == freezed
          ? _value.orderType
          : orderType // ignore: cast_nullable_to_non_nullable
              as OrderType,
      duplicateOrder: duplicateOrder == freezed
          ? _value.duplicateOrder
          : duplicateOrder // ignore: cast_nullable_to_non_nullable
              as Order?,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as EditOrderEffect?,
    ));
  }

  @override
  $OrderCopyWith<$Res>? get initialOrder {
    if (_value.initialOrder == null) {
      return null;
    }

    return $OrderCopyWith<$Res>(_value.initialOrder!, (value) {
      return _then(_value.copyWith(initialOrder: value));
    });
  }

  @override
  $OrderCopyWith<$Res>? get duplicateOrder {
    if (_value.duplicateOrder == null) {
      return null;
    }

    return $OrderCopyWith<$Res>(_value.duplicateOrder!, (value) {
      return _then(_value.copyWith(duplicateOrder: value));
    });
  }

  @override
  $EditOrderEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $EditOrderEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_EditOrderStateCopyWith<$Res>
    implements $EditOrderStateCopyWith<$Res> {
  factory _$$_EditOrderStateCopyWith(
          _$_EditOrderState value, $Res Function(_$_EditOrderState) then) =
      __$$_EditOrderStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool inProgressAction,
      Order? initialOrder,
      OrderType orderType,
      Order? duplicateOrder,
      EditOrderEffect? effect});

  @override
  $OrderCopyWith<$Res>? get initialOrder;
  @override
  $OrderCopyWith<$Res>? get duplicateOrder;
  @override
  $EditOrderEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_EditOrderStateCopyWithImpl<$Res>
    extends _$EditOrderStateCopyWithImpl<$Res>
    implements _$$_EditOrderStateCopyWith<$Res> {
  __$$_EditOrderStateCopyWithImpl(
      _$_EditOrderState _value, $Res Function(_$_EditOrderState) _then)
      : super(_value, (v) => _then(v as _$_EditOrderState));

  @override
  _$_EditOrderState get _value => super._value as _$_EditOrderState;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? initialOrder = freezed,
    Object? orderType = freezed,
    Object? duplicateOrder = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_EditOrderState(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      initialOrder: initialOrder == freezed
          ? _value.initialOrder
          : initialOrder // ignore: cast_nullable_to_non_nullable
              as Order?,
      orderType: orderType == freezed
          ? _value.orderType
          : orderType // ignore: cast_nullable_to_non_nullable
              as OrderType,
      duplicateOrder: duplicateOrder == freezed
          ? _value.duplicateOrder
          : duplicateOrder // ignore: cast_nullable_to_non_nullable
              as Order?,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as EditOrderEffect?,
    ));
  }
}

/// @nodoc

class _$_EditOrderState extends _EditOrderState {
  _$_EditOrderState(
      {required this.inProgressAction,
      this.initialOrder,
      required this.orderType,
      this.duplicateOrder,
      this.effect})
      : super._();

  @override
  final bool inProgressAction;
  @override
  final Order? initialOrder;
  @override
  final OrderType orderType;
  @override
  final Order? duplicateOrder;
  @override
  final EditOrderEffect? effect;

  @override
  String toString() {
    return 'EditOrderState(inProgressAction: $inProgressAction, initialOrder: $initialOrder, orderType: $orderType, duplicateOrder: $duplicateOrder, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_EditOrderState &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality()
                .equals(other.initialOrder, initialOrder) &&
            const DeepCollectionEquality().equals(other.orderType, orderType) &&
            const DeepCollectionEquality()
                .equals(other.duplicateOrder, duplicateOrder) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(initialOrder),
      const DeepCollectionEquality().hash(orderType),
      const DeepCollectionEquality().hash(duplicateOrder),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_EditOrderStateCopyWith<_$_EditOrderState> get copyWith =>
      __$$_EditOrderStateCopyWithImpl<_$_EditOrderState>(this, _$identity);
}

abstract class _EditOrderState extends EditOrderState {
  factory _EditOrderState(
      {required final bool inProgressAction,
      final Order? initialOrder,
      required final OrderType orderType,
      final Order? duplicateOrder,
      final EditOrderEffect? effect}) = _$_EditOrderState;
  _EditOrderState._() : super._();

  @override
  bool get inProgressAction;
  @override
  Order? get initialOrder;
  @override
  OrderType get orderType;
  @override
  Order? get duplicateOrder;
  @override
  EditOrderEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_EditOrderStateCopyWith<_$_EditOrderState> get copyWith =>
      throw _privateConstructorUsedError;
}
