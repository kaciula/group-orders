import 'package:freezed_annotation/freezed_annotation.dart';

part 'edit_order_event.freezed.dart';

@freezed
class EditOrderEvent with _$EditOrderEvent {
  factory EditOrderEvent.saveOrderRequested({
    required String title,
    required String details,
    required String currency,
    required DateTime deliveryDate,
    required DateTime dueDate,
  }) = SaveOrderRequested;
}
