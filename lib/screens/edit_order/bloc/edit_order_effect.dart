import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/order.dart';

part 'edit_order_effect.freezed.dart';

@freezed
class EditOrderEffect with _$EditOrderEffect {
  factory EditOrderEffect.goToOrder(Order order) = GoToOrder;

  factory EditOrderEffect.close() = Close;

  factory EditOrderEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory EditOrderEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
