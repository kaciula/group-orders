// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'edit_order_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$EditOrderEvent {
  String get title => throw _privateConstructorUsedError;
  String get details => throw _privateConstructorUsedError;
  String get currency => throw _privateConstructorUsedError;
  DateTime get deliveryDate => throw _privateConstructorUsedError;
  DateTime get dueDate => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String title, String details, String currency,
            DateTime deliveryDate, DateTime dueDate)
        saveOrderRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String title, String details, String currency,
            DateTime deliveryDate, DateTime dueDate)?
        saveOrderRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String title, String details, String currency,
            DateTime deliveryDate, DateTime dueDate)?
        saveOrderRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveOrderRequested value) saveOrderRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveOrderRequested value)? saveOrderRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveOrderRequested value)? saveOrderRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $EditOrderEventCopyWith<EditOrderEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EditOrderEventCopyWith<$Res> {
  factory $EditOrderEventCopyWith(
          EditOrderEvent value, $Res Function(EditOrderEvent) then) =
      _$EditOrderEventCopyWithImpl<$Res>;
  $Res call(
      {String title,
      String details,
      String currency,
      DateTime deliveryDate,
      DateTime dueDate});
}

/// @nodoc
class _$EditOrderEventCopyWithImpl<$Res>
    implements $EditOrderEventCopyWith<$Res> {
  _$EditOrderEventCopyWithImpl(this._value, this._then);

  final EditOrderEvent _value;
  // ignore: unused_field
  final $Res Function(EditOrderEvent) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? details = freezed,
    Object? currency = freezed,
    Object? deliveryDate = freezed,
    Object? dueDate = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      deliveryDate: deliveryDate == freezed
          ? _value.deliveryDate
          : deliveryDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      dueDate: dueDate == freezed
          ? _value.dueDate
          : dueDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$$SaveOrderRequestedCopyWith<$Res>
    implements $EditOrderEventCopyWith<$Res> {
  factory _$$SaveOrderRequestedCopyWith(_$SaveOrderRequested value,
          $Res Function(_$SaveOrderRequested) then) =
      __$$SaveOrderRequestedCopyWithImpl<$Res>;
  @override
  $Res call(
      {String title,
      String details,
      String currency,
      DateTime deliveryDate,
      DateTime dueDate});
}

/// @nodoc
class __$$SaveOrderRequestedCopyWithImpl<$Res>
    extends _$EditOrderEventCopyWithImpl<$Res>
    implements _$$SaveOrderRequestedCopyWith<$Res> {
  __$$SaveOrderRequestedCopyWithImpl(
      _$SaveOrderRequested _value, $Res Function(_$SaveOrderRequested) _then)
      : super(_value, (v) => _then(v as _$SaveOrderRequested));

  @override
  _$SaveOrderRequested get _value => super._value as _$SaveOrderRequested;

  @override
  $Res call({
    Object? title = freezed,
    Object? details = freezed,
    Object? currency = freezed,
    Object? deliveryDate = freezed,
    Object? dueDate = freezed,
  }) {
    return _then(_$SaveOrderRequested(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      deliveryDate: deliveryDate == freezed
          ? _value.deliveryDate
          : deliveryDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      dueDate: dueDate == freezed
          ? _value.dueDate
          : dueDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$SaveOrderRequested implements SaveOrderRequested {
  _$SaveOrderRequested(
      {required this.title,
      required this.details,
      required this.currency,
      required this.deliveryDate,
      required this.dueDate});

  @override
  final String title;
  @override
  final String details;
  @override
  final String currency;
  @override
  final DateTime deliveryDate;
  @override
  final DateTime dueDate;

  @override
  String toString() {
    return 'EditOrderEvent.saveOrderRequested(title: $title, details: $details, currency: $currency, deliveryDate: $deliveryDate, dueDate: $dueDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaveOrderRequested &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.details, details) &&
            const DeepCollectionEquality().equals(other.currency, currency) &&
            const DeepCollectionEquality()
                .equals(other.deliveryDate, deliveryDate) &&
            const DeepCollectionEquality().equals(other.dueDate, dueDate));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(details),
      const DeepCollectionEquality().hash(currency),
      const DeepCollectionEquality().hash(deliveryDate),
      const DeepCollectionEquality().hash(dueDate));

  @JsonKey(ignore: true)
  @override
  _$$SaveOrderRequestedCopyWith<_$SaveOrderRequested> get copyWith =>
      __$$SaveOrderRequestedCopyWithImpl<_$SaveOrderRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String title, String details, String currency,
            DateTime deliveryDate, DateTime dueDate)
        saveOrderRequested,
  }) {
    return saveOrderRequested(title, details, currency, deliveryDate, dueDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String title, String details, String currency,
            DateTime deliveryDate, DateTime dueDate)?
        saveOrderRequested,
  }) {
    return saveOrderRequested?.call(
        title, details, currency, deliveryDate, dueDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String title, String details, String currency,
            DateTime deliveryDate, DateTime dueDate)?
        saveOrderRequested,
    required TResult orElse(),
  }) {
    if (saveOrderRequested != null) {
      return saveOrderRequested(
          title, details, currency, deliveryDate, dueDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveOrderRequested value) saveOrderRequested,
  }) {
    return saveOrderRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveOrderRequested value)? saveOrderRequested,
  }) {
    return saveOrderRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveOrderRequested value)? saveOrderRequested,
    required TResult orElse(),
  }) {
    if (saveOrderRequested != null) {
      return saveOrderRequested(this);
    }
    return orElse();
  }
}

abstract class SaveOrderRequested implements EditOrderEvent {
  factory SaveOrderRequested(
      {required final String title,
      required final String details,
      required final String currency,
      required final DateTime deliveryDate,
      required final DateTime dueDate}) = _$SaveOrderRequested;

  @override
  String get title;
  @override
  String get details;
  @override
  String get currency;
  @override
  DateTime get deliveryDate;
  @override
  DateTime get dueDate;
  @override
  @JsonKey(ignore: true)
  _$$SaveOrderRequestedCopyWith<_$SaveOrderRequested> get copyWith =>
      throw _privateConstructorUsedError;
}
