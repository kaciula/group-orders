import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/currency.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_bloc.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_effect.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_event.dart';
import 'package:orders/screens/edit_order/bloc/edit_order_state.dart';
import 'package:orders/screens/edit_order/pick_date_time.dart';
import 'package:orders/screens/home/home_screen.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/order_screen.dart';
import 'package:orders/screens/utils/formatter_utils.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';
import 'package:orders/utils/available_currencies.dart';
import 'package:orders/utils/misc_utils.dart';
import 'package:universal_io/io.dart';

class EditOrderScreen extends StatefulWidget {
  static const String routeName = 'edit_order';

  @override
  State<EditOrderScreen> createState() => _EditOrderScreenState();
}

class _EditOrderScreenState extends State<EditOrderScreen>
    with AfterLayoutMixin<EditOrderScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _detailsController = TextEditingController();
  final TextEditingController _currencyController = TextEditingController();
  late FocusNode _currencyFocusNode;
  late FocusNode _detailsFocusNode;

  DateTime _deliveryDate = DateTime.now();
  DateTime _dueDate = DateTime.now();

  @override
  void afterFirstLayout(BuildContext context) {
    final EditOrderBloc bloc = BlocProvider.of<EditOrderBloc>(context);
    if (bloc.state.isEdit) {
      final Order initialOrder = bloc.state.initialOrder!;
      _titleController.text = initialOrder.title;
      _currencyController.text = initialOrder.currency;
      _detailsController.text = initialOrder.details;
      _deliveryDate = initialOrder.deliveryDate ?? DateTime.now();
      _dueDate = initialOrder.dueDate;
    }
    if (bloc.state.isDuplication) {
      _titleController.text = bloc.state.duplicateOrder!.title;
      _currencyController.text = bloc.state.duplicateOrder!.currency;
      _detailsController.text = bloc.state.duplicateOrder!.details;
      _deliveryDate = DateTime.now().add(const Duration(days: 4));
      _dueDate = DateTime.now().add(const Duration(days: 3));
    }
    if (bloc.state.isNew) {
      _currencyController.text = 'RON';
      _deliveryDate = DateTime.now().add(const Duration(days: 4));
      _dueDate = DateTime.now().add(const Duration(days: 3));
    }
  }

  @override
  void initState() {
    super.initState();
    _currencyFocusNode = FocusNode();
    _detailsFocusNode = FocusNode();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _detailsController.dispose();
    _currencyController.dispose();
    _currencyFocusNode.dispose();
    _detailsFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditOrderBloc, EditOrderState>(
      bloc: BlocProvider.of<EditOrderBloc>(context),
      buildWhen: EditOrderState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, EditOrderState state) {
    final EditOrderEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToOrder) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        OrderScreen.routeName,
        (Route<dynamic> route) => route.settings.name == HomeScreen.routeName,
        arguments: effect.order,
      );
    }
    if (effect is Close) {
      Navigator.pop(context);
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, EditOrderState state) {
    final EditOrderBloc bloc = BlocProvider.of<EditOrderBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
        title: Text(state.isEdit
            ? Strings.editOrderEditTitle
            : Strings.editOrderCreateTitle),
      ),
      body: BlocListener<EditOrderBloc, EditOrderState>(
        bloc: bloc,
        listenWhen: EditOrderState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, EditOrderState state) {
    final EditOrderBloc bloc = _bloc(context);
    final double currencyInputWidth = MediaQuery.of(context).size.width / 3;
    const double suggestionsBoxWidth = 250;
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            TextFormField(
              controller: _titleController,
              autofocus: true,
              decoration:
                  InputDecoration(hintText: Strings.editOrderTitleField),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              textCapitalization: TextCapitalization.sentences,
              validator: bloc.titleValidator,
              onFieldSubmitted: (String value) =>
                  FocusScope.of(context).requestFocus(_currencyFocusNode),
            ),
            const SizedBox(height: 16),
            if (state.orderType == OrderType.provider) ...<Widget>[
              _dateWidget(context, _deliveryDate, isDueDate: false),
              const SizedBox(height: 16),
            ],
            _dateWidget(context, _dueDate, isDueDate: true),
            const SizedBox(height: 16),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    Strings.editOrderCurrency,
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
                SizedBox(
                  width: currencyInputWidth,
                  child: TypeAheadFormField<Currency>(
                    suggestionsCallback: (String pattern) {
                      final String match = pattern.trim().toLowerCase();
                      final List<Currency> list = <Currency>[];
                      for (Currency currency in availableCurrencies.iter) {
                        if (currency.code.toLowerCase().contains(match) ||
                            currency.name.toLowerCase().contains(match)) {
                          list.add(currency);
                        }
                      }
                      return list;
                    },
                    onSuggestionSelected: (Currency currency) {
                      _currencyController.text = currency.code;
                    },
                    itemBuilder: (BuildContext context, Currency currency) {
                      return ListTile(
                        title: Text('${currency.code} (${currency.name})'),
                      );
                    },
                    suggestionsBoxDecoration: SuggestionsBoxDecoration(
                      constraints: const BoxConstraints.expand(
                          width: suggestionsBoxWidth),
                      offsetX: currencyInputWidth - suggestionsBoxWidth,
                    ),
                    hideOnEmpty: true,
                    textFieldConfiguration: TextFieldConfiguration(
                      controller: _currencyController,
                      focusNode: _currencyFocusNode,
                      textInputAction: TextInputAction.next,
                      textAlign: TextAlign.center,
                      textCapitalization: TextCapitalization.characters,
                    ),
                    validator: bloc.currencyValidator,
                  ),
                )
              ],
            ),
            const SizedBox(height: 16),
            TextFormField(
              controller: _detailsController,
              decoration: InputDecoration(hintText: Strings.editOrderDetails),
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              textCapitalization: TextCapitalization.sentences,
              minLines: 5,
              maxLines: 10,
            ),
            const SizedBox(height: 16),
            RaisedBtn(
              label:
                  state.isEdit ? Strings.genericSave : Strings.editOrderCreate,
              onPressed: () {
                final FormState form = _formKey.currentState!;
                if (form.validate()) {
                  final String title = _titleController.text.trim();
                  final String details = _detailsController.text.trim();
                  final String currency =
                      _currencyController.text.trim().toUpperCase();
                  bloc.add(
                    SaveOrderRequested(
                      title: title,
                      details: details,
                      currency: currency,
                      deliveryDate: _deliveryDate,
                      dueDate: _dueDate,
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _dateWidget(BuildContext context, DateTime date,
      {required bool isDueDate}) {
    final DatePickerCallback dateCallback = (DateTime pickedDate) {
      setState(() {
        if (Platform.isAndroid) {
          if (isDueDate) {
            final DateTime date = MiscUtils.mergeDateTime(
                pickedDate, TimeOfDay.fromDateTime(_dueDate));
            _dueDate = date;
          } else {
            final DateTime date = MiscUtils.mergeDateTime(
                pickedDate, TimeOfDay.fromDateTime(_deliveryDate));
            _deliveryDate = date;
          }
        } else {
          if (isDueDate) {
            _dueDate = pickedDate;
          } else {
            _deliveryDate = pickedDate;
          }
        }
      });
    };
    final TimePickerCallback timeCallback = (TimeOfDay pickedTime) {
      setState(() {
        if (isDueDate) {
          final DateTime date = MiscUtils.mergeDateTime(_dueDate, pickedTime);
          _dueDate = date;
        } else {
          final DateTime date =
              MiscUtils.mergeDateTime(_deliveryDate, pickedTime);
          _deliveryDate = date;
        }
      });
    };
    return InkWell(
      onTap: () => pickDate(context, initialDate: date, callback: dateCallback),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              isDueDate ? Strings.genericDueDate : Strings.genericDeliveryDate,
              style: const TextStyle(fontSize: 16),
            ),
          ),
          InkWell(
            onTap: () =>
                pickDate(context, initialDate: date, callback: dateCallback),
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.lightGrey,
                borderRadius: BorderRadius.circular(12),
              ),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 100),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Text(
                    FormatterUtils.date(date),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(width: 8),
          InkWell(
            onTap: () =>
                pickTime(context, initialDate: date, callback: timeCallback),
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.lightGrey,
                borderRadius: BorderRadius.circular(12),
              ),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 90),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Text(
                    FormatterUtils.time(context, date),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  EditOrderBloc _bloc(BuildContext context) {
    return BlocProvider.of<EditOrderBloc>(context);
  }
}

class EditOrderArgs {
  EditOrderArgs(
      {this.orderBloc,
      this.order,
      required this.orderType,
      this.duplicateOrder});

  final OrderBloc? orderBloc;
  final Order? order;
  final OrderType orderType;
  final Order? duplicateOrder;
}

// ignore: unused_element
final Logger _logger = Logger('EditOrderScreen');
