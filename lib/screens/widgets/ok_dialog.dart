import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:universal_io/io.dart';

class OkDialog extends StatelessWidget {
  const OkDialog({
    Key? key,
    required this.title,
    required this.content,
    this.onOKPressed,
  }) : super(key: key);

  final String title;
  final Widget content;
  final VoidCallback? onOKPressed;

  @override
  Widget build(BuildContext context) {
    return Platform.isAndroid
        ? _AndroidOkDialog(
            title: title,
            content: content,
            onOKPressed: _onPressed,
          )
        : _IosOkDialog(
            title: title,
            content: content,
            onOKPressed: _onPressed,
          );
  }

  void _onPressed(BuildContext context) {
    Navigator.of(context).pop();
    onOKPressed?.call();
  }
}

class _AndroidOkDialog extends StatelessWidget {
  const _AndroidOkDialog({
    Key? key,
    required this.title,
    required this.content,
    required this.onOKPressed,
  }) : super(key: key);
  final String title;
  final Widget content;
  final void Function(BuildContext context) onOKPressed;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: content,
      actions: <Widget>[
        FlatBtn(
          label: Strings.genericOk,
          onPressed: () => onOKPressed(context),
        )
      ],
    );
  }
}

class _IosOkDialog extends StatelessWidget {
  const _IosOkDialog({
    Key? key,
    required this.title,
    required this.content,
    required this.onOKPressed,
  }) : super(key: key);
  final String title;
  final Widget content;
  final void Function(BuildContext context) onOKPressed;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: content,
      actions: <Widget>[
        CupertinoDialogAction(
          onPressed: () => onOKPressed(context),
          child: Text(
            Strings.genericOk,
            style: const TextStyle(color: AppColors.accent),
          ),
        )
      ],
    );
  }
}
