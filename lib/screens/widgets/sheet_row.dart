import 'package:flutter/material.dart';
import 'package:orders/app/app_colors.dart';

class SheetRow extends StatelessWidget {
  const SheetRow({
    Key? key,
    required this.text,
    required this.iconData,
    required this.onTap,
    this.isEnabled = true,
    this.color = AppColors.accent,
  }) : super(key: key);

  final String text;
  final IconData iconData;
  final VoidCallback? onTap;
  final bool isEnabled;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final TextStyle style = Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(fontWeight: FontWeight.w300);
    return InkWell(
      onTap: onTap,
      child: ConstrainedBox(
        constraints: const BoxConstraints(minHeight: 48),
        child: Row(
          children: <Widget>[
            Icon(iconData, color: color),
            const SizedBox(width: 16),
            Text(text, style: isEnabled ? style : style.copyWith(color: color)),
          ],
        ),
      ),
    );
  }
}
