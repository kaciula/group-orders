import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/widgets/custom_url_linkifier.dart';
import 'package:orders/screens/widgets/sheet_row.dart';
import 'package:orders/screens/widgets/sure_dialog.dart';

class ActionSheet extends StatelessWidget {
  const ActionSheet({
    Key? key,
    required this.title,
    required this.details,
    required this.isEditVisible,
    required this.isDeleteVisible,
    this.onOpenLink,
    this.onTapEdit,
    this.deleteSureMsg,
    this.onTapDelete,
  }) : super(key: key);

  final String title;
  final String details;
  final bool isEditVisible;
  final bool isDeleteVisible;
  final void Function(String url)? onOpenLink;
  final VoidCallback? onTapEdit;
  final String? deleteSureMsg;
  final VoidCallback? onTapDelete;

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(16),
      shrinkWrap: true,
      children: <Widget>[
        Text(title, style: const TextStyle(fontSize: 18)),
        const SizedBox(height: 8),
        if (details.isNotEmpty) ...<Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Linkify(
              text: details,
              linkifiers: const <Linkifier>[CustomUrlLinkifier()],
              options: LinkifyOptions(looseUrl: true),
              onOpen: onOpenLink != null
                  ? (LinkableElement it) {
                      onOpenLink!.call(it.url);
                      Navigator.pop(context);
                    }
                  : null,
            ),
          ),
        ],
        if (isEditVisible) ...<Widget>[
          const Divider(),
          SheetRow(
            text: Strings.orderEdit,
            iconData: Icons.edit,
            onTap: onTapEdit,
            isEnabled: onTapEdit != null,
            color:
                onTapEdit != null ? AppColors.accent : AppColors.textGreyLight,
          ),
        ],
        if (isDeleteVisible) ...<Widget>[
          const Divider(),
          SheetRow(
            text: Strings.orderDelete,
            iconData: Icons.delete,
            onTap: onTapDelete != null
                ? () => showDialog(
                      builder: (BuildContext innerContext) => SureDialog(
                        msg: deleteSureMsg!,
                        onYesPressed: () {
                          onTapDelete!.call();
                          Navigator.pop(innerContext);
                        },
                      ),
                      context: context,
                    )
                : null,
            isEnabled: onTapDelete != null,
            color:
                onTapDelete != null ? AppColors.red : AppColors.textGreyLight,
          ),
        ],
      ],
    );
  }
}
