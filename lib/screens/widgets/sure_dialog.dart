import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:universal_io/io.dart';

class SureDialog extends StatelessWidget {
  const SureDialog({
    Key? key,
    required this.msg,
    required this.onYesPressed,
    this.onNoPressed,
  }) : super(key: key);

  final String msg;
  final VoidCallback onYesPressed;
  final VoidCallback? onNoPressed;

  @override
  Widget build(BuildContext context) {
    return Platform.isAndroid
        ? _AndroidSureDialog(
            msg: msg,
            onYesPressed: onYesPressed,
            onNoPressed: onNoPressed,
          )
        : _IosSureDialog(
            msg: msg,
            onYesPressed: onYesPressed,
            onNoPressed: onNoPressed,
          );
  }
}

class _AndroidSureDialog extends StatelessWidget {
  const _AndroidSureDialog({
    Key? key,
    required this.msg,
    required this.onYesPressed,
    this.onNoPressed,
  }) : super(key: key);

  final String msg;
  final VoidCallback onYesPressed;
  final VoidCallback? onNoPressed;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Text(msg),
      actions: <Widget>[
        FlatBtn(
          label: Strings.genericNo,
          onPressed: () {
            Navigator.pop(context);
            onNoPressed?.call();
          },
        ),
        FlatBtn(
          label: Strings.genericYes,
          onPressed: () {
            Navigator.pop(context);
            onYesPressed();
          },
        ),
      ],
    );
  }
}

class _IosSureDialog extends StatelessWidget {
  const _IosSureDialog({
    Key? key,
    required this.msg,
    required this.onYesPressed,
    this.onNoPressed,
  }) : super(key: key);

  final String msg;
  final VoidCallback onYesPressed;
  final VoidCallback? onNoPressed;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      content: Text(msg),
      actions: <Widget>[
        CupertinoDialogAction(
          onPressed: () {
            Navigator.pop(context);
            onNoPressed?.call();
          },
          child: Text(
            Strings.genericNo,
            style: const TextStyle(color: AppColors.accent),
          ),
        ),
        CupertinoDialogAction(
          onPressed: () {
            Navigator.pop(context);
            onYesPressed();
          },
          child: Text(
            Strings.genericYes,
            style: const TextStyle(color: AppColors.accent),
          ),
        )
      ],
    );
  }
}
