import 'package:linkify/linkify.dart';

final RegExp _urlRegex = RegExp(
  r'^((?:.|\n)*?)((?:https?):\/\/[^\s/$.?#].[^\s]*)',
  caseSensitive: false,
);

final RegExp _looseUrlRegex = RegExp(
  r'^((?:.|\n)*?)([^\s]*\.[^\s]+)',
  caseSensitive: false,
);

class CustomUrlLinkifier extends Linkifier {
  const CustomUrlLinkifier();

  @override
  List<LinkifyElement> parse(
      List<LinkifyElement> elements, LinkifyOptions options) {
    final List<LinkifyElement> list = <LinkifyElement>[];

    // ignore: avoid_function_literals_in_foreach_calls
    elements.forEach((LinkifyElement element) {
      if (element is TextElement) {
        bool loose = false;
        RegExpMatch? match = _urlRegex.firstMatch(element.text);

        if (match == null && options.looseUrl) {
          match = _looseUrlRegex.firstMatch(element.text);
          loose = true;
        }

        if (match == null) {
          list.add(element);
        } else {
          final String text = element.text.replaceFirst(match.group(0)!, '');

          if (match.group(1)?.isNotEmpty ?? false) {
            list.add(TextElement(match.group(1)!));
          }

          if (match.group(2)?.isNotEmpty ?? false) {
            String originalUrl = match.group(2)!;
            String? end;

            if ((options.excludeLastPeriod) &&
                originalUrl[originalUrl.length - 1] == '.') {
              end = '.';
              originalUrl = originalUrl.substring(0, originalUrl.length - 1);
            }

            String url = originalUrl;

            if (loose) {
              originalUrl = (options.defaultToHttps ? 'https://' : 'http://') +
                  originalUrl;
            }

            if (options.humanize || options.removeWww) {
              if (options.humanize) {
                url = url.replaceFirst(RegExp(r'https?://'), '');
              }
              if (options.removeWww) {
                url = url.replaceFirst(RegExp(r'www\.'), '');
              }

              list.add(UrlElement(
                originalUrl,
                url,
              ));
            } else {
              list.add(UrlElement(originalUrl));
            }

            if (end != null) {
              list.add(TextElement(end));
            }
          }

          if (text.isNotEmpty) {
            list.addAll(parse(<LinkifyElement>[TextElement(text)], options));
          }
        }
      } else {
        list.add(element);
      }
    });

    return list;
  }
}

/// Represents an element containing a link
class UrlElement extends LinkableElement {
  UrlElement(String url, [String? text]) : super(text, url);

  @override
  String toString() {
    return "LinkElement: '$url' ($text)";
  }

  @override
  bool operator ==(dynamic other) => equals(other);

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  bool equals(dynamic other) => other is UrlElement && super.equals(other);
}
