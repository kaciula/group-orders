import 'package:flutter/material.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/widgets/buttons.dart';

class RefreshErrorView extends StatelessWidget {
  const RefreshErrorView({
    Key? key,
    required this.errorTitle,
    required this.errorMsg,
    required this.onRetry,
  }) : super(key: key);

  final String errorTitle;
  final String errorMsg;
  final VoidCallback onRetry;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              errorTitle,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            const SizedBox(height: 16),
            Text(errorMsg, textAlign: TextAlign.center),
            const SizedBox(height: 16),
            RaisedBtn(label: Strings.genericRetry, onPressed: onRetry),
          ],
        ),
      ),
    );
  }
}
