import 'package:flutter/material.dart';
import 'package:orders/app/app_colors.dart';

class RaisedBtn extends StatelessWidget {
  const RaisedBtn({
    Key? key,
    required this.label,
    required this.onPressed,
    this.icon,
    this.color = AppColors.accent,
    this.textColor = AppColors.textWhite,
  }) : super(key: key);

  factory RaisedBtn.icon({
    required String label,
    required Widget icon,
    required VoidCallback onPressed,
  }) {
    return RaisedBtn(label: label, icon: icon, onPressed: onPressed);
  }

  final String label;
  final Widget? icon;
  final Color color;
  final Color textColor;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(
        backgroundColor: color, foregroundColor: textColor);
    return icon == null
        ? ElevatedButton(
            style: style,
            onPressed: onPressed,
            child: Text(label),
          )
        : ElevatedButton.icon(
            style: style,
            icon: icon!,
            label: Text(label),
            onPressed: onPressed,
          );
  }
}

class FlatBtn extends StatelessWidget {
  const FlatBtn({
    Key? key,
    required this.label,
    required this.onPressed,
    this.textColor = AppColors.accent,
    this.style,
    this.icon,
    this.padding,
  }) : super(key: key);

  factory FlatBtn.icon({
    required String label,
    required Widget icon,
    Color textColor = AppColors.accent,
    required VoidCallback onPressed,
  }) {
    return FlatBtn(
      label: label,
      icon: icon,
      textColor: textColor,
      onPressed: onPressed,
    );
  }

  final String label;
  final Color textColor;
  final TextStyle? style;
  final Widget? icon;
  final EdgeInsets? padding;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    final ButtonStyle btnStyle =
        TextButton.styleFrom(foregroundColor: textColor);
    return icon == null
        ? TextButton(
            style: btnStyle,
            onPressed: onPressed,
            child: Text(label, style: style),
          )
        : TextButton.icon(
            style: btnStyle,
            onPressed: onPressed,
            icon: icon!,
            label: Text(label),
          );
  }
}
