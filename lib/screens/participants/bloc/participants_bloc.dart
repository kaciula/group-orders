import 'package:bloc/bloc.dart';
import 'package:kt_dart/kt.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/data/local/misc/launcher.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart' as order_event;
import 'package:orders/screens/participants/bloc/participants_effect.dart';
import 'package:orders/screens/participants/bloc/participants_event.dart';
import 'package:orders/screens/participants/bloc/participants_state.dart';
import 'package:orders/service_locator.dart';

class ParticipantsBloc extends Bloc<ParticipantsEvent, ParticipantsState> {
  ParticipantsBloc({
    required this.orderBloc,
    required Order order,
    required KtList<Participant> participants,
    required KtList<CartEntry> cart,
    required bool isOwner,
    required Participant owner,
  }) : super(ParticipantsState.initial(
          order: order,
          activeParticipants: participants
              .filter((Participant it) => _filterActive(it, owner, cart))
              .sortedWith((Participant a, Participant b) =>
                  _comparatorParticipants(a, b, owner)),
          shyParticipants: participants
              .filterNot((Participant it) => _filterActive(it, owner, cart))
              .sortedWith((Participant a, Participant b) =>
                  _comparatorParticipants(a, b, owner)),
          isOwner: isOwner,
          owner: owner,
        ));

  final OrderBloc orderBloc;

  final Launcher _launcher = getIt<Launcher>();
  final DataRemoteStore _dataRemoteStore = getIt<DataRemoteStore>();

  @override
  Stream<ParticipantsState> mapEventToState(ParticipantsEvent event) async* {
    if (event is CallParticipantRequested) {
      _launcher.dial(phoneNum: event.participant.phoneNumber);
    }
    if (event is TalkOnWhatsAppRequested) {
      _launcher.launchWhatsApp(phoneNumber: event.participant.phoneNumber);
    }
    if (event is MarkParticipantRequested) {
      yield state.copyWith(inProgressAction: true);

      final bool? paid = (event.confirmedPaid ?? false) ? true : null;
      final StoreResult result = await _dataRemoteStore.updateParticipantFields(
        orderId: state.order.id,
        participantId: event.participant.userId,
        delivered: event.delivered,
        paid: paid,
        confirmedPaid: event.confirmedPaid,
      );
      if (result is StoreSuccess) {
        final Participant participant = event.participant.copyWith(
          delivered: event.delivered ?? event.participant.delivered,
          paid: paid ?? event.participant.paid,
          confirmedPaid: event.confirmedPaid ?? event.participant.confirmedPaid,
        );
        final KtList<Participant> activeParticipants = state.activeParticipants
            .map((Participant it) =>
                it.userId == event.participant.userId ? participant : it);
        yield state.copyWith(
            inProgressAction: false, activeParticipants: activeParticipants);
        orderBloc.add(order_event.ParticipantChanged(participant: participant));
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.genericErrorSnackBarMsg));
      }
    }
  }

  static int _comparatorParticipants(
      Participant a, Participant b, Participant owner) {
    if (a.userId == owner.userId) {
      return -1;
    }
    if (b.userId == owner.userId) {
      return 1;
    }
    return a.displayName.compareTo(b.displayName);
  }

  static bool _filterActive(
      Participant it, Participant owner, KtList<CartEntry> cart) {
    return it.userId == owner.userId ||
        cart.any((CartEntry entry) =>
            entry is CartProduct && entry.participantId == it.userId);
  }

  // ignore: unused_element
  Stream<ParticipantsState> _withEffect(
      ParticipantsState state, ParticipantsEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('ParticipantsBloc');
