import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/screens/participants/bloc/participants_effect.dart';

part 'participants_state.freezed.dart';

@freezed
class ParticipantsState with _$ParticipantsState {
  factory ParticipantsState({
    required Order order,
    required KtList<Participant> activeParticipants,
    required KtList<Participant> shyParticipants,
    required bool isOwner,
    required Participant owner,
    required bool inProgressAction,
    ParticipantsEffect? effect,
  }) = _ParticipantsState;

  factory ParticipantsState.initial({
    required Order order,
    required KtList<Participant> activeParticipants,
    required KtList<Participant> shyParticipants,
    required bool isOwner,
    required Participant owner,
  }) {
    return ParticipantsState(
      order: order,
      activeParticipants: activeParticipants,
      shyParticipants: shyParticipants,
      isOwner: isOwner,
      owner: owner,
      inProgressAction: false,
    );
  }

  static bool buildWhen(ParticipantsState previous, ParticipantsState current) {
    return previous.order != current.order ||
        previous.activeParticipants != current.shyParticipants ||
        previous.shyParticipants != current.shyParticipants ||
        previous.isOwner != current.isOwner ||
        previous.owner != current.owner ||
        previous.inProgressAction != current.inProgressAction;
  }

  static bool listenWhen(
      ParticipantsState previous, ParticipantsState current) {
    return current.effect != null;
  }
}
