// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'participants_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ParticipantsEvent {
  Participant get participant => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)
        markParticipantRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)?
        markParticipantRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)?
        markParticipantRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ParticipantsEventCopyWith<ParticipantsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ParticipantsEventCopyWith<$Res> {
  factory $ParticipantsEventCopyWith(
          ParticipantsEvent value, $Res Function(ParticipantsEvent) then) =
      _$ParticipantsEventCopyWithImpl<$Res>;
  $Res call({Participant participant});

  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class _$ParticipantsEventCopyWithImpl<$Res>
    implements $ParticipantsEventCopyWith<$Res> {
  _$ParticipantsEventCopyWithImpl(this._value, this._then);

  final ParticipantsEvent _value;
  // ignore: unused_field
  final $Res Function(ParticipantsEvent) _then;

  @override
  $Res call({
    Object? participant = freezed,
  }) {
    return _then(_value.copyWith(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
    ));
  }

  @override
  $ParticipantCopyWith<$Res> get participant {
    return $ParticipantCopyWith<$Res>(_value.participant, (value) {
      return _then(_value.copyWith(participant: value));
    });
  }
}

/// @nodoc
abstract class _$$CallParticipantRequestedCopyWith<$Res>
    implements $ParticipantsEventCopyWith<$Res> {
  factory _$$CallParticipantRequestedCopyWith(_$CallParticipantRequested value,
          $Res Function(_$CallParticipantRequested) then) =
      __$$CallParticipantRequestedCopyWithImpl<$Res>;
  @override
  $Res call({Participant participant});

  @override
  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class __$$CallParticipantRequestedCopyWithImpl<$Res>
    extends _$ParticipantsEventCopyWithImpl<$Res>
    implements _$$CallParticipantRequestedCopyWith<$Res> {
  __$$CallParticipantRequestedCopyWithImpl(_$CallParticipantRequested _value,
      $Res Function(_$CallParticipantRequested) _then)
      : super(_value, (v) => _then(v as _$CallParticipantRequested));

  @override
  _$CallParticipantRequested get _value =>
      super._value as _$CallParticipantRequested;

  @override
  $Res call({
    Object? participant = freezed,
  }) {
    return _then(_$CallParticipantRequested(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
    ));
  }
}

/// @nodoc

class _$CallParticipantRequested implements CallParticipantRequested {
  _$CallParticipantRequested({required this.participant});

  @override
  final Participant participant;

  @override
  String toString() {
    return 'ParticipantsEvent.callParticipantRequested(participant: $participant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CallParticipantRequested &&
            const DeepCollectionEquality()
                .equals(other.participant, participant));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(participant));

  @JsonKey(ignore: true)
  @override
  _$$CallParticipantRequestedCopyWith<_$CallParticipantRequested>
      get copyWith =>
          __$$CallParticipantRequestedCopyWithImpl<_$CallParticipantRequested>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)
        markParticipantRequested,
  }) {
    return callParticipantRequested(participant);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)?
        markParticipantRequested,
  }) {
    return callParticipantRequested?.call(participant);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)?
        markParticipantRequested,
    required TResult orElse(),
  }) {
    if (callParticipantRequested != null) {
      return callParticipantRequested(participant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
  }) {
    return callParticipantRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
  }) {
    return callParticipantRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    required TResult orElse(),
  }) {
    if (callParticipantRequested != null) {
      return callParticipantRequested(this);
    }
    return orElse();
  }
}

abstract class CallParticipantRequested implements ParticipantsEvent {
  factory CallParticipantRequested({required final Participant participant}) =
      _$CallParticipantRequested;

  @override
  Participant get participant;
  @override
  @JsonKey(ignore: true)
  _$$CallParticipantRequestedCopyWith<_$CallParticipantRequested>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$TalkOnWhatsAppRequestedCopyWith<$Res>
    implements $ParticipantsEventCopyWith<$Res> {
  factory _$$TalkOnWhatsAppRequestedCopyWith(_$TalkOnWhatsAppRequested value,
          $Res Function(_$TalkOnWhatsAppRequested) then) =
      __$$TalkOnWhatsAppRequestedCopyWithImpl<$Res>;
  @override
  $Res call({Participant participant});

  @override
  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class __$$TalkOnWhatsAppRequestedCopyWithImpl<$Res>
    extends _$ParticipantsEventCopyWithImpl<$Res>
    implements _$$TalkOnWhatsAppRequestedCopyWith<$Res> {
  __$$TalkOnWhatsAppRequestedCopyWithImpl(_$TalkOnWhatsAppRequested _value,
      $Res Function(_$TalkOnWhatsAppRequested) _then)
      : super(_value, (v) => _then(v as _$TalkOnWhatsAppRequested));

  @override
  _$TalkOnWhatsAppRequested get _value =>
      super._value as _$TalkOnWhatsAppRequested;

  @override
  $Res call({
    Object? participant = freezed,
  }) {
    return _then(_$TalkOnWhatsAppRequested(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
    ));
  }
}

/// @nodoc

class _$TalkOnWhatsAppRequested implements TalkOnWhatsAppRequested {
  _$TalkOnWhatsAppRequested({required this.participant});

  @override
  final Participant participant;

  @override
  String toString() {
    return 'ParticipantsEvent.talkOnWhatsAppRequested(participant: $participant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TalkOnWhatsAppRequested &&
            const DeepCollectionEquality()
                .equals(other.participant, participant));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(participant));

  @JsonKey(ignore: true)
  @override
  _$$TalkOnWhatsAppRequestedCopyWith<_$TalkOnWhatsAppRequested> get copyWith =>
      __$$TalkOnWhatsAppRequestedCopyWithImpl<_$TalkOnWhatsAppRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)
        markParticipantRequested,
  }) {
    return talkOnWhatsAppRequested(participant);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)?
        markParticipantRequested,
  }) {
    return talkOnWhatsAppRequested?.call(participant);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)?
        markParticipantRequested,
    required TResult orElse(),
  }) {
    if (talkOnWhatsAppRequested != null) {
      return talkOnWhatsAppRequested(participant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
  }) {
    return talkOnWhatsAppRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
  }) {
    return talkOnWhatsAppRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    required TResult orElse(),
  }) {
    if (talkOnWhatsAppRequested != null) {
      return talkOnWhatsAppRequested(this);
    }
    return orElse();
  }
}

abstract class TalkOnWhatsAppRequested implements ParticipantsEvent {
  factory TalkOnWhatsAppRequested({required final Participant participant}) =
      _$TalkOnWhatsAppRequested;

  @override
  Participant get participant;
  @override
  @JsonKey(ignore: true)
  _$$TalkOnWhatsAppRequestedCopyWith<_$TalkOnWhatsAppRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MarkParticipantRequestedCopyWith<$Res>
    implements $ParticipantsEventCopyWith<$Res> {
  factory _$$MarkParticipantRequestedCopyWith(_$MarkParticipantRequested value,
          $Res Function(_$MarkParticipantRequested) then) =
      __$$MarkParticipantRequestedCopyWithImpl<$Res>;
  @override
  $Res call({Participant participant, bool? delivered, bool? confirmedPaid});

  @override
  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class __$$MarkParticipantRequestedCopyWithImpl<$Res>
    extends _$ParticipantsEventCopyWithImpl<$Res>
    implements _$$MarkParticipantRequestedCopyWith<$Res> {
  __$$MarkParticipantRequestedCopyWithImpl(_$MarkParticipantRequested _value,
      $Res Function(_$MarkParticipantRequested) _then)
      : super(_value, (v) => _then(v as _$MarkParticipantRequested));

  @override
  _$MarkParticipantRequested get _value =>
      super._value as _$MarkParticipantRequested;

  @override
  $Res call({
    Object? participant = freezed,
    Object? delivered = freezed,
    Object? confirmedPaid = freezed,
  }) {
    return _then(_$MarkParticipantRequested(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
      delivered: delivered == freezed
          ? _value.delivered
          : delivered // ignore: cast_nullable_to_non_nullable
              as bool?,
      confirmedPaid: confirmedPaid == freezed
          ? _value.confirmedPaid
          : confirmedPaid // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc

class _$MarkParticipantRequested implements MarkParticipantRequested {
  _$MarkParticipantRequested(
      {required this.participant, this.delivered, this.confirmedPaid});

  @override
  final Participant participant;
  @override
  final bool? delivered;
  @override
  final bool? confirmedPaid;

  @override
  String toString() {
    return 'ParticipantsEvent.markParticipantRequested(participant: $participant, delivered: $delivered, confirmedPaid: $confirmedPaid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MarkParticipantRequested &&
            const DeepCollectionEquality()
                .equals(other.participant, participant) &&
            const DeepCollectionEquality().equals(other.delivered, delivered) &&
            const DeepCollectionEquality()
                .equals(other.confirmedPaid, confirmedPaid));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(participant),
      const DeepCollectionEquality().hash(delivered),
      const DeepCollectionEquality().hash(confirmedPaid));

  @JsonKey(ignore: true)
  @override
  _$$MarkParticipantRequestedCopyWith<_$MarkParticipantRequested>
      get copyWith =>
          __$$MarkParticipantRequestedCopyWithImpl<_$MarkParticipantRequested>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)
        markParticipantRequested,
  }) {
    return markParticipantRequested(participant, delivered, confirmedPaid);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)?
        markParticipantRequested,
  }) {
    return markParticipantRequested?.call(
        participant, delivered, confirmedPaid);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function(
            Participant participant, bool? delivered, bool? confirmedPaid)?
        markParticipantRequested,
    required TResult orElse(),
  }) {
    if (markParticipantRequested != null) {
      return markParticipantRequested(participant, delivered, confirmedPaid);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
  }) {
    return markParticipantRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
  }) {
    return markParticipantRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    required TResult orElse(),
  }) {
    if (markParticipantRequested != null) {
      return markParticipantRequested(this);
    }
    return orElse();
  }
}

abstract class MarkParticipantRequested implements ParticipantsEvent {
  factory MarkParticipantRequested(
      {required final Participant participant,
      final bool? delivered,
      final bool? confirmedPaid}) = _$MarkParticipantRequested;

  @override
  Participant get participant;
  bool? get delivered;
  bool? get confirmedPaid;
  @override
  @JsonKey(ignore: true)
  _$$MarkParticipantRequestedCopyWith<_$MarkParticipantRequested>
      get copyWith => throw _privateConstructorUsedError;
}
