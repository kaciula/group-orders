import 'package:freezed_annotation/freezed_annotation.dart';

part 'participants_effect.freezed.dart';

@freezed
class ParticipantsEffect with _$ParticipantsEffect {
  factory ParticipantsEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory ParticipantsEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
