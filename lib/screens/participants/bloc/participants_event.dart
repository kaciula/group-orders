import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/participant.dart';

part 'participants_event.freezed.dart';

@freezed
class ParticipantsEvent with _$ParticipantsEvent {
  factory ParticipantsEvent.callParticipantRequested(
      {required Participant participant}) = CallParticipantRequested;

  factory ParticipantsEvent.talkOnWhatsAppRequested(
      {required Participant participant}) = TalkOnWhatsAppRequested;

  factory ParticipantsEvent.markParticipantRequested({
    required Participant participant,
    bool? delivered,
    bool? confirmedPaid,
  }) = MarkParticipantRequested;
}
