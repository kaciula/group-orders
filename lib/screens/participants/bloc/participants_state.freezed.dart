// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'participants_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ParticipantsState {
  Order get order => throw _privateConstructorUsedError;
  KtList<Participant> get activeParticipants =>
      throw _privateConstructorUsedError;
  KtList<Participant> get shyParticipants => throw _privateConstructorUsedError;
  bool get isOwner => throw _privateConstructorUsedError;
  Participant get owner => throw _privateConstructorUsedError;
  bool get inProgressAction => throw _privateConstructorUsedError;
  ParticipantsEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ParticipantsStateCopyWith<ParticipantsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ParticipantsStateCopyWith<$Res> {
  factory $ParticipantsStateCopyWith(
          ParticipantsState value, $Res Function(ParticipantsState) then) =
      _$ParticipantsStateCopyWithImpl<$Res>;
  $Res call(
      {Order order,
      KtList<Participant> activeParticipants,
      KtList<Participant> shyParticipants,
      bool isOwner,
      Participant owner,
      bool inProgressAction,
      ParticipantsEffect? effect});

  $OrderCopyWith<$Res> get order;
  $ParticipantCopyWith<$Res> get owner;
  $ParticipantsEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$ParticipantsStateCopyWithImpl<$Res>
    implements $ParticipantsStateCopyWith<$Res> {
  _$ParticipantsStateCopyWithImpl(this._value, this._then);

  final ParticipantsState _value;
  // ignore: unused_field
  final $Res Function(ParticipantsState) _then;

  @override
  $Res call({
    Object? order = freezed,
    Object? activeParticipants = freezed,
    Object? shyParticipants = freezed,
    Object? isOwner = freezed,
    Object? owner = freezed,
    Object? inProgressAction = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      activeParticipants: activeParticipants == freezed
          ? _value.activeParticipants
          : activeParticipants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>,
      shyParticipants: shyParticipants == freezed
          ? _value.shyParticipants
          : shyParticipants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>,
      isOwner: isOwner == freezed
          ? _value.isOwner
          : isOwner // ignore: cast_nullable_to_non_nullable
              as bool,
      owner: owner == freezed
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Participant,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as ParticipantsEffect?,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $ParticipantCopyWith<$Res> get owner {
    return $ParticipantCopyWith<$Res>(_value.owner, (value) {
      return _then(_value.copyWith(owner: value));
    });
  }

  @override
  $ParticipantsEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $ParticipantsEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_ParticipantsStateCopyWith<$Res>
    implements $ParticipantsStateCopyWith<$Res> {
  factory _$$_ParticipantsStateCopyWith(_$_ParticipantsState value,
          $Res Function(_$_ParticipantsState) then) =
      __$$_ParticipantsStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Order order,
      KtList<Participant> activeParticipants,
      KtList<Participant> shyParticipants,
      bool isOwner,
      Participant owner,
      bool inProgressAction,
      ParticipantsEffect? effect});

  @override
  $OrderCopyWith<$Res> get order;
  @override
  $ParticipantCopyWith<$Res> get owner;
  @override
  $ParticipantsEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_ParticipantsStateCopyWithImpl<$Res>
    extends _$ParticipantsStateCopyWithImpl<$Res>
    implements _$$_ParticipantsStateCopyWith<$Res> {
  __$$_ParticipantsStateCopyWithImpl(
      _$_ParticipantsState _value, $Res Function(_$_ParticipantsState) _then)
      : super(_value, (v) => _then(v as _$_ParticipantsState));

  @override
  _$_ParticipantsState get _value => super._value as _$_ParticipantsState;

  @override
  $Res call({
    Object? order = freezed,
    Object? activeParticipants = freezed,
    Object? shyParticipants = freezed,
    Object? isOwner = freezed,
    Object? owner = freezed,
    Object? inProgressAction = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_ParticipantsState(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      activeParticipants: activeParticipants == freezed
          ? _value.activeParticipants
          : activeParticipants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>,
      shyParticipants: shyParticipants == freezed
          ? _value.shyParticipants
          : shyParticipants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>,
      isOwner: isOwner == freezed
          ? _value.isOwner
          : isOwner // ignore: cast_nullable_to_non_nullable
              as bool,
      owner: owner == freezed
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Participant,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as ParticipantsEffect?,
    ));
  }
}

/// @nodoc

class _$_ParticipantsState implements _ParticipantsState {
  _$_ParticipantsState(
      {required this.order,
      required this.activeParticipants,
      required this.shyParticipants,
      required this.isOwner,
      required this.owner,
      required this.inProgressAction,
      this.effect});

  @override
  final Order order;
  @override
  final KtList<Participant> activeParticipants;
  @override
  final KtList<Participant> shyParticipants;
  @override
  final bool isOwner;
  @override
  final Participant owner;
  @override
  final bool inProgressAction;
  @override
  final ParticipantsEffect? effect;

  @override
  String toString() {
    return 'ParticipantsState(order: $order, activeParticipants: $activeParticipants, shyParticipants: $shyParticipants, isOwner: $isOwner, owner: $owner, inProgressAction: $inProgressAction, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ParticipantsState &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality()
                .equals(other.activeParticipants, activeParticipants) &&
            const DeepCollectionEquality()
                .equals(other.shyParticipants, shyParticipants) &&
            const DeepCollectionEquality().equals(other.isOwner, isOwner) &&
            const DeepCollectionEquality().equals(other.owner, owner) &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(activeParticipants),
      const DeepCollectionEquality().hash(shyParticipants),
      const DeepCollectionEquality().hash(isOwner),
      const DeepCollectionEquality().hash(owner),
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_ParticipantsStateCopyWith<_$_ParticipantsState> get copyWith =>
      __$$_ParticipantsStateCopyWithImpl<_$_ParticipantsState>(
          this, _$identity);
}

abstract class _ParticipantsState implements ParticipantsState {
  factory _ParticipantsState(
      {required final Order order,
      required final KtList<Participant> activeParticipants,
      required final KtList<Participant> shyParticipants,
      required final bool isOwner,
      required final Participant owner,
      required final bool inProgressAction,
      final ParticipantsEffect? effect}) = _$_ParticipantsState;

  @override
  Order get order;
  @override
  KtList<Participant> get activeParticipants;
  @override
  KtList<Participant> get shyParticipants;
  @override
  bool get isOwner;
  @override
  Participant get owner;
  @override
  bool get inProgressAction;
  @override
  ParticipantsEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_ParticipantsStateCopyWith<_$_ParticipantsState> get copyWith =>
      throw _privateConstructorUsedError;
}
