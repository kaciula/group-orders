import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kt_dart/kt.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/participants/bloc/participants_bloc.dart';
import 'package:orders/screens/participants/bloc/participants_effect.dart';
import 'package:orders/screens/participants/bloc/participants_event.dart';
import 'package:orders/screens/participants/bloc/participants_state.dart';
import 'package:orders/screens/participants/participant_sheet.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';
import 'package:universal_io/io.dart';

class ParticipantsScreen extends StatefulWidget {
  static const String routeName = 'participants';

  @override
  State<ParticipantsScreen> createState() => _ParticipantsScreenState();
}

class _ParticipantsScreenState extends State<ParticipantsScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ParticipantsBloc, ParticipantsState>(
      bloc: BlocProvider.of<ParticipantsBloc>(context),
      buildWhen: ParticipantsState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, ParticipantsState state) {
    final ParticipantsEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, ParticipantsState state) {
    final ParticipantsBloc bloc = BlocProvider.of<ParticipantsBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(title: Text(Strings.participantsTitle)),
      body: BlocListener<ParticipantsBloc, ParticipantsState>(
        bloc: bloc,
        listenWhen: ParticipantsState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, ParticipantsState state) {
    final int numActive = state.activeParticipants.size;
    final int numShy = state.shyParticipants.size;
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: ListView.builder(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        itemCount: numActive + numShy,
        itemBuilder: (BuildContext context, int index) {
          final bool isActiveSection = index < numActive;
          final bool applyOpacity = !isActiveSection;
          Participant participant;
          bool applyHeader;
          if (isActiveSection) {
            participant = state.activeParticipants[index];
            applyHeader = index == 0;
          } else {
            participant = state.shyParticipants[index - numActive];
            applyHeader = index - numActive == 0;
          }
          if (applyHeader) {
            return _rowWithHeader(
              context,
              state,
              participant,
              isActiveSection
                  ? Strings.participantsActive
                  : Strings.participantsShy,
              applyTopPadding: !isActiveSection && numActive > 0,
              applyOpacity: applyOpacity,
              isActive: isActiveSection,
            );
          } else {
            return Opacity(
              opacity: applyOpacity ? 0.5 : 1,
              child:
                  _row(context, state, participant, isActive: isActiveSection),
            );
          }
        },
      ),
    );
  }

  Widget _rowWithHeader(BuildContext context, ParticipantsState state,
      Participant participant, String headerText,
      {required bool applyTopPadding,
      required bool applyOpacity,
      required bool isActive}) {
    return Padding(
      padding: EdgeInsets.only(top: applyTopPadding ? 32 : 0),
      child: Opacity(
        opacity: applyOpacity ? 0.5 : 1,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Text(
                headerText,
                style: const TextStyle(fontWeight: FontWeight.w600),
              ),
            ),
            const SizedBox(height: 8),
            _row(context, state, participant, isActive: isActive),
          ],
        ),
      ),
    );
  }

  Widget _row(
      BuildContext context, ParticipantsState state, Participant participant,
      {required bool isActive}) {
    final bool showParticipantFlags =
        state.isOwner && isActive && participant.userId != state.owner.userId;
    final ParticipantsBloc bloc = _bloc(context);
    return ConstrainedBox(
      constraints: const BoxConstraints(minWidth: double.infinity),
      child: Card(
        child: InkWell(
          onTap: showParticipantFlags
              ? () => showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) => ParticipantSheet(
                      participant: participant,
                      isOwner: state.isOwner,
                      onTapDelivered: participant.delivered
                          ? null
                          : () {
                              bloc.add(MarkParticipantRequested(
                                participant: participant,
                                delivered: true,
                              ));
                              Navigator.pop(context);
                            },
                      onTapPaid: participant.confirmedPaid
                          ? null
                          : () {
                              bloc.add(MarkParticipantRequested(
                                participant: participant,
                                confirmedPaid: true,
                              ));
                              Navigator.pop(context);
                            },
                    ),
                  )
              : null,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        participant.displayName,
                        style: const TextStyle(fontSize: 16),
                      ),
                      if (showParticipantFlags) ...<Widget>[
                        const SizedBox(height: 8),
                        ...ScreenUtils.participantFlags(participant,
                            isOwner: state.isOwner)
                      ],
                    ],
                  ),
                ),
                if (_showCallAndWhatsApp(state, participant)) ...<Widget>[
                  const SizedBox(width: 8),
                  IconButton(
                    icon: const Icon(
                      Icons.call,
                      color: AppColors.accent,
                    ),
                    onPressed: () => _bloc(context).add(
                        CallParticipantRequested(participant: participant)),
                  ),
                  IconButton(
                    icon: Icon(
                      Platform.isIOS
                          ? FontAwesomeIcons.whatsappSquare
                          : FontAwesomeIcons.whatsapp,
                      color: AppColors.accent,
                    ),
                    onPressed: () => _bloc(context)
                        .add(TalkOnWhatsAppRequested(participant: participant)),
                  ),
                ],
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _showCallAndWhatsApp(ParticipantsState state, Participant participant) {
    return participant.hasPhoneNumber &&
        ((state.isOwner && participant.userId != state.owner.userId) ||
            (!state.isOwner && participant.userId == state.owner.userId));
  }

  ParticipantsBloc _bloc(BuildContext context) {
    return BlocProvider.of<ParticipantsBloc>(context);
  }
}

class ParticipantsArgs {
  ParticipantsArgs({
    required this.orderBloc,
    required this.order,
    required this.participants,
    required this.cart,
    required this.isOwner,
    required this.owner,
  });

  final OrderBloc orderBloc;
  final Order order;
  final KtList<Participant> participants;
  final KtList<CartEntry> cart;
  final bool isOwner;
  final Participant owner;
}

// ignore: unused_element
final Logger _logger = Logger('ParticipantsScreen');
