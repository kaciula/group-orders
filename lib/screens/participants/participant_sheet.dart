import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/screens/widgets/sheet_row.dart';
import 'package:universal_io/io.dart';

class ParticipantSheet extends StatelessWidget {
  const ParticipantSheet({
    Key? key,
    required this.participant,
    required this.isOwner,
    required this.onTapDelivered,
    required this.onTapPaid,
    this.onTapWhatsApp,
    this.onTapCall,
  }) : super(key: key);

  final Participant participant;
  final bool isOwner;
  final VoidCallback? onTapDelivered;
  final VoidCallback? onTapPaid;
  final VoidCallback? onTapWhatsApp;
  final VoidCallback? onTapCall;

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(16),
      shrinkWrap: true,
      children: <Widget>[
        Text(participant.displayName, style: const TextStyle(fontSize: 18)),
        const SizedBox(height: 8),
        const Divider(),
        if (onTapCall != null) ...<Widget>[
          SheetRow(
            text: Strings.cartCall,
            iconData: Icons.call,
            onTap: onTapCall,
          ),
          const Divider(),
        ],
        if (onTapWhatsApp != null) ...<Widget>[
          SheetRow(
              text: Strings.genericTalkWhatsApp,
              iconData: Platform.isIOS
                  ? FontAwesomeIcons.whatsappSquare
                  : FontAwesomeIcons.whatsapp,
              onTap: onTapWhatsApp),
          const Divider(),
        ],
        SheetRow(
          text: !isOwner &&
                  participant.delivered &&
                  !participant.confirmedDelivered
              ? Strings.participantsConfirmDelivered
              : Strings.participantsMarkDelivered,
          iconData: Icons.house,
          onTap: onTapDelivered,
          isEnabled: onTapDelivered != null,
          color: onTapDelivered != null
              ? AppColors.accent
              : AppColors.textGreyLight,
        ),
        const Divider(),
        SheetRow(
          text: isOwner && participant.paid && !participant.confirmedPaid
              ? Strings.participantsConfirmPaid
              : Strings.participantsMarkPaid,
          iconData: Icons.payment,
          onTap: onTapPaid,
          isEnabled: onTapPaid != null,
          color: onTapPaid != null ? AppColors.accent : AppColors.textGreyLight,
        ),
      ],
    );
  }
}
