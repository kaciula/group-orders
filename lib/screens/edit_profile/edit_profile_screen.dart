import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_bloc.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_effect.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_event.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_state.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';

class EditProfileScreen extends StatefulWidget {
  static const String routeName = 'edit_profile';

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen>
    with AfterLayoutMixin<EditProfileScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _displayNameController = TextEditingController();

  @override
  void afterFirstLayout(BuildContext context) {
    final EditProfileBloc bloc = BlocProvider.of<EditProfileBloc>(context);
    _displayNameController.text = bloc.state.user.displayName;
  }

  @override
  void dispose() {
    _displayNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProfileBloc, EditProfileState>(
      bloc: BlocProvider.of<EditProfileBloc>(context),
      buildWhen: EditProfileState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, EditProfileState state) {
    final EditProfileEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is Close) {
      Navigator.pop(context);
    }
  }

  Widget _builder(BuildContext context, EditProfileState state) {
    final EditProfileBloc bloc = BlocProvider.of<EditProfileBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(title: Text(Strings.editProfileTitle)),
      body: BlocListener<EditProfileBloc, EditProfileState>(
        bloc: bloc,
        listenWhen: EditProfileState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, EditProfileState state) {
    final EditProfileBloc bloc = _bloc(context);
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            TextFormField(
              controller: _displayNameController,
              autofocus: true,
              decoration: InputDecoration(hintText: Strings.signUpDisplayName),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              validator: bloc.displayNameValidator,
            ),
            const SizedBox(height: 16),
            RaisedBtn(
              label: Strings.genericSave,
              onPressed: () {
                final FormState form = _formKey.currentState!;
                if (form.validate()) {
                  final String displayName = _displayNameController.text.trim();
                  bloc.add(SaveProfileRequested(displayName: displayName));
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  EditProfileBloc _bloc(BuildContext context) {
    return BlocProvider.of<EditProfileBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('EditProfileScreen');
