import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/data/local/store/user_provider.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_effect.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_event.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_state.dart';
import 'package:orders/service_locator.dart';

class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  EditProfileBloc(this.dataBloc, User user)
      : super(EditProfileState.initial(user));

  final DataBloc dataBloc;
  final DataRemoteStore _dataRemoteStore = getIt<DataRemoteStore>();
  final UserProvider _userProvider = getIt<UserProvider>();

  @override
  Stream<EditProfileState> mapEventToState(EditProfileEvent event) async* {
    if (event is SaveProfileRequested) {
      yield state.copyWith(inProgressAction: true);
      final StoreResult result = await _dataRemoteStore
          .updateUserFields(state.user.id, displayName: event.displayName);
      if (result is StoreSuccess) {
        dataBloc.add(UserUpdated(
            user: state.user.copyWith(displayName: event.displayName)));
        _userProvider
            .persistUser(state.user.copyWith(displayName: event.displayName));
        yield* _withEffect(state, Close());
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.editProductErrorMsg));
      }
    }
  }

  Stream<EditProfileState> _withEffect(
      EditProfileState state, EditProfileEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }

  String? displayNameValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    return null;
  }
}

// ignore: unused_element
final Logger _logger = Logger('EditProfileBloc');
