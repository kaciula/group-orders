import 'package:freezed_annotation/freezed_annotation.dart';

part 'edit_profile_event.freezed.dart';

@freezed
class EditProfileEvent with _$EditProfileEvent {
  factory EditProfileEvent.saveProfileRequested({
    required String displayName,
  }) = SaveProfileRequested;
}
