// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'edit_profile_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$EditProfileEvent {
  String get displayName => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String displayName) saveProfileRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String displayName)? saveProfileRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String displayName)? saveProfileRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveProfileRequested value) saveProfileRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveProfileRequested value)? saveProfileRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveProfileRequested value)? saveProfileRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $EditProfileEventCopyWith<EditProfileEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EditProfileEventCopyWith<$Res> {
  factory $EditProfileEventCopyWith(
          EditProfileEvent value, $Res Function(EditProfileEvent) then) =
      _$EditProfileEventCopyWithImpl<$Res>;
  $Res call({String displayName});
}

/// @nodoc
class _$EditProfileEventCopyWithImpl<$Res>
    implements $EditProfileEventCopyWith<$Res> {
  _$EditProfileEventCopyWithImpl(this._value, this._then);

  final EditProfileEvent _value;
  // ignore: unused_field
  final $Res Function(EditProfileEvent) _then;

  @override
  $Res call({
    Object? displayName = freezed,
  }) {
    return _then(_value.copyWith(
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$SaveProfileRequestedCopyWith<$Res>
    implements $EditProfileEventCopyWith<$Res> {
  factory _$$SaveProfileRequestedCopyWith(_$SaveProfileRequested value,
          $Res Function(_$SaveProfileRequested) then) =
      __$$SaveProfileRequestedCopyWithImpl<$Res>;
  @override
  $Res call({String displayName});
}

/// @nodoc
class __$$SaveProfileRequestedCopyWithImpl<$Res>
    extends _$EditProfileEventCopyWithImpl<$Res>
    implements _$$SaveProfileRequestedCopyWith<$Res> {
  __$$SaveProfileRequestedCopyWithImpl(_$SaveProfileRequested _value,
      $Res Function(_$SaveProfileRequested) _then)
      : super(_value, (v) => _then(v as _$SaveProfileRequested));

  @override
  _$SaveProfileRequested get _value => super._value as _$SaveProfileRequested;

  @override
  $Res call({
    Object? displayName = freezed,
  }) {
    return _then(_$SaveProfileRequested(
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SaveProfileRequested implements SaveProfileRequested {
  _$SaveProfileRequested({required this.displayName});

  @override
  final String displayName;

  @override
  String toString() {
    return 'EditProfileEvent.saveProfileRequested(displayName: $displayName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaveProfileRequested &&
            const DeepCollectionEquality()
                .equals(other.displayName, displayName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(displayName));

  @JsonKey(ignore: true)
  @override
  _$$SaveProfileRequestedCopyWith<_$SaveProfileRequested> get copyWith =>
      __$$SaveProfileRequestedCopyWithImpl<_$SaveProfileRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String displayName) saveProfileRequested,
  }) {
    return saveProfileRequested(displayName);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String displayName)? saveProfileRequested,
  }) {
    return saveProfileRequested?.call(displayName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String displayName)? saveProfileRequested,
    required TResult orElse(),
  }) {
    if (saveProfileRequested != null) {
      return saveProfileRequested(displayName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveProfileRequested value) saveProfileRequested,
  }) {
    return saveProfileRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveProfileRequested value)? saveProfileRequested,
  }) {
    return saveProfileRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveProfileRequested value)? saveProfileRequested,
    required TResult orElse(),
  }) {
    if (saveProfileRequested != null) {
      return saveProfileRequested(this);
    }
    return orElse();
  }
}

abstract class SaveProfileRequested implements EditProfileEvent {
  factory SaveProfileRequested({required final String displayName}) =
      _$SaveProfileRequested;

  @override
  String get displayName;
  @override
  @JsonKey(ignore: true)
  _$$SaveProfileRequestedCopyWith<_$SaveProfileRequested> get copyWith =>
      throw _privateConstructorUsedError;
}
