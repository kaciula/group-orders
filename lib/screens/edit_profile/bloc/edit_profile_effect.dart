import 'package:freezed_annotation/freezed_annotation.dart';

part 'edit_profile_effect.freezed.dart';

@freezed
class EditProfileEffect with _$EditProfileEffect {
  factory EditProfileEffect.close() = Close;

  factory EditProfileEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory EditProfileEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
