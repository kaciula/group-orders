import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/edit_profile/bloc/edit_profile_effect.dart';

part 'edit_profile_state.freezed.dart';

@freezed
class EditProfileState with _$EditProfileState {
  factory EditProfileState({
    required User user,
    required bool inProgressAction,
    EditProfileEffect? effect,
  }) = _EditProfileState;

  factory EditProfileState.initial(User user) {
    return EditProfileState(user: user, inProgressAction: false);
  }

  static bool buildWhen(EditProfileState previous, EditProfileState current) {
    return previous.user != current.user ||
        previous.inProgressAction != current.inProgressAction;
  }

  static bool listenWhen(EditProfileState previous, EditProfileState current) {
    return current.effect != null;
  }
}
