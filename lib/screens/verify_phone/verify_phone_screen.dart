import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/app/app_styles.dart';
import 'package:orders/screens/home/home_screen.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_bloc.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_effect.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_event.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_state.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/circular_progress.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';

class VerifyPhoneScreen extends StatefulWidget {
  static const String routeName = 'verify_phone';

  @override
  State<VerifyPhoneScreen> createState() => _VerifyPhoneScreenState();
}

class _VerifyPhoneScreenState extends State<VerifyPhoneScreen>
    with AfterLayoutMixin<VerifyPhoneScreen> {
//  final FocusNode _phoneFocusNode = FocusNode();
  final GlobalKey<FormState> _phoneNumberFormKey = GlobalKey<FormState>();
  PhoneNumber _phoneNumber = PhoneNumber(phoneNumber: '', isoCode: 'RO');
  final GlobalKey<FormState> _smsCodeFormKey = GlobalKey<FormState>();
  final TextEditingController _smsCodeController = TextEditingController();

  @override
  void afterFirstLayout(BuildContext context) {
//    FocusScope.of(context).requestFocus(_phoneFocusNode);
  }

  @override
  void dispose() {
//    _phoneFocusNode.dispose();
    _smsCodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VerifyPhoneBloc, VerifyPhoneState>(
      bloc: BlocProvider.of<VerifyPhoneBloc>(context),
      buildWhen: VerifyPhoneState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, VerifyPhoneState state) {
    final VerifyPhoneEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToHome) {
      Navigator.pushNamedAndRemoveUntil(
          context, HomeScreen.routeName, (Route<dynamic> route) => false);
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, VerifyPhoneState state) {
    final VerifyPhoneBloc bloc = BlocProvider.of<VerifyPhoneBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
          title: Text(Strings.verifyPhoneTitle),
          actions: state.showSkip
              ? <Widget>[
                  TextButton(
                    child: Text(
                      Strings.verifyPhoneSkip,
                      style: const TextStyle(color: AppColors.textWhite),
                    ),
                    onPressed: () => _bloc(context).add(SkipRequested()),
                  ),
                ]
              : null),
      body: BlocListener<VerifyPhoneBloc, VerifyPhoneState>(
        bloc: bloc,
        listenWhen: VerifyPhoneState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, VerifyPhoneState state) {
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: state.stage == Stage.enterPhoneNumber
          ? _enterPhoneNumber(context)
          : _enterSmsCode(context, state),
    );
  }

  Widget _enterPhoneNumber(BuildContext context) {
    final VerifyPhoneBloc bloc = _bloc(context);
    return Form(
      key: _phoneNumberFormKey,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            Text(
              Strings.verifyPhoneIntro,
              style: styleDirections,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 32),
            IntrinsicWidth(
              child: InternationalPhoneNumberInput(
                onInputChanged: (PhoneNumber phoneNumber) {
                  _logger.fine('Phone num = $phoneNumber');
                  _phoneNumber = phoneNumber;
                },
                validator: bloc.phoneNumberValidator,
//              focusNode: _phoneFocusNode,
                initialValue: PhoneNumber(isoCode: _phoneNumber.isoCode),
                hintText: Strings.verifyPhoneHint,
                errorMessage: Strings.verifyPhoneInputErr,
              ),
            ),
            const SizedBox(height: 24),
            ConstrainedBox(
              constraints: const BoxConstraints(minWidth: 250),
              child: RaisedBtn(
                label: Strings.verifyPhoneContinue,
                onPressed: () {
                  final FormState form = _phoneNumberFormKey.currentState!;
                  if (form.validate()) {
                    final String phoneNumber = _phoneNumber.phoneNumber!.trim();
                    bloc.add(
                      SendPhoneNumberRequested(phoneNumber: phoneNumber),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _enterSmsCode(BuildContext context, VerifyPhoneState state) {
    final VerifyPhoneBloc bloc = _bloc(context);
    return Form(
      key: _smsCodeFormKey,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                Strings.verifyPhoneCodeText(state.phoneNumber),
                style: styleDirections,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 16),
              SizedBox(
                width: 100,
                child: TextFormField(
                  controller: _smsCodeController,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  autofocus: true,
                  validator: bloc.smsCodeValidator,
                ),
              ),
              const SizedBox(height: 24),
              ConstrainedBox(
                constraints: const BoxConstraints(minWidth: double.infinity),
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    RaisedBtn(
                      label: Strings.verifyPhoneContinue,
                      onPressed: () {
                        final FormState form = _smsCodeFormKey.currentState!;
                        if (form.validate()) {
                          final String smsCode = _smsCodeController.text.trim();
                          _bloc(context).add(
                            SendSmsCodeRequested(smsCode: smsCode),
                          );
                        }
                      },
                    ),
                    const Positioned(
                      top: 0,
                      left: 0,
                      bottom: 0,
                      child: UnconstrainedBox(
                        child: SizedBox(
                          width: 12,
                          height: 12,
                          child: CircularProgress(),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      bottom: 0,
                      child: FlatBtn(
                        label: Strings.verifyPhoneReenter,
                        onPressed: () {
                          _bloc(context).add(ReenterPhoneNumberRequested());
                          _smsCodeController.clear();
                        },
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  VerifyPhoneBloc _bloc(BuildContext context) {
    return BlocProvider.of<VerifyPhoneBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('VerifyPhoneScreen');
