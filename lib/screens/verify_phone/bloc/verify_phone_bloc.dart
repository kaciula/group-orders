import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/data/remote/connectors/phone_verifier.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_effect.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_event.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_state.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/set_up_after_sign_in.dart';
import 'package:orders/usecases/use_case_results.dart';

class VerifyPhoneBloc extends Bloc<VerifyPhoneEvent, VerifyPhoneState> {
  VerifyPhoneBloc(User user) : super(VerifyPhoneState.initial(user: user));

  final PhoneVerifier _phoneVerifier = getIt<PhoneVerifier>();
  final SetUpAfterSignIn _setUpAfterSignIn = getIt<SetUpAfterSignIn>();

  @override
  Stream<VerifyPhoneState> mapEventToState(VerifyPhoneEvent event) async* {
    if (event is SendPhoneNumberRequested) {
      yield state.copyWith(
          inProgressAction: true, phoneNumber: event.phoneNumber);
      _phoneVerifier.verifyPhoneNumberAutomatically(
        phoneNumber: event.phoneNumber,
        successCallback: () => add(VerificationSuccess()),
        failureCallback: (String errorMsg) =>
            add(VerificationFailure(errorMsg)),
        smsCodeSentCallback: (String verificationId) =>
            add(SmsCodeSentFromBackend(verificationId: verificationId)),
      );
    }
    if (event is VerificationSuccess) {
      yield* _continue();
    }
    if (event is VerificationFailure) {
      yield* _withEffect(
          VerifyPhoneState.initial(user: state.user),
          ShowErrorSnackBar(
              errorMsg: '${Strings.verifyPhoneCodeErr}. ${event.errorMsg}'));
    }
    if (event is SmsCodeSentFromBackend) {
      yield state.copyWith(
          inProgressAction: false,
          verificationId: event.verificationId,
          stage: Stage.enterCode);
    }
    if (event is SendSmsCodeRequested) {
      yield state.copyWith(inProgressAction: true);
      final VerifyPhoneNumberManuallyResult result =
          await _phoneVerifier.verifyPhoneNumberManually(
              verificationId: state.verificationId, smsCode: event.smsCode);
      if (result is VerifyPhoneNumberManuallySuccess) {
        yield* _continue();
      } else {
        final VerifyPhoneNumberManuallyFailure failure =
            result as VerifyPhoneNumberManuallyFailure;
        yield* _withEffect(
            state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(
                errorMsg:
                    '${Strings.verifyPhoneCodeErr}. ${failure.errorMsg}'));
      }
    }
    if (event is ReenterPhoneNumberRequested) {
      yield VerifyPhoneState.initial(user: state.user);
    }
    if (event is SkipRequested) {
      yield state.copyWith(inProgressAction: true);
      yield* _continue();
    }
  }

  Stream<VerifyPhoneState> _continue() async* {
    final UseCaseResult result = await _setUpAfterSignIn(
      state.user.copyWith(phoneNumber: state.phoneNumber),
      checkHasAccount: false,
    );
    if (result is UseCaseSuccess) {
      yield* _withEffect(state, GoToHome());
    } else {
      yield* _withEffect(
          state.copyWith(
              inProgressAction: false, stage: Stage.enterPhoneNumber),
          ShowErrorSnackBar(errorMsg: Strings.genericErrorSnackBarMsg));
    }
  }

  String? phoneNumberValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    return null;
  }

  String? smsCodeValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    return null;
  }

  Stream<VerifyPhoneState> _withEffect(
      VerifyPhoneState state, VerifyPhoneEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('VerifyPhoneBloc');
