// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'verify_phone_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$VerifyPhoneState {
  bool get inProgressAction => throw _privateConstructorUsedError;
  Stage get stage => throw _privateConstructorUsedError;
  String get phoneNumber => throw _privateConstructorUsedError;
  String get verificationId => throw _privateConstructorUsedError;
  User get user => throw _privateConstructorUsedError;
  VerifyPhoneEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $VerifyPhoneStateCopyWith<VerifyPhoneState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VerifyPhoneStateCopyWith<$Res> {
  factory $VerifyPhoneStateCopyWith(
          VerifyPhoneState value, $Res Function(VerifyPhoneState) then) =
      _$VerifyPhoneStateCopyWithImpl<$Res>;
  $Res call(
      {bool inProgressAction,
      Stage stage,
      String phoneNumber,
      String verificationId,
      User user,
      VerifyPhoneEffect? effect});

  $UserCopyWith<$Res> get user;
  $VerifyPhoneEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$VerifyPhoneStateCopyWithImpl<$Res>
    implements $VerifyPhoneStateCopyWith<$Res> {
  _$VerifyPhoneStateCopyWithImpl(this._value, this._then);

  final VerifyPhoneState _value;
  // ignore: unused_field
  final $Res Function(VerifyPhoneState) _then;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? stage = freezed,
    Object? phoneNumber = freezed,
    Object? verificationId = freezed,
    Object? user = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      stage: stage == freezed
          ? _value.stage
          : stage // ignore: cast_nullable_to_non_nullable
              as Stage,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      verificationId: verificationId == freezed
          ? _value.verificationId
          : verificationId // ignore: cast_nullable_to_non_nullable
              as String,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as VerifyPhoneEffect?,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }

  @override
  $VerifyPhoneEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $VerifyPhoneEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_VerifyPhoneStateCopyWith<$Res>
    implements $VerifyPhoneStateCopyWith<$Res> {
  factory _$$_VerifyPhoneStateCopyWith(
          _$_VerifyPhoneState value, $Res Function(_$_VerifyPhoneState) then) =
      __$$_VerifyPhoneStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool inProgressAction,
      Stage stage,
      String phoneNumber,
      String verificationId,
      User user,
      VerifyPhoneEffect? effect});

  @override
  $UserCopyWith<$Res> get user;
  @override
  $VerifyPhoneEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_VerifyPhoneStateCopyWithImpl<$Res>
    extends _$VerifyPhoneStateCopyWithImpl<$Res>
    implements _$$_VerifyPhoneStateCopyWith<$Res> {
  __$$_VerifyPhoneStateCopyWithImpl(
      _$_VerifyPhoneState _value, $Res Function(_$_VerifyPhoneState) _then)
      : super(_value, (v) => _then(v as _$_VerifyPhoneState));

  @override
  _$_VerifyPhoneState get _value => super._value as _$_VerifyPhoneState;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? stage = freezed,
    Object? phoneNumber = freezed,
    Object? verificationId = freezed,
    Object? user = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_VerifyPhoneState(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      stage: stage == freezed
          ? _value.stage
          : stage // ignore: cast_nullable_to_non_nullable
              as Stage,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      verificationId: verificationId == freezed
          ? _value.verificationId
          : verificationId // ignore: cast_nullable_to_non_nullable
              as String,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as VerifyPhoneEffect?,
    ));
  }
}

/// @nodoc

class _$_VerifyPhoneState extends _VerifyPhoneState {
  _$_VerifyPhoneState(
      {required this.inProgressAction,
      required this.stage,
      required this.phoneNumber,
      required this.verificationId,
      required this.user,
      this.effect})
      : super._();

  @override
  final bool inProgressAction;
  @override
  final Stage stage;
  @override
  final String phoneNumber;
  @override
  final String verificationId;
  @override
  final User user;
  @override
  final VerifyPhoneEffect? effect;

  @override
  String toString() {
    return 'VerifyPhoneState(inProgressAction: $inProgressAction, stage: $stage, phoneNumber: $phoneNumber, verificationId: $verificationId, user: $user, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_VerifyPhoneState &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality().equals(other.stage, stage) &&
            const DeepCollectionEquality()
                .equals(other.phoneNumber, phoneNumber) &&
            const DeepCollectionEquality()
                .equals(other.verificationId, verificationId) &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(stage),
      const DeepCollectionEquality().hash(phoneNumber),
      const DeepCollectionEquality().hash(verificationId),
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_VerifyPhoneStateCopyWith<_$_VerifyPhoneState> get copyWith =>
      __$$_VerifyPhoneStateCopyWithImpl<_$_VerifyPhoneState>(this, _$identity);
}

abstract class _VerifyPhoneState extends VerifyPhoneState {
  factory _VerifyPhoneState(
      {required final bool inProgressAction,
      required final Stage stage,
      required final String phoneNumber,
      required final String verificationId,
      required final User user,
      final VerifyPhoneEffect? effect}) = _$_VerifyPhoneState;
  _VerifyPhoneState._() : super._();

  @override
  bool get inProgressAction;
  @override
  Stage get stage;
  @override
  String get phoneNumber;
  @override
  String get verificationId;
  @override
  User get user;
  @override
  VerifyPhoneEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_VerifyPhoneStateCopyWith<_$_VerifyPhoneState> get copyWith =>
      throw _privateConstructorUsedError;
}
