import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/verify_phone/bloc/verify_phone_effect.dart';

part 'verify_phone_state.freezed.dart';

@freezed
class VerifyPhoneState with _$VerifyPhoneState {
  factory VerifyPhoneState({
    required bool inProgressAction,
    required Stage stage,
    required String phoneNumber,
    required String verificationId,
    required User user,
    VerifyPhoneEffect? effect,
  }) = _VerifyPhoneState;

  VerifyPhoneState._();

  factory VerifyPhoneState.initial({required User user}) {
    return VerifyPhoneState(
      inProgressAction: false,
      stage: Stage.enterPhoneNumber,
      phoneNumber: '',
      verificationId: '',
      user: user,
    );
  }

  static bool buildWhen(VerifyPhoneState previous, VerifyPhoneState current) {
    return previous.inProgressAction != current.inProgressAction ||
        previous.stage != current.stage ||
        previous.phoneNumber != current.phoneNumber ||
        previous.verificationId != current.verificationId ||
        previous.user != current.user;
  }

  static bool listenWhen(VerifyPhoneState previous, VerifyPhoneState current) {
    return current.effect != null;
  }

  bool get showSkip => user.signInProvider == SignInProvider.apple;
}

enum Stage { enterPhoneNumber, enterCode }
