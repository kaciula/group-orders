// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'verify_phone_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$VerifyPhoneEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String phoneNumber) sendPhoneNumberRequested,
    required TResult Function() verificationSuccess,
    required TResult Function(String errorMsg) verificationFailure,
    required TResult Function(String verificationId) smsCodeSentFromBackend,
    required TResult Function(String smsCode) sendSmsCodeRequested,
    required TResult Function() reenterPhoneNumberRequested,
    required TResult Function() skipRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SendPhoneNumberRequested value)
        sendPhoneNumberRequested,
    required TResult Function(VerificationSuccess value) verificationSuccess,
    required TResult Function(VerificationFailure value) verificationFailure,
    required TResult Function(SmsCodeSentFromBackend value)
        smsCodeSentFromBackend,
    required TResult Function(SendSmsCodeRequested value) sendSmsCodeRequested,
    required TResult Function(ReenterPhoneNumberRequested value)
        reenterPhoneNumberRequested,
    required TResult Function(SkipRequested value) skipRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VerifyPhoneEventCopyWith<$Res> {
  factory $VerifyPhoneEventCopyWith(
          VerifyPhoneEvent value, $Res Function(VerifyPhoneEvent) then) =
      _$VerifyPhoneEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$VerifyPhoneEventCopyWithImpl<$Res>
    implements $VerifyPhoneEventCopyWith<$Res> {
  _$VerifyPhoneEventCopyWithImpl(this._value, this._then);

  final VerifyPhoneEvent _value;
  // ignore: unused_field
  final $Res Function(VerifyPhoneEvent) _then;
}

/// @nodoc
abstract class _$$SendPhoneNumberRequestedCopyWith<$Res> {
  factory _$$SendPhoneNumberRequestedCopyWith(_$SendPhoneNumberRequested value,
          $Res Function(_$SendPhoneNumberRequested) then) =
      __$$SendPhoneNumberRequestedCopyWithImpl<$Res>;
  $Res call({String phoneNumber});
}

/// @nodoc
class __$$SendPhoneNumberRequestedCopyWithImpl<$Res>
    extends _$VerifyPhoneEventCopyWithImpl<$Res>
    implements _$$SendPhoneNumberRequestedCopyWith<$Res> {
  __$$SendPhoneNumberRequestedCopyWithImpl(_$SendPhoneNumberRequested _value,
      $Res Function(_$SendPhoneNumberRequested) _then)
      : super(_value, (v) => _then(v as _$SendPhoneNumberRequested));

  @override
  _$SendPhoneNumberRequested get _value =>
      super._value as _$SendPhoneNumberRequested;

  @override
  $Res call({
    Object? phoneNumber = freezed,
  }) {
    return _then(_$SendPhoneNumberRequested(
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SendPhoneNumberRequested implements SendPhoneNumberRequested {
  _$SendPhoneNumberRequested({required this.phoneNumber});

  @override
  final String phoneNumber;

  @override
  String toString() {
    return 'VerifyPhoneEvent.sendPhoneNumberRequested(phoneNumber: $phoneNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SendPhoneNumberRequested &&
            const DeepCollectionEquality()
                .equals(other.phoneNumber, phoneNumber));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(phoneNumber));

  @JsonKey(ignore: true)
  @override
  _$$SendPhoneNumberRequestedCopyWith<_$SendPhoneNumberRequested>
      get copyWith =>
          __$$SendPhoneNumberRequestedCopyWithImpl<_$SendPhoneNumberRequested>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String phoneNumber) sendPhoneNumberRequested,
    required TResult Function() verificationSuccess,
    required TResult Function(String errorMsg) verificationFailure,
    required TResult Function(String verificationId) smsCodeSentFromBackend,
    required TResult Function(String smsCode) sendSmsCodeRequested,
    required TResult Function() reenterPhoneNumberRequested,
    required TResult Function() skipRequested,
  }) {
    return sendPhoneNumberRequested(phoneNumber);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
  }) {
    return sendPhoneNumberRequested?.call(phoneNumber);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
    required TResult orElse(),
  }) {
    if (sendPhoneNumberRequested != null) {
      return sendPhoneNumberRequested(phoneNumber);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SendPhoneNumberRequested value)
        sendPhoneNumberRequested,
    required TResult Function(VerificationSuccess value) verificationSuccess,
    required TResult Function(VerificationFailure value) verificationFailure,
    required TResult Function(SmsCodeSentFromBackend value)
        smsCodeSentFromBackend,
    required TResult Function(SendSmsCodeRequested value) sendSmsCodeRequested,
    required TResult Function(ReenterPhoneNumberRequested value)
        reenterPhoneNumberRequested,
    required TResult Function(SkipRequested value) skipRequested,
  }) {
    return sendPhoneNumberRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
  }) {
    return sendPhoneNumberRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
    required TResult orElse(),
  }) {
    if (sendPhoneNumberRequested != null) {
      return sendPhoneNumberRequested(this);
    }
    return orElse();
  }
}

abstract class SendPhoneNumberRequested implements VerifyPhoneEvent {
  factory SendPhoneNumberRequested({required final String phoneNumber}) =
      _$SendPhoneNumberRequested;

  String get phoneNumber;
  @JsonKey(ignore: true)
  _$$SendPhoneNumberRequestedCopyWith<_$SendPhoneNumberRequested>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$VerificationSuccessCopyWith<$Res> {
  factory _$$VerificationSuccessCopyWith(_$VerificationSuccess value,
          $Res Function(_$VerificationSuccess) then) =
      __$$VerificationSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$VerificationSuccessCopyWithImpl<$Res>
    extends _$VerifyPhoneEventCopyWithImpl<$Res>
    implements _$$VerificationSuccessCopyWith<$Res> {
  __$$VerificationSuccessCopyWithImpl(
      _$VerificationSuccess _value, $Res Function(_$VerificationSuccess) _then)
      : super(_value, (v) => _then(v as _$VerificationSuccess));

  @override
  _$VerificationSuccess get _value => super._value as _$VerificationSuccess;
}

/// @nodoc

class _$VerificationSuccess implements VerificationSuccess {
  _$VerificationSuccess();

  @override
  String toString() {
    return 'VerifyPhoneEvent.verificationSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$VerificationSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String phoneNumber) sendPhoneNumberRequested,
    required TResult Function() verificationSuccess,
    required TResult Function(String errorMsg) verificationFailure,
    required TResult Function(String verificationId) smsCodeSentFromBackend,
    required TResult Function(String smsCode) sendSmsCodeRequested,
    required TResult Function() reenterPhoneNumberRequested,
    required TResult Function() skipRequested,
  }) {
    return verificationSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
  }) {
    return verificationSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
    required TResult orElse(),
  }) {
    if (verificationSuccess != null) {
      return verificationSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SendPhoneNumberRequested value)
        sendPhoneNumberRequested,
    required TResult Function(VerificationSuccess value) verificationSuccess,
    required TResult Function(VerificationFailure value) verificationFailure,
    required TResult Function(SmsCodeSentFromBackend value)
        smsCodeSentFromBackend,
    required TResult Function(SendSmsCodeRequested value) sendSmsCodeRequested,
    required TResult Function(ReenterPhoneNumberRequested value)
        reenterPhoneNumberRequested,
    required TResult Function(SkipRequested value) skipRequested,
  }) {
    return verificationSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
  }) {
    return verificationSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
    required TResult orElse(),
  }) {
    if (verificationSuccess != null) {
      return verificationSuccess(this);
    }
    return orElse();
  }
}

abstract class VerificationSuccess implements VerifyPhoneEvent {
  factory VerificationSuccess() = _$VerificationSuccess;
}

/// @nodoc
abstract class _$$VerificationFailureCopyWith<$Res> {
  factory _$$VerificationFailureCopyWith(_$VerificationFailure value,
          $Res Function(_$VerificationFailure) then) =
      __$$VerificationFailureCopyWithImpl<$Res>;
  $Res call({String errorMsg});
}

/// @nodoc
class __$$VerificationFailureCopyWithImpl<$Res>
    extends _$VerifyPhoneEventCopyWithImpl<$Res>
    implements _$$VerificationFailureCopyWith<$Res> {
  __$$VerificationFailureCopyWithImpl(
      _$VerificationFailure _value, $Res Function(_$VerificationFailure) _then)
      : super(_value, (v) => _then(v as _$VerificationFailure));

  @override
  _$VerificationFailure get _value => super._value as _$VerificationFailure;

  @override
  $Res call({
    Object? errorMsg = freezed,
  }) {
    return _then(_$VerificationFailure(
      errorMsg == freezed
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$VerificationFailure implements VerificationFailure {
  _$VerificationFailure(this.errorMsg);

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'VerifyPhoneEvent.verificationFailure(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$VerificationFailure &&
            const DeepCollectionEquality().equals(other.errorMsg, errorMsg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(errorMsg));

  @JsonKey(ignore: true)
  @override
  _$$VerificationFailureCopyWith<_$VerificationFailure> get copyWith =>
      __$$VerificationFailureCopyWithImpl<_$VerificationFailure>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String phoneNumber) sendPhoneNumberRequested,
    required TResult Function() verificationSuccess,
    required TResult Function(String errorMsg) verificationFailure,
    required TResult Function(String verificationId) smsCodeSentFromBackend,
    required TResult Function(String smsCode) sendSmsCodeRequested,
    required TResult Function() reenterPhoneNumberRequested,
    required TResult Function() skipRequested,
  }) {
    return verificationFailure(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
  }) {
    return verificationFailure?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
    required TResult orElse(),
  }) {
    if (verificationFailure != null) {
      return verificationFailure(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SendPhoneNumberRequested value)
        sendPhoneNumberRequested,
    required TResult Function(VerificationSuccess value) verificationSuccess,
    required TResult Function(VerificationFailure value) verificationFailure,
    required TResult Function(SmsCodeSentFromBackend value)
        smsCodeSentFromBackend,
    required TResult Function(SendSmsCodeRequested value) sendSmsCodeRequested,
    required TResult Function(ReenterPhoneNumberRequested value)
        reenterPhoneNumberRequested,
    required TResult Function(SkipRequested value) skipRequested,
  }) {
    return verificationFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
  }) {
    return verificationFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
    required TResult orElse(),
  }) {
    if (verificationFailure != null) {
      return verificationFailure(this);
    }
    return orElse();
  }
}

abstract class VerificationFailure implements VerifyPhoneEvent {
  factory VerificationFailure(final String errorMsg) = _$VerificationFailure;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$VerificationFailureCopyWith<_$VerificationFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SmsCodeSentFromBackendCopyWith<$Res> {
  factory _$$SmsCodeSentFromBackendCopyWith(_$SmsCodeSentFromBackend value,
          $Res Function(_$SmsCodeSentFromBackend) then) =
      __$$SmsCodeSentFromBackendCopyWithImpl<$Res>;
  $Res call({String verificationId});
}

/// @nodoc
class __$$SmsCodeSentFromBackendCopyWithImpl<$Res>
    extends _$VerifyPhoneEventCopyWithImpl<$Res>
    implements _$$SmsCodeSentFromBackendCopyWith<$Res> {
  __$$SmsCodeSentFromBackendCopyWithImpl(_$SmsCodeSentFromBackend _value,
      $Res Function(_$SmsCodeSentFromBackend) _then)
      : super(_value, (v) => _then(v as _$SmsCodeSentFromBackend));

  @override
  _$SmsCodeSentFromBackend get _value =>
      super._value as _$SmsCodeSentFromBackend;

  @override
  $Res call({
    Object? verificationId = freezed,
  }) {
    return _then(_$SmsCodeSentFromBackend(
      verificationId: verificationId == freezed
          ? _value.verificationId
          : verificationId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SmsCodeSentFromBackend implements SmsCodeSentFromBackend {
  _$SmsCodeSentFromBackend({required this.verificationId});

  @override
  final String verificationId;

  @override
  String toString() {
    return 'VerifyPhoneEvent.smsCodeSentFromBackend(verificationId: $verificationId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SmsCodeSentFromBackend &&
            const DeepCollectionEquality()
                .equals(other.verificationId, verificationId));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(verificationId));

  @JsonKey(ignore: true)
  @override
  _$$SmsCodeSentFromBackendCopyWith<_$SmsCodeSentFromBackend> get copyWith =>
      __$$SmsCodeSentFromBackendCopyWithImpl<_$SmsCodeSentFromBackend>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String phoneNumber) sendPhoneNumberRequested,
    required TResult Function() verificationSuccess,
    required TResult Function(String errorMsg) verificationFailure,
    required TResult Function(String verificationId) smsCodeSentFromBackend,
    required TResult Function(String smsCode) sendSmsCodeRequested,
    required TResult Function() reenterPhoneNumberRequested,
    required TResult Function() skipRequested,
  }) {
    return smsCodeSentFromBackend(verificationId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
  }) {
    return smsCodeSentFromBackend?.call(verificationId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
    required TResult orElse(),
  }) {
    if (smsCodeSentFromBackend != null) {
      return smsCodeSentFromBackend(verificationId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SendPhoneNumberRequested value)
        sendPhoneNumberRequested,
    required TResult Function(VerificationSuccess value) verificationSuccess,
    required TResult Function(VerificationFailure value) verificationFailure,
    required TResult Function(SmsCodeSentFromBackend value)
        smsCodeSentFromBackend,
    required TResult Function(SendSmsCodeRequested value) sendSmsCodeRequested,
    required TResult Function(ReenterPhoneNumberRequested value)
        reenterPhoneNumberRequested,
    required TResult Function(SkipRequested value) skipRequested,
  }) {
    return smsCodeSentFromBackend(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
  }) {
    return smsCodeSentFromBackend?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
    required TResult orElse(),
  }) {
    if (smsCodeSentFromBackend != null) {
      return smsCodeSentFromBackend(this);
    }
    return orElse();
  }
}

abstract class SmsCodeSentFromBackend implements VerifyPhoneEvent {
  factory SmsCodeSentFromBackend({required final String verificationId}) =
      _$SmsCodeSentFromBackend;

  String get verificationId;
  @JsonKey(ignore: true)
  _$$SmsCodeSentFromBackendCopyWith<_$SmsCodeSentFromBackend> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SendSmsCodeRequestedCopyWith<$Res> {
  factory _$$SendSmsCodeRequestedCopyWith(_$SendSmsCodeRequested value,
          $Res Function(_$SendSmsCodeRequested) then) =
      __$$SendSmsCodeRequestedCopyWithImpl<$Res>;
  $Res call({String smsCode});
}

/// @nodoc
class __$$SendSmsCodeRequestedCopyWithImpl<$Res>
    extends _$VerifyPhoneEventCopyWithImpl<$Res>
    implements _$$SendSmsCodeRequestedCopyWith<$Res> {
  __$$SendSmsCodeRequestedCopyWithImpl(_$SendSmsCodeRequested _value,
      $Res Function(_$SendSmsCodeRequested) _then)
      : super(_value, (v) => _then(v as _$SendSmsCodeRequested));

  @override
  _$SendSmsCodeRequested get _value => super._value as _$SendSmsCodeRequested;

  @override
  $Res call({
    Object? smsCode = freezed,
  }) {
    return _then(_$SendSmsCodeRequested(
      smsCode: smsCode == freezed
          ? _value.smsCode
          : smsCode // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SendSmsCodeRequested implements SendSmsCodeRequested {
  _$SendSmsCodeRequested({required this.smsCode});

  @override
  final String smsCode;

  @override
  String toString() {
    return 'VerifyPhoneEvent.sendSmsCodeRequested(smsCode: $smsCode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SendSmsCodeRequested &&
            const DeepCollectionEquality().equals(other.smsCode, smsCode));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(smsCode));

  @JsonKey(ignore: true)
  @override
  _$$SendSmsCodeRequestedCopyWith<_$SendSmsCodeRequested> get copyWith =>
      __$$SendSmsCodeRequestedCopyWithImpl<_$SendSmsCodeRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String phoneNumber) sendPhoneNumberRequested,
    required TResult Function() verificationSuccess,
    required TResult Function(String errorMsg) verificationFailure,
    required TResult Function(String verificationId) smsCodeSentFromBackend,
    required TResult Function(String smsCode) sendSmsCodeRequested,
    required TResult Function() reenterPhoneNumberRequested,
    required TResult Function() skipRequested,
  }) {
    return sendSmsCodeRequested(smsCode);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
  }) {
    return sendSmsCodeRequested?.call(smsCode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
    required TResult orElse(),
  }) {
    if (sendSmsCodeRequested != null) {
      return sendSmsCodeRequested(smsCode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SendPhoneNumberRequested value)
        sendPhoneNumberRequested,
    required TResult Function(VerificationSuccess value) verificationSuccess,
    required TResult Function(VerificationFailure value) verificationFailure,
    required TResult Function(SmsCodeSentFromBackend value)
        smsCodeSentFromBackend,
    required TResult Function(SendSmsCodeRequested value) sendSmsCodeRequested,
    required TResult Function(ReenterPhoneNumberRequested value)
        reenterPhoneNumberRequested,
    required TResult Function(SkipRequested value) skipRequested,
  }) {
    return sendSmsCodeRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
  }) {
    return sendSmsCodeRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
    required TResult orElse(),
  }) {
    if (sendSmsCodeRequested != null) {
      return sendSmsCodeRequested(this);
    }
    return orElse();
  }
}

abstract class SendSmsCodeRequested implements VerifyPhoneEvent {
  factory SendSmsCodeRequested({required final String smsCode}) =
      _$SendSmsCodeRequested;

  String get smsCode;
  @JsonKey(ignore: true)
  _$$SendSmsCodeRequestedCopyWith<_$SendSmsCodeRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ReenterPhoneNumberRequestedCopyWith<$Res> {
  factory _$$ReenterPhoneNumberRequestedCopyWith(
          _$ReenterPhoneNumberRequested value,
          $Res Function(_$ReenterPhoneNumberRequested) then) =
      __$$ReenterPhoneNumberRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ReenterPhoneNumberRequestedCopyWithImpl<$Res>
    extends _$VerifyPhoneEventCopyWithImpl<$Res>
    implements _$$ReenterPhoneNumberRequestedCopyWith<$Res> {
  __$$ReenterPhoneNumberRequestedCopyWithImpl(
      _$ReenterPhoneNumberRequested _value,
      $Res Function(_$ReenterPhoneNumberRequested) _then)
      : super(_value, (v) => _then(v as _$ReenterPhoneNumberRequested));

  @override
  _$ReenterPhoneNumberRequested get _value =>
      super._value as _$ReenterPhoneNumberRequested;
}

/// @nodoc

class _$ReenterPhoneNumberRequested implements ReenterPhoneNumberRequested {
  _$ReenterPhoneNumberRequested();

  @override
  String toString() {
    return 'VerifyPhoneEvent.reenterPhoneNumberRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReenterPhoneNumberRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String phoneNumber) sendPhoneNumberRequested,
    required TResult Function() verificationSuccess,
    required TResult Function(String errorMsg) verificationFailure,
    required TResult Function(String verificationId) smsCodeSentFromBackend,
    required TResult Function(String smsCode) sendSmsCodeRequested,
    required TResult Function() reenterPhoneNumberRequested,
    required TResult Function() skipRequested,
  }) {
    return reenterPhoneNumberRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
  }) {
    return reenterPhoneNumberRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
    required TResult orElse(),
  }) {
    if (reenterPhoneNumberRequested != null) {
      return reenterPhoneNumberRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SendPhoneNumberRequested value)
        sendPhoneNumberRequested,
    required TResult Function(VerificationSuccess value) verificationSuccess,
    required TResult Function(VerificationFailure value) verificationFailure,
    required TResult Function(SmsCodeSentFromBackend value)
        smsCodeSentFromBackend,
    required TResult Function(SendSmsCodeRequested value) sendSmsCodeRequested,
    required TResult Function(ReenterPhoneNumberRequested value)
        reenterPhoneNumberRequested,
    required TResult Function(SkipRequested value) skipRequested,
  }) {
    return reenterPhoneNumberRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
  }) {
    return reenterPhoneNumberRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
    required TResult orElse(),
  }) {
    if (reenterPhoneNumberRequested != null) {
      return reenterPhoneNumberRequested(this);
    }
    return orElse();
  }
}

abstract class ReenterPhoneNumberRequested implements VerifyPhoneEvent {
  factory ReenterPhoneNumberRequested() = _$ReenterPhoneNumberRequested;
}

/// @nodoc
abstract class _$$SkipRequestedCopyWith<$Res> {
  factory _$$SkipRequestedCopyWith(
          _$SkipRequested value, $Res Function(_$SkipRequested) then) =
      __$$SkipRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SkipRequestedCopyWithImpl<$Res>
    extends _$VerifyPhoneEventCopyWithImpl<$Res>
    implements _$$SkipRequestedCopyWith<$Res> {
  __$$SkipRequestedCopyWithImpl(
      _$SkipRequested _value, $Res Function(_$SkipRequested) _then)
      : super(_value, (v) => _then(v as _$SkipRequested));

  @override
  _$SkipRequested get _value => super._value as _$SkipRequested;
}

/// @nodoc

class _$SkipRequested implements SkipRequested {
  _$SkipRequested();

  @override
  String toString() {
    return 'VerifyPhoneEvent.skipRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SkipRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String phoneNumber) sendPhoneNumberRequested,
    required TResult Function() verificationSuccess,
    required TResult Function(String errorMsg) verificationFailure,
    required TResult Function(String verificationId) smsCodeSentFromBackend,
    required TResult Function(String smsCode) sendSmsCodeRequested,
    required TResult Function() reenterPhoneNumberRequested,
    required TResult Function() skipRequested,
  }) {
    return skipRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
  }) {
    return skipRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String phoneNumber)? sendPhoneNumberRequested,
    TResult Function()? verificationSuccess,
    TResult Function(String errorMsg)? verificationFailure,
    TResult Function(String verificationId)? smsCodeSentFromBackend,
    TResult Function(String smsCode)? sendSmsCodeRequested,
    TResult Function()? reenterPhoneNumberRequested,
    TResult Function()? skipRequested,
    required TResult orElse(),
  }) {
    if (skipRequested != null) {
      return skipRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SendPhoneNumberRequested value)
        sendPhoneNumberRequested,
    required TResult Function(VerificationSuccess value) verificationSuccess,
    required TResult Function(VerificationFailure value) verificationFailure,
    required TResult Function(SmsCodeSentFromBackend value)
        smsCodeSentFromBackend,
    required TResult Function(SendSmsCodeRequested value) sendSmsCodeRequested,
    required TResult Function(ReenterPhoneNumberRequested value)
        reenterPhoneNumberRequested,
    required TResult Function(SkipRequested value) skipRequested,
  }) {
    return skipRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
  }) {
    return skipRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SendPhoneNumberRequested value)? sendPhoneNumberRequested,
    TResult Function(VerificationSuccess value)? verificationSuccess,
    TResult Function(VerificationFailure value)? verificationFailure,
    TResult Function(SmsCodeSentFromBackend value)? smsCodeSentFromBackend,
    TResult Function(SendSmsCodeRequested value)? sendSmsCodeRequested,
    TResult Function(ReenterPhoneNumberRequested value)?
        reenterPhoneNumberRequested,
    TResult Function(SkipRequested value)? skipRequested,
    required TResult orElse(),
  }) {
    if (skipRequested != null) {
      return skipRequested(this);
    }
    return orElse();
  }
}

abstract class SkipRequested implements VerifyPhoneEvent {
  factory SkipRequested() = _$SkipRequested;
}
