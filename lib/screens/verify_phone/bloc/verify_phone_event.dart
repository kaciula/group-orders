import 'package:freezed_annotation/freezed_annotation.dart';

part 'verify_phone_event.freezed.dart';

@freezed
class VerifyPhoneEvent with _$VerifyPhoneEvent {
  factory VerifyPhoneEvent.sendPhoneNumberRequested({
    required String phoneNumber,
  }) = SendPhoneNumberRequested;

  factory VerifyPhoneEvent.verificationSuccess() = VerificationSuccess;

  factory VerifyPhoneEvent.verificationFailure(String errorMsg) =
      VerificationFailure;

  factory VerifyPhoneEvent.smsCodeSentFromBackend({
    required String verificationId,
  }) = SmsCodeSentFromBackend;

  factory VerifyPhoneEvent.sendSmsCodeRequested({
    required String smsCode,
  }) = SendSmsCodeRequested;

  factory VerifyPhoneEvent.reenterPhoneNumberRequested() =
      ReenterPhoneNumberRequested;

  factory VerifyPhoneEvent.skipRequested() = SkipRequested;
}
