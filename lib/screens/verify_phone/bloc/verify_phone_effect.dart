import 'package:freezed_annotation/freezed_annotation.dart';

part 'verify_phone_effect.freezed.dart';

@freezed
class VerifyPhoneEffect with _$VerifyPhoneEffect {
  factory VerifyPhoneEffect.goToHome() = GoToHome;

  factory VerifyPhoneEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory VerifyPhoneEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
