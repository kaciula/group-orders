import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/app/app_styles.dart';
import 'package:orders/screens/invite/bloc/invite_bloc.dart';
import 'package:orders/screens/invite/bloc/invite_effect.dart';
import 'package:orders/screens/invite/bloc/invite_event.dart';
import 'package:orders/screens/invite/bloc/invite_state.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/circular_progress.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';
import 'package:orders/screens/widgets/refresh_error_view.dart';

class InviteScreen extends StatefulWidget {
  static const String routeName = 'invite';

  @override
  State<InviteScreen> createState() => _InviteScreenState();
}

class _InviteScreenState extends State<InviteScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<InviteBloc, InviteState>(
      bloc: BlocProvider.of<InviteBloc>(context),
      buildWhen: InviteState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, InviteState state) {
    final InviteEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, InviteState state) {
    final InviteBloc bloc = BlocProvider.of<InviteBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(Strings.inviteTitle),
            Text(state.order.title, style: styleAppBarSubtitle),
          ],
        ),
      ),
      body: BlocListener<InviteBloc, InviteState>(
        bloc: bloc,
        listenWhen: InviteState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, InviteState state) {
    return state.refreshState.when(
      inProgress: () => _progress(),
      success: () => _content(context, state),
      error: (bool isInternetConnected) =>
          _error(context, isInternetConnected: isInternetConnected),
    );
  }

  Widget _content(BuildContext context, InviteState state) {
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(Strings.inviteDesc, style: styleDirections),
            const SizedBox(height: 16),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    decoration: const BoxDecoration(
                      color: AppColors.textGreyLight,
                      borderRadius: BorderRadius.all(Radius.circular(9)),
                    ),
                    child: Text(state.inviteLink),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.content_copy),
                  onPressed: () => _bloc(context).add(CopyLinkRequested()),
                )
              ],
            ),
            const SizedBox(height: 16),
            RaisedBtn(
              label: Strings.inviteShare,
              onPressed: () => _bloc(context).add(ShareRequested()),
            ),
          ],
        ),
      ),
    );
  }

  Widget _progress() {
    return const Center(child: CircularProgress());
  }

  Widget _error(BuildContext context, {required bool isInternetConnected}) {
    return RefreshErrorView(
      errorTitle: isInternetConnected
          ? Strings.genericErrorTitle
          : Strings.genericNoInternetTitle,
      errorMsg: isInternetConnected
          ? Strings.genericErrorMsg
          : Strings.genericNoInternetMsg,
      onRetry: () => _bloc(context).add(RefreshRequested()),
    );
  }

  InviteBloc _bloc(BuildContext context) {
    return BlocProvider.of<InviteBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('InviteScreen');
