import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/invite/bloc/invite_effect.dart';
import 'package:orders/screens/utils/refresh_state.dart';

part 'invite_state.freezed.dart';

@freezed
class InviteState with _$InviteState {
  factory InviteState({
    required RefreshState refreshState,
    required bool inProgressAction,
    required Order order,
    required String inviteLink,
    InviteEffect? effect,
  }) = _InviteState;

  factory InviteState.initial(Order order) {
    return InviteState(
      refreshState: RefreshState.success(),
      inProgressAction: false,
      order: order,
      inviteLink: order.inviteLink,
    );
  }

  static bool buildWhen(InviteState previous, InviteState current) {
    return previous.refreshState != current.refreshState ||
        previous.inProgressAction != current.inProgressAction ||
        previous.order != current.order ||
        previous.inviteLink != current.inviteLink;
  }

  static bool listenWhen(InviteState previous, InviteState current) {
    return current.effect != null;
  }
}
