// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'invite_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$InviteState {
  RefreshState get refreshState => throw _privateConstructorUsedError;
  bool get inProgressAction => throw _privateConstructorUsedError;
  Order get order => throw _privateConstructorUsedError;
  String get inviteLink => throw _privateConstructorUsedError;
  InviteEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InviteStateCopyWith<InviteState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InviteStateCopyWith<$Res> {
  factory $InviteStateCopyWith(
          InviteState value, $Res Function(InviteState) then) =
      _$InviteStateCopyWithImpl<$Res>;
  $Res call(
      {RefreshState refreshState,
      bool inProgressAction,
      Order order,
      String inviteLink,
      InviteEffect? effect});

  $RefreshStateCopyWith<$Res> get refreshState;
  $OrderCopyWith<$Res> get order;
  $InviteEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$InviteStateCopyWithImpl<$Res> implements $InviteStateCopyWith<$Res> {
  _$InviteStateCopyWithImpl(this._value, this._then);

  final InviteState _value;
  // ignore: unused_field
  final $Res Function(InviteState) _then;

  @override
  $Res call({
    Object? refreshState = freezed,
    Object? inProgressAction = freezed,
    Object? order = freezed,
    Object? inviteLink = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      refreshState: refreshState == freezed
          ? _value.refreshState
          : refreshState // ignore: cast_nullable_to_non_nullable
              as RefreshState,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      inviteLink: inviteLink == freezed
          ? _value.inviteLink
          : inviteLink // ignore: cast_nullable_to_non_nullable
              as String,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as InviteEffect?,
    ));
  }

  @override
  $RefreshStateCopyWith<$Res> get refreshState {
    return $RefreshStateCopyWith<$Res>(_value.refreshState, (value) {
      return _then(_value.copyWith(refreshState: value));
    });
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $InviteEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $InviteEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_InviteStateCopyWith<$Res>
    implements $InviteStateCopyWith<$Res> {
  factory _$$_InviteStateCopyWith(
          _$_InviteState value, $Res Function(_$_InviteState) then) =
      __$$_InviteStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {RefreshState refreshState,
      bool inProgressAction,
      Order order,
      String inviteLink,
      InviteEffect? effect});

  @override
  $RefreshStateCopyWith<$Res> get refreshState;
  @override
  $OrderCopyWith<$Res> get order;
  @override
  $InviteEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_InviteStateCopyWithImpl<$Res> extends _$InviteStateCopyWithImpl<$Res>
    implements _$$_InviteStateCopyWith<$Res> {
  __$$_InviteStateCopyWithImpl(
      _$_InviteState _value, $Res Function(_$_InviteState) _then)
      : super(_value, (v) => _then(v as _$_InviteState));

  @override
  _$_InviteState get _value => super._value as _$_InviteState;

  @override
  $Res call({
    Object? refreshState = freezed,
    Object? inProgressAction = freezed,
    Object? order = freezed,
    Object? inviteLink = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_InviteState(
      refreshState: refreshState == freezed
          ? _value.refreshState
          : refreshState // ignore: cast_nullable_to_non_nullable
              as RefreshState,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      inviteLink: inviteLink == freezed
          ? _value.inviteLink
          : inviteLink // ignore: cast_nullable_to_non_nullable
              as String,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as InviteEffect?,
    ));
  }
}

/// @nodoc

class _$_InviteState implements _InviteState {
  _$_InviteState(
      {required this.refreshState,
      required this.inProgressAction,
      required this.order,
      required this.inviteLink,
      this.effect});

  @override
  final RefreshState refreshState;
  @override
  final bool inProgressAction;
  @override
  final Order order;
  @override
  final String inviteLink;
  @override
  final InviteEffect? effect;

  @override
  String toString() {
    return 'InviteState(refreshState: $refreshState, inProgressAction: $inProgressAction, order: $order, inviteLink: $inviteLink, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InviteState &&
            const DeepCollectionEquality()
                .equals(other.refreshState, refreshState) &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality()
                .equals(other.inviteLink, inviteLink) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(refreshState),
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(inviteLink),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_InviteStateCopyWith<_$_InviteState> get copyWith =>
      __$$_InviteStateCopyWithImpl<_$_InviteState>(this, _$identity);
}

abstract class _InviteState implements InviteState {
  factory _InviteState(
      {required final RefreshState refreshState,
      required final bool inProgressAction,
      required final Order order,
      required final String inviteLink,
      final InviteEffect? effect}) = _$_InviteState;

  @override
  RefreshState get refreshState;
  @override
  bool get inProgressAction;
  @override
  Order get order;
  @override
  String get inviteLink;
  @override
  InviteEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_InviteStateCopyWith<_$_InviteState> get copyWith =>
      throw _privateConstructorUsedError;
}
