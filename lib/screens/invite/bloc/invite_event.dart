import 'package:freezed_annotation/freezed_annotation.dart';

part 'invite_event.freezed.dart';

@freezed
class InviteEvent with _$InviteEvent {
  factory InviteEvent.screenStarted() = ScreenStarted;

  factory InviteEvent.refreshRequested() = RefreshRequested;

  factory InviteEvent.shareRequested() = ShareRequested;

  factory InviteEvent.copyLinkRequested() = CopyLinkRequested;
}
