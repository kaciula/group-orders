import 'package:bloc/bloc.dart';
import 'package:clipboard/clipboard.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/local/misc/dynamic_links.dart';
import 'package:orders/data/local/misc/launcher.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/invite/bloc/invite_effect.dart';
import 'package:orders/screens/invite/bloc/invite_event.dart';
import 'package:orders/screens/invite/bloc/invite_state.dart';
import 'package:orders/screens/utils/refresh_state.dart';
import 'package:orders/service_locator.dart';

class InviteBloc extends Bloc<InviteEvent, InviteState> {
  InviteBloc(this.dataBloc, Order order) : super(InviteState.initial(order));

  final DataBloc dataBloc;

  final DynamicLinks _dynamicLinks = getIt<DynamicLinks>();
  final Launcher _launcher = getIt<Launcher>();

  @override
  Stream<InviteState> mapEventToState(InviteEvent event) async* {
    if (event is ScreenStarted) {
      if (state.inviteLink.isEmpty) {
        yield* _refresh(state);
      }
    }
    if (event is RefreshRequested) {
      if (state.inviteLink.isEmpty) {
        yield* _refresh(state);
      }
    }
    if (event is ShareRequested) {
      final ShareTextResult result = await _launcher.shareText(
          text: Strings.inviteShareTxt(
            userDisplayName: dataBloc.user.displayName,
            link: state.inviteLink,
            orderTitle: state.order.title,
          ),
          subject: Strings.inviteShareSubject(
              displayName: dataBloc.user.displayName));
      if (result is ShareTextFailure) {
        yield* _withEffect(
            state, ShowErrorSnackBar(errorMsg: Strings.inviteShareErrorMsg));
      }
    }
    if (event is CopyLinkRequested) {
      await FlutterClipboard.copy(state.inviteLink);
      yield* _withEffect(
          state, ShowInfoSnackBar(Strings.inviteCopyLinkSuccess));
    }
  }

  Stream<InviteState> _refresh(InviteState state) async* {
    yield state.copyWith(refreshState: RefreshState.inProgress());
    final String link =
        await _dynamicLinks.generateLink(orderId: state.order.id);
    yield state.copyWith(
        refreshState: RefreshState.success(), inviteLink: link);
  }

  Stream<InviteState> _withEffect(
      InviteState state, InviteEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('InviteBloc');
