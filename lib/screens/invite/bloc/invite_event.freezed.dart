// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'invite_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$InviteEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() shareRequested,
    required TResult Function() copyLinkRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(ShareRequested value) shareRequested,
    required TResult Function(CopyLinkRequested value) copyLinkRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InviteEventCopyWith<$Res> {
  factory $InviteEventCopyWith(
          InviteEvent value, $Res Function(InviteEvent) then) =
      _$InviteEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$InviteEventCopyWithImpl<$Res> implements $InviteEventCopyWith<$Res> {
  _$InviteEventCopyWithImpl(this._value, this._then);

  final InviteEvent _value;
  // ignore: unused_field
  final $Res Function(InviteEvent) _then;
}

/// @nodoc
abstract class _$$ScreenStartedCopyWith<$Res> {
  factory _$$ScreenStartedCopyWith(
          _$ScreenStarted value, $Res Function(_$ScreenStarted) then) =
      __$$ScreenStartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ScreenStartedCopyWithImpl<$Res>
    extends _$InviteEventCopyWithImpl<$Res>
    implements _$$ScreenStartedCopyWith<$Res> {
  __$$ScreenStartedCopyWithImpl(
      _$ScreenStarted _value, $Res Function(_$ScreenStarted) _then)
      : super(_value, (v) => _then(v as _$ScreenStarted));

  @override
  _$ScreenStarted get _value => super._value as _$ScreenStarted;
}

/// @nodoc

class _$ScreenStarted implements ScreenStarted {
  _$ScreenStarted();

  @override
  String toString() {
    return 'InviteEvent.screenStarted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ScreenStarted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() shareRequested,
    required TResult Function() copyLinkRequested,
  }) {
    return screenStarted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
  }) {
    return screenStarted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
    required TResult orElse(),
  }) {
    if (screenStarted != null) {
      return screenStarted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(ShareRequested value) shareRequested,
    required TResult Function(CopyLinkRequested value) copyLinkRequested,
  }) {
    return screenStarted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
  }) {
    return screenStarted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
    required TResult orElse(),
  }) {
    if (screenStarted != null) {
      return screenStarted(this);
    }
    return orElse();
  }
}

abstract class ScreenStarted implements InviteEvent {
  factory ScreenStarted() = _$ScreenStarted;
}

/// @nodoc
abstract class _$$RefreshRequestedCopyWith<$Res> {
  factory _$$RefreshRequestedCopyWith(
          _$RefreshRequested value, $Res Function(_$RefreshRequested) then) =
      __$$RefreshRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RefreshRequestedCopyWithImpl<$Res>
    extends _$InviteEventCopyWithImpl<$Res>
    implements _$$RefreshRequestedCopyWith<$Res> {
  __$$RefreshRequestedCopyWithImpl(
      _$RefreshRequested _value, $Res Function(_$RefreshRequested) _then)
      : super(_value, (v) => _then(v as _$RefreshRequested));

  @override
  _$RefreshRequested get _value => super._value as _$RefreshRequested;
}

/// @nodoc

class _$RefreshRequested implements RefreshRequested {
  _$RefreshRequested();

  @override
  String toString() {
    return 'InviteEvent.refreshRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RefreshRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() shareRequested,
    required TResult Function() copyLinkRequested,
  }) {
    return refreshRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
  }) {
    return refreshRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
    required TResult orElse(),
  }) {
    if (refreshRequested != null) {
      return refreshRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(ShareRequested value) shareRequested,
    required TResult Function(CopyLinkRequested value) copyLinkRequested,
  }) {
    return refreshRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
  }) {
    return refreshRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
    required TResult orElse(),
  }) {
    if (refreshRequested != null) {
      return refreshRequested(this);
    }
    return orElse();
  }
}

abstract class RefreshRequested implements InviteEvent {
  factory RefreshRequested() = _$RefreshRequested;
}

/// @nodoc
abstract class _$$ShareRequestedCopyWith<$Res> {
  factory _$$ShareRequestedCopyWith(
          _$ShareRequested value, $Res Function(_$ShareRequested) then) =
      __$$ShareRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ShareRequestedCopyWithImpl<$Res>
    extends _$InviteEventCopyWithImpl<$Res>
    implements _$$ShareRequestedCopyWith<$Res> {
  __$$ShareRequestedCopyWithImpl(
      _$ShareRequested _value, $Res Function(_$ShareRequested) _then)
      : super(_value, (v) => _then(v as _$ShareRequested));

  @override
  _$ShareRequested get _value => super._value as _$ShareRequested;
}

/// @nodoc

class _$ShareRequested implements ShareRequested {
  _$ShareRequested();

  @override
  String toString() {
    return 'InviteEvent.shareRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ShareRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() shareRequested,
    required TResult Function() copyLinkRequested,
  }) {
    return shareRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
  }) {
    return shareRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
    required TResult orElse(),
  }) {
    if (shareRequested != null) {
      return shareRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(ShareRequested value) shareRequested,
    required TResult Function(CopyLinkRequested value) copyLinkRequested,
  }) {
    return shareRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
  }) {
    return shareRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
    required TResult orElse(),
  }) {
    if (shareRequested != null) {
      return shareRequested(this);
    }
    return orElse();
  }
}

abstract class ShareRequested implements InviteEvent {
  factory ShareRequested() = _$ShareRequested;
}

/// @nodoc
abstract class _$$CopyLinkRequestedCopyWith<$Res> {
  factory _$$CopyLinkRequestedCopyWith(
          _$CopyLinkRequested value, $Res Function(_$CopyLinkRequested) then) =
      __$$CopyLinkRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CopyLinkRequestedCopyWithImpl<$Res>
    extends _$InviteEventCopyWithImpl<$Res>
    implements _$$CopyLinkRequestedCopyWith<$Res> {
  __$$CopyLinkRequestedCopyWithImpl(
      _$CopyLinkRequested _value, $Res Function(_$CopyLinkRequested) _then)
      : super(_value, (v) => _then(v as _$CopyLinkRequested));

  @override
  _$CopyLinkRequested get _value => super._value as _$CopyLinkRequested;
}

/// @nodoc

class _$CopyLinkRequested implements CopyLinkRequested {
  _$CopyLinkRequested();

  @override
  String toString() {
    return 'InviteEvent.copyLinkRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CopyLinkRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() shareRequested,
    required TResult Function() copyLinkRequested,
  }) {
    return copyLinkRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
  }) {
    return copyLinkRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? shareRequested,
    TResult Function()? copyLinkRequested,
    required TResult orElse(),
  }) {
    if (copyLinkRequested != null) {
      return copyLinkRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(ShareRequested value) shareRequested,
    required TResult Function(CopyLinkRequested value) copyLinkRequested,
  }) {
    return copyLinkRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
  }) {
    return copyLinkRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(ShareRequested value)? shareRequested,
    TResult Function(CopyLinkRequested value)? copyLinkRequested,
    required TResult orElse(),
  }) {
    if (copyLinkRequested != null) {
      return copyLinkRequested(this);
    }
    return orElse();
  }
}

abstract class CopyLinkRequested implements InviteEvent {
  factory CopyLinkRequested() = _$CopyLinkRequested;
}
