import 'package:freezed_annotation/freezed_annotation.dart';

part 'invite_effect.freezed.dart';

@freezed
class InviteEffect with _$InviteEffect {
  factory InviteEffect() = _InviteEffect;

  factory InviteEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory InviteEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
