import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/connect/connect_screen.dart';
import 'package:orders/screens/home/home_screen.dart';
import 'package:orders/screens/splash/bloc/splash_bloc.dart';
import 'package:orders/screens/splash/bloc/splash_effect.dart';
import 'package:orders/screens/splash/bloc/splash_event.dart';
import 'package:orders/screens/splash/bloc/splash_state.dart';
import 'package:orders/screens/widgets/circular_progress.dart';
import 'package:orders/screens/widgets/refresh_error_view.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName = 'splash';

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SplashBloc, SplashState>(
      bloc: BlocProvider.of<SplashBloc>(context),
      buildWhen: SplashState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, SplashState state) {
    final SplashEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToConnect) {
      Navigator.pushReplacementNamed(context, ConnectScreen.routeName);
    }
    if (effect is GoToHome) {
      Navigator.pushReplacementNamed(context, HomeScreen.routeName);
    }
  }

  Widget _builder(BuildContext context, SplashState state) {
    final SplashBloc bloc = BlocProvider.of<SplashBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      body: BlocListener<SplashBloc, SplashState>(
        bloc: bloc,
        listenWhen: SplashState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, SplashState state) {
    return state.refreshState.when(
      inProgress: () => _progress(),
      success: () => Container(),
      error: (bool isInternetConnected) =>
          _error(context, isInternetConnected: isInternetConnected),
    );
  }

  Widget _progress() {
    return const Center(child: CircularProgress());
  }

  Widget _error(BuildContext context, {required bool isInternetConnected}) {
    return RefreshErrorView(
      errorTitle: isInternetConnected
          ? Strings.genericErrorTitle
          : Strings.genericNoInternetTitle,
      errorMsg: isInternetConnected
          ? Strings.genericErrorMsg
          : Strings.genericNoInternetMsg,
      onRetry: () => _bloc(context).add(RefreshRequested()),
    );
  }

  SplashBloc _bloc(BuildContext context) {
    return BlocProvider.of<SplashBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('SplashScreen');
