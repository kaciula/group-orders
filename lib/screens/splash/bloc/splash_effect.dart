import 'package:freezed_annotation/freezed_annotation.dart';

part 'splash_effect.freezed.dart';

@freezed
class SplashEffect with _$SplashEffect {
  factory SplashEffect.goToConnect() = GoToConnect;

  factory SplashEffect.goToHome() = GoToHome;
}
