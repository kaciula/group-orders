// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'splash_effect.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SplashEffect {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToConnect,
    required TResult Function() goToHome,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function()? goToHome,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function()? goToHome,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToConnect value) goToConnect,
    required TResult Function(GoToHome value) goToHome,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(GoToHome value)? goToHome,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(GoToHome value)? goToHome,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SplashEffectCopyWith<$Res> {
  factory $SplashEffectCopyWith(
          SplashEffect value, $Res Function(SplashEffect) then) =
      _$SplashEffectCopyWithImpl<$Res>;
}

/// @nodoc
class _$SplashEffectCopyWithImpl<$Res> implements $SplashEffectCopyWith<$Res> {
  _$SplashEffectCopyWithImpl(this._value, this._then);

  final SplashEffect _value;
  // ignore: unused_field
  final $Res Function(SplashEffect) _then;
}

/// @nodoc
abstract class _$$GoToConnectCopyWith<$Res> {
  factory _$$GoToConnectCopyWith(
          _$GoToConnect value, $Res Function(_$GoToConnect) then) =
      __$$GoToConnectCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GoToConnectCopyWithImpl<$Res> extends _$SplashEffectCopyWithImpl<$Res>
    implements _$$GoToConnectCopyWith<$Res> {
  __$$GoToConnectCopyWithImpl(
      _$GoToConnect _value, $Res Function(_$GoToConnect) _then)
      : super(_value, (v) => _then(v as _$GoToConnect));

  @override
  _$GoToConnect get _value => super._value as _$GoToConnect;
}

/// @nodoc

class _$GoToConnect implements GoToConnect {
  _$GoToConnect();

  @override
  String toString() {
    return 'SplashEffect.goToConnect()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GoToConnect);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToConnect,
    required TResult Function() goToHome,
  }) {
    return goToConnect();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function()? goToHome,
  }) {
    return goToConnect?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function()? goToHome,
    required TResult orElse(),
  }) {
    if (goToConnect != null) {
      return goToConnect();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToConnect value) goToConnect,
    required TResult Function(GoToHome value) goToHome,
  }) {
    return goToConnect(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(GoToHome value)? goToHome,
  }) {
    return goToConnect?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(GoToHome value)? goToHome,
    required TResult orElse(),
  }) {
    if (goToConnect != null) {
      return goToConnect(this);
    }
    return orElse();
  }
}

abstract class GoToConnect implements SplashEffect {
  factory GoToConnect() = _$GoToConnect;
}

/// @nodoc
abstract class _$$GoToHomeCopyWith<$Res> {
  factory _$$GoToHomeCopyWith(
          _$GoToHome value, $Res Function(_$GoToHome) then) =
      __$$GoToHomeCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GoToHomeCopyWithImpl<$Res> extends _$SplashEffectCopyWithImpl<$Res>
    implements _$$GoToHomeCopyWith<$Res> {
  __$$GoToHomeCopyWithImpl(_$GoToHome _value, $Res Function(_$GoToHome) _then)
      : super(_value, (v) => _then(v as _$GoToHome));

  @override
  _$GoToHome get _value => super._value as _$GoToHome;
}

/// @nodoc

class _$GoToHome implements GoToHome {
  _$GoToHome();

  @override
  String toString() {
    return 'SplashEffect.goToHome()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GoToHome);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToConnect,
    required TResult Function() goToHome,
  }) {
    return goToHome();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function()? goToHome,
  }) {
    return goToHome?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function()? goToHome,
    required TResult orElse(),
  }) {
    if (goToHome != null) {
      return goToHome();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToConnect value) goToConnect,
    required TResult Function(GoToHome value) goToHome,
  }) {
    return goToHome(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(GoToHome value)? goToHome,
  }) {
    return goToHome?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(GoToHome value)? goToHome,
    required TResult orElse(),
  }) {
    if (goToHome != null) {
      return goToHome(this);
    }
    return orElse();
  }
}

abstract class GoToHome implements SplashEffect {
  factory GoToHome() = _$GoToHome;
}
