import 'package:freezed_annotation/freezed_annotation.dart';

part 'splash_event.freezed.dart';

@freezed
class SplashEvent with _$SplashEvent {
  factory SplashEvent.appStarted() = AppStarted;

  factory SplashEvent.refreshRequested() = RefreshRequested;
}
