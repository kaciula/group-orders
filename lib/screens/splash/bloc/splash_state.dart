import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/screens/splash/bloc/splash_effect.dart';
import 'package:orders/screens/utils/refresh_state.dart';

part 'splash_state.freezed.dart';

@freezed
class SplashState with _$SplashState {
  factory SplashState({
    required RefreshState refreshState,
    SplashEffect? effect,
  }) = _SplashState;

  factory SplashState.initial() {
    return SplashState(refreshState: RefreshState.inProgress());
  }

  static bool buildWhen(SplashState previous, SplashState current) {
    return previous.refreshState != current.refreshState;
  }

  static bool listenWhen(SplashState previous, SplashState current) {
    return current.effect != null;
  }
}
