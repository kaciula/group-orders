import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_locale.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/data/local/misc/dynamic_links.dart';
import 'package:orders/data/local/misc/file_storage_service.dart';
import 'package:orders/data/local/store/app_info_provider.dart';
import 'package:orders/data/local/store/preferences.dart';
import 'package:orders/data/remote/push_messaging.dart';
import 'package:orders/screens/splash/bloc/splash_effect.dart';
import 'package:orders/screens/splash/bloc/splash_event.dart';
import 'package:orders/screens/splash/bloc/splash_state.dart';
import 'package:orders/screens/utils/refresh_state.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/set_up_on_start.dart';
import 'package:package_info_plus/package_info_plus.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc(this.dataBloc) : super(SplashState.initial());

  final DataBloc dataBloc;

  final Preferences _preferences = getIt<Preferences>();
  final AppInfoProvider _appInfoProvider = getIt<AppInfoProvider>();
  final SetUpOnStart _setUpOnStart = getIt<SetUpOnStart>();
  final DynamicLinks _dynamicLinks = getIt<DynamicLinks>();
  final PushMessaging _pushMessaging = getIt<PushMessaging>();
  final FileStorageService _fileStorageService = getIt<FileStorageService>();

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is AppStarted) {
      yield state.copyWith(refreshState: RefreshState.inProgress());
      await _onAppStart();
      yield* _decideWhatToDo();
    }
    if (event is RefreshRequested) {
      yield state.copyWith(refreshState: RefreshState.inProgress());
      yield* _decideWhatToDo();
    }
  }

  Stream<SplashState> _decideWhatToDo() async* {
    final SetUpOnStartResult result = await _setUpOnStart();
    if (result is SetUpOnStartSuccess) {
      yield* _withEffect(state, result.isSignedIn ? GoToHome() : GoToConnect());
    } else {
      yield state.copyWith(refreshState: RefreshState.error());
    }
  }

  Future<void> _onAppStart() async {
    await AppLocale.init();
    await Hive.initFlutter();
    await _preferences.init();
    await _appInfoProvider.init();
    await _fileStorageService.init();
    if (!kIsWeb) {
      await _dynamicLinks.init();
      await _pushMessaging.init();
    }

    final AppInfo appInfo = _appInfoProvider.get();

    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    dataBloc.add(PackageInfoRefreshed(
      versionName: packageInfo.version,
      packageName: packageInfo.packageName,
    ));

    final int currentVersionCode = int.parse(packageInfo.buildNumber);
    _logger.fine('version code = $currentVersionCode');
    if (appInfo.isFirstTime) {
      _logger.info('First time running the app');
      _appInfoProvider.save(
          isFirstTime: false, versionCode: currentVersionCode);
    } else {
      if (appInfo.versionCode < currentVersionCode) {
        _logger.info(
            'Old version code ${appInfo.versionCode} replaced with $currentVersionCode');
        if (appInfo.versionCode < 8) {
          await _appInfoProvider.saveMigrateMessagingToken(true);
        }
        if (appInfo.versionCode < 23) {
          await _appInfoProvider.saveRefreshUser(true);
        }
        await _appInfoProvider.save(
            isFirstTime: false, versionCode: currentVersionCode);
      } else {
        _logger.info('Just a basic cold start');
      }
    }
  }

  Stream<SplashState> _withEffect(
      SplashState state, SplashEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('SplashBloc');
