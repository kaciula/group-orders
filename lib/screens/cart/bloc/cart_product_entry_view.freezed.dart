// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cart_product_entry_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CartProductEntryView {
  CartProduct get cartProduct => throw _privateConstructorUsedError;
  double get total => throw _privateConstructorUsedError;
  double get discountedTotal => throw _privateConstructorUsedError;
  String get participantName => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CartProductEntryViewCopyWith<CartProductEntryView> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CartProductEntryViewCopyWith<$Res> {
  factory $CartProductEntryViewCopyWith(CartProductEntryView value,
          $Res Function(CartProductEntryView) then) =
      _$CartProductEntryViewCopyWithImpl<$Res>;
  $Res call(
      {CartProduct cartProduct,
      double total,
      double discountedTotal,
      String participantName});
}

/// @nodoc
class _$CartProductEntryViewCopyWithImpl<$Res>
    implements $CartProductEntryViewCopyWith<$Res> {
  _$CartProductEntryViewCopyWithImpl(this._value, this._then);

  final CartProductEntryView _value;
  // ignore: unused_field
  final $Res Function(CartProductEntryView) _then;

  @override
  $Res call({
    Object? cartProduct = freezed,
    Object? total = freezed,
    Object? discountedTotal = freezed,
    Object? participantName = freezed,
  }) {
    return _then(_value.copyWith(
      cartProduct: cartProduct == freezed
          ? _value.cartProduct
          : cartProduct // ignore: cast_nullable_to_non_nullable
              as CartProduct,
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as double,
      discountedTotal: discountedTotal == freezed
          ? _value.discountedTotal
          : discountedTotal // ignore: cast_nullable_to_non_nullable
              as double,
      participantName: participantName == freezed
          ? _value.participantName
          : participantName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_CartProductEntryViewCopyWith<$Res>
    implements $CartProductEntryViewCopyWith<$Res> {
  factory _$$_CartProductEntryViewCopyWith(_$_CartProductEntryView value,
          $Res Function(_$_CartProductEntryView) then) =
      __$$_CartProductEntryViewCopyWithImpl<$Res>;
  @override
  $Res call(
      {CartProduct cartProduct,
      double total,
      double discountedTotal,
      String participantName});
}

/// @nodoc
class __$$_CartProductEntryViewCopyWithImpl<$Res>
    extends _$CartProductEntryViewCopyWithImpl<$Res>
    implements _$$_CartProductEntryViewCopyWith<$Res> {
  __$$_CartProductEntryViewCopyWithImpl(_$_CartProductEntryView _value,
      $Res Function(_$_CartProductEntryView) _then)
      : super(_value, (v) => _then(v as _$_CartProductEntryView));

  @override
  _$_CartProductEntryView get _value => super._value as _$_CartProductEntryView;

  @override
  $Res call({
    Object? cartProduct = freezed,
    Object? total = freezed,
    Object? discountedTotal = freezed,
    Object? participantName = freezed,
  }) {
    return _then(_$_CartProductEntryView(
      cartProduct: cartProduct == freezed
          ? _value.cartProduct
          : cartProduct // ignore: cast_nullable_to_non_nullable
              as CartProduct,
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as double,
      discountedTotal: discountedTotal == freezed
          ? _value.discountedTotal
          : discountedTotal // ignore: cast_nullable_to_non_nullable
              as double,
      participantName: participantName == freezed
          ? _value.participantName
          : participantName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_CartProductEntryView implements _CartProductEntryView {
  _$_CartProductEntryView(
      {required this.cartProduct,
      required this.total,
      required this.discountedTotal,
      required this.participantName});

  @override
  final CartProduct cartProduct;
  @override
  final double total;
  @override
  final double discountedTotal;
  @override
  final String participantName;

  @override
  String toString() {
    return 'CartProductEntryView(cartProduct: $cartProduct, total: $total, discountedTotal: $discountedTotal, participantName: $participantName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CartProductEntryView &&
            const DeepCollectionEquality()
                .equals(other.cartProduct, cartProduct) &&
            const DeepCollectionEquality().equals(other.total, total) &&
            const DeepCollectionEquality()
                .equals(other.discountedTotal, discountedTotal) &&
            const DeepCollectionEquality()
                .equals(other.participantName, participantName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(cartProduct),
      const DeepCollectionEquality().hash(total),
      const DeepCollectionEquality().hash(discountedTotal),
      const DeepCollectionEquality().hash(participantName));

  @JsonKey(ignore: true)
  @override
  _$$_CartProductEntryViewCopyWith<_$_CartProductEntryView> get copyWith =>
      __$$_CartProductEntryViewCopyWithImpl<_$_CartProductEntryView>(
          this, _$identity);
}

abstract class _CartProductEntryView implements CartProductEntryView {
  factory _CartProductEntryView(
      {required final CartProduct cartProduct,
      required final double total,
      required final double discountedTotal,
      required final String participantName}) = _$_CartProductEntryView;

  @override
  CartProduct get cartProduct;
  @override
  double get total;
  @override
  double get discountedTotal;
  @override
  String get participantName;
  @override
  @JsonKey(ignore: true)
  _$$_CartProductEntryViewCopyWith<_$_CartProductEntryView> get copyWith =>
      throw _privateConstructorUsedError;
}
