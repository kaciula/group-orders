import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/participant.dart';

part 'participant_item.freezed.dart';

@freezed
class ParticipantItem with _$ParticipantItem {
  factory ParticipantItem({
    required Participant participant,
    required bool isActive,
  }) = _ParticipantItem;

  ParticipantItem._();

  factory ParticipantItem.all() => ParticipantItem(
        participant: _dummyParticipant,
        isActive: true,
      );

  static final Participant _dummyParticipant = Participant(
    userId: _dummyParticipantId,
    displayName: '',
    phoneNumber: 'null',
    delivered: false,
    confirmedDelivered: false,
    paid: false,
    confirmedPaid: false,
    paidDetails: '',
  );

  bool get isAll => participant.userId == _dummyParticipantId;

  String get userId => participant.userId;

  String get displayName => participant.displayName;
}

const String _dummyParticipantId = '';
