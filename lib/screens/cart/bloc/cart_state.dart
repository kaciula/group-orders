import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/cart/bloc/cart_effect.dart';
import 'package:orders/screens/cart/bloc/cart_view.dart';
import 'package:orders/screens/cart/bloc/participant_item.dart';

part 'cart_state.freezed.dart';

@freezed
class CartState with _$CartState {
  factory CartState({
    required bool inProgressAction,
    required Order order,
    required User user,
    required Participant owner,
    required KtList<ParticipantItem> participants,
    required KtList<Product> products,
    required KtList<CartEntry> cart,
    required CartView cartView,
    CartEffect? effect,
  }) = _CartState;

  CartState._();

  factory CartState.initial(
    Order order,
    User user,
    Participant owner,
    KtList<ParticipantItem> participants,
    KtList<Product> products,
    KtList<CartEntry> cart,
    CartView cartView,
  ) {
    return CartState(
      inProgressAction: false,
      order: order,
      user: user,
      owner: owner,
      participants: participants,
      products: products,
      cart: cart,
      cartView: cartView,
    );
  }

  bool get isOwner => owner.userId == user.id;

  bool get canRemoveFromCart =>
      isOwner || order.status == OrderStatus.inProgress;

  bool get canEditEntry => isOwner && order.status == OrderStatus.arrived;

  bool get canFilterParticipants => isOwner || order.hasFeatureSeeParticipants;

  static bool buildWhen(CartState previous, CartState current) {
    return previous.inProgressAction != current.inProgressAction ||
        previous.order != current.order ||
        previous.user != current.user ||
        previous.owner != current.owner ||
        previous.participants != current.participants ||
        previous.products != current.products ||
        previous.cart != current.cart ||
        previous.cartView != current.cartView;
  }

  static bool listenWhen(CartState previous, CartState current) {
    return current.effect != null;
  }
}
