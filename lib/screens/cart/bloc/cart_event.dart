import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/screens/cart/bloc/participant_item.dart';

part 'cart_event.freezed.dart';

@freezed
class CartEvent with _$CartEvent {
  factory CartEvent.linkRequested(String url) = LinkRequested;

  factory CartEvent.modifyEntryRequested({
    required CartProduct cartProduct,
    required int numItems,
    double? modifiedPrice,
  }) = ModifyEntryRequested;

  factory CartEvent.removeEntryRequested(CartEntry cartEntry) =
      RemoveEntryRequested;

  factory CartEvent.participantRequested(ParticipantItem participantItem) =
      ParticipantRequested;

  factory CartEvent.markParticipantRequested({
    required ParticipantItem participantItem,
    bool? delivered,
    bool? confirmedDelivered,
    bool? paid,
    bool? confirmedPaid,
    String? paidDetails,
  }) = MarkParticipantRequested;

  factory CartEvent.callParticipantRequested(
      {required Participant participant}) = CallParticipantRequested;

  factory CartEvent.talkOnWhatsAppRequested(
      {required Participant participant}) = TalkOnWhatsAppRequested;

  factory CartEvent.exportRequested() = ExportRequested;
}
