import 'package:bloc/bloc.dart';
import 'package:csv/csv.dart';
import 'package:kt_dart/collection.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/local/misc/file_storage_service.dart';
import 'package:orders/data/local/misc/launcher.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';
import 'package:orders/model/validators.dart';
import 'package:orders/screens/cart/bloc/cart_effect.dart';
import 'package:orders/screens/cart/bloc/cart_event.dart';
import 'package:orders/screens/cart/bloc/cart_product_entry_view.dart';
import 'package:orders/screens/cart/bloc/cart_product_view.dart';
import 'package:orders/screens/cart/bloc/cart_state.dart';
import 'package:orders/screens/cart/bloc/cart_view.dart';
import 'package:orders/screens/cart/bloc/participant_item.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart' as order_event;
import 'package:orders/screens/order/bloc/order_state.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/remove_from_cart.dart';
import 'package:orders/usecases/use_case_results.dart';
import 'package:universal_io/io.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc(this.dataBloc, this.orderBloc)
      : super(
          CartState.initial(
            orderBloc.state.order,
            orderBloc.state.user,
            orderBloc.state.owner!,
            _participantsSelector(orderBloc.state),
            orderBloc.state.products!,
            orderBloc.state.cart!,
            _buildCartView(
              orderBloc.state.products!,
              orderBloc.state.order.currency,
              orderBloc.state.cart!,
              orderBloc.state.isOwner
                  ? ParticipantItem.all()
                  : _participantsSelector(orderBloc.state).first(
                      (ParticipantItem it) =>
                          it.userId == orderBloc.state.user.id),
              _participantsSelector(orderBloc.state),
            ),
          ),
        );

  final DataBloc dataBloc;
  final OrderBloc orderBloc;

  final RemoveFromCart _removeFromCart = getIt<RemoveFromCart>();
  final Launcher _launcher = getIt<Launcher>();
  final DataRemoteStore _dataRemoteStore = getIt<DataRemoteStore>();
  final FileStorageService _fileStorageService = getIt<FileStorageService>();

  @override
  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is LinkRequested) {
      _launcher.openExternalUrl(event.url);
    }
    if (event is ModifyEntryRequested) {
      yield state.copyWith(inProgressAction: true);
      final CartProduct cartProduct = event.cartProduct;
      final CartProduct modifiedEntry = cartProduct.copyWith(
        numItems: event.numItems,
        modifiedPrice: event.modifiedPrice ?? cartProduct.modifiedPrice,
      );
      final StoreResult result = await _dataRemoteStore
          .saveCartItem(modifiedEntry, orderId: state.order.id);
      if (result is StoreSuccess) {
        final String productId = cartProduct.productId;
        final int numItemsToAdd = cartProduct.numItems - event.numItems;
        final KtList<Product> modifiedProducts = state.products
            .map((Product it) {
              final Stock stock = it.stock;
              return cartProduct.productId == it.id && stock is Limited
                  ? it.copyWith(
                      stock: Limited(
                          numItemsAvailable:
                              stock.numItemsAvailable + numItemsToAdd))
                  : it;
            })
            .toList()
            .sortedBy(_sortProducts);
        final KtList<CartEntry> modifiedCart = state.cart.map(
            (CartEntry entry) =>
                entry.id == modifiedEntry.id ? modifiedEntry : entry);
        final CartView modifiedCartView = _buildCartView(
            modifiedProducts,
            state.order.currency,
            modifiedCart,
            state.cartView.participantItem,
            state.participants);
        yield* _withEffect(
          state.copyWith(
            inProgressAction: false,
            products: modifiedProducts,
            cart: modifiedCart,
            cartView: modifiedCartView,
          ),
          ShowInfoSnackBar(Strings.cartEditSuccess),
        );

        orderBloc.add(
          order_event.CartUpdated(
            modifiedCart,
            modifiedProductId: productId,
            numItemsToAdd: numItemsToAdd,
          ),
        );
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.cartEditErrorMsg));
      }
    }
    if (event is RemoveEntryRequested) {
      yield state.copyWith(inProgressAction: true);

      final CartEntry cartEntry = event.cartEntry;
      final UseCaseResult result =
          await _removeFromCart(orderId: state.order.id, cartEntry: cartEntry);
      if (result is UseCaseSuccess) {
        String? productId;
        int? numItemsToAdd;
        KtList<Product> modifiedProducts = state.products;
        if (cartEntry is CartProduct) {
          productId = cartEntry.productId;
          numItemsToAdd = cartEntry.numItems;
          modifiedProducts = state.products
              .map((Product it) {
                final Stock stock = it.stock;
                return productId == it.id && stock is Limited
                    ? it.copyWith(
                        stock: Limited(
                            numItemsAvailable:
                                stock.numItemsAvailable + numItemsToAdd!))
                    : it;
              })
              .toList()
              .sortedBy(_sortProducts);
        }

        final KtList<CartEntry> modifiedCart =
            state.cart.minusElement(cartEntry);
        final CartView modifiedCartView = _buildCartView(
          modifiedProducts,
          state.order.currency,
          modifiedCart,
          state.cartView.participantItem,
          state.participants,
        );
        yield* _withEffect(
          state.copyWith(
            inProgressAction: false,
            products: modifiedProducts,
            cart: modifiedCart,
            cartView: modifiedCartView,
            participants:
                _updateParticipantsSelector(state.participants, modifiedCart),
          ),
          ShowInfoSnackBar(Strings.cartRemoveSuccess),
        );

        orderBloc.add(
          order_event.CartUpdated(
            modifiedCart,
            modifiedProductId: productId,
            numItemsToAdd: numItemsToAdd,
          ),
        );
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.cartRemoveErrorMsg));
      }
    }
    if (event is ParticipantRequested) {
      yield state.copyWith(
          cartView: _buildCartView(state.products, state.order.currency,
              state.cart, event.participantItem, state.participants));
    }
    if (event is MarkParticipantRequested) {
      yield state.copyWith(inProgressAction: true);

      final bool? delivered =
          (event.delivered ?? event.confirmedDelivered ?? false) ? true : null;
      final bool? paid =
          (event.paid ?? event.confirmedPaid ?? false) ? true : null;
      final StoreResult result = await _dataRemoteStore.updateParticipantFields(
        orderId: state.order.id,
        participantId: event.participantItem.userId,
        delivered: delivered,
        confirmedDelivered: event.confirmedDelivered,
        paid: paid,
        confirmedPaid: event.confirmedPaid,
        paidDetails: event.paidDetails,
      );
      if (result is StoreSuccess) {
        final Participant oldParticipant = event.participantItem.participant;
        final Participant participant = oldParticipant.copyWith(
          delivered: delivered ?? oldParticipant.delivered,
          confirmedDelivered:
              event.confirmedDelivered ?? oldParticipant.confirmedDelivered,
          paid: paid ?? oldParticipant.paid,
          confirmedPaid: event.confirmedPaid ?? oldParticipant.confirmedPaid,
          paidDetails: event.paidDetails ?? oldParticipant.paidDetails,
        );
        final ParticipantItem participantItem =
            event.participantItem.copyWith(participant: participant);
        final KtList<ParticipantItem> participants = state.participants.map(
            (ParticipantItem it) =>
                it.userId == participantItem.userId ? participantItem : it);
        yield state.copyWith(
          inProgressAction: false,
          participants: participants,
          cartView: state.cartView.copyWith(participantItem: participantItem),
        );
        orderBloc.add(order_event.ParticipantChanged(
            participant: participantItem.participant));
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.genericErrorSnackBarMsg));
      }
    }
    if (event is CallParticipantRequested) {
      _launcher.dial(phoneNum: event.participant.phoneNumber);
    }
    if (event is TalkOnWhatsAppRequested) {
      _launcher.launchWhatsApp(phoneNumber: event.participant.phoneNumber);
    }
    if (event is ExportRequested) {
      // LATER: Show dialog to first freeze the order and then export to make sure
      _export();
    }
  }

  static CartView _buildCartView(
    KtList<Product> products,
    String currency,
    KtList<CartEntry> cart,
    ParticipantItem participantItem,
    KtList<ParticipantItem> participants,
  ) {
    final KtMutableMap<Product, CartProductView> map =
        KtMutableMap<Product, CartProductView>.empty();
    final KtMutableSet<String> activeParticipants =
        KtMutableSet<String>.empty();
    double total = 0;
    final Discount? discount =
        cart.filter((CartEntry it) => it is Discount).getOrNull(0) as Discount?;
    final Transport? transport = cart
        .filter((CartEntry it) => it is Transport)
        .getOrNull(0) as Transport?;

    for (CartEntry entry in cart.iter) {
      if (entry is CartProduct) {
        if (participantItem.isAll ||
            entry.participantId == participantItem.userId) {
          final Product product =
              products.first((Product it) => it.id == entry.productId);
          final CartProductView productView = map[product] ??
              CartProductView(
                product: product,
                entries: const KtList<CartProductEntryView>.empty(),
                numItems: 0,
                total: 0,
                discountedTotal: 0,
              );

          final double cost =
              entry.modifiedPrice ?? product.price * entry.numItems;
          final double discountedCost =
              discount != null ? (1 - discount.percent) * cost : cost;
          total += cost;

          final CartProductEntryView entryView = CartProductEntryView(
              cartProduct: entry,
              total: cost,
              discountedTotal: discountedCost,
              participantName: participants
                  .first(
                      (ParticipantItem it) => it.userId == entry.participantId)
                  .displayName);

          map[product] = productView.copyWith(
            entries: productView.entries.toMutableList()..add(entryView),
            numItems: productView.numItems + entry.numItems,
            total: productView.total + cost,
            discountedTotal: productView.discountedTotal + discountedCost,
          );
        }
        activeParticipants.add(entry.participantId);
      }
    }

    int numTotalItems = 0;
    for (KtPair<Product, CartProductView> pair in map.toList().iter) {
      map[pair.first] = pair.second.copyWith(
        entries: pair.second.entries
            .sortedBy((CartProductEntryView it) => it.participantName),
      );
      numTotalItems += pair.second.numItems;
    }

    double discountedTotal = total;
    final List<CartEntry> extraEntries = <CartEntry>[];
    if (discount != null) {
      discountedTotal = (1 - discount.percent) * total;
      extraEntries.add(discount);
    }
    if (transport != null) {
      if (participantItem.isAll) {
        total += transport.cost;
        discountedTotal += transport.cost;
      } else {
        final int numActiveParticipants = activeParticipants.size;
        total += numActiveParticipants != 0
            ? transport.cost / activeParticipants.size
            : 0;
        discountedTotal += numActiveParticipants != 0
            ? transport.cost / activeParticipants.size
            : 0;
      }
      extraEntries.add(transport);
    }
    return CartView(
      products: map
          .toList()
          .sortedBy((KtPair<Product, CartProductView> it) => it.first.title),
      extraEntries: extraEntries.toImmutableList(),
      participantItem: participantItem,
      total: total,
      discountedTotal: discountedTotal,
      currency: currency,
      numActiveParticipants: activeParticipants.size,
      numTotalItems: numTotalItems,
    );
  }

  Future<void> _export() async {
    final List<dynamic> rows = <List<dynamic>>[];
    for (ParticipantItem participant in state.participants.iter) {
      if (participant.isActive) {
        _buildRows(participant, rows);
      }
    }

    final String csvString =
        ListToCsvConverter().convert(rows as List<List<dynamic>>);
    final File file = await _fileStorageService.saveFileFromString(csvString,
        fileName: 'Comanda ${state.order.title}.csv');

    await _launcher.shareFile(file);
  }

  void _buildRows(ParticipantItem participant, List<dynamic> rows) {
    final CartView cartView = _buildCartView(state.products,
        state.order.currency, state.cart, participant, state.participants);
    List<dynamic> row = <dynamic>[
      participant.isAll ? 'Total' : participant.displayName
    ];
    rows.add(row);
    row = <dynamic>[];
    rows.add(row);
    for (KtPair<Product, CartProductView> pair in cartView.products.iter) {
      final Product product = pair.first;
      final CartProductView cartProductView = pair.second;
      row = <dynamic>[
        product.title,
        cartProductView.numItems,
        cartProductView.total
      ];
      rows.add(row);
    }
    row = <dynamic>[];
    rows.add(row);
    row = <dynamic>[];
    rows.add(row);
    row = <dynamic>[];
    rows.add(row);
  }

  String? priceValidator(String? value) {
    return Validators.price(
      value: value,
      invalidMsg: Strings.editProductInvalidPrice,
    );
  }

  static KtList<ParticipantItem> _participantsSelector(OrderState orderState) {
    final KtList<ParticipantItem> sortedParticipants = orderState.participants!
        .filterNot((Participant it) =>
            orderState.order.isProvider &&
            orderState.isOwner &&
            it.userId == orderState.owner?.userId)
        .map((Participant it) {
      final bool isActive = orderState.cart!.any((CartEntry entry) =>
          entry is CartProduct && entry.participantId == it.userId);
      return ParticipantItem(participant: it, isActive: isActive);
    }).sortedWith(_sortParticipantItems);

    return KtList<ParticipantItem>.from(
        <ParticipantItem>[ParticipantItem.all()]).plus(sortedParticipants);
  }

  static KtList<ParticipantItem> _updateParticipantsSelector(
      KtList<ParticipantItem> items, KtList<CartEntry> cart) {
    return items.map((ParticipantItem it) {
      final bool isActive = it.isAll ||
          cart.any((CartEntry entry) =>
              entry is CartProduct && entry.participantId == it.userId);
      return it.copyWith(isActive: isActive);
    }).sortedWith(_sortParticipantItems);
  }

  static final Comparator<ParticipantItem> _sortParticipantItems =
      (ParticipantItem a, ParticipantItem b) {
    if (a.isAll) {
      return -1;
    }
    if (b.isAll) {
      return 1;
    }
    if (a.isActive && !b.isActive) {
      return -1;
    }
    if (!a.isActive && b.isActive) {
      return 1;
    }
    return a.displayName.compareTo(b.displayName);
  };

  Stream<CartState> _withEffect(CartState state, CartEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

Comparable<dynamic> _sortProducts(Product it) => it.title;

// ignore: unused_element
final Logger _logger = Logger('CartBloc');
