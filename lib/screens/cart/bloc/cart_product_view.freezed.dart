// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cart_product_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CartProductView {
  Product get product => throw _privateConstructorUsedError;
  KtList<CartProductEntryView> get entries =>
      throw _privateConstructorUsedError;
  int get numItems => throw _privateConstructorUsedError;
  double get total => throw _privateConstructorUsedError;
  double get discountedTotal => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CartProductViewCopyWith<CartProductView> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CartProductViewCopyWith<$Res> {
  factory $CartProductViewCopyWith(
          CartProductView value, $Res Function(CartProductView) then) =
      _$CartProductViewCopyWithImpl<$Res>;
  $Res call(
      {Product product,
      KtList<CartProductEntryView> entries,
      int numItems,
      double total,
      double discountedTotal});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class _$CartProductViewCopyWithImpl<$Res>
    implements $CartProductViewCopyWith<$Res> {
  _$CartProductViewCopyWithImpl(this._value, this._then);

  final CartProductView _value;
  // ignore: unused_field
  final $Res Function(CartProductView) _then;

  @override
  $Res call({
    Object? product = freezed,
    Object? entries = freezed,
    Object? numItems = freezed,
    Object? total = freezed,
    Object? discountedTotal = freezed,
  }) {
    return _then(_value.copyWith(
      product: product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
      entries: entries == freezed
          ? _value.entries
          : entries // ignore: cast_nullable_to_non_nullable
              as KtList<CartProductEntryView>,
      numItems: numItems == freezed
          ? _value.numItems
          : numItems // ignore: cast_nullable_to_non_nullable
              as int,
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as double,
      discountedTotal: discountedTotal == freezed
          ? _value.discountedTotal
          : discountedTotal // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc
abstract class _$$_CartProductViewCopyWith<$Res>
    implements $CartProductViewCopyWith<$Res> {
  factory _$$_CartProductViewCopyWith(
          _$_CartProductView value, $Res Function(_$_CartProductView) then) =
      __$$_CartProductViewCopyWithImpl<$Res>;
  @override
  $Res call(
      {Product product,
      KtList<CartProductEntryView> entries,
      int numItems,
      double total,
      double discountedTotal});

  @override
  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$_CartProductViewCopyWithImpl<$Res>
    extends _$CartProductViewCopyWithImpl<$Res>
    implements _$$_CartProductViewCopyWith<$Res> {
  __$$_CartProductViewCopyWithImpl(
      _$_CartProductView _value, $Res Function(_$_CartProductView) _then)
      : super(_value, (v) => _then(v as _$_CartProductView));

  @override
  _$_CartProductView get _value => super._value as _$_CartProductView;

  @override
  $Res call({
    Object? product = freezed,
    Object? entries = freezed,
    Object? numItems = freezed,
    Object? total = freezed,
    Object? discountedTotal = freezed,
  }) {
    return _then(_$_CartProductView(
      product: product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
      entries: entries == freezed
          ? _value.entries
          : entries // ignore: cast_nullable_to_non_nullable
              as KtList<CartProductEntryView>,
      numItems: numItems == freezed
          ? _value.numItems
          : numItems // ignore: cast_nullable_to_non_nullable
              as int,
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as double,
      discountedTotal: discountedTotal == freezed
          ? _value.discountedTotal
          : discountedTotal // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$_CartProductView implements _CartProductView {
  _$_CartProductView(
      {required this.product,
      required this.entries,
      required this.numItems,
      required this.total,
      required this.discountedTotal});

  @override
  final Product product;
  @override
  final KtList<CartProductEntryView> entries;
  @override
  final int numItems;
  @override
  final double total;
  @override
  final double discountedTotal;

  @override
  String toString() {
    return 'CartProductView(product: $product, entries: $entries, numItems: $numItems, total: $total, discountedTotal: $discountedTotal)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CartProductView &&
            const DeepCollectionEquality().equals(other.product, product) &&
            const DeepCollectionEquality().equals(other.entries, entries) &&
            const DeepCollectionEquality().equals(other.numItems, numItems) &&
            const DeepCollectionEquality().equals(other.total, total) &&
            const DeepCollectionEquality()
                .equals(other.discountedTotal, discountedTotal));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(product),
      const DeepCollectionEquality().hash(entries),
      const DeepCollectionEquality().hash(numItems),
      const DeepCollectionEquality().hash(total),
      const DeepCollectionEquality().hash(discountedTotal));

  @JsonKey(ignore: true)
  @override
  _$$_CartProductViewCopyWith<_$_CartProductView> get copyWith =>
      __$$_CartProductViewCopyWithImpl<_$_CartProductView>(this, _$identity);
}

abstract class _CartProductView implements CartProductView {
  factory _CartProductView(
      {required final Product product,
      required final KtList<CartProductEntryView> entries,
      required final int numItems,
      required final double total,
      required final double discountedTotal}) = _$_CartProductView;

  @override
  Product get product;
  @override
  KtList<CartProductEntryView> get entries;
  @override
  int get numItems;
  @override
  double get total;
  @override
  double get discountedTotal;
  @override
  @JsonKey(ignore: true)
  _$$_CartProductViewCopyWith<_$_CartProductView> get copyWith =>
      throw _privateConstructorUsedError;
}
