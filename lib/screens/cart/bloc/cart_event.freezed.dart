// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cart_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CartEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CartEventCopyWith<$Res> {
  factory $CartEventCopyWith(CartEvent value, $Res Function(CartEvent) then) =
      _$CartEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$CartEventCopyWithImpl<$Res> implements $CartEventCopyWith<$Res> {
  _$CartEventCopyWithImpl(this._value, this._then);

  final CartEvent _value;
  // ignore: unused_field
  final $Res Function(CartEvent) _then;
}

/// @nodoc
abstract class _$$LinkRequestedCopyWith<$Res> {
  factory _$$LinkRequestedCopyWith(
          _$LinkRequested value, $Res Function(_$LinkRequested) then) =
      __$$LinkRequestedCopyWithImpl<$Res>;
  $Res call({String url});
}

/// @nodoc
class __$$LinkRequestedCopyWithImpl<$Res> extends _$CartEventCopyWithImpl<$Res>
    implements _$$LinkRequestedCopyWith<$Res> {
  __$$LinkRequestedCopyWithImpl(
      _$LinkRequested _value, $Res Function(_$LinkRequested) _then)
      : super(_value, (v) => _then(v as _$LinkRequested));

  @override
  _$LinkRequested get _value => super._value as _$LinkRequested;

  @override
  $Res call({
    Object? url = freezed,
  }) {
    return _then(_$LinkRequested(
      url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LinkRequested implements LinkRequested {
  _$LinkRequested(this.url);

  @override
  final String url;

  @override
  String toString() {
    return 'CartEvent.linkRequested(url: $url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LinkRequested &&
            const DeepCollectionEquality().equals(other.url, url));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(url));

  @JsonKey(ignore: true)
  @override
  _$$LinkRequestedCopyWith<_$LinkRequested> get copyWith =>
      __$$LinkRequestedCopyWithImpl<_$LinkRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) {
    return linkRequested(url);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) {
    return linkRequested?.call(url);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) {
    if (linkRequested != null) {
      return linkRequested(url);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) {
    return linkRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) {
    return linkRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) {
    if (linkRequested != null) {
      return linkRequested(this);
    }
    return orElse();
  }
}

abstract class LinkRequested implements CartEvent {
  factory LinkRequested(final String url) = _$LinkRequested;

  String get url;
  @JsonKey(ignore: true)
  _$$LinkRequestedCopyWith<_$LinkRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ModifyEntryRequestedCopyWith<$Res> {
  factory _$$ModifyEntryRequestedCopyWith(_$ModifyEntryRequested value,
          $Res Function(_$ModifyEntryRequested) then) =
      __$$ModifyEntryRequestedCopyWithImpl<$Res>;
  $Res call({CartProduct cartProduct, int numItems, double? modifiedPrice});
}

/// @nodoc
class __$$ModifyEntryRequestedCopyWithImpl<$Res>
    extends _$CartEventCopyWithImpl<$Res>
    implements _$$ModifyEntryRequestedCopyWith<$Res> {
  __$$ModifyEntryRequestedCopyWithImpl(_$ModifyEntryRequested _value,
      $Res Function(_$ModifyEntryRequested) _then)
      : super(_value, (v) => _then(v as _$ModifyEntryRequested));

  @override
  _$ModifyEntryRequested get _value => super._value as _$ModifyEntryRequested;

  @override
  $Res call({
    Object? cartProduct = freezed,
    Object? numItems = freezed,
    Object? modifiedPrice = freezed,
  }) {
    return _then(_$ModifyEntryRequested(
      cartProduct: cartProduct == freezed
          ? _value.cartProduct
          : cartProduct // ignore: cast_nullable_to_non_nullable
              as CartProduct,
      numItems: numItems == freezed
          ? _value.numItems
          : numItems // ignore: cast_nullable_to_non_nullable
              as int,
      modifiedPrice: modifiedPrice == freezed
          ? _value.modifiedPrice
          : modifiedPrice // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc

class _$ModifyEntryRequested implements ModifyEntryRequested {
  _$ModifyEntryRequested(
      {required this.cartProduct, required this.numItems, this.modifiedPrice});

  @override
  final CartProduct cartProduct;
  @override
  final int numItems;
  @override
  final double? modifiedPrice;

  @override
  String toString() {
    return 'CartEvent.modifyEntryRequested(cartProduct: $cartProduct, numItems: $numItems, modifiedPrice: $modifiedPrice)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ModifyEntryRequested &&
            const DeepCollectionEquality()
                .equals(other.cartProduct, cartProduct) &&
            const DeepCollectionEquality().equals(other.numItems, numItems) &&
            const DeepCollectionEquality()
                .equals(other.modifiedPrice, modifiedPrice));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(cartProduct),
      const DeepCollectionEquality().hash(numItems),
      const DeepCollectionEquality().hash(modifiedPrice));

  @JsonKey(ignore: true)
  @override
  _$$ModifyEntryRequestedCopyWith<_$ModifyEntryRequested> get copyWith =>
      __$$ModifyEntryRequestedCopyWithImpl<_$ModifyEntryRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) {
    return modifyEntryRequested(cartProduct, numItems, modifiedPrice);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) {
    return modifyEntryRequested?.call(cartProduct, numItems, modifiedPrice);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) {
    if (modifyEntryRequested != null) {
      return modifyEntryRequested(cartProduct, numItems, modifiedPrice);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) {
    return modifyEntryRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) {
    return modifyEntryRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) {
    if (modifyEntryRequested != null) {
      return modifyEntryRequested(this);
    }
    return orElse();
  }
}

abstract class ModifyEntryRequested implements CartEvent {
  factory ModifyEntryRequested(
      {required final CartProduct cartProduct,
      required final int numItems,
      final double? modifiedPrice}) = _$ModifyEntryRequested;

  CartProduct get cartProduct;
  int get numItems;
  double? get modifiedPrice;
  @JsonKey(ignore: true)
  _$$ModifyEntryRequestedCopyWith<_$ModifyEntryRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RemoveEntryRequestedCopyWith<$Res> {
  factory _$$RemoveEntryRequestedCopyWith(_$RemoveEntryRequested value,
          $Res Function(_$RemoveEntryRequested) then) =
      __$$RemoveEntryRequestedCopyWithImpl<$Res>;
  $Res call({CartEntry cartEntry});

  $CartEntryCopyWith<$Res> get cartEntry;
}

/// @nodoc
class __$$RemoveEntryRequestedCopyWithImpl<$Res>
    extends _$CartEventCopyWithImpl<$Res>
    implements _$$RemoveEntryRequestedCopyWith<$Res> {
  __$$RemoveEntryRequestedCopyWithImpl(_$RemoveEntryRequested _value,
      $Res Function(_$RemoveEntryRequested) _then)
      : super(_value, (v) => _then(v as _$RemoveEntryRequested));

  @override
  _$RemoveEntryRequested get _value => super._value as _$RemoveEntryRequested;

  @override
  $Res call({
    Object? cartEntry = freezed,
  }) {
    return _then(_$RemoveEntryRequested(
      cartEntry == freezed
          ? _value.cartEntry
          : cartEntry // ignore: cast_nullable_to_non_nullable
              as CartEntry,
    ));
  }

  @override
  $CartEntryCopyWith<$Res> get cartEntry {
    return $CartEntryCopyWith<$Res>(_value.cartEntry, (value) {
      return _then(_value.copyWith(cartEntry: value));
    });
  }
}

/// @nodoc

class _$RemoveEntryRequested implements RemoveEntryRequested {
  _$RemoveEntryRequested(this.cartEntry);

  @override
  final CartEntry cartEntry;

  @override
  String toString() {
    return 'CartEvent.removeEntryRequested(cartEntry: $cartEntry)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RemoveEntryRequested &&
            const DeepCollectionEquality().equals(other.cartEntry, cartEntry));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(cartEntry));

  @JsonKey(ignore: true)
  @override
  _$$RemoveEntryRequestedCopyWith<_$RemoveEntryRequested> get copyWith =>
      __$$RemoveEntryRequestedCopyWithImpl<_$RemoveEntryRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) {
    return removeEntryRequested(cartEntry);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) {
    return removeEntryRequested?.call(cartEntry);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) {
    if (removeEntryRequested != null) {
      return removeEntryRequested(cartEntry);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) {
    return removeEntryRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) {
    return removeEntryRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) {
    if (removeEntryRequested != null) {
      return removeEntryRequested(this);
    }
    return orElse();
  }
}

abstract class RemoveEntryRequested implements CartEvent {
  factory RemoveEntryRequested(final CartEntry cartEntry) =
      _$RemoveEntryRequested;

  CartEntry get cartEntry;
  @JsonKey(ignore: true)
  _$$RemoveEntryRequestedCopyWith<_$RemoveEntryRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ParticipantRequestedCopyWith<$Res> {
  factory _$$ParticipantRequestedCopyWith(_$ParticipantRequested value,
          $Res Function(_$ParticipantRequested) then) =
      __$$ParticipantRequestedCopyWithImpl<$Res>;
  $Res call({ParticipantItem participantItem});

  $ParticipantItemCopyWith<$Res> get participantItem;
}

/// @nodoc
class __$$ParticipantRequestedCopyWithImpl<$Res>
    extends _$CartEventCopyWithImpl<$Res>
    implements _$$ParticipantRequestedCopyWith<$Res> {
  __$$ParticipantRequestedCopyWithImpl(_$ParticipantRequested _value,
      $Res Function(_$ParticipantRequested) _then)
      : super(_value, (v) => _then(v as _$ParticipantRequested));

  @override
  _$ParticipantRequested get _value => super._value as _$ParticipantRequested;

  @override
  $Res call({
    Object? participantItem = freezed,
  }) {
    return _then(_$ParticipantRequested(
      participantItem == freezed
          ? _value.participantItem
          : participantItem // ignore: cast_nullable_to_non_nullable
              as ParticipantItem,
    ));
  }

  @override
  $ParticipantItemCopyWith<$Res> get participantItem {
    return $ParticipantItemCopyWith<$Res>(_value.participantItem, (value) {
      return _then(_value.copyWith(participantItem: value));
    });
  }
}

/// @nodoc

class _$ParticipantRequested implements ParticipantRequested {
  _$ParticipantRequested(this.participantItem);

  @override
  final ParticipantItem participantItem;

  @override
  String toString() {
    return 'CartEvent.participantRequested(participantItem: $participantItem)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ParticipantRequested &&
            const DeepCollectionEquality()
                .equals(other.participantItem, participantItem));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(participantItem));

  @JsonKey(ignore: true)
  @override
  _$$ParticipantRequestedCopyWith<_$ParticipantRequested> get copyWith =>
      __$$ParticipantRequestedCopyWithImpl<_$ParticipantRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) {
    return participantRequested(participantItem);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) {
    return participantRequested?.call(participantItem);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) {
    if (participantRequested != null) {
      return participantRequested(participantItem);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) {
    return participantRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) {
    return participantRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) {
    if (participantRequested != null) {
      return participantRequested(this);
    }
    return orElse();
  }
}

abstract class ParticipantRequested implements CartEvent {
  factory ParticipantRequested(final ParticipantItem participantItem) =
      _$ParticipantRequested;

  ParticipantItem get participantItem;
  @JsonKey(ignore: true)
  _$$ParticipantRequestedCopyWith<_$ParticipantRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MarkParticipantRequestedCopyWith<$Res> {
  factory _$$MarkParticipantRequestedCopyWith(_$MarkParticipantRequested value,
          $Res Function(_$MarkParticipantRequested) then) =
      __$$MarkParticipantRequestedCopyWithImpl<$Res>;
  $Res call(
      {ParticipantItem participantItem,
      bool? delivered,
      bool? confirmedDelivered,
      bool? paid,
      bool? confirmedPaid,
      String? paidDetails});

  $ParticipantItemCopyWith<$Res> get participantItem;
}

/// @nodoc
class __$$MarkParticipantRequestedCopyWithImpl<$Res>
    extends _$CartEventCopyWithImpl<$Res>
    implements _$$MarkParticipantRequestedCopyWith<$Res> {
  __$$MarkParticipantRequestedCopyWithImpl(_$MarkParticipantRequested _value,
      $Res Function(_$MarkParticipantRequested) _then)
      : super(_value, (v) => _then(v as _$MarkParticipantRequested));

  @override
  _$MarkParticipantRequested get _value =>
      super._value as _$MarkParticipantRequested;

  @override
  $Res call({
    Object? participantItem = freezed,
    Object? delivered = freezed,
    Object? confirmedDelivered = freezed,
    Object? paid = freezed,
    Object? confirmedPaid = freezed,
    Object? paidDetails = freezed,
  }) {
    return _then(_$MarkParticipantRequested(
      participantItem: participantItem == freezed
          ? _value.participantItem
          : participantItem // ignore: cast_nullable_to_non_nullable
              as ParticipantItem,
      delivered: delivered == freezed
          ? _value.delivered
          : delivered // ignore: cast_nullable_to_non_nullable
              as bool?,
      confirmedDelivered: confirmedDelivered == freezed
          ? _value.confirmedDelivered
          : confirmedDelivered // ignore: cast_nullable_to_non_nullable
              as bool?,
      paid: paid == freezed
          ? _value.paid
          : paid // ignore: cast_nullable_to_non_nullable
              as bool?,
      confirmedPaid: confirmedPaid == freezed
          ? _value.confirmedPaid
          : confirmedPaid // ignore: cast_nullable_to_non_nullable
              as bool?,
      paidDetails: paidDetails == freezed
          ? _value.paidDetails
          : paidDetails // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $ParticipantItemCopyWith<$Res> get participantItem {
    return $ParticipantItemCopyWith<$Res>(_value.participantItem, (value) {
      return _then(_value.copyWith(participantItem: value));
    });
  }
}

/// @nodoc

class _$MarkParticipantRequested implements MarkParticipantRequested {
  _$MarkParticipantRequested(
      {required this.participantItem,
      this.delivered,
      this.confirmedDelivered,
      this.paid,
      this.confirmedPaid,
      this.paidDetails});

  @override
  final ParticipantItem participantItem;
  @override
  final bool? delivered;
  @override
  final bool? confirmedDelivered;
  @override
  final bool? paid;
  @override
  final bool? confirmedPaid;
  @override
  final String? paidDetails;

  @override
  String toString() {
    return 'CartEvent.markParticipantRequested(participantItem: $participantItem, delivered: $delivered, confirmedDelivered: $confirmedDelivered, paid: $paid, confirmedPaid: $confirmedPaid, paidDetails: $paidDetails)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MarkParticipantRequested &&
            const DeepCollectionEquality()
                .equals(other.participantItem, participantItem) &&
            const DeepCollectionEquality().equals(other.delivered, delivered) &&
            const DeepCollectionEquality()
                .equals(other.confirmedDelivered, confirmedDelivered) &&
            const DeepCollectionEquality().equals(other.paid, paid) &&
            const DeepCollectionEquality()
                .equals(other.confirmedPaid, confirmedPaid) &&
            const DeepCollectionEquality()
                .equals(other.paidDetails, paidDetails));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(participantItem),
      const DeepCollectionEquality().hash(delivered),
      const DeepCollectionEquality().hash(confirmedDelivered),
      const DeepCollectionEquality().hash(paid),
      const DeepCollectionEquality().hash(confirmedPaid),
      const DeepCollectionEquality().hash(paidDetails));

  @JsonKey(ignore: true)
  @override
  _$$MarkParticipantRequestedCopyWith<_$MarkParticipantRequested>
      get copyWith =>
          __$$MarkParticipantRequestedCopyWithImpl<_$MarkParticipantRequested>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) {
    return markParticipantRequested(participantItem, delivered,
        confirmedDelivered, paid, confirmedPaid, paidDetails);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) {
    return markParticipantRequested?.call(participantItem, delivered,
        confirmedDelivered, paid, confirmedPaid, paidDetails);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) {
    if (markParticipantRequested != null) {
      return markParticipantRequested(participantItem, delivered,
          confirmedDelivered, paid, confirmedPaid, paidDetails);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) {
    return markParticipantRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) {
    return markParticipantRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) {
    if (markParticipantRequested != null) {
      return markParticipantRequested(this);
    }
    return orElse();
  }
}

abstract class MarkParticipantRequested implements CartEvent {
  factory MarkParticipantRequested(
      {required final ParticipantItem participantItem,
      final bool? delivered,
      final bool? confirmedDelivered,
      final bool? paid,
      final bool? confirmedPaid,
      final String? paidDetails}) = _$MarkParticipantRequested;

  ParticipantItem get participantItem;
  bool? get delivered;
  bool? get confirmedDelivered;
  bool? get paid;
  bool? get confirmedPaid;
  String? get paidDetails;
  @JsonKey(ignore: true)
  _$$MarkParticipantRequestedCopyWith<_$MarkParticipantRequested>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CallParticipantRequestedCopyWith<$Res> {
  factory _$$CallParticipantRequestedCopyWith(_$CallParticipantRequested value,
          $Res Function(_$CallParticipantRequested) then) =
      __$$CallParticipantRequestedCopyWithImpl<$Res>;
  $Res call({Participant participant});

  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class __$$CallParticipantRequestedCopyWithImpl<$Res>
    extends _$CartEventCopyWithImpl<$Res>
    implements _$$CallParticipantRequestedCopyWith<$Res> {
  __$$CallParticipantRequestedCopyWithImpl(_$CallParticipantRequested _value,
      $Res Function(_$CallParticipantRequested) _then)
      : super(_value, (v) => _then(v as _$CallParticipantRequested));

  @override
  _$CallParticipantRequested get _value =>
      super._value as _$CallParticipantRequested;

  @override
  $Res call({
    Object? participant = freezed,
  }) {
    return _then(_$CallParticipantRequested(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
    ));
  }

  @override
  $ParticipantCopyWith<$Res> get participant {
    return $ParticipantCopyWith<$Res>(_value.participant, (value) {
      return _then(_value.copyWith(participant: value));
    });
  }
}

/// @nodoc

class _$CallParticipantRequested implements CallParticipantRequested {
  _$CallParticipantRequested({required this.participant});

  @override
  final Participant participant;

  @override
  String toString() {
    return 'CartEvent.callParticipantRequested(participant: $participant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CallParticipantRequested &&
            const DeepCollectionEquality()
                .equals(other.participant, participant));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(participant));

  @JsonKey(ignore: true)
  @override
  _$$CallParticipantRequestedCopyWith<_$CallParticipantRequested>
      get copyWith =>
          __$$CallParticipantRequestedCopyWithImpl<_$CallParticipantRequested>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) {
    return callParticipantRequested(participant);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) {
    return callParticipantRequested?.call(participant);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) {
    if (callParticipantRequested != null) {
      return callParticipantRequested(participant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) {
    return callParticipantRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) {
    return callParticipantRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) {
    if (callParticipantRequested != null) {
      return callParticipantRequested(this);
    }
    return orElse();
  }
}

abstract class CallParticipantRequested implements CartEvent {
  factory CallParticipantRequested({required final Participant participant}) =
      _$CallParticipantRequested;

  Participant get participant;
  @JsonKey(ignore: true)
  _$$CallParticipantRequestedCopyWith<_$CallParticipantRequested>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$TalkOnWhatsAppRequestedCopyWith<$Res> {
  factory _$$TalkOnWhatsAppRequestedCopyWith(_$TalkOnWhatsAppRequested value,
          $Res Function(_$TalkOnWhatsAppRequested) then) =
      __$$TalkOnWhatsAppRequestedCopyWithImpl<$Res>;
  $Res call({Participant participant});

  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class __$$TalkOnWhatsAppRequestedCopyWithImpl<$Res>
    extends _$CartEventCopyWithImpl<$Res>
    implements _$$TalkOnWhatsAppRequestedCopyWith<$Res> {
  __$$TalkOnWhatsAppRequestedCopyWithImpl(_$TalkOnWhatsAppRequested _value,
      $Res Function(_$TalkOnWhatsAppRequested) _then)
      : super(_value, (v) => _then(v as _$TalkOnWhatsAppRequested));

  @override
  _$TalkOnWhatsAppRequested get _value =>
      super._value as _$TalkOnWhatsAppRequested;

  @override
  $Res call({
    Object? participant = freezed,
  }) {
    return _then(_$TalkOnWhatsAppRequested(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
    ));
  }

  @override
  $ParticipantCopyWith<$Res> get participant {
    return $ParticipantCopyWith<$Res>(_value.participant, (value) {
      return _then(_value.copyWith(participant: value));
    });
  }
}

/// @nodoc

class _$TalkOnWhatsAppRequested implements TalkOnWhatsAppRequested {
  _$TalkOnWhatsAppRequested({required this.participant});

  @override
  final Participant participant;

  @override
  String toString() {
    return 'CartEvent.talkOnWhatsAppRequested(participant: $participant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TalkOnWhatsAppRequested &&
            const DeepCollectionEquality()
                .equals(other.participant, participant));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(participant));

  @JsonKey(ignore: true)
  @override
  _$$TalkOnWhatsAppRequestedCopyWith<_$TalkOnWhatsAppRequested> get copyWith =>
      __$$TalkOnWhatsAppRequestedCopyWithImpl<_$TalkOnWhatsAppRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) {
    return talkOnWhatsAppRequested(participant);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) {
    return talkOnWhatsAppRequested?.call(participant);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) {
    if (talkOnWhatsAppRequested != null) {
      return talkOnWhatsAppRequested(participant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) {
    return talkOnWhatsAppRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) {
    return talkOnWhatsAppRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) {
    if (talkOnWhatsAppRequested != null) {
      return talkOnWhatsAppRequested(this);
    }
    return orElse();
  }
}

abstract class TalkOnWhatsAppRequested implements CartEvent {
  factory TalkOnWhatsAppRequested({required final Participant participant}) =
      _$TalkOnWhatsAppRequested;

  Participant get participant;
  @JsonKey(ignore: true)
  _$$TalkOnWhatsAppRequestedCopyWith<_$TalkOnWhatsAppRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ExportRequestedCopyWith<$Res> {
  factory _$$ExportRequestedCopyWith(
          _$ExportRequested value, $Res Function(_$ExportRequested) then) =
      __$$ExportRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ExportRequestedCopyWithImpl<$Res>
    extends _$CartEventCopyWithImpl<$Res>
    implements _$$ExportRequestedCopyWith<$Res> {
  __$$ExportRequestedCopyWithImpl(
      _$ExportRequested _value, $Res Function(_$ExportRequested) _then)
      : super(_value, (v) => _then(v as _$ExportRequested));

  @override
  _$ExportRequested get _value => super._value as _$ExportRequested;
}

/// @nodoc

class _$ExportRequested implements ExportRequested {
  _$ExportRequested();

  @override
  String toString() {
    return 'CartEvent.exportRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ExportRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String url) linkRequested,
    required TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)
        modifyEntryRequested,
    required TResult Function(CartEntry cartEntry) removeEntryRequested,
    required TResult Function(ParticipantItem participantItem)
        participantRequested,
    required TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)
        markParticipantRequested,
    required TResult Function(Participant participant) callParticipantRequested,
    required TResult Function(Participant participant) talkOnWhatsAppRequested,
    required TResult Function() exportRequested,
  }) {
    return exportRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
  }) {
    return exportRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String url)? linkRequested,
    TResult Function(
            CartProduct cartProduct, int numItems, double? modifiedPrice)?
        modifyEntryRequested,
    TResult Function(CartEntry cartEntry)? removeEntryRequested,
    TResult Function(ParticipantItem participantItem)? participantRequested,
    TResult Function(
            ParticipantItem participantItem,
            bool? delivered,
            bool? confirmedDelivered,
            bool? paid,
            bool? confirmedPaid,
            String? paidDetails)?
        markParticipantRequested,
    TResult Function(Participant participant)? callParticipantRequested,
    TResult Function(Participant participant)? talkOnWhatsAppRequested,
    TResult Function()? exportRequested,
    required TResult orElse(),
  }) {
    if (exportRequested != null) {
      return exportRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(ModifyEntryRequested value) modifyEntryRequested,
    required TResult Function(RemoveEntryRequested value) removeEntryRequested,
    required TResult Function(ParticipantRequested value) participantRequested,
    required TResult Function(MarkParticipantRequested value)
        markParticipantRequested,
    required TResult Function(CallParticipantRequested value)
        callParticipantRequested,
    required TResult Function(TalkOnWhatsAppRequested value)
        talkOnWhatsAppRequested,
    required TResult Function(ExportRequested value) exportRequested,
  }) {
    return exportRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
  }) {
    return exportRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(ModifyEntryRequested value)? modifyEntryRequested,
    TResult Function(RemoveEntryRequested value)? removeEntryRequested,
    TResult Function(ParticipantRequested value)? participantRequested,
    TResult Function(MarkParticipantRequested value)? markParticipantRequested,
    TResult Function(CallParticipantRequested value)? callParticipantRequested,
    TResult Function(TalkOnWhatsAppRequested value)? talkOnWhatsAppRequested,
    TResult Function(ExportRequested value)? exportRequested,
    required TResult orElse(),
  }) {
    if (exportRequested != null) {
      return exportRequested(this);
    }
    return orElse();
  }
}

abstract class ExportRequested implements CartEvent {
  factory ExportRequested() = _$ExportRequested;
}
