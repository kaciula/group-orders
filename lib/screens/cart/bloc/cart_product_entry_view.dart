import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/cart_entry.dart';

part 'cart_product_entry_view.freezed.dart';

@freezed
class CartProductEntryView with _$CartProductEntryView {
  factory CartProductEntryView({
    required CartProduct cartProduct,
    required double total,
    required double discountedTotal,
    required String participantName,
  }) = _CartProductEntryView;
}
