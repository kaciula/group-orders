// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'participant_item.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ParticipantItem {
  Participant get participant => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ParticipantItemCopyWith<ParticipantItem> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ParticipantItemCopyWith<$Res> {
  factory $ParticipantItemCopyWith(
          ParticipantItem value, $Res Function(ParticipantItem) then) =
      _$ParticipantItemCopyWithImpl<$Res>;
  $Res call({Participant participant, bool isActive});

  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class _$ParticipantItemCopyWithImpl<$Res>
    implements $ParticipantItemCopyWith<$Res> {
  _$ParticipantItemCopyWithImpl(this._value, this._then);

  final ParticipantItem _value;
  // ignore: unused_field
  final $Res Function(ParticipantItem) _then;

  @override
  $Res call({
    Object? participant = freezed,
    Object? isActive = freezed,
  }) {
    return _then(_value.copyWith(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }

  @override
  $ParticipantCopyWith<$Res> get participant {
    return $ParticipantCopyWith<$Res>(_value.participant, (value) {
      return _then(_value.copyWith(participant: value));
    });
  }
}

/// @nodoc
abstract class _$$_ParticipantItemCopyWith<$Res>
    implements $ParticipantItemCopyWith<$Res> {
  factory _$$_ParticipantItemCopyWith(
          _$_ParticipantItem value, $Res Function(_$_ParticipantItem) then) =
      __$$_ParticipantItemCopyWithImpl<$Res>;
  @override
  $Res call({Participant participant, bool isActive});

  @override
  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class __$$_ParticipantItemCopyWithImpl<$Res>
    extends _$ParticipantItemCopyWithImpl<$Res>
    implements _$$_ParticipantItemCopyWith<$Res> {
  __$$_ParticipantItemCopyWithImpl(
      _$_ParticipantItem _value, $Res Function(_$_ParticipantItem) _then)
      : super(_value, (v) => _then(v as _$_ParticipantItem));

  @override
  _$_ParticipantItem get _value => super._value as _$_ParticipantItem;

  @override
  $Res call({
    Object? participant = freezed,
    Object? isActive = freezed,
  }) {
    return _then(_$_ParticipantItem(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ParticipantItem extends _ParticipantItem {
  _$_ParticipantItem({required this.participant, required this.isActive})
      : super._();

  @override
  final Participant participant;
  @override
  final bool isActive;

  @override
  String toString() {
    return 'ParticipantItem(participant: $participant, isActive: $isActive)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ParticipantItem &&
            const DeepCollectionEquality()
                .equals(other.participant, participant) &&
            const DeepCollectionEquality().equals(other.isActive, isActive));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(participant),
      const DeepCollectionEquality().hash(isActive));

  @JsonKey(ignore: true)
  @override
  _$$_ParticipantItemCopyWith<_$_ParticipantItem> get copyWith =>
      __$$_ParticipantItemCopyWithImpl<_$_ParticipantItem>(this, _$identity);
}

abstract class _ParticipantItem extends ParticipantItem {
  factory _ParticipantItem(
      {required final Participant participant,
      required final bool isActive}) = _$_ParticipantItem;
  _ParticipantItem._() : super._();

  @override
  Participant get participant;
  @override
  bool get isActive;
  @override
  @JsonKey(ignore: true)
  _$$_ParticipantItemCopyWith<_$_ParticipantItem> get copyWith =>
      throw _privateConstructorUsedError;
}
