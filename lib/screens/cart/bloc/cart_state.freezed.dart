// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cart_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CartState {
  bool get inProgressAction => throw _privateConstructorUsedError;
  Order get order => throw _privateConstructorUsedError;
  User get user => throw _privateConstructorUsedError;
  Participant get owner => throw _privateConstructorUsedError;
  KtList<ParticipantItem> get participants =>
      throw _privateConstructorUsedError;
  KtList<Product> get products => throw _privateConstructorUsedError;
  KtList<CartEntry> get cart => throw _privateConstructorUsedError;
  CartView get cartView => throw _privateConstructorUsedError;
  CartEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CartStateCopyWith<CartState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CartStateCopyWith<$Res> {
  factory $CartStateCopyWith(CartState value, $Res Function(CartState) then) =
      _$CartStateCopyWithImpl<$Res>;
  $Res call(
      {bool inProgressAction,
      Order order,
      User user,
      Participant owner,
      KtList<ParticipantItem> participants,
      KtList<Product> products,
      KtList<CartEntry> cart,
      CartView cartView,
      CartEffect? effect});

  $OrderCopyWith<$Res> get order;
  $UserCopyWith<$Res> get user;
  $ParticipantCopyWith<$Res> get owner;
  $CartViewCopyWith<$Res> get cartView;
  $CartEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$CartStateCopyWithImpl<$Res> implements $CartStateCopyWith<$Res> {
  _$CartStateCopyWithImpl(this._value, this._then);

  final CartState _value;
  // ignore: unused_field
  final $Res Function(CartState) _then;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? order = freezed,
    Object? user = freezed,
    Object? owner = freezed,
    Object? participants = freezed,
    Object? products = freezed,
    Object? cart = freezed,
    Object? cartView = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      owner: owner == freezed
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Participant,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as KtList<ParticipantItem>,
      products: products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<Product>,
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>,
      cartView: cartView == freezed
          ? _value.cartView
          : cartView // ignore: cast_nullable_to_non_nullable
              as CartView,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as CartEffect?,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }

  @override
  $ParticipantCopyWith<$Res> get owner {
    return $ParticipantCopyWith<$Res>(_value.owner, (value) {
      return _then(_value.copyWith(owner: value));
    });
  }

  @override
  $CartViewCopyWith<$Res> get cartView {
    return $CartViewCopyWith<$Res>(_value.cartView, (value) {
      return _then(_value.copyWith(cartView: value));
    });
  }

  @override
  $CartEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $CartEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_CartStateCopyWith<$Res> implements $CartStateCopyWith<$Res> {
  factory _$$_CartStateCopyWith(
          _$_CartState value, $Res Function(_$_CartState) then) =
      __$$_CartStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool inProgressAction,
      Order order,
      User user,
      Participant owner,
      KtList<ParticipantItem> participants,
      KtList<Product> products,
      KtList<CartEntry> cart,
      CartView cartView,
      CartEffect? effect});

  @override
  $OrderCopyWith<$Res> get order;
  @override
  $UserCopyWith<$Res> get user;
  @override
  $ParticipantCopyWith<$Res> get owner;
  @override
  $CartViewCopyWith<$Res> get cartView;
  @override
  $CartEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_CartStateCopyWithImpl<$Res> extends _$CartStateCopyWithImpl<$Res>
    implements _$$_CartStateCopyWith<$Res> {
  __$$_CartStateCopyWithImpl(
      _$_CartState _value, $Res Function(_$_CartState) _then)
      : super(_value, (v) => _then(v as _$_CartState));

  @override
  _$_CartState get _value => super._value as _$_CartState;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? order = freezed,
    Object? user = freezed,
    Object? owner = freezed,
    Object? participants = freezed,
    Object? products = freezed,
    Object? cart = freezed,
    Object? cartView = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_CartState(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      owner: owner == freezed
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Participant,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as KtList<ParticipantItem>,
      products: products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<Product>,
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>,
      cartView: cartView == freezed
          ? _value.cartView
          : cartView // ignore: cast_nullable_to_non_nullable
              as CartView,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as CartEffect?,
    ));
  }
}

/// @nodoc

class _$_CartState extends _CartState {
  _$_CartState(
      {required this.inProgressAction,
      required this.order,
      required this.user,
      required this.owner,
      required this.participants,
      required this.products,
      required this.cart,
      required this.cartView,
      this.effect})
      : super._();

  @override
  final bool inProgressAction;
  @override
  final Order order;
  @override
  final User user;
  @override
  final Participant owner;
  @override
  final KtList<ParticipantItem> participants;
  @override
  final KtList<Product> products;
  @override
  final KtList<CartEntry> cart;
  @override
  final CartView cartView;
  @override
  final CartEffect? effect;

  @override
  String toString() {
    return 'CartState(inProgressAction: $inProgressAction, order: $order, user: $user, owner: $owner, participants: $participants, products: $products, cart: $cart, cartView: $cartView, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CartState &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality().equals(other.owner, owner) &&
            const DeepCollectionEquality()
                .equals(other.participants, participants) &&
            const DeepCollectionEquality().equals(other.products, products) &&
            const DeepCollectionEquality().equals(other.cart, cart) &&
            const DeepCollectionEquality().equals(other.cartView, cartView) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(owner),
      const DeepCollectionEquality().hash(participants),
      const DeepCollectionEquality().hash(products),
      const DeepCollectionEquality().hash(cart),
      const DeepCollectionEquality().hash(cartView),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_CartStateCopyWith<_$_CartState> get copyWith =>
      __$$_CartStateCopyWithImpl<_$_CartState>(this, _$identity);
}

abstract class _CartState extends CartState {
  factory _CartState(
      {required final bool inProgressAction,
      required final Order order,
      required final User user,
      required final Participant owner,
      required final KtList<ParticipantItem> participants,
      required final KtList<Product> products,
      required final KtList<CartEntry> cart,
      required final CartView cartView,
      final CartEffect? effect}) = _$_CartState;
  _CartState._() : super._();

  @override
  bool get inProgressAction;
  @override
  Order get order;
  @override
  User get user;
  @override
  Participant get owner;
  @override
  KtList<ParticipantItem> get participants;
  @override
  KtList<Product> get products;
  @override
  KtList<CartEntry> get cart;
  @override
  CartView get cartView;
  @override
  CartEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_CartStateCopyWith<_$_CartState> get copyWith =>
      throw _privateConstructorUsedError;
}
