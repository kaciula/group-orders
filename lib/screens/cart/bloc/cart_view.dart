import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/product.dart';
import 'package:orders/screens/cart/bloc/cart_product_view.dart';
import 'package:orders/screens/cart/bloc/participant_item.dart';

part 'cart_view.freezed.dart';

@freezed
class CartView with _$CartView {
  factory CartView({
    required KtList<KtPair<Product, CartProductView>> products,
    required KtList<CartEntry> extraEntries,
    required ParticipantItem participantItem,
    required double total,
    required double discountedTotal,
    required String currency,
    required int numActiveParticipants,
    required int numTotalItems,
  }) = _CartView;

  CartView._();

  bool get hasDiscount => extraEntries.any((CartEntry it) => it is Discount);

  bool get isForAll => participantItem.isAll;
}
