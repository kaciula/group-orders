import 'package:freezed_annotation/freezed_annotation.dart';

part 'cart_effect.freezed.dart';

@freezed
class CartEffect with _$CartEffect {
  factory CartEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory CartEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
