// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cart_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CartView {
  KtList<KtPair<Product, CartProductView>> get products =>
      throw _privateConstructorUsedError;
  KtList<CartEntry> get extraEntries => throw _privateConstructorUsedError;
  ParticipantItem get participantItem => throw _privateConstructorUsedError;
  double get total => throw _privateConstructorUsedError;
  double get discountedTotal => throw _privateConstructorUsedError;
  String get currency => throw _privateConstructorUsedError;
  int get numActiveParticipants => throw _privateConstructorUsedError;
  int get numTotalItems => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CartViewCopyWith<CartView> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CartViewCopyWith<$Res> {
  factory $CartViewCopyWith(CartView value, $Res Function(CartView) then) =
      _$CartViewCopyWithImpl<$Res>;
  $Res call(
      {KtList<KtPair<Product, CartProductView>> products,
      KtList<CartEntry> extraEntries,
      ParticipantItem participantItem,
      double total,
      double discountedTotal,
      String currency,
      int numActiveParticipants,
      int numTotalItems});

  $ParticipantItemCopyWith<$Res> get participantItem;
}

/// @nodoc
class _$CartViewCopyWithImpl<$Res> implements $CartViewCopyWith<$Res> {
  _$CartViewCopyWithImpl(this._value, this._then);

  final CartView _value;
  // ignore: unused_field
  final $Res Function(CartView) _then;

  @override
  $Res call({
    Object? products = freezed,
    Object? extraEntries = freezed,
    Object? participantItem = freezed,
    Object? total = freezed,
    Object? discountedTotal = freezed,
    Object? currency = freezed,
    Object? numActiveParticipants = freezed,
    Object? numTotalItems = freezed,
  }) {
    return _then(_value.copyWith(
      products: products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<KtPair<Product, CartProductView>>,
      extraEntries: extraEntries == freezed
          ? _value.extraEntries
          : extraEntries // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>,
      participantItem: participantItem == freezed
          ? _value.participantItem
          : participantItem // ignore: cast_nullable_to_non_nullable
              as ParticipantItem,
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as double,
      discountedTotal: discountedTotal == freezed
          ? _value.discountedTotal
          : discountedTotal // ignore: cast_nullable_to_non_nullable
              as double,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      numActiveParticipants: numActiveParticipants == freezed
          ? _value.numActiveParticipants
          : numActiveParticipants // ignore: cast_nullable_to_non_nullable
              as int,
      numTotalItems: numTotalItems == freezed
          ? _value.numTotalItems
          : numTotalItems // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }

  @override
  $ParticipantItemCopyWith<$Res> get participantItem {
    return $ParticipantItemCopyWith<$Res>(_value.participantItem, (value) {
      return _then(_value.copyWith(participantItem: value));
    });
  }
}

/// @nodoc
abstract class _$$_CartViewCopyWith<$Res> implements $CartViewCopyWith<$Res> {
  factory _$$_CartViewCopyWith(
          _$_CartView value, $Res Function(_$_CartView) then) =
      __$$_CartViewCopyWithImpl<$Res>;
  @override
  $Res call(
      {KtList<KtPair<Product, CartProductView>> products,
      KtList<CartEntry> extraEntries,
      ParticipantItem participantItem,
      double total,
      double discountedTotal,
      String currency,
      int numActiveParticipants,
      int numTotalItems});

  @override
  $ParticipantItemCopyWith<$Res> get participantItem;
}

/// @nodoc
class __$$_CartViewCopyWithImpl<$Res> extends _$CartViewCopyWithImpl<$Res>
    implements _$$_CartViewCopyWith<$Res> {
  __$$_CartViewCopyWithImpl(
      _$_CartView _value, $Res Function(_$_CartView) _then)
      : super(_value, (v) => _then(v as _$_CartView));

  @override
  _$_CartView get _value => super._value as _$_CartView;

  @override
  $Res call({
    Object? products = freezed,
    Object? extraEntries = freezed,
    Object? participantItem = freezed,
    Object? total = freezed,
    Object? discountedTotal = freezed,
    Object? currency = freezed,
    Object? numActiveParticipants = freezed,
    Object? numTotalItems = freezed,
  }) {
    return _then(_$_CartView(
      products: products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<KtPair<Product, CartProductView>>,
      extraEntries: extraEntries == freezed
          ? _value.extraEntries
          : extraEntries // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>,
      participantItem: participantItem == freezed
          ? _value.participantItem
          : participantItem // ignore: cast_nullable_to_non_nullable
              as ParticipantItem,
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as double,
      discountedTotal: discountedTotal == freezed
          ? _value.discountedTotal
          : discountedTotal // ignore: cast_nullable_to_non_nullable
              as double,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      numActiveParticipants: numActiveParticipants == freezed
          ? _value.numActiveParticipants
          : numActiveParticipants // ignore: cast_nullable_to_non_nullable
              as int,
      numTotalItems: numTotalItems == freezed
          ? _value.numTotalItems
          : numTotalItems // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_CartView extends _CartView {
  _$_CartView(
      {required this.products,
      required this.extraEntries,
      required this.participantItem,
      required this.total,
      required this.discountedTotal,
      required this.currency,
      required this.numActiveParticipants,
      required this.numTotalItems})
      : super._();

  @override
  final KtList<KtPair<Product, CartProductView>> products;
  @override
  final KtList<CartEntry> extraEntries;
  @override
  final ParticipantItem participantItem;
  @override
  final double total;
  @override
  final double discountedTotal;
  @override
  final String currency;
  @override
  final int numActiveParticipants;
  @override
  final int numTotalItems;

  @override
  String toString() {
    return 'CartView(products: $products, extraEntries: $extraEntries, participantItem: $participantItem, total: $total, discountedTotal: $discountedTotal, currency: $currency, numActiveParticipants: $numActiveParticipants, numTotalItems: $numTotalItems)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CartView &&
            const DeepCollectionEquality().equals(other.products, products) &&
            const DeepCollectionEquality()
                .equals(other.extraEntries, extraEntries) &&
            const DeepCollectionEquality()
                .equals(other.participantItem, participantItem) &&
            const DeepCollectionEquality().equals(other.total, total) &&
            const DeepCollectionEquality()
                .equals(other.discountedTotal, discountedTotal) &&
            const DeepCollectionEquality().equals(other.currency, currency) &&
            const DeepCollectionEquality()
                .equals(other.numActiveParticipants, numActiveParticipants) &&
            const DeepCollectionEquality()
                .equals(other.numTotalItems, numTotalItems));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(products),
      const DeepCollectionEquality().hash(extraEntries),
      const DeepCollectionEquality().hash(participantItem),
      const DeepCollectionEquality().hash(total),
      const DeepCollectionEquality().hash(discountedTotal),
      const DeepCollectionEquality().hash(currency),
      const DeepCollectionEquality().hash(numActiveParticipants),
      const DeepCollectionEquality().hash(numTotalItems));

  @JsonKey(ignore: true)
  @override
  _$$_CartViewCopyWith<_$_CartView> get copyWith =>
      __$$_CartViewCopyWithImpl<_$_CartView>(this, _$identity);
}

abstract class _CartView extends CartView {
  factory _CartView(
      {required final KtList<KtPair<Product, CartProductView>> products,
      required final KtList<CartEntry> extraEntries,
      required final ParticipantItem participantItem,
      required final double total,
      required final double discountedTotal,
      required final String currency,
      required final int numActiveParticipants,
      required final int numTotalItems}) = _$_CartView;
  _CartView._() : super._();

  @override
  KtList<KtPair<Product, CartProductView>> get products;
  @override
  KtList<CartEntry> get extraEntries;
  @override
  ParticipantItem get participantItem;
  @override
  double get total;
  @override
  double get discountedTotal;
  @override
  String get currency;
  @override
  int get numActiveParticipants;
  @override
  int get numTotalItems;
  @override
  @JsonKey(ignore: true)
  _$$_CartViewCopyWith<_$_CartView> get copyWith =>
      throw _privateConstructorUsedError;
}
