import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:orders/model/product.dart';
import 'package:orders/screens/cart/bloc/cart_product_entry_view.dart';

part 'cart_product_view.freezed.dart';

@freezed
class CartProductView with _$CartProductView {
  factory CartProductView({
    required Product product,
    required KtList<CartProductEntryView> entries,
    required int numItems,
    required double total,
    required double discountedTotal,
  }) = _CartProductView;
}
