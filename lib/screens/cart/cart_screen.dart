import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:kt_dart/collection.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/app/app_styles.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';
import 'package:orders/screens/cart/bloc/cart_bloc.dart';
import 'package:orders/screens/cart/bloc/cart_effect.dart';
import 'package:orders/screens/cart/bloc/cart_event.dart';
import 'package:orders/screens/cart/bloc/cart_product_entry_view.dart';
import 'package:orders/screens/cart/bloc/cart_product_view.dart';
import 'package:orders/screens/cart/bloc/cart_state.dart';
import 'package:orders/screens/cart/bloc/cart_view.dart';
import 'package:orders/screens/cart/bloc/participant_item.dart';
import 'package:orders/screens/cart/edit_cart_entry_dialog.dart';
import 'package:orders/screens/cart/payment_method_dialog.dart';
import 'package:orders/screens/participants/participant_sheet.dart';
import 'package:orders/screens/utils/formatter_utils.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/action_sheet.dart';
import 'package:orders/screens/widgets/custom_url_linkifier.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';
import 'package:orders/screens/widgets/ok_dialog.dart';
import 'package:orders/screens/widgets/sure_dialog.dart';

class CartScreen extends StatefulWidget {
  static const String routeName = 'cart';

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
      bloc: BlocProvider.of<CartBloc>(context),
      buildWhen: CartState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, CartState state) {
    final CartEffect? effect = state.effect;
    _logger.info('effect=$effect');
  }

  Widget _builder(BuildContext context, CartState state) {
    final CartBloc bloc = BlocProvider.of<CartBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(Strings.cartTitle),
            Text(state.order.title, style: styleAppBarSubtitle),
          ],
        ),
        actions: state.isOwner
            ? <Widget>[
                IconButton(
                  icon: const Icon(Icons.download),
                  onPressed: () => _bloc(context).add(ExportRequested()),
                ),
              ]
            : null,
      ),
      body: BlocListener<CartBloc, CartState>(
        bloc: bloc,
        listenWhen: CartState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, CartState state) {
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: Column(
        children: <Widget>[
          if (state.canFilterParticipants) _selector(context, state),
          Expanded(
            child: state.cartView.products.size > 0
                ? ListView.builder(
                    padding:
                        const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                    itemCount: state.cartView.products.size +
                        state.cartView.extraEntries.size,
                    itemBuilder: (BuildContext context, int index) {
                      if (index < state.cartView.products.size) {
                        final KtPair<Product, CartProductView> pair =
                            state.cartView.products[index];
                        return _product(
                            context, state, pair.first, pair.second);
                      } else {
                        return _extraEntry(
                            context,
                            state,
                            state.cartView.extraEntries[
                                index - state.cartView.products.size]);
                      }
                    },
                  )
                : Center(
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Text(
                        state.cartView.participantItem.userId == state.user.id
                            ? Strings.cartEmptyOwner
                            : Strings.cartEmpty,
                        style: styleEmpty,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
          ),
          _total(state.cartView),
        ],
      ),
    );
  }

  Widget _selector(BuildContext context, CartState state) {
    final ParticipantItem participantItem = state.cartView.participantItem;
    final ParticipantItem participantView = state.participants
        .first((ParticipantItem it) => it.userId == participantItem.userId);
    final KtList<ParticipantItem> items = state.participants;
    final Color selectorColor = Theme.of(context).textTheme.bodyText1!.color!;
    final List<DropdownMenuItem<ParticipantItem>> menuItems = items
        .map(
          (ParticipantItem it) => DropdownMenuItem<ParticipantItem>(
            value: it,
            child: Text(
              it.isAll ? Strings.cartAllParticipants : it.displayName,
              style: TextStyle(
                color: it.isActive
                    ? selectorColor
                    : selectorColor.withOpacity(0.3),
              ),
            ),
          ),
        )
        .asList();
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      color: AppColors.primary.withOpacity(0.3),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 55,
            child: DropdownButtonHideUnderline(
              child: DropdownButton<ParticipantItem>(
                items: menuItems,
                value: participantView,
                isExpanded: true,
                onChanged: (ParticipantItem? it) =>
                    _bloc(context).add(ParticipantRequested(it!)),
              ),
            ),
          ),
          const SizedBox(width: 16),
          Expanded(
            flex: 45,
            child: _canSeeParticipantFlags(state, participantItem)
                ? _participantFlags(context, state, participantItem)
                : Container(),
          ),
        ],
      ),
    );
  }

  bool _canSeeParticipantFlags(
      CartState state, ParticipantItem participantItem) {
    return !participantItem.isAll &&
        state.cartView.numTotalItems > 0 &&
        ((state.isOwner && state.owner.userId != participantItem.userId) ||
            (!state.isOwner && state.user.id == participantItem.userId));
  }

  Widget _participantFlags(
      BuildContext context, CartState state, ParticipantItem participantItem) {
    final CartBloc bloc = _bloc(context);
    final Participant participant = participantItem.participant;
    return InkWell(
      onTap: () => showModalBottomSheet(
        context: context,
        builder: (BuildContext context) => ParticipantSheet(
          participant: participant,
          isOwner: state.isOwner,
          onTapDelivered: (state.isOwner && participant.delivered) ||
                  (!state.isOwner && participant.confirmedDelivered)
              ? null
              : () {
                  bloc.add(MarkParticipantRequested(
                    participantItem: participantItem,
                    delivered: state.isOwner ? true : null,
                    confirmedDelivered: state.isOwner ? null : true,
                  ));
                  Navigator.pop(context);
                },
          onTapPaid: (state.isOwner && participant.confirmedPaid) ||
                  (!state.isOwner && participant.paid)
              ? null
              : () {
                  if (state.isOwner) {
                    bloc.add(MarkParticipantRequested(
                      participantItem: participantItem,
                      paid: !state.isOwner ? true : null,
                      confirmedPaid: !state.isOwner ? null : true,
                    ));
                    Navigator.pop(context);
                  } else {
                    showDialog(
                      context: context,
                      builder: (BuildContext innerContext) {
                        return PaymentMethodDialog(
                          participant: participant,
                          onTap: (String paidDetails) {
                            bloc.add(MarkParticipantRequested(
                              participantItem: participantItem,
                              paid: true,
                              confirmedPaid: null,
                              paidDetails: paidDetails,
                            ));
                            Navigator.pop(innerContext);
                          },
                        );
                      },
                    );
                  }
                },
          onTapWhatsApp: state.isOwner && participant.hasPhoneNumber
              ? () {
                  bloc.add(TalkOnWhatsAppRequested(participant: participant));
                  Navigator.pop(context);
                }
              : null,
          onTapCall: state.isOwner && participant.hasPhoneNumber
              ? () {
                  bloc.add(CallParticipantRequested(participant: participant));
                  Navigator.pop(context);
                }
              : null,
        ),
      ),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ...ScreenUtils.participantFlags(participant,
                isOwner: state.isOwner),
          ],
        ),
      ),
    );
  }

  Widget _product(
    BuildContext context,
    CartState state,
    Product product,
    CartProductView productView,
  ) {
    final List<Widget> rows = <Widget>[];
    for (CartProductEntryView entryView in productView.entries.iter) {
      rows.add(_participant(context, state, product, entryView));
      rows.add(const Divider(height: 1, indent: 16));
    }
    final CartBloc bloc = BlocProvider.of(context);
    return Card(
      child: InkWell(
        splashColor: AppColors.accent.withOpacity(0.8),
        onTap: () => showDialog(
          context: context,
          builder: (BuildContext innerContext) => OkDialog(
            title: product.title,
            content: Linkify(
              text: product.details,
              linkifiers: const <Linkifier>[CustomUrlLinkifier()],
              options: LinkifyOptions(looseUrl: true),
              onOpen: (LinkableElement it) {
                bloc.add(LinkRequested(it.url));
                Navigator.pop(innerContext);
              },
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      product.title,
                      style: styleProductAndPrice,
                    ),
                  ),
                  const SizedBox(width: 16),
                  Text(productView.numItems.toString()),
                  SizedBox(
                    width: _widthEntryPrice(
                        hasDiscount: state.cartView.hasDiscount),
                    child: Text.rich(
                      TextSpan(
                        children: <TextSpan>[
                          if (state.cartView.hasDiscount) ...<TextSpan>[
                            TextSpan(
                              text:
                                  '${FormatterUtils.format(productView.total)}',
                              style: styleProductAndPrice.copyWith(
                                  decoration: TextDecoration.lineThrough),
                            ),
                            const TextSpan(text: ' '),
                          ],
                          TextSpan(
                            text:
                                '${FormatterUtils.format(productView.discountedTotal)} ${state.order.currency}',
                          ),
                        ],
                      ),
                      style: styleProductAndPrice,
                      textAlign: TextAlign.end,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 8),
              const Divider(height: 1),
              ...rows,
            ],
          ),
        ),
      ),
    );
  }

  Widget _participant(BuildContext context, CartState state, Product product,
      CartProductEntryView entryView) {
    final CartProduct cartProduct = entryView.cartProduct;
    final bool isOwnProduct =
        state.isOwner || cartProduct.participantId == state.user.id;
    return InkWell(
      splashColor: AppColors.accent.withOpacity(0.8),
      onTap: () => showModalBottomSheet(
        context: context,
        builder: (BuildContext ctx) => ActionSheet(
          title: product.title,
          details: entryView.participantName,
          isEditVisible: state.isOwner,
          onTapEdit: state.canEditEntry
              ? () => showDialog(
                    context: context,
                    builder: (BuildContext innerContext) {
                      return EditCartEntryDialog(
                        cartBloc: _bloc(context),
                        order: state.order,
                        product: product,
                        entryView: entryView,
                      );
                    },
                  )
              : null,
          isDeleteVisible: isOwnProduct,
          deleteSureMsg: Strings.cartAreYouSureProduct,
          onTapDelete: isOwnProduct && state.canRemoveFromCart
              ? () => _bloc(context)
                  .add(RemoveEntryRequested(entryView.cartProduct))
              : null,
        ),
      ),
      child: ConstrainedBox(
        constraints: const BoxConstraints(minHeight: 48),
        child: Padding(
          padding: const EdgeInsets.only(left: 16),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(child: Text(entryView.participantName)),
                    Text(
                      cartProduct.numItems.toString(),
                      style: const TextStyle(fontWeight: FontWeight.w200),
                    ),
                    SizedBox(
                      width: _widthEntryPrice(
                          hasDiscount: state.cartView.hasDiscount),
                      child: Text.rich(
                        TextSpan(
                          children: <TextSpan>[
                            if (state.cartView.hasDiscount) ...<TextSpan>[
                              TextSpan(
                                text:
                                    '${FormatterUtils.format(entryView.total)}',
                                style: const TextStyle(
                                  fontWeight: FontWeight.w200,
                                  decoration: TextDecoration.lineThrough,
                                ),
                              ),
                              const TextSpan(text: ' '),
                            ],
                            TextSpan(
                              text:
                                  '${FormatterUtils.format(entryView.discountedTotal)} ${state.order.currency}',
                              style:
                                  const TextStyle(fontWeight: FontWeight.w200),
                            ),
                          ],
                        ),
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ],
                ),
                if (entryView.cartProduct.details.isNotEmpty) ...<Widget>[
                  const SizedBox(height: 4),
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Text(
                      entryView.cartProduct.details,
                      style: const TextStyle(fontWeight: FontWeight.w200),
                    ),
                  ),
                ]
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _extraEntry(BuildContext context, CartState state, CartEntry entry) {
    late Widget title;
    late String value;
    if (entry is Discount) {
      title = Text(Strings.orderEntryDiscount, style: styleProductAndPrice);
      value = '${FormatterUtils.format(entry.percent * 100)} %';
    } else if (entry is Transport) {
      final CartView cartView = state.cartView;
      title = Text(Strings.orderEntryTransport, style: styleProductAndPrice);
      final double cost = cartView.participantItem.isAll
          ? entry.cost
          : entry.cost / cartView.numActiveParticipants;
      value = '${FormatterUtils.format(cost)} ${state.order.currency}';
    }
    return Card(
      child: InkWell(
        splashColor: AppColors.accent.withOpacity(0.8),
        onTap: state.isOwner
            ? () => showDialog(
                  context: context,
                  builder: (BuildContext innerContext) {
                    return SureDialog(
                      msg: Strings.cartAreYouSureEntry,
                      onYesPressed: () =>
                          _bloc(context).add(RemoveEntryRequested(entry)),
                    );
                  },
                )
            : null,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(child: title),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(value, style: styleProductAndPrice),
                      if (state.isOwner) ...<Widget>[
                        const SizedBox(height: 8),
                        Icon(
                          Icons.remove_circle_outline,
                          color: state.isOwner
                              ? AppColors.red
                              : AppColors.textGreyLight,
                        ),
                      ]
                    ],
                  ),
                ],
              ),
              if (entry.details.isNotEmpty) ...<Widget>[
                const SizedBox(height: 4),
                Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: Text(
                    entry.details,
                    style: const TextStyle(fontWeight: FontWeight.w200),
                  ),
                ),
              ]
            ],
          ),
        ),
      ),
    );
  }

  Widget _total(CartView cartView) {
    const TextStyle style =
        TextStyle(fontWeight: FontWeight.bold, fontSize: 18);
    return Container(
      padding: const EdgeInsets.all(16),
      color: AppColors.accent.withOpacity(0.3),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              Strings.cartTotal,
              style: const TextStyle(fontSize: 18),
            ),
          ),
          if (cartView.numTotalItems > 0)
            Text('(${cartView.numTotalItems})',
                style: const TextStyle(fontSize: 18)),
          SizedBox(
            width: _widthTotalPrice(hasDiscount: cartView.hasDiscount),
            child: Text.rich(
              TextSpan(
                children: <TextSpan>[
                  if (cartView.hasDiscount) ...<TextSpan>[
                    TextSpan(
                      text: '${FormatterUtils.format(cartView.total)}',
                      style: style.copyWith(
                          decoration: TextDecoration.lineThrough),
                    ),
                    const TextSpan(text: ' '),
                  ],
                  TextSpan(
                    text:
                        '${FormatterUtils.format(cartView.discountedTotal)} ${cartView.currency}',
                  ),
                ],
              ),
              style: style,
              textAlign: TextAlign.end,
            ),
          )
        ],
      ),
    );
  }

  double _widthEntryPrice({required bool hasDiscount}) {
    return hasDiscount ? 128 : 96;
  }

  double _widthTotalPrice({required bool hasDiscount}) {
    return hasDiscount ? 172 : 128;
  }

  CartBloc _bloc(BuildContext context) {
    return BlocProvider.of<CartBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('CartScreen');
