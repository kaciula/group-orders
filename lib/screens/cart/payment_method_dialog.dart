import 'package:flutter/material.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/screens/widgets/buttons.dart';

class PaymentMethodDialog extends StatefulWidget {
  const PaymentMethodDialog(
      {Key? key, required this.participant, required this.onTap})
      : super(key: key);

  final Participant participant;
  final MarkPaidCallback onTap;

  @override
  _PaymentMethodDialogState createState() => _PaymentMethodDialogState();
}

class _PaymentMethodDialogState extends State<PaymentMethodDialog> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _detailsController = TextEditingController();

  @override
  void dispose() {
    _detailsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SimpleDialog(
        contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        title: Text(Strings.participantsPaymentMethodTitle),
        children: <Widget>[
          TextFormField(
            autofocus: true,
            controller: _detailsController,
            decoration: InputDecoration(
                hintText: Strings.participantsPaymentMethodHint),
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            textCapitalization: TextCapitalization.sentences,
            maxLength: 20,
          ),
          const SizedBox(height: 16),
          RaisedBtn(
            label: Strings.participantsMarkPaid,
            onPressed: () {
              final String paidDetails = _detailsController.text.trim();
              widget.onTap(paidDetails);
            },
          ),
        ],
      ),
    );
  }
}

typedef MarkPaidCallback = void Function(String paidDetails);
