import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/product.dart';
import 'package:orders/screens/cart/bloc/cart_bloc.dart';
import 'package:orders/screens/cart/bloc/cart_event.dart';
import 'package:orders/screens/cart/bloc/cart_product_entry_view.dart';
import 'package:orders/screens/utils/formatter_utils.dart';
import 'package:orders/screens/widgets/buttons.dart';

class EditCartEntryDialog extends StatefulWidget {
  const EditCartEntryDialog({
    Key? key,
    required this.cartBloc,
    required this.order,
    required this.product,
    required this.entryView,
  }) : super(key: key);

  final CartBloc cartBloc;
  final Order order;
  final Product product;
  final CartProductEntryView entryView;

  @override
  _EditCartEntryDialogState createState() => _EditCartEntryDialogState();
}

class _EditCartEntryDialogState extends State<EditCartEntryDialog>
    with AfterLayoutMixin<EditCartEntryDialog> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _priceController = TextEditingController();

  int _dropdownValue = 1;
  late double initialPrice;

  @override
  void afterFirstLayout(BuildContext context) {
    final CartProduct cartProduct = widget.entryView.cartProduct;
    setState(() {
      _dropdownValue = cartProduct.numItems;
    });
    initialPrice = cartProduct.modifiedPrice ??
        cartProduct.numItems * widget.product.price;
    _priceController.text = FormatterUtils.format(initialPrice);
  }

  @override
  void dispose() {
    _priceController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Stock stock = widget.product.stock;
    final CartProduct cartProduct = widget.entryView.cartProduct;
    int count = 9;
    if (stock is Limited &&
        stock.numItemsAvailable + cartProduct.numItems < count) {
      count = stock.numItemsAvailable + cartProduct.numItems;
    }
    final List<String> dropdownOptions =
        List<String>.generate(count, (int index) => (index + 1).toString());

    return Form(
      key: _formKey,
      child: SimpleDialog(
        contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        title: Text(widget.product.title),
        children: <Widget>[
          const SizedBox(height: 8),
          Text(
            widget.entryView.participantName,
            style: const TextStyle(fontSize: 16),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  Strings.orderNumItems,
                  style: const TextStyle(fontSize: 16),
                ),
              ),
              DropdownButton<String>(
                  value: _dropdownValue.toString(),
                  items: dropdownOptions
                      .map(
                        (String item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(item),
                        ),
                      )
                      .toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      _dropdownValue = int.parse(newValue!);
                      _priceController.text = FormatterUtils.format(
                          _dropdownValue * widget.product.price);
                    });
                  }),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  controller: _priceController,
                  decoration:
                      InputDecoration(hintText: Strings.editProductPrice),
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  validator: widget.cartBloc.priceValidator,
                  textInputAction: TextInputAction.done,
                ),
              ),
              const SizedBox(width: 16),
              Text(widget.order.currency),
            ],
          ),
          const SizedBox(height: 16),
          RaisedBtn(
              label: Strings.genericSave,
              onPressed: () {
                final FormState form = _formKey.currentState!;
                if (form.validate()) {
                  final String price = _priceController.text.trim();
                  final double modifiedPrice =
                      double.parse(price.replaceFirst(',', '.'));
                  widget.cartBloc.add(
                    ModifyEntryRequested(
                      cartProduct: widget.entryView.cartProduct,
                      numItems: _dropdownValue,
                      modifiedPrice:
                          initialPrice != modifiedPrice ? modifiedPrice : null,
                    ),
                  );
                  Navigator.pop(context);
                }
              }),
        ],
      ),
    );
  }
}

// ignore: unused_element
final Logger _logger = Logger('EditCartEntryDialog');
