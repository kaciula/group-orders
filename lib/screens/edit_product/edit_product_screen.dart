import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/app/app_styles.dart';
import 'package:orders/model/product.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_bloc.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_effect.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_event.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_state.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/utils/formatter_utils.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';

class EditProductScreen extends StatefulWidget {
  static const String routeName = 'edit_product';

  @override
  State<EditProductScreen> createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen>
    with AfterLayoutMixin<EditProductScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _detailsController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _numItemsController = TextEditingController();
  late FocusNode _detailsFocusNode;
  late FocusNode _numItemsFocusNode;
  _StockType _stockType = _StockType.unlimited;

  @override
  void initState() {
    super.initState();
    _detailsFocusNode = FocusNode();
    _numItemsFocusNode = FocusNode();

    _numItemsFocusNode.addListener(() {
      if (_numItemsFocusNode.hasFocus) {
        setState(() {
          _stockType = _StockType.limited;
        });
      }
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    final EditProductBloc bloc = BlocProvider.of<EditProductBloc>(context);
    final EditProductState state = bloc.state;
    if (state.isEdit) {
      final Product initialProduct = state.initialProduct!;
      _titleController.text = initialProduct.title;
      _detailsController.text = initialProduct.details;
      _priceController.text = FormatterUtils.format(initialProduct.price);
      if (state.isStockVisible) {
        final Stock stock = initialProduct.stock;
        if (stock is Limited) {
          setState(() {
            _stockType = _StockType.limited;
            _numItemsController.text = stock.numItemsAvailable.toString();
          });
        } else {
          _stockType = _StockType.unlimited;
        }
      }
    }
  }

  @override
  void dispose() {
    _titleController.dispose();
    _detailsController.dispose();
    _priceController.dispose();
    _numItemsController.dispose();
    _detailsFocusNode.dispose();
    _numItemsFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProductBloc, EditProductState>(
      bloc: BlocProvider.of<EditProductBloc>(context),
      buildWhen: EditProductState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, EditProductState state) {
    final EditProductEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToOrder) {
      Navigator.pop(context, true);
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, EditProductState state) {
    final EditProductBloc bloc = BlocProvider.of<EditProductBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(state.isEdit
                ? Strings.editProductEditTitle
                : Strings.editProductCreateTitle),
            Text(state.order.title, style: styleAppBarSubtitle),
          ],
        ),
      ),
      body: BlocListener<EditProductBloc, EditProductState>(
        bloc: bloc,
        listenWhen: EditProductState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, EditProductState state) {
    final EditProductBloc bloc = _bloc(context);
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            TextFormField(
              controller: _titleController,
              autofocus: true,
              decoration:
                  InputDecoration(hintText: Strings.editProductTitleField),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              textCapitalization: TextCapitalization.sentences,
              validator: bloc.titleValidator,
              onFieldSubmitted: (String value) =>
                  FocusScope.of(context).requestFocus(_detailsFocusNode),
            ),
            const SizedBox(height: 16),
            TextFormField(
              controller: _detailsController,
              decoration: InputDecoration(hintText: Strings.editProductDetails),
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              textCapitalization: TextCapitalization.sentences,
              minLines: 2,
              maxLines: 4,
            ),
            const SizedBox(height: 16),
            Row(
              children: <Widget>[
                Expanded(
                  child: TextFormField(
                    controller: _priceController,
                    decoration:
                        InputDecoration(hintText: Strings.editProductPrice),
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    validator: bloc.priceValidator,
                    textInputAction: TextInputAction.done,
                  ),
                ),
                const SizedBox(width: 16),
                Text(state.order.currency),
              ],
            ),
            if (state.isStockVisible) ..._stock(),
            const SizedBox(height: 16),
            RaisedBtn(
              label: state.isEdit
                  ? Strings.genericSave
                  : Strings.editProductCreate,
              onPressed: () {
                final FormState form = _formKey.currentState!;
                if (form.validate()) {
                  final String title = _titleController.text.trim();
                  final String details = _detailsController.text.trim();
                  final String price = _priceController.text.trim();
                  Stock stock;
                  if (state.isStockVisible) {
                    if (_stockType == _StockType.unlimited) {
                      stock = Stock.unlimited();
                    } else {
                      stock = Stock.limited(
                          numItemsAvailable:
                              int.parse(_numItemsController.text.trim()));
                    }
                  } else {
                    stock = Stock.unlimited();
                  }
                  bloc.add(SaveProductRequested(
                    title: title,
                    details: details,
                    price: double.parse(price.replaceFirst(',', '.')),
                    stock: stock,
                  ));
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _stock() {
    return <Widget>[
      const SizedBox(height: 24),
      Text(
        Strings.editProductStock,
        style: const TextStyle(fontSize: 16),
      ),
      const SizedBox(height: 4),
      RadioListTile<_StockType>(
          title: Text(Strings.editProductUnlimited),
          value: _StockType.unlimited,
          groupValue: _stockType,
          activeColor: AppColors.accent,
          onChanged: (_StockType? value) {
            setState(() {
              _stockType = value!;
              _numItemsFocusNode.unfocus();
            });
          }),
      RadioListTile<_StockType>(
          title: Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  controller: _numItemsController,
                  focusNode: _numItemsFocusNode,
                  decoration: InputDecoration(hintText: Strings.editProductNum),
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  validator: _stockType == _StockType.limited
                      ? _bloc(context).numItemsValidator
                      : null,
                  textInputAction: TextInputAction.done,
                ),
              ),
              const SizedBox(width: 16),
              Text(Strings.editProductNumItemsAvailable),
            ],
          ),
          value: _StockType.limited,
          groupValue: _stockType,
          activeColor: AppColors.accent,
          onChanged: (_StockType? value) {
            setState(() {
              _stockType = value!;
            });
          }),
    ];
  }

  EditProductBloc _bloc(BuildContext context) {
    return BlocProvider.of<EditProductBloc>(context);
  }
}

class EditProductArgs {
  EditProductArgs({required this.orderBloc, this.product});

  final OrderBloc orderBloc;
  final Product? product;
}

enum _StockType { unlimited, limited }

// ignore: unused_element
final Logger _logger = Logger('EditProductScreen');
