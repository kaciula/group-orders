import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/model/product.dart';
import 'package:orders/model/validators.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_effect.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_event.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_state.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/save_product.dart';

class EditProductBloc extends Bloc<EditProductEvent, EditProductState> {
  EditProductBloc(this.dataBloc, this.orderBloc, {Product? initialProduct})
      : super(EditProductState.initial(
          orderBloc.state.order,
          dataBloc.user,
          initialProduct: initialProduct,
        ));

  final DataBloc dataBloc;
  final OrderBloc orderBloc;

  final SaveProduct _saveProduct = getIt<SaveProduct>();

  @override
  Stream<EditProductState> mapEventToState(EditProductEvent event) async* {
    if (event is SaveProductRequested) {
      yield state.copyWith(inProgressAction: true);
      final SaveProductResult result = await _saveProduct(
        orderId: state.order.id,
        title: event.title,
        details: event.details,
        price: event.price,
        currency: state.order.currency,
        productId: state.initialProduct?.id,
        stock: event.stock,
      );
      if (result is SaveProductSuccess) {
        orderBloc.add(state.isEdit
            ? ProductUpdated(result.product)
            : ProductCreated(result.product));
        yield* _withEffect(state, GoToOrder());
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.editProductErrorMsg));
      }
    }
  }

  String? titleValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    return null;
  }

  String? priceValidator(String? value) {
    return Validators.price(
      value: value,
      invalidMsg: Strings.editProductInvalidPrice,
    );
  }

  String? numItemsValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    final int? val = int.tryParse(value?.trim() ?? '');
    if (val == null) {
      return Strings.editProductInvalidStock;
    }
    if (val < 0) {
      return Strings.editProductInvalidStock;
    }
    return null;
  }

  Stream<EditProductState> _withEffect(
      EditProductState state, EditProductEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('EditProductBloc');
