import 'package:freezed_annotation/freezed_annotation.dart';

part 'edit_product_effect.freezed.dart';

@freezed
class EditProductEffect with _$EditProductEffect {
  factory EditProductEffect.goToOrder() = GoToOrder;

  factory EditProductEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory EditProductEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
