import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/product.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/edit_product/bloc/edit_product_effect.dart';

part 'edit_product_state.freezed.dart';

@freezed
class EditProductState with _$EditProductState {
  factory EditProductState({
    required Order order,
    required User user,
    required bool inProgressAction,
    Product? initialProduct,
    EditProductEffect? effect,
  }) = _EditProductState;

  EditProductState._();

  factory EditProductState.initial(Order order, User user,
      {Product? initialProduct}) {
    return EditProductState(
      order: order,
      user: user,
      inProgressAction: false,
      initialProduct: initialProduct,
    );
  }

  static bool buildWhen(EditProductState previous, EditProductState current) {
    return previous.order != current.order ||
        previous.user != current.user ||
        previous.inProgressAction != current.inProgressAction ||
        previous.initialProduct != current.initialProduct;
  }

  static bool listenWhen(EditProductState previous, EditProductState current) {
    return current.effect != null;
  }

  bool get isEdit => initialProduct != null;

  bool get isStockVisible => order.ownerId == user.id;
}
