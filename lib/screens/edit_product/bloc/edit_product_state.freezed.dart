// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'edit_product_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$EditProductState {
  Order get order => throw _privateConstructorUsedError;
  User get user => throw _privateConstructorUsedError;
  bool get inProgressAction => throw _privateConstructorUsedError;
  Product? get initialProduct => throw _privateConstructorUsedError;
  EditProductEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $EditProductStateCopyWith<EditProductState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EditProductStateCopyWith<$Res> {
  factory $EditProductStateCopyWith(
          EditProductState value, $Res Function(EditProductState) then) =
      _$EditProductStateCopyWithImpl<$Res>;
  $Res call(
      {Order order,
      User user,
      bool inProgressAction,
      Product? initialProduct,
      EditProductEffect? effect});

  $OrderCopyWith<$Res> get order;
  $UserCopyWith<$Res> get user;
  $ProductCopyWith<$Res>? get initialProduct;
  $EditProductEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$EditProductStateCopyWithImpl<$Res>
    implements $EditProductStateCopyWith<$Res> {
  _$EditProductStateCopyWithImpl(this._value, this._then);

  final EditProductState _value;
  // ignore: unused_field
  final $Res Function(EditProductState) _then;

  @override
  $Res call({
    Object? order = freezed,
    Object? user = freezed,
    Object? inProgressAction = freezed,
    Object? initialProduct = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      initialProduct: initialProduct == freezed
          ? _value.initialProduct
          : initialProduct // ignore: cast_nullable_to_non_nullable
              as Product?,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as EditProductEffect?,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }

  @override
  $ProductCopyWith<$Res>? get initialProduct {
    if (_value.initialProduct == null) {
      return null;
    }

    return $ProductCopyWith<$Res>(_value.initialProduct!, (value) {
      return _then(_value.copyWith(initialProduct: value));
    });
  }

  @override
  $EditProductEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $EditProductEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_EditProductStateCopyWith<$Res>
    implements $EditProductStateCopyWith<$Res> {
  factory _$$_EditProductStateCopyWith(
          _$_EditProductState value, $Res Function(_$_EditProductState) then) =
      __$$_EditProductStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Order order,
      User user,
      bool inProgressAction,
      Product? initialProduct,
      EditProductEffect? effect});

  @override
  $OrderCopyWith<$Res> get order;
  @override
  $UserCopyWith<$Res> get user;
  @override
  $ProductCopyWith<$Res>? get initialProduct;
  @override
  $EditProductEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_EditProductStateCopyWithImpl<$Res>
    extends _$EditProductStateCopyWithImpl<$Res>
    implements _$$_EditProductStateCopyWith<$Res> {
  __$$_EditProductStateCopyWithImpl(
      _$_EditProductState _value, $Res Function(_$_EditProductState) _then)
      : super(_value, (v) => _then(v as _$_EditProductState));

  @override
  _$_EditProductState get _value => super._value as _$_EditProductState;

  @override
  $Res call({
    Object? order = freezed,
    Object? user = freezed,
    Object? inProgressAction = freezed,
    Object? initialProduct = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_EditProductState(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      initialProduct: initialProduct == freezed
          ? _value.initialProduct
          : initialProduct // ignore: cast_nullable_to_non_nullable
              as Product?,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as EditProductEffect?,
    ));
  }
}

/// @nodoc

class _$_EditProductState extends _EditProductState {
  _$_EditProductState(
      {required this.order,
      required this.user,
      required this.inProgressAction,
      this.initialProduct,
      this.effect})
      : super._();

  @override
  final Order order;
  @override
  final User user;
  @override
  final bool inProgressAction;
  @override
  final Product? initialProduct;
  @override
  final EditProductEffect? effect;

  @override
  String toString() {
    return 'EditProductState(order: $order, user: $user, inProgressAction: $inProgressAction, initialProduct: $initialProduct, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_EditProductState &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality()
                .equals(other.initialProduct, initialProduct) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(initialProduct),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_EditProductStateCopyWith<_$_EditProductState> get copyWith =>
      __$$_EditProductStateCopyWithImpl<_$_EditProductState>(this, _$identity);
}

abstract class _EditProductState extends EditProductState {
  factory _EditProductState(
      {required final Order order,
      required final User user,
      required final bool inProgressAction,
      final Product? initialProduct,
      final EditProductEffect? effect}) = _$_EditProductState;
  _EditProductState._() : super._();

  @override
  Order get order;
  @override
  User get user;
  @override
  bool get inProgressAction;
  @override
  Product? get initialProduct;
  @override
  EditProductEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_EditProductStateCopyWith<_$_EditProductState> get copyWith =>
      throw _privateConstructorUsedError;
}
