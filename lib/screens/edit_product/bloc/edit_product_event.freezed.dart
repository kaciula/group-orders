// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'edit_product_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$EditProductEvent {
  String get title => throw _privateConstructorUsedError;
  String get details => throw _privateConstructorUsedError;
  double get price => throw _privateConstructorUsedError;
  Stock get stock => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String title, String details, double price, Stock stock)
        saveProductRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String title, String details, double price, Stock stock)?
        saveProductRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String title, String details, double price, Stock stock)?
        saveProductRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveProductRequested value) saveProductRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveProductRequested value)? saveProductRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveProductRequested value)? saveProductRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $EditProductEventCopyWith<EditProductEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EditProductEventCopyWith<$Res> {
  factory $EditProductEventCopyWith(
          EditProductEvent value, $Res Function(EditProductEvent) then) =
      _$EditProductEventCopyWithImpl<$Res>;
  $Res call({String title, String details, double price, Stock stock});

  $StockCopyWith<$Res> get stock;
}

/// @nodoc
class _$EditProductEventCopyWithImpl<$Res>
    implements $EditProductEventCopyWith<$Res> {
  _$EditProductEventCopyWithImpl(this._value, this._then);

  final EditProductEvent _value;
  // ignore: unused_field
  final $Res Function(EditProductEvent) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? details = freezed,
    Object? price = freezed,
    Object? stock = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double,
      stock: stock == freezed
          ? _value.stock
          : stock // ignore: cast_nullable_to_non_nullable
              as Stock,
    ));
  }

  @override
  $StockCopyWith<$Res> get stock {
    return $StockCopyWith<$Res>(_value.stock, (value) {
      return _then(_value.copyWith(stock: value));
    });
  }
}

/// @nodoc
abstract class _$$SaveProductRequestedCopyWith<$Res>
    implements $EditProductEventCopyWith<$Res> {
  factory _$$SaveProductRequestedCopyWith(_$SaveProductRequested value,
          $Res Function(_$SaveProductRequested) then) =
      __$$SaveProductRequestedCopyWithImpl<$Res>;
  @override
  $Res call({String title, String details, double price, Stock stock});

  @override
  $StockCopyWith<$Res> get stock;
}

/// @nodoc
class __$$SaveProductRequestedCopyWithImpl<$Res>
    extends _$EditProductEventCopyWithImpl<$Res>
    implements _$$SaveProductRequestedCopyWith<$Res> {
  __$$SaveProductRequestedCopyWithImpl(_$SaveProductRequested _value,
      $Res Function(_$SaveProductRequested) _then)
      : super(_value, (v) => _then(v as _$SaveProductRequested));

  @override
  _$SaveProductRequested get _value => super._value as _$SaveProductRequested;

  @override
  $Res call({
    Object? title = freezed,
    Object? details = freezed,
    Object? price = freezed,
    Object? stock = freezed,
  }) {
    return _then(_$SaveProductRequested(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double,
      stock: stock == freezed
          ? _value.stock
          : stock // ignore: cast_nullable_to_non_nullable
              as Stock,
    ));
  }
}

/// @nodoc

class _$SaveProductRequested implements SaveProductRequested {
  _$SaveProductRequested(
      {required this.title,
      required this.details,
      required this.price,
      required this.stock});

  @override
  final String title;
  @override
  final String details;
  @override
  final double price;
  @override
  final Stock stock;

  @override
  String toString() {
    return 'EditProductEvent.saveProductRequested(title: $title, details: $details, price: $price, stock: $stock)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaveProductRequested &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.details, details) &&
            const DeepCollectionEquality().equals(other.price, price) &&
            const DeepCollectionEquality().equals(other.stock, stock));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(details),
      const DeepCollectionEquality().hash(price),
      const DeepCollectionEquality().hash(stock));

  @JsonKey(ignore: true)
  @override
  _$$SaveProductRequestedCopyWith<_$SaveProductRequested> get copyWith =>
      __$$SaveProductRequestedCopyWithImpl<_$SaveProductRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String title, String details, double price, Stock stock)
        saveProductRequested,
  }) {
    return saveProductRequested(title, details, price, stock);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String title, String details, double price, Stock stock)?
        saveProductRequested,
  }) {
    return saveProductRequested?.call(title, details, price, stock);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String title, String details, double price, Stock stock)?
        saveProductRequested,
    required TResult orElse(),
  }) {
    if (saveProductRequested != null) {
      return saveProductRequested(title, details, price, stock);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveProductRequested value) saveProductRequested,
  }) {
    return saveProductRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveProductRequested value)? saveProductRequested,
  }) {
    return saveProductRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveProductRequested value)? saveProductRequested,
    required TResult orElse(),
  }) {
    if (saveProductRequested != null) {
      return saveProductRequested(this);
    }
    return orElse();
  }
}

abstract class SaveProductRequested implements EditProductEvent {
  factory SaveProductRequested(
      {required final String title,
      required final String details,
      required final double price,
      required final Stock stock}) = _$SaveProductRequested;

  @override
  String get title;
  @override
  String get details;
  @override
  double get price;
  @override
  Stock get stock;
  @override
  @JsonKey(ignore: true)
  _$$SaveProductRequestedCopyWith<_$SaveProductRequested> get copyWith =>
      throw _privateConstructorUsedError;
}
