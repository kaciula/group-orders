import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/product.dart';

part 'edit_product_event.freezed.dart';

@freezed
class EditProductEvent with _$EditProductEvent {
  factory EditProductEvent.saveProductRequested({
    required String title,
    required String details,
    required double price,
    required Stock stock,
  }) = SaveProductRequested;
}
