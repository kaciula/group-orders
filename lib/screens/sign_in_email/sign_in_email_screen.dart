import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/home/home_screen.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_bloc.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_effect.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_event.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_state.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/verify_phone/verify_phone_screen.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';

class SignInEmailScreen extends StatefulWidget {
  static const String routeName = 'sign_in_email';

  @override
  State<SignInEmailScreen> createState() => _SignInEmailScreenState();
}

class _SignInEmailScreenState extends State<SignInEmailScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  late FocusNode passwordFocusNode;

  @override
  void initState() {
    super.initState();
    passwordFocusNode = FocusNode();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    passwordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignInEmailBloc, SignInEmailState>(
      bloc: BlocProvider.of<SignInEmailBloc>(context),
      buildWhen: SignInEmailState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, SignInEmailState state) {
    final SignInEmailEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToHome) {
      Navigator.pushNamedAndRemoveUntil(
          context, HomeScreen.routeName, (Route<dynamic> route) => false);
    }
    if (effect is GoToVerifyPhone) {
      Navigator.pushNamedAndRemoveUntil(
          context, VerifyPhoneScreen.routeName, (Route<dynamic> route) => false,
          arguments: effect.user);
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, SignInEmailState state) {
    final SignInEmailBloc bloc = BlocProvider.of<SignInEmailBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(title: Text(Strings.signInEmailTitle)),
      body: BlocListener<SignInEmailBloc, SignInEmailState>(
        bloc: bloc,
        listenWhen: SignInEmailState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, SignInEmailState state) {
    final SignInEmailBloc bloc = _bloc(context);
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            TextFormField(
              controller: _emailController,
              autofocus: true,
              decoration: InputDecoration(hintText: Strings.signUpInput),
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              validator: bloc.emailValidator,
              onFieldSubmitted: (String value) =>
                  FocusScope.of(context).requestFocus(passwordFocusNode),
            ),
            const SizedBox(height: 16),
            TextFormField(
              controller: _passwordController,
              decoration: InputDecoration(hintText: Strings.signUpPassword),
              keyboardType: TextInputType.visiblePassword,
              textInputAction: TextInputAction.next,
              validator: bloc.passwordValidator,
              onFieldSubmitted: (String value) =>
                  FocusScope.of(context).requestFocus(passwordFocusNode),
            ),
            const SizedBox(height: 16),
            RaisedBtn(
              label: Strings.signInEmailAction,
              onPressed: () {
                final FormState form = _formKey.currentState!;
                if (form.validate()) {
                  final String email = _emailController.text.trim();
                  final String password = _passwordController.text.trim();
                  _bloc(context).add(SignInRequested(
                    email: email,
                    password: password,
                  ));
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  SignInEmailBloc _bloc(BuildContext context) {
    return BlocProvider.of<SignInEmailBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('SignInEmailScreen');
