// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sign_in_email_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SignInEmailState {
  bool get inProgressAction => throw _privateConstructorUsedError;
  SignInEmailEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SignInEmailStateCopyWith<SignInEmailState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignInEmailStateCopyWith<$Res> {
  factory $SignInEmailStateCopyWith(
          SignInEmailState value, $Res Function(SignInEmailState) then) =
      _$SignInEmailStateCopyWithImpl<$Res>;
  $Res call({bool inProgressAction, SignInEmailEffect? effect});

  $SignInEmailEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$SignInEmailStateCopyWithImpl<$Res>
    implements $SignInEmailStateCopyWith<$Res> {
  _$SignInEmailStateCopyWithImpl(this._value, this._then);

  final SignInEmailState _value;
  // ignore: unused_field
  final $Res Function(SignInEmailState) _then;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as SignInEmailEffect?,
    ));
  }

  @override
  $SignInEmailEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $SignInEmailEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_SignInEmailStateCopyWith<$Res>
    implements $SignInEmailStateCopyWith<$Res> {
  factory _$$_SignInEmailStateCopyWith(
          _$_SignInEmailState value, $Res Function(_$_SignInEmailState) then) =
      __$$_SignInEmailStateCopyWithImpl<$Res>;
  @override
  $Res call({bool inProgressAction, SignInEmailEffect? effect});

  @override
  $SignInEmailEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_SignInEmailStateCopyWithImpl<$Res>
    extends _$SignInEmailStateCopyWithImpl<$Res>
    implements _$$_SignInEmailStateCopyWith<$Res> {
  __$$_SignInEmailStateCopyWithImpl(
      _$_SignInEmailState _value, $Res Function(_$_SignInEmailState) _then)
      : super(_value, (v) => _then(v as _$_SignInEmailState));

  @override
  _$_SignInEmailState get _value => super._value as _$_SignInEmailState;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_SignInEmailState(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as SignInEmailEffect?,
    ));
  }
}

/// @nodoc

class _$_SignInEmailState implements _SignInEmailState {
  _$_SignInEmailState({required this.inProgressAction, this.effect});

  @override
  final bool inProgressAction;
  @override
  final SignInEmailEffect? effect;

  @override
  String toString() {
    return 'SignInEmailState(inProgressAction: $inProgressAction, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SignInEmailState &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_SignInEmailStateCopyWith<_$_SignInEmailState> get copyWith =>
      __$$_SignInEmailStateCopyWithImpl<_$_SignInEmailState>(this, _$identity);
}

abstract class _SignInEmailState implements SignInEmailState {
  factory _SignInEmailState(
      {required final bool inProgressAction,
      final SignInEmailEffect? effect}) = _$_SignInEmailState;

  @override
  bool get inProgressAction;
  @override
  SignInEmailEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_SignInEmailStateCopyWith<_$_SignInEmailState> get copyWith =>
      throw _privateConstructorUsedError;
}
