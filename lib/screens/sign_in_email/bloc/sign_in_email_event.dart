import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_in_email_event.freezed.dart';

@freezed
class SignInEmailEvent with _$SignInEmailEvent {
  factory SignInEmailEvent.signInRequested({
    required String email,
    required String password,
  }) = SignInRequested;
}
