import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_effect.dart';

part 'sign_in_email_state.freezed.dart';

@freezed
class SignInEmailState with _$SignInEmailState {
  factory SignInEmailState({
    required bool inProgressAction,
    SignInEmailEffect? effect,
  }) = _SignInEmailState;

  factory SignInEmailState.initial() {
    return SignInEmailState(inProgressAction: false);
  }

  static bool buildWhen(SignInEmailState previous, SignInEmailState current) {
    return previous.inProgressAction != current.inProgressAction;
  }

  static bool listenWhen(SignInEmailState previous, SignInEmailState current) {
    return current.effect != null;
  }
}
