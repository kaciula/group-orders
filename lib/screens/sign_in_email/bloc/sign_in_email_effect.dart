import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/user.dart';

part 'sign_in_email_effect.freezed.dart';

@freezed
class SignInEmailEffect with _$SignInEmailEffect {
  factory SignInEmailEffect.goToHome() = GoToHome;

  factory SignInEmailEffect.goToVerifyPhone(User user) = GoToVerifyPhone;

  factory SignInEmailEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory SignInEmailEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
