import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/data/remote/connectors/connector_results.dart';
import 'package:orders/data/remote/connectors/email_connector.dart';
import 'package:orders/model/user.dart';
import 'package:orders/model/validators.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_effect.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_event.dart';
import 'package:orders/screens/sign_in_email/bloc/sign_in_email_state.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/check_user_existing.dart';
import 'package:orders/usecases/set_up_after_sign_in.dart';
import 'package:orders/usecases/use_case_results.dart';

class SignInEmailBloc extends Bloc<SignInEmailEvent, SignInEmailState> {
  SignInEmailBloc() : super(SignInEmailState.initial());

  final EmailConnector _emailConnector = getIt<EmailConnector>();
  final CheckUserExisting _checkUserExisting = getIt<CheckUserExisting>();
  final SetUpAfterSignIn _setUpAfterSignIn = getIt<SetUpAfterSignIn>();

  @override
  Stream<SignInEmailState> mapEventToState(SignInEmailEvent event) async* {
    if (event is SignInRequested) {
      yield state.copyWith(inProgressAction: true);
      final SignInResult result =
          await _emailConnector.signIn(event.email, event.password);
      if (result is SignInSuccess) {
        final User user = result.user;
        final CheckUserExistingResult existingResult =
            await _checkUserExisting(user);
        if (existingResult is CheckUserExistingSuccess) {
          if (existingResult.userExists) {
            final UseCaseResult useCaseResult =
                await _setUpAfterSignIn(result.user, checkHasAccount: true);
            if (useCaseResult is UseCaseSuccess) {
              yield* _withEffect(state, GoToHome());
            } else {
              yield* _withEffect(
                  state.copyWith(inProgressAction: false),
                  ShowErrorSnackBar(
                    errorMsg: Strings.signInEmailFailure,
                  ));
            }
          } else {
            yield* _withEffect(state, GoToVerifyPhone(user));
          }
        } else {
          yield* _withEffect(
            state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(
              errorMsg: '${Strings.connectSignInErrorMsg}.',
            ),
          );
        }
      } else {
        final SignInFailure failure = result as SignInFailure;
        yield* _withEffect(
          state.copyWith(inProgressAction: false),
          ShowErrorSnackBar(
            errorMsg:
                '${Strings.signInEmailFailure}. ${failureMsg(failure.type)}',
          ),
        );
      }
    }
  }

  String? emailValidator(String? value) {
    if (!Validators.isEmailValid(value?.trim())) {
      return Strings.genericInvalidEmail;
    }
    return null;
  }

  String? passwordValidator(String? value) {
    if (!Validators.isPasswordValid(value?.trim())) {
      return Strings.signUpInvalidPassword;
    }
    return null;
  }

  Stream<SignInEmailState> _withEffect(
      SignInEmailState state, SignInEmailEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('SignInEmailBloc');
