import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/home/bloc/home_effect.dart';
import 'package:orders/screens/utils/refresh_state.dart';

part 'home_state.freezed.dart';

@freezed
class HomeState with _$HomeState {
  factory HomeState({
    required RefreshState refreshState,
    required bool inProgressAction,
    required KtList<Order> activeOrders,
    required KtList<Order> pastOrders,
    HomeEffect? effect,
  }) = _HomeState;

  HomeState._();

  factory HomeState.initial({
    required KtList<Order> activeOrders,
    required KtList<Order> pastOrders,
  }) {
    return HomeState(
      refreshState: RefreshState.inProgress(),
      inProgressAction: false,
      activeOrders: activeOrders,
      pastOrders: pastOrders,
    );
  }

  bool get hasAtLeastOneOrder => activeOrders.size + pastOrders.size > 0;

  static bool buildWhen(HomeState previous, HomeState current) {
    return previous.refreshState != current.refreshState ||
        previous.inProgressAction != current.inProgressAction ||
        previous.activeOrders != current.activeOrders ||
        previous.pastOrders != current.pastOrders;
  }

  static bool listenWhen(HomeState previous, HomeState current) {
    return current.effect != null;
  }
}
