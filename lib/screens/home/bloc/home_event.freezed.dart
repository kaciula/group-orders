// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'home_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function() refreshRequested,
    required TResult Function() createOrderRequested,
    required TResult Function() profileRequested,
    required TResult Function(Order order) orderRequested,
    required TResult Function(
            KtList<Order> orders,
            String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)
        dataChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(CreateOrderRequested value) createOrderRequested,
    required TResult Function(ProfileRequested value) profileRequested,
    required TResult Function(OrderRequested value) orderRequested,
    required TResult Function(DataChanged value) dataChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeEventCopyWith<$Res> {
  factory $HomeEventCopyWith(HomeEvent value, $Res Function(HomeEvent) then) =
      _$HomeEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeEventCopyWithImpl<$Res> implements $HomeEventCopyWith<$Res> {
  _$HomeEventCopyWithImpl(this._value, this._then);

  final HomeEvent _value;
  // ignore: unused_field
  final $Res Function(HomeEvent) _then;
}

/// @nodoc
abstract class _$$ScreenStartedCopyWith<$Res> {
  factory _$$ScreenStartedCopyWith(
          _$ScreenStarted value, $Res Function(_$ScreenStarted) then) =
      __$$ScreenStartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ScreenStartedCopyWithImpl<$Res> extends _$HomeEventCopyWithImpl<$Res>
    implements _$$ScreenStartedCopyWith<$Res> {
  __$$ScreenStartedCopyWithImpl(
      _$ScreenStarted _value, $Res Function(_$ScreenStarted) _then)
      : super(_value, (v) => _then(v as _$ScreenStarted));

  @override
  _$ScreenStarted get _value => super._value as _$ScreenStarted;
}

/// @nodoc

class _$ScreenStarted implements ScreenStarted {
  _$ScreenStarted();

  @override
  String toString() {
    return 'HomeEvent.screenStarted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ScreenStarted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function() refreshRequested,
    required TResult Function() createOrderRequested,
    required TResult Function() profileRequested,
    required TResult Function(Order order) orderRequested,
    required TResult Function(
            KtList<Order> orders,
            String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)
        dataChanged,
  }) {
    return screenStarted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
  }) {
    return screenStarted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
    required TResult orElse(),
  }) {
    if (screenStarted != null) {
      return screenStarted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(CreateOrderRequested value) createOrderRequested,
    required TResult Function(ProfileRequested value) profileRequested,
    required TResult Function(OrderRequested value) orderRequested,
    required TResult Function(DataChanged value) dataChanged,
  }) {
    return screenStarted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
  }) {
    return screenStarted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
    required TResult orElse(),
  }) {
    if (screenStarted != null) {
      return screenStarted(this);
    }
    return orElse();
  }
}

abstract class ScreenStarted implements HomeEvent {
  factory ScreenStarted() = _$ScreenStarted;
}

/// @nodoc
abstract class _$$AppResumedCopyWith<$Res> {
  factory _$$AppResumedCopyWith(
          _$AppResumed value, $Res Function(_$AppResumed) then) =
      __$$AppResumedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AppResumedCopyWithImpl<$Res> extends _$HomeEventCopyWithImpl<$Res>
    implements _$$AppResumedCopyWith<$Res> {
  __$$AppResumedCopyWithImpl(
      _$AppResumed _value, $Res Function(_$AppResumed) _then)
      : super(_value, (v) => _then(v as _$AppResumed));

  @override
  _$AppResumed get _value => super._value as _$AppResumed;
}

/// @nodoc

class _$AppResumed implements AppResumed {
  _$AppResumed();

  @override
  String toString() {
    return 'HomeEvent.appResumed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AppResumed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function() refreshRequested,
    required TResult Function() createOrderRequested,
    required TResult Function() profileRequested,
    required TResult Function(Order order) orderRequested,
    required TResult Function(
            KtList<Order> orders,
            String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)
        dataChanged,
  }) {
    return appResumed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
  }) {
    return appResumed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
    required TResult orElse(),
  }) {
    if (appResumed != null) {
      return appResumed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(CreateOrderRequested value) createOrderRequested,
    required TResult Function(ProfileRequested value) profileRequested,
    required TResult Function(OrderRequested value) orderRequested,
    required TResult Function(DataChanged value) dataChanged,
  }) {
    return appResumed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
  }) {
    return appResumed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
    required TResult orElse(),
  }) {
    if (appResumed != null) {
      return appResumed(this);
    }
    return orElse();
  }
}

abstract class AppResumed implements HomeEvent {
  factory AppResumed() = _$AppResumed;
}

/// @nodoc
abstract class _$$RefreshRequestedCopyWith<$Res> {
  factory _$$RefreshRequestedCopyWith(
          _$RefreshRequested value, $Res Function(_$RefreshRequested) then) =
      __$$RefreshRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RefreshRequestedCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res>
    implements _$$RefreshRequestedCopyWith<$Res> {
  __$$RefreshRequestedCopyWithImpl(
      _$RefreshRequested _value, $Res Function(_$RefreshRequested) _then)
      : super(_value, (v) => _then(v as _$RefreshRequested));

  @override
  _$RefreshRequested get _value => super._value as _$RefreshRequested;
}

/// @nodoc

class _$RefreshRequested implements RefreshRequested {
  _$RefreshRequested();

  @override
  String toString() {
    return 'HomeEvent.refreshRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RefreshRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function() refreshRequested,
    required TResult Function() createOrderRequested,
    required TResult Function() profileRequested,
    required TResult Function(Order order) orderRequested,
    required TResult Function(
            KtList<Order> orders,
            String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)
        dataChanged,
  }) {
    return refreshRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
  }) {
    return refreshRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
    required TResult orElse(),
  }) {
    if (refreshRequested != null) {
      return refreshRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(CreateOrderRequested value) createOrderRequested,
    required TResult Function(ProfileRequested value) profileRequested,
    required TResult Function(OrderRequested value) orderRequested,
    required TResult Function(DataChanged value) dataChanged,
  }) {
    return refreshRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
  }) {
    return refreshRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
    required TResult orElse(),
  }) {
    if (refreshRequested != null) {
      return refreshRequested(this);
    }
    return orElse();
  }
}

abstract class RefreshRequested implements HomeEvent {
  factory RefreshRequested() = _$RefreshRequested;
}

/// @nodoc
abstract class _$$CreateOrderRequestedCopyWith<$Res> {
  factory _$$CreateOrderRequestedCopyWith(_$CreateOrderRequested value,
          $Res Function(_$CreateOrderRequested) then) =
      __$$CreateOrderRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CreateOrderRequestedCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res>
    implements _$$CreateOrderRequestedCopyWith<$Res> {
  __$$CreateOrderRequestedCopyWithImpl(_$CreateOrderRequested _value,
      $Res Function(_$CreateOrderRequested) _then)
      : super(_value, (v) => _then(v as _$CreateOrderRequested));

  @override
  _$CreateOrderRequested get _value => super._value as _$CreateOrderRequested;
}

/// @nodoc

class _$CreateOrderRequested implements CreateOrderRequested {
  _$CreateOrderRequested();

  @override
  String toString() {
    return 'HomeEvent.createOrderRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CreateOrderRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function() refreshRequested,
    required TResult Function() createOrderRequested,
    required TResult Function() profileRequested,
    required TResult Function(Order order) orderRequested,
    required TResult Function(
            KtList<Order> orders,
            String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)
        dataChanged,
  }) {
    return createOrderRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
  }) {
    return createOrderRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
    required TResult orElse(),
  }) {
    if (createOrderRequested != null) {
      return createOrderRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(CreateOrderRequested value) createOrderRequested,
    required TResult Function(ProfileRequested value) profileRequested,
    required TResult Function(OrderRequested value) orderRequested,
    required TResult Function(DataChanged value) dataChanged,
  }) {
    return createOrderRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
  }) {
    return createOrderRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
    required TResult orElse(),
  }) {
    if (createOrderRequested != null) {
      return createOrderRequested(this);
    }
    return orElse();
  }
}

abstract class CreateOrderRequested implements HomeEvent {
  factory CreateOrderRequested() = _$CreateOrderRequested;
}

/// @nodoc
abstract class _$$ProfileRequestedCopyWith<$Res> {
  factory _$$ProfileRequestedCopyWith(
          _$ProfileRequested value, $Res Function(_$ProfileRequested) then) =
      __$$ProfileRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileRequestedCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res>
    implements _$$ProfileRequestedCopyWith<$Res> {
  __$$ProfileRequestedCopyWithImpl(
      _$ProfileRequested _value, $Res Function(_$ProfileRequested) _then)
      : super(_value, (v) => _then(v as _$ProfileRequested));

  @override
  _$ProfileRequested get _value => super._value as _$ProfileRequested;
}

/// @nodoc

class _$ProfileRequested implements ProfileRequested {
  _$ProfileRequested();

  @override
  String toString() {
    return 'HomeEvent.profileRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function() refreshRequested,
    required TResult Function() createOrderRequested,
    required TResult Function() profileRequested,
    required TResult Function(Order order) orderRequested,
    required TResult Function(
            KtList<Order> orders,
            String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)
        dataChanged,
  }) {
    return profileRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
  }) {
    return profileRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
    required TResult orElse(),
  }) {
    if (profileRequested != null) {
      return profileRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(CreateOrderRequested value) createOrderRequested,
    required TResult Function(ProfileRequested value) profileRequested,
    required TResult Function(OrderRequested value) orderRequested,
    required TResult Function(DataChanged value) dataChanged,
  }) {
    return profileRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
  }) {
    return profileRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
    required TResult orElse(),
  }) {
    if (profileRequested != null) {
      return profileRequested(this);
    }
    return orElse();
  }
}

abstract class ProfileRequested implements HomeEvent {
  factory ProfileRequested() = _$ProfileRequested;
}

/// @nodoc
abstract class _$$OrderRequestedCopyWith<$Res> {
  factory _$$OrderRequestedCopyWith(
          _$OrderRequested value, $Res Function(_$OrderRequested) then) =
      __$$OrderRequestedCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$OrderRequestedCopyWithImpl<$Res> extends _$HomeEventCopyWithImpl<$Res>
    implements _$$OrderRequestedCopyWith<$Res> {
  __$$OrderRequestedCopyWithImpl(
      _$OrderRequested _value, $Res Function(_$OrderRequested) _then)
      : super(_value, (v) => _then(v as _$OrderRequested));

  @override
  _$OrderRequested get _value => super._value as _$OrderRequested;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$OrderRequested(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$OrderRequested implements OrderRequested {
  _$OrderRequested({required this.order});

  @override
  final Order order;

  @override
  String toString() {
    return 'HomeEvent.orderRequested(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrderRequested &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$OrderRequestedCopyWith<_$OrderRequested> get copyWith =>
      __$$OrderRequestedCopyWithImpl<_$OrderRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function() refreshRequested,
    required TResult Function() createOrderRequested,
    required TResult Function() profileRequested,
    required TResult Function(Order order) orderRequested,
    required TResult Function(
            KtList<Order> orders,
            String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)
        dataChanged,
  }) {
    return orderRequested(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
  }) {
    return orderRequested?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
    required TResult orElse(),
  }) {
    if (orderRequested != null) {
      return orderRequested(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(CreateOrderRequested value) createOrderRequested,
    required TResult Function(ProfileRequested value) profileRequested,
    required TResult Function(OrderRequested value) orderRequested,
    required TResult Function(DataChanged value) dataChanged,
  }) {
    return orderRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
  }) {
    return orderRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
    required TResult orElse(),
  }) {
    if (orderRequested != null) {
      return orderRequested(this);
    }
    return orElse();
  }
}

abstract class OrderRequested implements HomeEvent {
  factory OrderRequested({required final Order order}) = _$OrderRequested;

  Order get order;
  @JsonKey(ignore: true)
  _$$OrderRequestedCopyWith<_$OrderRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DataChangedCopyWith<$Res> {
  factory _$$DataChangedCopyWith(
          _$DataChanged value, $Res Function(_$DataChanged) then) =
      __$$DataChangedCopyWithImpl<$Res>;
  $Res call(
      {KtList<Order> orders,
      String? pendingInviteOrderId,
      PendingPushNotification? pendingPushNotification});

  $PendingPushNotificationCopyWith<$Res>? get pendingPushNotification;
}

/// @nodoc
class __$$DataChangedCopyWithImpl<$Res> extends _$HomeEventCopyWithImpl<$Res>
    implements _$$DataChangedCopyWith<$Res> {
  __$$DataChangedCopyWithImpl(
      _$DataChanged _value, $Res Function(_$DataChanged) _then)
      : super(_value, (v) => _then(v as _$DataChanged));

  @override
  _$DataChanged get _value => super._value as _$DataChanged;

  @override
  $Res call({
    Object? orders = freezed,
    Object? pendingInviteOrderId = freezed,
    Object? pendingPushNotification = freezed,
  }) {
    return _then(_$DataChanged(
      orders: orders == freezed
          ? _value.orders
          : orders // ignore: cast_nullable_to_non_nullable
              as KtList<Order>,
      pendingInviteOrderId: pendingInviteOrderId == freezed
          ? _value.pendingInviteOrderId
          : pendingInviteOrderId // ignore: cast_nullable_to_non_nullable
              as String?,
      pendingPushNotification: pendingPushNotification == freezed
          ? _value.pendingPushNotification
          : pendingPushNotification // ignore: cast_nullable_to_non_nullable
              as PendingPushNotification?,
    ));
  }

  @override
  $PendingPushNotificationCopyWith<$Res>? get pendingPushNotification {
    if (_value.pendingPushNotification == null) {
      return null;
    }

    return $PendingPushNotificationCopyWith<$Res>(
        _value.pendingPushNotification!, (value) {
      return _then(_value.copyWith(pendingPushNotification: value));
    });
  }
}

/// @nodoc

class _$DataChanged implements DataChanged {
  _$DataChanged(
      {required this.orders,
      this.pendingInviteOrderId,
      this.pendingPushNotification});

  @override
  final KtList<Order> orders;
  @override
  final String? pendingInviteOrderId;
  @override
  final PendingPushNotification? pendingPushNotification;

  @override
  String toString() {
    return 'HomeEvent.dataChanged(orders: $orders, pendingInviteOrderId: $pendingInviteOrderId, pendingPushNotification: $pendingPushNotification)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataChanged &&
            const DeepCollectionEquality().equals(other.orders, orders) &&
            const DeepCollectionEquality()
                .equals(other.pendingInviteOrderId, pendingInviteOrderId) &&
            const DeepCollectionEquality().equals(
                other.pendingPushNotification, pendingPushNotification));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(orders),
      const DeepCollectionEquality().hash(pendingInviteOrderId),
      const DeepCollectionEquality().hash(pendingPushNotification));

  @JsonKey(ignore: true)
  @override
  _$$DataChangedCopyWith<_$DataChanged> get copyWith =>
      __$$DataChangedCopyWithImpl<_$DataChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function() refreshRequested,
    required TResult Function() createOrderRequested,
    required TResult Function() profileRequested,
    required TResult Function(Order order) orderRequested,
    required TResult Function(
            KtList<Order> orders,
            String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)
        dataChanged,
  }) {
    return dataChanged(orders, pendingInviteOrderId, pendingPushNotification);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
  }) {
    return dataChanged?.call(
        orders, pendingInviteOrderId, pendingPushNotification);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function()? refreshRequested,
    TResult Function()? createOrderRequested,
    TResult Function()? profileRequested,
    TResult Function(Order order)? orderRequested,
    TResult Function(KtList<Order> orders, String? pendingInviteOrderId,
            PendingPushNotification? pendingPushNotification)?
        dataChanged,
    required TResult orElse(),
  }) {
    if (dataChanged != null) {
      return dataChanged(orders, pendingInviteOrderId, pendingPushNotification);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(CreateOrderRequested value) createOrderRequested,
    required TResult Function(ProfileRequested value) profileRequested,
    required TResult Function(OrderRequested value) orderRequested,
    required TResult Function(DataChanged value) dataChanged,
  }) {
    return dataChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
  }) {
    return dataChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(CreateOrderRequested value)? createOrderRequested,
    TResult Function(ProfileRequested value)? profileRequested,
    TResult Function(OrderRequested value)? orderRequested,
    TResult Function(DataChanged value)? dataChanged,
    required TResult orElse(),
  }) {
    if (dataChanged != null) {
      return dataChanged(this);
    }
    return orElse();
  }
}

abstract class DataChanged implements HomeEvent {
  factory DataChanged(
      {required final KtList<Order> orders,
      final String? pendingInviteOrderId,
      final PendingPushNotification? pendingPushNotification}) = _$DataChanged;

  KtList<Order> get orders;
  String? get pendingInviteOrderId;
  PendingPushNotification? get pendingPushNotification;
  @JsonKey(ignore: true)
  _$$DataChangedCopyWith<_$DataChanged> get copyWith =>
      throw _privateConstructorUsedError;
}
