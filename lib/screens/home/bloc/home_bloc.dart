import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:kt_dart/kt.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/blocs/data_bloc/data_state.dart';
import 'package:orders/blocs/data_bloc/pending_push_notification.dart';
import 'package:orders/data/local/store/app_info_provider.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/home/bloc/home_effect.dart';
import 'package:orders/screens/home/bloc/home_event.dart';
import 'package:orders/screens/home/bloc/home_state.dart';
import 'package:orders/screens/utils/refresh_state.dart';
import 'package:orders/service_locator.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(this.dataBloc)
      : super(HomeState.initial(
          activeOrders: _activeOrders(dataBloc.orders),
          pastOrders: _pastOrders(dataBloc.orders),
        )) {
    dataSub = dataBloc.stream.distinct((DataState previous, DataState next) {
      return previous.orders == next.orders &&
          previous.pendingInviteOrderId == next.pendingInviteOrderId &&
          previous.pendingPushNotification == next.pendingPushNotification;
    }).listen((DataState state) {
      _logger.fine('Received new state from data bloc $state');
      add(DataChanged(
        orders: state.orders.toList(),
        pendingInviteOrderId: state.pendingInviteOrderId,
        pendingPushNotification: state.pendingPushNotification,
      ));
    });
  }

  final DataBloc dataBloc;
  late final StreamSubscription<DataState> dataSub;

  final DataRemoteStore _dataRemoteStore = getIt<DataRemoteStore>();
  final AppInfoProvider _appInfoProvider = getIt<AppInfoProvider>();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is ScreenStarted) {
      yield* _refresh(state);
      if (dataBloc.pendingInviteOrderId != null) {
        yield* _dealWithPendingInvite(state, dataBloc.pendingInviteOrderId!);
      }
      if (dataBloc.pendingPushNotification != null) {
        yield* _dealWithPendingPushNotification(
            state, dataBloc.pendingPushNotification!);
      }
    }
    if (event is AppResumed) {
      final AppInfo appInfo = _appInfoProvider.get();
      final int millis = DateTime.now().millisecondsSinceEpoch -
          appInfo.timestampLastSyncRemote;
      final Duration duration = Duration(milliseconds: millis);
      if (duration.inMinutes >= 20) {
        _logger.fine('Time has elapsed since last remote sync. Force sync now');
        yield* _refresh(state);
      }
    }
    if (event is RefreshRequested) {
      yield* _refresh(state);
    }
    if (event is DataChanged) {
      if (event.pendingInviteOrderId != null) {
        yield* _dealWithPendingInvite(state, event.pendingInviteOrderId!);
      }
      if (event.pendingPushNotification != null) {
        yield* _dealWithPendingPushNotification(
            state, event.pendingPushNotification!);
      }
      yield state.copyWith(
        refreshState: RefreshState.success(),
        activeOrders: _activeOrders(event.orders),
        pastOrders: _pastOrders(event.orders),
      );
    }
    if (event is OrderRequested) {
      yield* _withEffect(state, GoToOrder(order: event.order));
    }
    if (event is CreateOrderRequested) {
      yield* _withEffect(state, GoToCreateOrder());
    }
    if (event is ProfileRequested) {
      yield* _withEffect(state, GoToProfile());
    }
  }

  Stream<HomeState> _dealWithPendingInvite(
    HomeState state,
    String pendingInviteOrderId,
  ) async* {
    dataBloc.add(InvitationShown());
    yield* _withEffect(state, GoToAcceptInvite(orderId: pendingInviteOrderId));
  }

  Stream<HomeState> _dealWithPendingPushNotification(
    HomeState state,
    PendingPushNotification pendingPushNotification,
  ) async* {
    dataBloc.add(PendingPushNotificationHandled());
    yield* _withEffect(
        state, GoToOrder(order: Order.empty(pendingPushNotification.orderId)));
  }

  Stream<HomeState> _refresh(HomeState state) async* {
    yield state.copyWith(refreshState: RefreshState.inProgress());
    final GetOrdersResult ordersResult =
        await _dataRemoteStore.getOrders(userId: dataBloc.userId);
    if (ordersResult is GetOrdersSuccess) {
      dataBloc.add(
        DataRefreshed(
            user: dataBloc.user,
            orders: ordersResult.orders.sortedByDescending(_sortOrders)),
      );
      await _appInfoProvider
          .saveTimestampLastSyncRemote(DateTime.now().millisecondsSinceEpoch);
      yield state.copyWith(refreshState: RefreshState.success());
    } else {
      yield state.copyWith(
          refreshState: RefreshState.error(isInternetConnected: true));
    }
  }

  static KtList<Order> _activeOrders(KtList<Order> orders) {
    return orders
        .filter((Order it) => it.isActive && !it.isDeleted)
        .sortedByDescending(_sortOrders);
  }

  static KtList<Order> _pastOrders(KtList<Order> orders) {
    return orders
        .filter((Order it) => !it.isActive && !it.isDeleted)
        .sortedByDescending(_sortOrders);
  }

  Stream<HomeState> _withEffect(HomeState state, HomeEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }

  @override
  Future<void> close() async {
    dataSub.cancel();
    super.close();
  }
}

Comparable<dynamic> _sortOrders(Order it) => it.creationDate;

// ignore: unused_element
final Logger _logger = Logger('HomeBloc');
