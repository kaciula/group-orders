import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/order.dart';

part 'home_effect.freezed.dart';

@freezed
class HomeEffect with _$HomeEffect {
  factory HomeEffect.goToCreateOrder() = GoToCreateOrder;

  factory HomeEffect.goToProfile() = GoToProfile;

  factory HomeEffect.goToAcceptInvite({required String orderId}) =
      GoToAcceptInvite;

  factory HomeEffect.goToOrder({required Order order}) = GoToOrder;

  factory HomeEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory HomeEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
