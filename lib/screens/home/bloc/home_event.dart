import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:orders/blocs/data_bloc/pending_push_notification.dart';
import 'package:orders/model/order.dart';

part 'home_event.freezed.dart';

@freezed
class HomeEvent with _$HomeEvent {
  factory HomeEvent.screenStarted() = ScreenStarted;

  factory HomeEvent.appResumed() = AppResumed;

  factory HomeEvent.refreshRequested() = RefreshRequested;

  factory HomeEvent.createOrderRequested() = CreateOrderRequested;

  factory HomeEvent.profileRequested() = ProfileRequested;

  factory HomeEvent.orderRequested({required Order order}) = OrderRequested;

  factory HomeEvent.dataChanged({
    required KtList<Order> orders,
    String? pendingInviteOrderId,
    PendingPushNotification? pendingPushNotification,
  }) = DataChanged;
}
