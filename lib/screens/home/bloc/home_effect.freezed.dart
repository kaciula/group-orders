// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'home_effect.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeEffect {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToCreateOrder,
    required TResult Function() goToProfile,
    required TResult Function(String orderId) goToAcceptInvite,
    required TResult Function(Order order) goToOrder,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToCreateOrder value) goToCreateOrder,
    required TResult Function(GoToProfile value) goToProfile,
    required TResult Function(GoToAcceptInvite value) goToAcceptInvite,
    required TResult Function(GoToOrder value) goToOrder,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeEffectCopyWith<$Res> {
  factory $HomeEffectCopyWith(
          HomeEffect value, $Res Function(HomeEffect) then) =
      _$HomeEffectCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeEffectCopyWithImpl<$Res> implements $HomeEffectCopyWith<$Res> {
  _$HomeEffectCopyWithImpl(this._value, this._then);

  final HomeEffect _value;
  // ignore: unused_field
  final $Res Function(HomeEffect) _then;
}

/// @nodoc
abstract class _$$GoToCreateOrderCopyWith<$Res> {
  factory _$$GoToCreateOrderCopyWith(
          _$GoToCreateOrder value, $Res Function(_$GoToCreateOrder) then) =
      __$$GoToCreateOrderCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GoToCreateOrderCopyWithImpl<$Res>
    extends _$HomeEffectCopyWithImpl<$Res>
    implements _$$GoToCreateOrderCopyWith<$Res> {
  __$$GoToCreateOrderCopyWithImpl(
      _$GoToCreateOrder _value, $Res Function(_$GoToCreateOrder) _then)
      : super(_value, (v) => _then(v as _$GoToCreateOrder));

  @override
  _$GoToCreateOrder get _value => super._value as _$GoToCreateOrder;
}

/// @nodoc

class _$GoToCreateOrder implements GoToCreateOrder {
  _$GoToCreateOrder();

  @override
  String toString() {
    return 'HomeEffect.goToCreateOrder()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GoToCreateOrder);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToCreateOrder,
    required TResult Function() goToProfile,
    required TResult Function(String orderId) goToAcceptInvite,
    required TResult Function(Order order) goToOrder,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToCreateOrder();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToCreateOrder?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToCreateOrder != null) {
      return goToCreateOrder();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToCreateOrder value) goToCreateOrder,
    required TResult Function(GoToProfile value) goToProfile,
    required TResult Function(GoToAcceptInvite value) goToAcceptInvite,
    required TResult Function(GoToOrder value) goToOrder,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToCreateOrder(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToCreateOrder?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToCreateOrder != null) {
      return goToCreateOrder(this);
    }
    return orElse();
  }
}

abstract class GoToCreateOrder implements HomeEffect {
  factory GoToCreateOrder() = _$GoToCreateOrder;
}

/// @nodoc
abstract class _$$GoToProfileCopyWith<$Res> {
  factory _$$GoToProfileCopyWith(
          _$GoToProfile value, $Res Function(_$GoToProfile) then) =
      __$$GoToProfileCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GoToProfileCopyWithImpl<$Res> extends _$HomeEffectCopyWithImpl<$Res>
    implements _$$GoToProfileCopyWith<$Res> {
  __$$GoToProfileCopyWithImpl(
      _$GoToProfile _value, $Res Function(_$GoToProfile) _then)
      : super(_value, (v) => _then(v as _$GoToProfile));

  @override
  _$GoToProfile get _value => super._value as _$GoToProfile;
}

/// @nodoc

class _$GoToProfile implements GoToProfile {
  _$GoToProfile();

  @override
  String toString() {
    return 'HomeEffect.goToProfile()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GoToProfile);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToCreateOrder,
    required TResult Function() goToProfile,
    required TResult Function(String orderId) goToAcceptInvite,
    required TResult Function(Order order) goToOrder,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToProfile();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToProfile?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToProfile != null) {
      return goToProfile();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToCreateOrder value) goToCreateOrder,
    required TResult Function(GoToProfile value) goToProfile,
    required TResult Function(GoToAcceptInvite value) goToAcceptInvite,
    required TResult Function(GoToOrder value) goToOrder,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToProfile(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToProfile?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToProfile != null) {
      return goToProfile(this);
    }
    return orElse();
  }
}

abstract class GoToProfile implements HomeEffect {
  factory GoToProfile() = _$GoToProfile;
}

/// @nodoc
abstract class _$$GoToAcceptInviteCopyWith<$Res> {
  factory _$$GoToAcceptInviteCopyWith(
          _$GoToAcceptInvite value, $Res Function(_$GoToAcceptInvite) then) =
      __$$GoToAcceptInviteCopyWithImpl<$Res>;
  $Res call({String orderId});
}

/// @nodoc
class __$$GoToAcceptInviteCopyWithImpl<$Res>
    extends _$HomeEffectCopyWithImpl<$Res>
    implements _$$GoToAcceptInviteCopyWith<$Res> {
  __$$GoToAcceptInviteCopyWithImpl(
      _$GoToAcceptInvite _value, $Res Function(_$GoToAcceptInvite) _then)
      : super(_value, (v) => _then(v as _$GoToAcceptInvite));

  @override
  _$GoToAcceptInvite get _value => super._value as _$GoToAcceptInvite;

  @override
  $Res call({
    Object? orderId = freezed,
  }) {
    return _then(_$GoToAcceptInvite(
      orderId: orderId == freezed
          ? _value.orderId
          : orderId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$GoToAcceptInvite implements GoToAcceptInvite {
  _$GoToAcceptInvite({required this.orderId});

  @override
  final String orderId;

  @override
  String toString() {
    return 'HomeEffect.goToAcceptInvite(orderId: $orderId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GoToAcceptInvite &&
            const DeepCollectionEquality().equals(other.orderId, orderId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(orderId));

  @JsonKey(ignore: true)
  @override
  _$$GoToAcceptInviteCopyWith<_$GoToAcceptInvite> get copyWith =>
      __$$GoToAcceptInviteCopyWithImpl<_$GoToAcceptInvite>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToCreateOrder,
    required TResult Function() goToProfile,
    required TResult Function(String orderId) goToAcceptInvite,
    required TResult Function(Order order) goToOrder,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToAcceptInvite(orderId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToAcceptInvite?.call(orderId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToAcceptInvite != null) {
      return goToAcceptInvite(orderId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToCreateOrder value) goToCreateOrder,
    required TResult Function(GoToProfile value) goToProfile,
    required TResult Function(GoToAcceptInvite value) goToAcceptInvite,
    required TResult Function(GoToOrder value) goToOrder,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToAcceptInvite(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToAcceptInvite?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToAcceptInvite != null) {
      return goToAcceptInvite(this);
    }
    return orElse();
  }
}

abstract class GoToAcceptInvite implements HomeEffect {
  factory GoToAcceptInvite({required final String orderId}) =
      _$GoToAcceptInvite;

  String get orderId;
  @JsonKey(ignore: true)
  _$$GoToAcceptInviteCopyWith<_$GoToAcceptInvite> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GoToOrderCopyWith<$Res> {
  factory _$$GoToOrderCopyWith(
          _$GoToOrder value, $Res Function(_$GoToOrder) then) =
      __$$GoToOrderCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$GoToOrderCopyWithImpl<$Res> extends _$HomeEffectCopyWithImpl<$Res>
    implements _$$GoToOrderCopyWith<$Res> {
  __$$GoToOrderCopyWithImpl(
      _$GoToOrder _value, $Res Function(_$GoToOrder) _then)
      : super(_value, (v) => _then(v as _$GoToOrder));

  @override
  _$GoToOrder get _value => super._value as _$GoToOrder;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$GoToOrder(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$GoToOrder implements GoToOrder {
  _$GoToOrder({required this.order});

  @override
  final Order order;

  @override
  String toString() {
    return 'HomeEffect.goToOrder(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GoToOrder &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$GoToOrderCopyWith<_$GoToOrder> get copyWith =>
      __$$GoToOrderCopyWithImpl<_$GoToOrder>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToCreateOrder,
    required TResult Function() goToProfile,
    required TResult Function(String orderId) goToAcceptInvite,
    required TResult Function(Order order) goToOrder,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToOrder(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToOrder?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToOrder != null) {
      return goToOrder(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToCreateOrder value) goToCreateOrder,
    required TResult Function(GoToProfile value) goToProfile,
    required TResult Function(GoToAcceptInvite value) goToAcceptInvite,
    required TResult Function(GoToOrder value) goToOrder,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToOrder(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToOrder?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToOrder != null) {
      return goToOrder(this);
    }
    return orElse();
  }
}

abstract class GoToOrder implements HomeEffect {
  factory GoToOrder({required final Order order}) = _$GoToOrder;

  Order get order;
  @JsonKey(ignore: true)
  _$$GoToOrderCopyWith<_$GoToOrder> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ShowInfoSnackBarCopyWith<$Res> {
  factory _$$ShowInfoSnackBarCopyWith(
          _$ShowInfoSnackBar value, $Res Function(_$ShowInfoSnackBar) then) =
      __$$ShowInfoSnackBarCopyWithImpl<$Res>;
  $Res call({String msg});
}

/// @nodoc
class __$$ShowInfoSnackBarCopyWithImpl<$Res>
    extends _$HomeEffectCopyWithImpl<$Res>
    implements _$$ShowInfoSnackBarCopyWith<$Res> {
  __$$ShowInfoSnackBarCopyWithImpl(
      _$ShowInfoSnackBar _value, $Res Function(_$ShowInfoSnackBar) _then)
      : super(_value, (v) => _then(v as _$ShowInfoSnackBar));

  @override
  _$ShowInfoSnackBar get _value => super._value as _$ShowInfoSnackBar;

  @override
  $Res call({
    Object? msg = freezed,
  }) {
    return _then(_$ShowInfoSnackBar(
      msg == freezed
          ? _value.msg
          : msg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ShowInfoSnackBar implements ShowInfoSnackBar {
  _$ShowInfoSnackBar(this.msg);

  @override
  final String msg;

  @override
  String toString() {
    return 'HomeEffect.showInfoSnackBar(msg: $msg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShowInfoSnackBar &&
            const DeepCollectionEquality().equals(other.msg, msg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(msg));

  @JsonKey(ignore: true)
  @override
  _$$ShowInfoSnackBarCopyWith<_$ShowInfoSnackBar> get copyWith =>
      __$$ShowInfoSnackBarCopyWithImpl<_$ShowInfoSnackBar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToCreateOrder,
    required TResult Function() goToProfile,
    required TResult Function(String orderId) goToAcceptInvite,
    required TResult Function(Order order) goToOrder,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return showInfoSnackBar(msg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return showInfoSnackBar?.call(msg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showInfoSnackBar != null) {
      return showInfoSnackBar(msg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToCreateOrder value) goToCreateOrder,
    required TResult Function(GoToProfile value) goToProfile,
    required TResult Function(GoToAcceptInvite value) goToAcceptInvite,
    required TResult Function(GoToOrder value) goToOrder,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return showInfoSnackBar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return showInfoSnackBar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showInfoSnackBar != null) {
      return showInfoSnackBar(this);
    }
    return orElse();
  }
}

abstract class ShowInfoSnackBar implements HomeEffect {
  factory ShowInfoSnackBar(final String msg) = _$ShowInfoSnackBar;

  String get msg;
  @JsonKey(ignore: true)
  _$$ShowInfoSnackBarCopyWith<_$ShowInfoSnackBar> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ShowErrorSnackBarCopyWith<$Res> {
  factory _$$ShowErrorSnackBarCopyWith(
          _$ShowErrorSnackBar value, $Res Function(_$ShowErrorSnackBar) then) =
      __$$ShowErrorSnackBarCopyWithImpl<$Res>;
  $Res call({String errorMsg});
}

/// @nodoc
class __$$ShowErrorSnackBarCopyWithImpl<$Res>
    extends _$HomeEffectCopyWithImpl<$Res>
    implements _$$ShowErrorSnackBarCopyWith<$Res> {
  __$$ShowErrorSnackBarCopyWithImpl(
      _$ShowErrorSnackBar _value, $Res Function(_$ShowErrorSnackBar) _then)
      : super(_value, (v) => _then(v as _$ShowErrorSnackBar));

  @override
  _$ShowErrorSnackBar get _value => super._value as _$ShowErrorSnackBar;

  @override
  $Res call({
    Object? errorMsg = freezed,
  }) {
    return _then(_$ShowErrorSnackBar(
      errorMsg: errorMsg == freezed
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ShowErrorSnackBar implements ShowErrorSnackBar {
  _$ShowErrorSnackBar({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'HomeEffect.showErrorSnackBar(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShowErrorSnackBar &&
            const DeepCollectionEquality().equals(other.errorMsg, errorMsg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(errorMsg));

  @JsonKey(ignore: true)
  @override
  _$$ShowErrorSnackBarCopyWith<_$ShowErrorSnackBar> get copyWith =>
      __$$ShowErrorSnackBarCopyWithImpl<_$ShowErrorSnackBar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToCreateOrder,
    required TResult Function() goToProfile,
    required TResult Function(String orderId) goToAcceptInvite,
    required TResult Function(Order order) goToOrder,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return showErrorSnackBar(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return showErrorSnackBar?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToCreateOrder,
    TResult Function()? goToProfile,
    TResult Function(String orderId)? goToAcceptInvite,
    TResult Function(Order order)? goToOrder,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showErrorSnackBar != null) {
      return showErrorSnackBar(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToCreateOrder value) goToCreateOrder,
    required TResult Function(GoToProfile value) goToProfile,
    required TResult Function(GoToAcceptInvite value) goToAcceptInvite,
    required TResult Function(GoToOrder value) goToOrder,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return showErrorSnackBar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return showErrorSnackBar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToCreateOrder value)? goToCreateOrder,
    TResult Function(GoToProfile value)? goToProfile,
    TResult Function(GoToAcceptInvite value)? goToAcceptInvite,
    TResult Function(GoToOrder value)? goToOrder,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showErrorSnackBar != null) {
      return showErrorSnackBar(this);
    }
    return orElse();
  }
}

abstract class ShowErrorSnackBar implements HomeEffect {
  factory ShowErrorSnackBar({required final String errorMsg}) =
      _$ShowErrorSnackBar;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$ShowErrorSnackBarCopyWith<_$ShowErrorSnackBar> get copyWith =>
      throw _privateConstructorUsedError;
}
