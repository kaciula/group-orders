import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/app/app_styles.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/accept_invite/accept_invite_screen.dart';
import 'package:orders/screens/create_order/create_order_screen.dart';
import 'package:orders/screens/home/bloc/home_bloc.dart';
import 'package:orders/screens/home/bloc/home_effect.dart';
import 'package:orders/screens/home/bloc/home_event.dart';
import 'package:orders/screens/home/bloc/home_state.dart';
import 'package:orders/screens/order/order_screen.dart';
import 'package:orders/screens/profile/profile_screen.dart';
import 'package:orders/screens/utils/formatter_utils.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/circular_progress.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';
import 'package:orders/screens/widgets/refresh_error_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = 'home';

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  final RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _refreshController.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      _logger.fine('app paused');
    }
    if (state == AppLifecycleState.resumed) {
      _bloc(context).add(AppResumed());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      bloc: BlocProvider.of<HomeBloc>(context),
      buildWhen: HomeState.buildWhen,
      builder: _builder,
    );
  }

  Future<void> _effectListener(BuildContext context, HomeState state) async {
    final HomeEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToCreateOrder) {
      Navigator.pushNamed(context, CreateOrderScreen.routeName);
    }
    if (effect is GoToProfile) {
      Navigator.pushNamed(context, ProfileScreen.routeName);
    }
    if (effect is GoToOrder) {
      final OrderScreenPopArgs? popArgs = await Navigator.pushNamed(
          context, OrderScreen.routeName,
          arguments: effect.order);
      if (popArgs != null && popArgs.refreshOrders) {
        _bloc(context).add(RefreshRequested());
      }
    }
    if (effect is GoToAcceptInvite) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        AcceptInviteScreen.routeName,
        (Route<dynamic> route) => route.settings.name == HomeScreen.routeName,
        arguments: effect.orderId,
      );
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, HomeState state) {
    final HomeBloc bloc = BlocProvider.of<HomeBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.homeTitle),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.settings),
              onPressed: () => _bloc(context).add(ProfileRequested()))
        ],
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8),
        child: FloatingActionButton(
          child: Icon(
            Icons.add,
            size: 32,
            color: Theme.of(context).primaryIconTheme.color,
          ),
          onPressed: () => _bloc(context).add(CreateOrderRequested()),
        ),
      ),
      body: BlocListener<HomeBloc, HomeState>(
        bloc: bloc,
        listenWhen: HomeState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, HomeState state) {
    return state.refreshState.when(
      inProgress: () => _progress(),
      success: () => _content(context, state),
      error: (bool isInternetConnected) =>
          _error(context, isInternetConnected: isInternetConnected),
    );
  }

  Widget _content(BuildContext context, HomeState state) {
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: SmartRefresher(
        controller: _refreshController,
        onRefresh: () => _bloc(context).add(RefreshRequested()),
        child:
            state.hasAtLeastOneOrder ? _list(context, state) : _empty(context),
      ),
    );
  }

  Widget _list(BuildContext context, HomeState state) {
    final int numActive = state.activeOrders.size;
    final int numPast = state.pastOrders.size;
    final bool showHeaders = numActive > 0 && numPast > 0;
    return ListView.builder(
      padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
      itemCount: numActive + numPast,
      itemBuilder: (BuildContext context, int index) {
        final bool isActiveSection = index < numActive;
        final bool applyOpacity = !isActiveSection;
        Order order;
        bool applyHeader;
        if (isActiveSection) {
          order = state.activeOrders[index];
          applyHeader = index == 0;
        } else {
          order = state.pastOrders[index - numActive];
          applyHeader = index - numActive == 0;
        }
        if (showHeaders && applyHeader) {
          return _rowWithHeader(
            context,
            order,
            isActiveSection ? Strings.homeActiveOrders : Strings.homePastOrders,
            applyTopPadding: !isActiveSection,
            applyOpacity: applyOpacity,
          );
        } else {
          return Opacity(
            opacity: applyOpacity ? 0.5 : 1,
            child: _row(context, order),
          );
        }
      },
    );
  }

  Widget _rowWithHeader(BuildContext context, Order order, String headerText,
      {required bool applyTopPadding, required bool applyOpacity}) {
    return Padding(
      padding: EdgeInsets.only(top: applyTopPadding ? 16 : 0),
      child: Opacity(
        opacity: applyOpacity ? 0.5 : 1,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Text(
                headerText,
                style: const TextStyle(fontWeight: FontWeight.w600),
              ),
            ),
            const SizedBox(height: 8),
            _row(context, order),
          ],
        ),
      ),
    );
  }

  Widget _row(BuildContext context, Order order) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Card(
        child: InkWell(
          splashColor: AppColors.accent.withOpacity(0.8),
          onTap: () => _bloc(context).add(OrderRequested(order: order)),
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        order.title,
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      const SizedBox(height: 8),
                      Text(
                        order.ownerName,
                        style: Theme.of(context).textTheme.subtitle2!.copyWith(
                              color: AppColors.textGreyDark,
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                      const SizedBox(height: 8),
                      Text(
                        FormatterUtils.date(order.creationDate),
                        style: Theme.of(context)
                            .textTheme
                            .caption!
                            .copyWith(fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                ),
                Text(
                  FormatterUtils.orderStatus(order.status),
                  style: const TextStyle(fontSize: 11),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _empty(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Text(
          Strings.homeEmpty,
          textAlign: TextAlign.center,
          style: styleEmpty,
        ),
      ),
    );
  }

  Widget _progress() {
    return const Center(child: CircularProgress());
  }

  Widget _error(BuildContext context, {required bool isInternetConnected}) {
    return RefreshErrorView(
      errorTitle: isInternetConnected
          ? Strings.genericErrorTitle
          : Strings.genericNoInternetTitle,
      errorMsg: isInternetConnected
          ? Strings.genericErrorMsg
          : Strings.genericNoInternetMsg,
      onRetry: () => _bloc(context).add(RefreshRequested()),
    );
  }

  HomeBloc _bloc(BuildContext context) {
    return BlocProvider.of<HomeBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('HomeScreen');
