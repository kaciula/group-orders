import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:orders/app/app_locale.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/order.dart';

// ignore: avoid_classes_with_only_static_members
class FormatterUtils {
  static String format(double d) {
    int decimalDigits = 2;
    if (d.truncateToDouble() == d) {
      decimalDigits = 0;
    }
    return NumberFormat.currency(decimalDigits: decimalDigits, name: '')
        .format(d);
  }

  static String date(DateTime date) {
    final DateFormat formatter =
        DateFormat('d MMM, yyyy', AppLocale.languageCode);
    return '${formatter.format(date)}';
  }

  static String time(BuildContext context, DateTime date) {
    final String format =
        MediaQuery.of(context).alwaysUse24HourFormat ? 'HH:mm' : 'hh:mm a';
    final DateFormat formatter = DateFormat(format, AppLocale.languageCode);
    return '${formatter.format(date)}';
  }

  static String dateTime(BuildContext context, DateTime date) {
    final String timeFormat =
        MediaQuery.of(context).alwaysUse24HourFormat ? 'HH:mm' : 'hh:mm a';
    final DateFormat formatter =
        DateFormat('d MMM yyyy, $timeFormat', AppLocale.languageCode);
    return '${formatter.format(date)}';
  }

  static String orderStatus(OrderStatus orderStatus, {bool addPrefix = false}) {
    switch (orderStatus) {
      case OrderStatus.inProgress:
        return (addPrefix ? Strings.orderStatusOrder + ' ' : '') +
            Strings.orderStatusInProgress;
      case OrderStatus.blocked:
        return (addPrefix ? Strings.orderStatusOrder + ' ' : '') +
            Strings.orderStatusBlocked;
      case OrderStatus.madeUpstream:
        return (addPrefix ? Strings.orderStatusOrder + ' ' : '') +
            Strings.orderStatusMadeUpstream;
      case OrderStatus.arrived:
        return (addPrefix ? Strings.orderStatusOrder + ' ' : '') +
            Strings.orderStatusArrived;
      case OrderStatus.completed:
        return (addPrefix ? Strings.orderStatusOrder + ' ' : '') +
            Strings.orderStatusCompleted;
      case OrderStatus.forcefullyClosed:
        return (addPrefix ? Strings.orderStatusOrder + ' ' : '') +
            Strings.orderStatusForcefullyClosed;
    }
  }
}
