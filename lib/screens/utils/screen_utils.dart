import 'package:flutter/material.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/participant.dart';

// ignore: avoid_classes_with_only_static_members
class ScreenUtils {
  static const Duration _snackBarDisplayDuration = Duration(milliseconds: 4000);
  static const Duration _snackBarErrorDisplayDuration =
      Duration(milliseconds: 10000);

  static void showErrorSnackBar(
    BuildContext context, {
    required String errorMsg,
    Color backgroundColor = AppColors.snackBarError,
    Duration duration = _snackBarErrorDisplayDuration,
    SnackBarAction? action,
  }) {
    showSnackBar(
      context,
      text: errorMsg,
      backgroundColor: backgroundColor,
      duration: duration,
      action: action,
    );
  }

  static void showSnackBar(
    BuildContext context, {
    required String text,
    Color? backgroundColor,
    Duration duration = _snackBarDisplayDuration,
    SnackBarAction? action,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(text, textAlign: TextAlign.center),
      backgroundColor: backgroundColor,
      duration: duration,
      action: action,
    ));
  }

  static List<Widget> participantFlags(Participant participant,
      {required bool isOwner}) {
    const double iconSize = 16;
    const Color color = AppColors.textGreyDark;
    const Color checkedColor = AppColors.blue;
    final Color uncheckedColor = color.withOpacity(0.25);
    return <Widget>[
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Icon(Icons.house, size: iconSize, color: color),
          const SizedBox(width: 8),
          Text(
            Strings.participantsDelivery,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
          const SizedBox(width: 8),
          Icon(
            Icons.check_circle_outline,
            size: iconSize,
            color: participant.delivered ? checkedColor : uncheckedColor,
          ),
          if (!isOwner)
            Icon(
              Icons.check_circle_outline,
              size: iconSize,
              color: participant.confirmedDelivered
                  ? checkedColor
                  : uncheckedColor,
            ),
        ],
      ),
      const SizedBox(height: 8),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const Icon(Icons.payment, size: iconSize, color: color),
              const SizedBox(width: 8),
              Text(
                Strings.participantsPayment,
                style: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: color,
                ),
              ),
              const SizedBox(width: 8),
              Icon(
                Icons.check_circle_outline,
                size: iconSize,
                color: participant.paid ? checkedColor : uncheckedColor,
              ),
              if (isOwner)
                Icon(
                  Icons.check_circle_outline,
                  size: iconSize,
                  color:
                      participant.confirmedPaid ? checkedColor : uncheckedColor,
                ),
            ],
          ),
          if (participant.paidDetails.isNotEmpty)
            Padding(
              padding: const EdgeInsets.only(left: 24, top: 4),
              child: Text(
                participant.paidDetails,
                style: const TextStyle(
                  fontWeight: FontWeight.w200,
                  fontSize: 11,
                ),
              ),
            ),
        ],
      ),
    ];
  }
}
