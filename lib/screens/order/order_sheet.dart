import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart';
import 'package:orders/screens/order/bloc/order_state.dart';
import 'package:orders/screens/utils/formatter_utils.dart';
import 'package:orders/screens/widgets/custom_url_linkifier.dart';
import 'package:orders/screens/widgets/sheet_row.dart';
import 'package:orders/screens/widgets/sure_dialog.dart';

class OrderSheet extends StatelessWidget {
  const OrderSheet({
    Key? key,
    required this.orderBloc,
    required this.order,
  }) : super(key: key);

  final OrderBloc orderBloc;
  final Order order;

  @override
  Widget build(BuildContext context) {
    final OrderState state = orderBloc.state;
    return ListView(
      padding: const EdgeInsets.all(16),
      shrinkWrap: true,
      children: <Widget>[
        Text(order.title, style: const TextStyle(fontSize: 18)),
        const SizedBox(height: 8),
        if (order.isProvider) ...<Widget>[
          Row(
            children: <Widget>[
              Text(Strings.genericDeliveryDate),
              const SizedBox(width: 8),
              Text(
                FormatterUtils.dateTime(context, state.order.deliveryDate!),
                style: const TextStyle(fontWeight: FontWeight.w300),
              )
            ],
          ),
          const SizedBox(height: 8),
        ],
        Row(
          children: <Widget>[
            Text(Strings.genericDueDate),
            const SizedBox(width: 8),
            Text(
              FormatterUtils.dateTime(context, state.order.dueDate),
              style: const TextStyle(fontWeight: FontWeight.w300),
            )
          ],
        ),
        const SizedBox(height: 8),
        if (order.details.isNotEmpty) ...<Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Linkify(
              text: order.details,
              linkifiers: const <Linkifier>[CustomUrlLinkifier()],
              options: LinkifyOptions(looseUrl: true),
              onOpen: (LinkableElement it) {
                orderBloc.add(LinkRequested(it.url));
                Navigator.pop(context);
              },
            ),
          ),
        ],
        if (state.isOwner) ...<Widget>[
          const Divider(),
          SheetRow(
            text: Strings.orderInvite,
            iconData: Icons.person_add,
            onTap: () {
              orderBloc.add(InviteRequested());
              Navigator.pop(context);
            },
            color: AppColors.accent,
          ),
        ],
        if (state.isSeeParticipantsVisible) ...<Widget>[
          const Divider(),
          SheetRow(
            text: Strings.orderViewParticipants,
            iconData: Icons.people,
            onTap: () {
              orderBloc.add(ViewParticipantsRequested());
              Navigator.pop(context);
            },
            color: AppColors.accent,
          ),
        ],
        if (state.isOwner) ...<Widget>[
          const Divider(),
          SheetRow(
            text: Strings.orderEdit,
            iconData: Icons.edit,
            onTap: () {
              orderBloc.add(EditOrderRequested());
              Navigator.pop(context);
            },
            color: AppColors.accent,
          ),
          if (!order.isDeleted) ...<Widget>[
            const Divider(),
            SheetRow(
              text: Strings.orderDelete,
              iconData: Icons.delete,
              onTap: () => showDialog(
                builder: (BuildContext context) => SureDialog(
                  msg: Strings.orderAreYouSureDeleteOrder,
                  onYesPressed: () {
                    orderBloc.add(DeleteOrderRequested());
                    Navigator.pop(context);
                  },
                ),
                context: context,
              ),
              color: AppColors.red,
            ),
          ]
        ],
      ],
    );
  }
}
