import 'package:flutter/material.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart';
import 'package:orders/screens/widgets/buttons.dart';

class AddTransportToCartDialog extends StatefulWidget {
  const AddTransportToCartDialog({
    Key? key,
    required this.orderBloc,
    required this.currency,
  }) : super(key: key);

  final OrderBloc orderBloc;
  final String currency;

  @override
  _AddTransportToCartDialogState createState() =>
      _AddTransportToCartDialogState();
}

class _AddTransportToCartDialogState extends State<AddTransportToCartDialog> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _costController = TextEditingController();
  final TextEditingController _detailsController = TextEditingController();

  @override
  void dispose() {
    _costController.dispose();
    _detailsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SimpleDialog(
        contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        title: Text(Strings.orderEntryTransport),
        children: <Widget>[
          Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: 50,
                  child: TextFormField(
                    autofocus: true,
                    textAlign: TextAlign.center,
                    controller: _costController,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    validator: widget.orderBloc.costValidator,
                  ),
                ),
                const SizedBox(width: 16),
                Text(widget.currency),
              ],
            ),
          ),
          const SizedBox(height: 8),
          TextFormField(
            controller: _detailsController,
            decoration: InputDecoration(
                hintText: Strings.orderAddTransportToCartDetails),
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            textCapitalization: TextCapitalization.sentences,
            minLines: 1,
            maxLines: 2,
          ),
          const SizedBox(height: 16),
          RaisedBtn(
              label: Strings.orderAddToCart,
              onPressed: () {
                final FormState form = _formKey.currentState!;
                if (form.validate()) {
                  final double cost = double.parse(
                      _costController.text.trim().replaceFirst(',', '.'));
                  widget.orderBloc.add(AddTransportToCartRequested(
                    cost: cost,
                    details: _detailsController.text.trim(),
                  ));
                  Navigator.pop(context);
                }
              }),
        ],
      ),
    );
  }
}
