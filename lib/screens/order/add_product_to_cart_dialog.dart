import 'package:flutter/material.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/product.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart';
import 'package:orders/screens/widgets/buttons.dart';

class AddProductToCartDialog extends StatefulWidget {
  const AddProductToCartDialog(
      {Key? key, required this.orderBloc, required this.product})
      : super(key: key);

  final OrderBloc orderBloc;
  final Product product;

  @override
  _AddProductToCartDialogState createState() => _AddProductToCartDialogState();
}

class _AddProductToCartDialogState extends State<AddProductToCartDialog> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _detailsController = TextEditingController();

  int _dropdownValue = 1;

  @override
  void dispose() {
    _detailsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Stock stock = widget.product.stock;
    int count = 9;
    if (stock is Limited && stock.numItemsAvailable < count) {
      count = stock.numItemsAvailable;
    }
    final List<String> dropdownOptions =
        List<String>.generate(count, (int index) => (index + 1).toString());

    return Form(
      key: _formKey,
      child: SimpleDialog(
        contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        title: Text(widget.product.title),
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  Strings.orderNumItems,
                  style: const TextStyle(fontSize: 16),
                ),
              ),
              DropdownButton<String>(
                  value: _dropdownValue.toString(),
                  items: dropdownOptions
                      .map(
                        (String item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(item),
                        ),
                      )
                      .toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      _dropdownValue = int.parse(newValue!);
                    });
                  }),
            ],
          ),
          TextFormField(
            autofocus: true,
            controller: _detailsController,
            decoration:
                InputDecoration(hintText: Strings.orderAddProductToCartDetails),
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            textCapitalization: TextCapitalization.sentences,
            minLines: 1,
            maxLines: 2,
          ),
          const SizedBox(height: 16),
          RaisedBtn(
              label: Strings.orderAddToCart,
              onPressed: () {
                widget.orderBloc.add(
                  AddProductToCartRequested(
                    product: widget.product,
                    details: _detailsController.text.trim(),
                    numItems: _dropdownValue,
                  ),
                );
                Navigator.pop(context);
              }),
        ],
      ),
    );
  }
}
