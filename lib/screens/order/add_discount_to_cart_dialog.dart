import 'package:flutter/material.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_event.dart';
import 'package:orders/screens/widgets/buttons.dart';

class AddDiscountToCartDialog extends StatefulWidget {
  const AddDiscountToCartDialog({Key? key, required this.orderBloc})
      : super(key: key);

  final OrderBloc orderBloc;

  @override
  _AddDiscountToCartDialogState createState() =>
      _AddDiscountToCartDialogState();
}

class _AddDiscountToCartDialogState extends State<AddDiscountToCartDialog> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _percentController = TextEditingController();
  final TextEditingController _detailsController = TextEditingController();

  @override
  void dispose() {
    _percentController.dispose();
    _detailsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SimpleDialog(
        contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        title: Text(Strings.orderEntryDiscount),
        children: <Widget>[
          Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  width: 50,
                  child: TextFormField(
                    autofocus: true,
                    textAlign: TextAlign.center,
                    controller: _percentController,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    validator: widget.orderBloc.percentValidator,
                  ),
                ),
                const SizedBox(width: 8),
                const Text('%'),
              ],
            ),
          ),
          const SizedBox(height: 16),
          TextFormField(
            controller: _detailsController,
            decoration: InputDecoration(
                hintText: Strings.orderAddDiscountToCartDetails),
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            textCapitalization: TextCapitalization.sentences,
            minLines: 1,
            maxLines: 2,
          ),
          const SizedBox(height: 16),
          RaisedBtn(
              label: Strings.orderAddToCart,
              onPressed: () {
                final FormState form = _formKey.currentState!;
                if (form.validate()) {
                  final double percent = double.parse(_percentController.text
                          .trim()
                          .replaceFirst(',', '.')) /
                      100;
                  widget.orderBloc.add(AddDiscountToCartRequested(
                    percent: percent,
                    details: _detailsController.text.trim(),
                  ));
                  Navigator.pop(context);
                }
              }),
        ],
      ),
    );
  }
}
