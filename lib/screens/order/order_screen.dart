import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/app/app_styles.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/product.dart';
import 'package:orders/screens/cart/cart_screen.dart';
import 'package:orders/screens/edit_order/edit_order_screen.dart';
import 'package:orders/screens/edit_product/edit_product_screen.dart';
import 'package:orders/screens/invite/invite_screen.dart';
import 'package:orders/screens/order/add_discount_to_cart_dialog.dart';
import 'package:orders/screens/order/add_product_to_cart_dialog.dart';
import 'package:orders/screens/order/add_transport_to_cart_dialog.dart';
import 'package:orders/screens/order/bloc/order_bloc.dart';
import 'package:orders/screens/order/bloc/order_effect.dart';
import 'package:orders/screens/order/bloc/order_event.dart';
import 'package:orders/screens/order/bloc/order_state.dart';
import 'package:orders/screens/order/order_sheet.dart';
import 'package:orders/screens/participants/participants_screen.dart';
import 'package:orders/screens/utils/formatter_utils.dart';
import 'package:orders/screens/utils/refresh_state.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/action_sheet.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/circular_progress.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';
import 'package:orders/screens/widgets/refresh_error_view.dart';

class OrderScreen extends StatefulWidget {
  static const String routeName = 'order';

  @override
  State<OrderScreen> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      _logger.fine('app paused');
    }
    if (state == AppLifecycleState.resumed) {
      _bloc(context).add(AppResumed());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderBloc, OrderState>(
      bloc: BlocProvider.of<OrderBloc>(context),
      buildWhen: OrderState.buildWhen,
      builder: _builder,
    );
  }

  Future<void> _effectListener(BuildContext context, OrderState state) async {
    final OrderEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToCart) {
      Navigator.pushNamed(context, CartScreen.routeName,
          arguments: _bloc(context));
    }
    if (effect is GoToEditProduct) {
      final bool success = await Navigator.pushNamed(
            context,
            EditProductScreen.routeName,
            arguments: EditProductArgs(
                orderBloc: _bloc(context), product: effect.product),
          ) ??
          false;
      final bool isNewProduct = effect.product == null;
      if (success && isNewProduct && state.isOwner) {
        ScreenUtils.showSnackBar(
          context,
          text: Strings.orderDoNotForget,
          backgroundColor: AppColors.orange,
          duration: const Duration(seconds: 10),
        );
      }
    }
    if (effect is GoToEditOrder) {
      Navigator.pushNamed(
        context,
        EditOrderScreen.routeName,
        arguments: EditOrderArgs(
          orderBloc: _bloc(context),
          order: effect.order,
          orderType: effect.order.type,
        ),
      );
    }
    if (effect is GoToHome) {
      Navigator.pop(context, OrderScreenPopArgs(refreshOrders: true));
    }
    if (effect is GoToInvite) {
      Navigator.pushNamed(context, InviteScreen.routeName,
          arguments: effect.order);
    }
    if (effect is GoToParticipants) {
      Navigator.pushNamed(
        context,
        ParticipantsScreen.routeName,
        arguments: ParticipantsArgs(
          orderBloc: _bloc(context),
          order: effect.order,
          participants: effect.participants,
          cart: effect.cart,
          isOwner: effect.isOwner,
          owner: effect.owner,
        ),
      );
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, OrderState state) {
    final OrderBloc bloc = BlocProvider.of<OrderBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(state.order.title),
            Text(state.order.ownerName, style: styleAppBarSubtitle),
          ],
        ),
        actions: state.refreshState is Success
            ? <Widget>[
                IconButton(
                  icon: const Icon(Icons.aspect_ratio),
                  onPressed: () => showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) => OrderSheet(
                      orderBloc: bloc,
                      order: state.order,
                    ),
                  ),
                ),
              ]
            : null,
      ),
      floatingActionButton: state.refreshState is Success
          ? Padding(
              padding: const EdgeInsets.only(left: 8, bottom: _minStatusHeight),
              child: FloatingActionButton.extended(
                icon: const Icon(
                  Icons.shopping_cart,
                  size: 32,
                  color: AppColors.textWhite,
                ),
                label: Text(
                  Strings.orderCart,
                  style: const TextStyle(
                    color: AppColors.textWhite,
                    fontSize: 23,
                  ),
                ),
                onPressed: () => _bloc(context).add(CartRequested()),
              ),
            )
          : null,
      body: BlocListener<OrderBloc, OrderState>(
        bloc: bloc,
        listenWhen: OrderState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, OrderState state) {
    return state.refreshState.when(
      inProgress: () => _progress(),
      success: () => _content(context, state),
      error: (bool isInternetConnected) =>
          _error(context, isInternetConnected: isInternetConnected),
    );
  }

  Widget _content(BuildContext context, OrderState state) {
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: Column(
        children: <Widget>[
          Expanded(child: _products(context, state)),
          _status(context, state),
        ],
      ),
    );
  }

  Widget _status(BuildContext context, OrderState state) {
    String title;
    String subtitle;
    switch (state.order.status) {
      case OrderStatus.inProgress:
        final List<String> strings = state.isOwner
            ? Strings.orderStatusOwnerInProgressDesc(
                hasFeatureEditProducts: state.order.hasFeatureEditProducts)
            : Strings.orderStatusInProgressDesc(
                hasFeatureEditProducts: state.order.hasFeatureEditProducts);
        title = strings[0];
        subtitle = strings[1];
        break;
      case OrderStatus.blocked:
        final List<String> strings = state.isOwner
            ? Strings.orderStatusOwnerBlockedDesc
            : Strings.orderStatusBlockedDesc;
        title = strings[0];
        subtitle = strings[1];
        break;
      case OrderStatus.madeUpstream:
        final List<String> strings = state.isOwner
            ? Strings.orderStatusOwnerMadeUpstreamDesc()
            : Strings.orderStatusMadeUpstreamDesc(state.owner!.displayName);
        title = strings[0];
        subtitle = strings[1];
        break;
      case OrderStatus.arrived:
        final List<String> strings = state.isOwner
            ? Strings.orderStatusOwnerArrivedDesc()
            : Strings.orderStatusArrivedDesc(state.owner!.displayName);
        title = strings[0];
        subtitle = strings[1];
        break;
      case OrderStatus.completed:
        final List<String> strings = state.isOwner
            ? Strings.orderStatusOwnerCompletedDesc
            : Strings.orderStatusCompletedDesc;
        title = strings[0];
        subtitle = strings[1];
        break;
      case OrderStatus.forcefullyClosed:
        final List<String> strings = state.isOwner
            ? Strings.orderStatusOwnerForcefullyClosedDesc
            : Strings.orderStatusForcefullyClosedDesc;
        title = strings[0];
        subtitle = strings[1];
        break;
    }
    return ConstrainedBox(
      constraints: const BoxConstraints(minHeight: _minStatusHeight),
      child: InkWell(
        onTap: state.isOwner
            ? () => showDialog(
                  context: context,
                  builder: (BuildContext innerContext) =>
                      _statusDialog(innerContext, _bloc(context), state),
                )
            : null,
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(8),
          color: AppColors.blue.withOpacity(0.6),
          child: Row(
            children: <Widget>[
              if (state.isOwner)
                Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  child: Icon(Icons.edit,
                      color: Theme.of(context).primaryIconTheme.color),
                ),
              Expanded(
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        title,
                        textAlign: TextAlign.center,
                        style: Theme.of(context).primaryTextTheme.bodyText1,
                      ),
                      const SizedBox(height: 4),
                      Text(
                        subtitle,
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .bodyText2!
                            .copyWith(fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _products(BuildContext context, OrderState state) {
    return state.products!.size > 0
        ? ListView.builder(
            padding:
                const EdgeInsets.only(top: 16, left: 8, right: 8, bottom: 64),
            itemCount:
                state.products!.size + 1 + (state.hasExtraEntries ? 2 : 0),
            itemBuilder: (BuildContext context, int index) {
              if (index < state.products!.size) {
                final Product product = state.products![index];
                return _product(context, _bloc(context), state, product);
              } else if (index == state.products!.size) {
                return state.isEditProductsVisible
                    ? Padding(
                        padding: const EdgeInsets.only(top: 16, bottom: 16),
                        child: _createProductBtn(context, state),
                      )
                    : Container();
              } else if (index == state.products!.size + 1) {
                return _discount(context, state);
              } else {
                return _transport(context, state);
              }
            },
          )
        : _productEmpty(context, state);
  }

  Widget _product(
      BuildContext context, OrderBloc bloc, OrderState state, Product product) {
    final Stock stock = product.stock;
    final VoidCallback? addToCartCallback =
        state.canAddToCart && product.hasItemsInStock
            ? () => _onTapAddProductToCart(bloc, product)
            : null;
    return Card(
      color: product.hasItemsInStock ? null : Colors.white60,
      child: InkWell(
        splashColor: AppColors.accent.withOpacity(0.8),
        onTap: () => showModalBottomSheet(
          context: context,
          builder: (BuildContext context) => ActionSheet(
            title: product.title,
            details: product.details,
            onOpenLink: (String url) => bloc.add(LinkRequested(url)),
            isEditVisible: state.isEditProductsVisible,
            onTapEdit: state.canEditProducts
                ? () {
                    bloc.add(EditProductRequested(product));
                    Navigator.pop(context);
                  }
                : null,
            isDeleteVisible: state.isEditProductsVisible,
            deleteSureMsg: Strings.orderAreYouSureDelete,
            onTapDelete: state.canEditProducts
                ? () => bloc.add(DeleteProductRequested(product))
                : null,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 8, bottom: 0, left: 8, right: 8),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(product.title, style: styleProductAndPrice),
                    if (stock is Limited &&
                        stock.numItemsAvailable <= 9) ...<Widget>[
                      const SizedBox(height: 4),
                      Text(
                        stock.numItemsAvailable > 0
                            ? Strings.orderNumItemsAvailable(
                                stock.numItemsAvailable)
                            : Strings.orderOutOfStock,
                        style: const TextStyle(
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ],
                  ],
                ),
              ),
              InkWell(
                onTap: addToCartCallback,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      '${FormatterUtils.format(product.price)} ${state.order.currency}',
                      style: styleProductAndPrice,
                    ),
                    FlatBtn(
                      label: Strings.orderAddToCart,
                      padding: const EdgeInsets.only(left: 8),
                      onPressed: addToCartCallback,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _discount(BuildContext context, OrderState state) {
    final VoidCallback? callback = state.canAddDiscount
        ? () => showDialog(
              context: context,
              builder: (BuildContext innerContext) =>
                  AddDiscountToCartDialog(orderBloc: _bloc(context)),
            )
        : null;
    return Card(
      child: InkWell(
        splashColor: AppColors.accent.withOpacity(0.8),
        onTap: callback,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: Text(Strings.orderEntryDiscount,
                      style: styleProductAndPrice)),
              FlatBtn(
                label: Strings.orderAddToCart,
                padding: const EdgeInsets.only(left: 8),
                onPressed: callback,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _transport(BuildContext context, OrderState state) {
    final VoidCallback? callback = state.canAddTransport
        ? () => showDialog(
              context: context,
              builder: (BuildContext innerContext) => AddTransportToCartDialog(
                orderBloc: _bloc(context),
                currency: state.order.currency,
              ),
            )
        : null;
    return Card(
      child: InkWell(
        splashColor: AppColors.accent.withOpacity(0.8),
        onTap: callback,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: Text(Strings.orderEntryTransport,
                      style: styleProductAndPrice)),
              FlatBtn(
                label: Strings.orderAddToCart,
                padding: const EdgeInsets.only(left: 8),
                onPressed: callback,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onTapAddProductToCart(OrderBloc bloc, Product product) {
    showDialog(
      context: context,
      builder: (BuildContext context) =>
          AddProductToCartDialog(orderBloc: bloc, product: product),
    );
  }

  Widget _createProductBtn(BuildContext context, OrderState state) {
    return UnconstrainedBox(
      child: RaisedBtn(
        label: Strings.orderCreateProduct,
        onPressed: state.canEditProducts
            ? () => _bloc(context).add(AddProductRequested())
            : null,
      ),
    );
  }

  Widget _productEmpty(BuildContext context, OrderState state) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              Strings.orderNoProducts,
              textAlign: TextAlign.center,
              style: styleEmpty,
            ),
            if (state.isEditProductsVisible) ...<Widget>[
              const SizedBox(height: 16),
              _createProductBtn(context, state),
            ],
          ],
        ),
      ),
    );
  }

  Widget _statusDialog(
      BuildContext innerContext, OrderBloc orderBloc, OrderState state) {
    final List<OrderStatus> statusValues = OrderStatus.values.toList();
    if (state.order.isProvider) {
      statusValues.remove(OrderStatus.madeUpstream);
    }
    final List<Widget> options = statusValues
        .map(
          (OrderStatus it) => SimpleDialogOption(
            onPressed: () {
              Navigator.pop(innerContext);
              orderBloc.add(StatusChangeRequested(it));
            },
            padding: EdgeInsets.zero,
            child: SizedBox(
              height: 48,
              child: Column(
                children: <Widget>[
                  const Divider(height: 1, indent: 16),
                  Expanded(
                    child: Center(
                      child: Text(
                        _statusOption(it),
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
        .toList();
    return SimpleDialog(
      title: Text(Strings.orderChangeStatus),
      children: options,
    );
  }

  String _statusOption(OrderStatus status) {
    switch (status) {
      case OrderStatus.inProgress:
        return Strings.orderStatusInProgress;
      case OrderStatus.blocked:
        return Strings.orderStatusBlocked;
      case OrderStatus.madeUpstream:
        return Strings.orderStatusMadeUpstream;
      case OrderStatus.arrived:
        return Strings.orderStatusArrived;
      case OrderStatus.completed:
        return Strings.orderStatusCompleted;
      case OrderStatus.forcefullyClosed:
        return Strings.orderStatusForcefullyClosed;
    }
  }

  Widget _progress() {
    return const Center(child: CircularProgress());
  }

  Widget _error(BuildContext context, {required bool isInternetConnected}) {
    return RefreshErrorView(
      errorTitle: isInternetConnected
          ? Strings.genericErrorTitle
          : Strings.genericNoInternetTitle,
      errorMsg: isInternetConnected
          ? Strings.genericErrorMsg
          : Strings.genericNoInternetMsg,
      onRetry: () => _bloc(context).add(RefreshRequested()),
    );
  }

  OrderBloc _bloc(BuildContext context) {
    return BlocProvider.of<OrderBloc>(context);
  }
}

const double _minStatusHeight = 64;

class OrderScreenPopArgs {
  OrderScreenPopArgs({required this.refreshOrders});

  final bool refreshOrders;
}

// ignore: unused_element
final Logger _logger = Logger('OrderScreen');
