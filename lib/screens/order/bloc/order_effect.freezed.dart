// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'order_effect.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$OrderEffect {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderEffectCopyWith<$Res> {
  factory $OrderEffectCopyWith(
          OrderEffect value, $Res Function(OrderEffect) then) =
      _$OrderEffectCopyWithImpl<$Res>;
}

/// @nodoc
class _$OrderEffectCopyWithImpl<$Res> implements $OrderEffectCopyWith<$Res> {
  _$OrderEffectCopyWithImpl(this._value, this._then);

  final OrderEffect _value;
  // ignore: unused_field
  final $Res Function(OrderEffect) _then;
}

/// @nodoc
abstract class _$$GoToHomeCopyWith<$Res> {
  factory _$$GoToHomeCopyWith(
          _$GoToHome value, $Res Function(_$GoToHome) then) =
      __$$GoToHomeCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GoToHomeCopyWithImpl<$Res> extends _$OrderEffectCopyWithImpl<$Res>
    implements _$$GoToHomeCopyWith<$Res> {
  __$$GoToHomeCopyWithImpl(_$GoToHome _value, $Res Function(_$GoToHome) _then)
      : super(_value, (v) => _then(v as _$GoToHome));

  @override
  _$GoToHome get _value => super._value as _$GoToHome;
}

/// @nodoc

class _$GoToHome implements GoToHome {
  _$GoToHome();

  @override
  String toString() {
    return 'OrderEffect.goToHome()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GoToHome);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToHome();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToHome?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToHome != null) {
      return goToHome();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToHome(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToHome?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToHome != null) {
      return goToHome(this);
    }
    return orElse();
  }
}

abstract class GoToHome implements OrderEffect {
  factory GoToHome() = _$GoToHome;
}

/// @nodoc
abstract class _$$GoToCartCopyWith<$Res> {
  factory _$$GoToCartCopyWith(
          _$GoToCart value, $Res Function(_$GoToCart) then) =
      __$$GoToCartCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GoToCartCopyWithImpl<$Res> extends _$OrderEffectCopyWithImpl<$Res>
    implements _$$GoToCartCopyWith<$Res> {
  __$$GoToCartCopyWithImpl(_$GoToCart _value, $Res Function(_$GoToCart) _then)
      : super(_value, (v) => _then(v as _$GoToCart));

  @override
  _$GoToCart get _value => super._value as _$GoToCart;
}

/// @nodoc

class _$GoToCart implements GoToCart {
  _$GoToCart();

  @override
  String toString() {
    return 'OrderEffect.goToCart()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GoToCart);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToCart();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToCart?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToCart != null) {
      return goToCart();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToCart(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToCart?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToCart != null) {
      return goToCart(this);
    }
    return orElse();
  }
}

abstract class GoToCart implements OrderEffect {
  factory GoToCart() = _$GoToCart;
}

/// @nodoc
abstract class _$$GoToEditProductCopyWith<$Res> {
  factory _$$GoToEditProductCopyWith(
          _$GoToEditProduct value, $Res Function(_$GoToEditProduct) then) =
      __$$GoToEditProductCopyWithImpl<$Res>;
  $Res call({Product? product});

  $ProductCopyWith<$Res>? get product;
}

/// @nodoc
class __$$GoToEditProductCopyWithImpl<$Res>
    extends _$OrderEffectCopyWithImpl<$Res>
    implements _$$GoToEditProductCopyWith<$Res> {
  __$$GoToEditProductCopyWithImpl(
      _$GoToEditProduct _value, $Res Function(_$GoToEditProduct) _then)
      : super(_value, (v) => _then(v as _$GoToEditProduct));

  @override
  _$GoToEditProduct get _value => super._value as _$GoToEditProduct;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$GoToEditProduct(
      product: product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product?,
    ));
  }

  @override
  $ProductCopyWith<$Res>? get product {
    if (_value.product == null) {
      return null;
    }

    return $ProductCopyWith<$Res>(_value.product!, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$GoToEditProduct implements GoToEditProduct {
  _$GoToEditProduct({this.product});

  @override
  final Product? product;

  @override
  String toString() {
    return 'OrderEffect.goToEditProduct(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GoToEditProduct &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$GoToEditProductCopyWith<_$GoToEditProduct> get copyWith =>
      __$$GoToEditProductCopyWithImpl<_$GoToEditProduct>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToEditProduct(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToEditProduct?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToEditProduct != null) {
      return goToEditProduct(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToEditProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToEditProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToEditProduct != null) {
      return goToEditProduct(this);
    }
    return orElse();
  }
}

abstract class GoToEditProduct implements OrderEffect {
  factory GoToEditProduct({final Product? product}) = _$GoToEditProduct;

  Product? get product;
  @JsonKey(ignore: true)
  _$$GoToEditProductCopyWith<_$GoToEditProduct> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GoToEditOrderCopyWith<$Res> {
  factory _$$GoToEditOrderCopyWith(
          _$GoToEditOrder value, $Res Function(_$GoToEditOrder) then) =
      __$$GoToEditOrderCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$GoToEditOrderCopyWithImpl<$Res>
    extends _$OrderEffectCopyWithImpl<$Res>
    implements _$$GoToEditOrderCopyWith<$Res> {
  __$$GoToEditOrderCopyWithImpl(
      _$GoToEditOrder _value, $Res Function(_$GoToEditOrder) _then)
      : super(_value, (v) => _then(v as _$GoToEditOrder));

  @override
  _$GoToEditOrder get _value => super._value as _$GoToEditOrder;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$GoToEditOrder(
      order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$GoToEditOrder implements GoToEditOrder {
  _$GoToEditOrder(this.order);

  @override
  final Order order;

  @override
  String toString() {
    return 'OrderEffect.goToEditOrder(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GoToEditOrder &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$GoToEditOrderCopyWith<_$GoToEditOrder> get copyWith =>
      __$$GoToEditOrderCopyWithImpl<_$GoToEditOrder>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToEditOrder(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToEditOrder?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToEditOrder != null) {
      return goToEditOrder(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToEditOrder(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToEditOrder?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToEditOrder != null) {
      return goToEditOrder(this);
    }
    return orElse();
  }
}

abstract class GoToEditOrder implements OrderEffect {
  factory GoToEditOrder(final Order order) = _$GoToEditOrder;

  Order get order;
  @JsonKey(ignore: true)
  _$$GoToEditOrderCopyWith<_$GoToEditOrder> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GoToInviteCopyWith<$Res> {
  factory _$$GoToInviteCopyWith(
          _$GoToInvite value, $Res Function(_$GoToInvite) then) =
      __$$GoToInviteCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$GoToInviteCopyWithImpl<$Res> extends _$OrderEffectCopyWithImpl<$Res>
    implements _$$GoToInviteCopyWith<$Res> {
  __$$GoToInviteCopyWithImpl(
      _$GoToInvite _value, $Res Function(_$GoToInvite) _then)
      : super(_value, (v) => _then(v as _$GoToInvite));

  @override
  _$GoToInvite get _value => super._value as _$GoToInvite;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$GoToInvite(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$GoToInvite implements GoToInvite {
  _$GoToInvite({required this.order});

  @override
  final Order order;

  @override
  String toString() {
    return 'OrderEffect.goToInvite(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GoToInvite &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$GoToInviteCopyWith<_$GoToInvite> get copyWith =>
      __$$GoToInviteCopyWithImpl<_$GoToInvite>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToInvite(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToInvite?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToInvite != null) {
      return goToInvite(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToInvite(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToInvite?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToInvite != null) {
      return goToInvite(this);
    }
    return orElse();
  }
}

abstract class GoToInvite implements OrderEffect {
  factory GoToInvite({required final Order order}) = _$GoToInvite;

  Order get order;
  @JsonKey(ignore: true)
  _$$GoToInviteCopyWith<_$GoToInvite> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GoToParticipantsCopyWith<$Res> {
  factory _$$GoToParticipantsCopyWith(
          _$GoToParticipants value, $Res Function(_$GoToParticipants) then) =
      __$$GoToParticipantsCopyWithImpl<$Res>;
  $Res call(
      {Order order,
      KtList<Participant> participants,
      KtList<CartEntry> cart,
      bool isOwner,
      Participant owner});

  $OrderCopyWith<$Res> get order;
  $ParticipantCopyWith<$Res> get owner;
}

/// @nodoc
class __$$GoToParticipantsCopyWithImpl<$Res>
    extends _$OrderEffectCopyWithImpl<$Res>
    implements _$$GoToParticipantsCopyWith<$Res> {
  __$$GoToParticipantsCopyWithImpl(
      _$GoToParticipants _value, $Res Function(_$GoToParticipants) _then)
      : super(_value, (v) => _then(v as _$GoToParticipants));

  @override
  _$GoToParticipants get _value => super._value as _$GoToParticipants;

  @override
  $Res call({
    Object? order = freezed,
    Object? participants = freezed,
    Object? cart = freezed,
    Object? isOwner = freezed,
    Object? owner = freezed,
  }) {
    return _then(_$GoToParticipants(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>,
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>,
      isOwner: isOwner == freezed
          ? _value.isOwner
          : isOwner // ignore: cast_nullable_to_non_nullable
              as bool,
      owner: owner == freezed
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Participant,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $ParticipantCopyWith<$Res> get owner {
    return $ParticipantCopyWith<$Res>(_value.owner, (value) {
      return _then(_value.copyWith(owner: value));
    });
  }
}

/// @nodoc

class _$GoToParticipants implements GoToParticipants {
  _$GoToParticipants(
      {required this.order,
      required this.participants,
      required this.cart,
      required this.isOwner,
      required this.owner});

  @override
  final Order order;
  @override
  final KtList<Participant> participants;
  @override
  final KtList<CartEntry> cart;
  @override
  final bool isOwner;
  @override
  final Participant owner;

  @override
  String toString() {
    return 'OrderEffect.goToParticipants(order: $order, participants: $participants, cart: $cart, isOwner: $isOwner, owner: $owner)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GoToParticipants &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality()
                .equals(other.participants, participants) &&
            const DeepCollectionEquality().equals(other.cart, cart) &&
            const DeepCollectionEquality().equals(other.isOwner, isOwner) &&
            const DeepCollectionEquality().equals(other.owner, owner));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(participants),
      const DeepCollectionEquality().hash(cart),
      const DeepCollectionEquality().hash(isOwner),
      const DeepCollectionEquality().hash(owner));

  @JsonKey(ignore: true)
  @override
  _$$GoToParticipantsCopyWith<_$GoToParticipants> get copyWith =>
      __$$GoToParticipantsCopyWithImpl<_$GoToParticipants>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToParticipants(order, participants, cart, isOwner, owner);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToParticipants?.call(order, participants, cart, isOwner, owner);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToParticipants != null) {
      return goToParticipants(order, participants, cart, isOwner, owner);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToParticipants(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToParticipants?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToParticipants != null) {
      return goToParticipants(this);
    }
    return orElse();
  }
}

abstract class GoToParticipants implements OrderEffect {
  factory GoToParticipants(
      {required final Order order,
      required final KtList<Participant> participants,
      required final KtList<CartEntry> cart,
      required final bool isOwner,
      required final Participant owner}) = _$GoToParticipants;

  Order get order;
  KtList<Participant> get participants;
  KtList<CartEntry> get cart;
  bool get isOwner;
  Participant get owner;
  @JsonKey(ignore: true)
  _$$GoToParticipantsCopyWith<_$GoToParticipants> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ShowInfoSnackBarCopyWith<$Res> {
  factory _$$ShowInfoSnackBarCopyWith(
          _$ShowInfoSnackBar value, $Res Function(_$ShowInfoSnackBar) then) =
      __$$ShowInfoSnackBarCopyWithImpl<$Res>;
  $Res call({String msg});
}

/// @nodoc
class __$$ShowInfoSnackBarCopyWithImpl<$Res>
    extends _$OrderEffectCopyWithImpl<$Res>
    implements _$$ShowInfoSnackBarCopyWith<$Res> {
  __$$ShowInfoSnackBarCopyWithImpl(
      _$ShowInfoSnackBar _value, $Res Function(_$ShowInfoSnackBar) _then)
      : super(_value, (v) => _then(v as _$ShowInfoSnackBar));

  @override
  _$ShowInfoSnackBar get _value => super._value as _$ShowInfoSnackBar;

  @override
  $Res call({
    Object? msg = freezed,
  }) {
    return _then(_$ShowInfoSnackBar(
      msg == freezed
          ? _value.msg
          : msg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ShowInfoSnackBar implements ShowInfoSnackBar {
  _$ShowInfoSnackBar(this.msg);

  @override
  final String msg;

  @override
  String toString() {
    return 'OrderEffect.showInfoSnackBar(msg: $msg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShowInfoSnackBar &&
            const DeepCollectionEquality().equals(other.msg, msg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(msg));

  @JsonKey(ignore: true)
  @override
  _$$ShowInfoSnackBarCopyWith<_$ShowInfoSnackBar> get copyWith =>
      __$$ShowInfoSnackBarCopyWithImpl<_$ShowInfoSnackBar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return showInfoSnackBar(msg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return showInfoSnackBar?.call(msg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showInfoSnackBar != null) {
      return showInfoSnackBar(msg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return showInfoSnackBar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return showInfoSnackBar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showInfoSnackBar != null) {
      return showInfoSnackBar(this);
    }
    return orElse();
  }
}

abstract class ShowInfoSnackBar implements OrderEffect {
  factory ShowInfoSnackBar(final String msg) = _$ShowInfoSnackBar;

  String get msg;
  @JsonKey(ignore: true)
  _$$ShowInfoSnackBarCopyWith<_$ShowInfoSnackBar> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ShowErrorSnackBarCopyWith<$Res> {
  factory _$$ShowErrorSnackBarCopyWith(
          _$ShowErrorSnackBar value, $Res Function(_$ShowErrorSnackBar) then) =
      __$$ShowErrorSnackBarCopyWithImpl<$Res>;
  $Res call({String errorMsg});
}

/// @nodoc
class __$$ShowErrorSnackBarCopyWithImpl<$Res>
    extends _$OrderEffectCopyWithImpl<$Res>
    implements _$$ShowErrorSnackBarCopyWith<$Res> {
  __$$ShowErrorSnackBarCopyWithImpl(
      _$ShowErrorSnackBar _value, $Res Function(_$ShowErrorSnackBar) _then)
      : super(_value, (v) => _then(v as _$ShowErrorSnackBar));

  @override
  _$ShowErrorSnackBar get _value => super._value as _$ShowErrorSnackBar;

  @override
  $Res call({
    Object? errorMsg = freezed,
  }) {
    return _then(_$ShowErrorSnackBar(
      errorMsg: errorMsg == freezed
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ShowErrorSnackBar implements ShowErrorSnackBar {
  _$ShowErrorSnackBar({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'OrderEffect.showErrorSnackBar(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShowErrorSnackBar &&
            const DeepCollectionEquality().equals(other.errorMsg, errorMsg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(errorMsg));

  @JsonKey(ignore: true)
  @override
  _$$ShowErrorSnackBarCopyWith<_$ShowErrorSnackBar> get copyWith =>
      __$$ShowErrorSnackBarCopyWithImpl<_$ShowErrorSnackBar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function() goToCart,
    required TResult Function(Product? product) goToEditProduct,
    required TResult Function(Order order) goToEditOrder,
    required TResult Function(Order order) goToInvite,
    required TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)
        goToParticipants,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return showErrorSnackBar(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return showErrorSnackBar?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function()? goToCart,
    TResult Function(Product? product)? goToEditProduct,
    TResult Function(Order order)? goToEditOrder,
    TResult Function(Order order)? goToInvite,
    TResult Function(Order order, KtList<Participant> participants,
            KtList<CartEntry> cart, bool isOwner, Participant owner)?
        goToParticipants,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showErrorSnackBar != null) {
      return showErrorSnackBar(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(GoToCart value) goToCart,
    required TResult Function(GoToEditProduct value) goToEditProduct,
    required TResult Function(GoToEditOrder value) goToEditOrder,
    required TResult Function(GoToInvite value) goToInvite,
    required TResult Function(GoToParticipants value) goToParticipants,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return showErrorSnackBar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return showErrorSnackBar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(GoToCart value)? goToCart,
    TResult Function(GoToEditProduct value)? goToEditProduct,
    TResult Function(GoToEditOrder value)? goToEditOrder,
    TResult Function(GoToInvite value)? goToInvite,
    TResult Function(GoToParticipants value)? goToParticipants,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showErrorSnackBar != null) {
      return showErrorSnackBar(this);
    }
    return orElse();
  }
}

abstract class ShowErrorSnackBar implements OrderEffect {
  factory ShowErrorSnackBar({required final String errorMsg}) =
      _$ShowErrorSnackBar;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$ShowErrorSnackBarCopyWith<_$ShowErrorSnackBar> get copyWith =>
      throw _privateConstructorUsedError;
}
