// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'order_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$OrderState {
  RefreshState get refreshState => throw _privateConstructorUsedError;
  bool get inProgressAction => throw _privateConstructorUsedError;
  Order get order => throw _privateConstructorUsedError;
  User get user => throw _privateConstructorUsedError;
  Participant? get owner => throw _privateConstructorUsedError;
  KtList<Participant>? get participants => throw _privateConstructorUsedError;
  KtList<Product>? get products => throw _privateConstructorUsedError;
  KtList<CartEntry>? get cart => throw _privateConstructorUsedError;
  OrderEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OrderStateCopyWith<OrderState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderStateCopyWith<$Res> {
  factory $OrderStateCopyWith(
          OrderState value, $Res Function(OrderState) then) =
      _$OrderStateCopyWithImpl<$Res>;
  $Res call(
      {RefreshState refreshState,
      bool inProgressAction,
      Order order,
      User user,
      Participant? owner,
      KtList<Participant>? participants,
      KtList<Product>? products,
      KtList<CartEntry>? cart,
      OrderEffect? effect});

  $RefreshStateCopyWith<$Res> get refreshState;
  $OrderCopyWith<$Res> get order;
  $UserCopyWith<$Res> get user;
  $ParticipantCopyWith<$Res>? get owner;
  $OrderEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$OrderStateCopyWithImpl<$Res> implements $OrderStateCopyWith<$Res> {
  _$OrderStateCopyWithImpl(this._value, this._then);

  final OrderState _value;
  // ignore: unused_field
  final $Res Function(OrderState) _then;

  @override
  $Res call({
    Object? refreshState = freezed,
    Object? inProgressAction = freezed,
    Object? order = freezed,
    Object? user = freezed,
    Object? owner = freezed,
    Object? participants = freezed,
    Object? products = freezed,
    Object? cart = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      refreshState: refreshState == freezed
          ? _value.refreshState
          : refreshState // ignore: cast_nullable_to_non_nullable
              as RefreshState,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      owner: owner == freezed
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Participant?,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>?,
      products: products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<Product>?,
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>?,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as OrderEffect?,
    ));
  }

  @override
  $RefreshStateCopyWith<$Res> get refreshState {
    return $RefreshStateCopyWith<$Res>(_value.refreshState, (value) {
      return _then(_value.copyWith(refreshState: value));
    });
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }

  @override
  $ParticipantCopyWith<$Res>? get owner {
    if (_value.owner == null) {
      return null;
    }

    return $ParticipantCopyWith<$Res>(_value.owner!, (value) {
      return _then(_value.copyWith(owner: value));
    });
  }

  @override
  $OrderEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $OrderEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_OrderStateCopyWith<$Res>
    implements $OrderStateCopyWith<$Res> {
  factory _$$_OrderStateCopyWith(
          _$_OrderState value, $Res Function(_$_OrderState) then) =
      __$$_OrderStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {RefreshState refreshState,
      bool inProgressAction,
      Order order,
      User user,
      Participant? owner,
      KtList<Participant>? participants,
      KtList<Product>? products,
      KtList<CartEntry>? cart,
      OrderEffect? effect});

  @override
  $RefreshStateCopyWith<$Res> get refreshState;
  @override
  $OrderCopyWith<$Res> get order;
  @override
  $UserCopyWith<$Res> get user;
  @override
  $ParticipantCopyWith<$Res>? get owner;
  @override
  $OrderEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_OrderStateCopyWithImpl<$Res> extends _$OrderStateCopyWithImpl<$Res>
    implements _$$_OrderStateCopyWith<$Res> {
  __$$_OrderStateCopyWithImpl(
      _$_OrderState _value, $Res Function(_$_OrderState) _then)
      : super(_value, (v) => _then(v as _$_OrderState));

  @override
  _$_OrderState get _value => super._value as _$_OrderState;

  @override
  $Res call({
    Object? refreshState = freezed,
    Object? inProgressAction = freezed,
    Object? order = freezed,
    Object? user = freezed,
    Object? owner = freezed,
    Object? participants = freezed,
    Object? products = freezed,
    Object? cart = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_OrderState(
      refreshState: refreshState == freezed
          ? _value.refreshState
          : refreshState // ignore: cast_nullable_to_non_nullable
              as RefreshState,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      owner: owner == freezed
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Participant?,
      participants: participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>?,
      products: products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<Product>?,
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>?,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as OrderEffect?,
    ));
  }
}

/// @nodoc

class _$_OrderState extends _OrderState {
  _$_OrderState(
      {required this.refreshState,
      required this.inProgressAction,
      required this.order,
      required this.user,
      this.owner,
      this.participants,
      this.products,
      this.cart,
      this.effect})
      : super._();

  @override
  final RefreshState refreshState;
  @override
  final bool inProgressAction;
  @override
  final Order order;
  @override
  final User user;
  @override
  final Participant? owner;
  @override
  final KtList<Participant>? participants;
  @override
  final KtList<Product>? products;
  @override
  final KtList<CartEntry>? cart;
  @override
  final OrderEffect? effect;

  @override
  String toString() {
    return 'OrderState(refreshState: $refreshState, inProgressAction: $inProgressAction, order: $order, user: $user, owner: $owner, participants: $participants, products: $products, cart: $cart, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_OrderState &&
            const DeepCollectionEquality()
                .equals(other.refreshState, refreshState) &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality().equals(other.owner, owner) &&
            const DeepCollectionEquality()
                .equals(other.participants, participants) &&
            const DeepCollectionEquality().equals(other.products, products) &&
            const DeepCollectionEquality().equals(other.cart, cart) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(refreshState),
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(owner),
      const DeepCollectionEquality().hash(participants),
      const DeepCollectionEquality().hash(products),
      const DeepCollectionEquality().hash(cart),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_OrderStateCopyWith<_$_OrderState> get copyWith =>
      __$$_OrderStateCopyWithImpl<_$_OrderState>(this, _$identity);
}

abstract class _OrderState extends OrderState {
  factory _OrderState(
      {required final RefreshState refreshState,
      required final bool inProgressAction,
      required final Order order,
      required final User user,
      final Participant? owner,
      final KtList<Participant>? participants,
      final KtList<Product>? products,
      final KtList<CartEntry>? cart,
      final OrderEffect? effect}) = _$_OrderState;
  _OrderState._() : super._();

  @override
  RefreshState get refreshState;
  @override
  bool get inProgressAction;
  @override
  Order get order;
  @override
  User get user;
  @override
  Participant? get owner;
  @override
  KtList<Participant>? get participants;
  @override
  KtList<Product>? get products;
  @override
  KtList<CartEntry>? get cart;
  @override
  OrderEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_OrderStateCopyWith<_$_OrderState> get copyWith =>
      throw _privateConstructorUsedError;
}
