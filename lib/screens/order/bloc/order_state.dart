import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/order/bloc/order_effect.dart';
import 'package:orders/screens/utils/refresh_state.dart';

part 'order_state.freezed.dart';

@freezed
class OrderState with _$OrderState {
  factory OrderState({
    required RefreshState refreshState,
    required bool inProgressAction,
    required Order order,
    required User user,
    Participant? owner,
    KtList<Participant>? participants,
    KtList<Product>? products,
    KtList<CartEntry>? cart,
    OrderEffect? effect,
  }) = _OrderState;

  OrderState._();

  factory OrderState.initial(Order order, User user) {
    return OrderState(
      refreshState: RefreshState.inProgress(),
      inProgressAction: false,
      order: order,
      user: user,
    );
  }

  bool get isOwner => owner?.userId == user.id;

  bool get isEditProductsVisible => isOwner || order.hasFeatureEditProducts;

  bool get canEditProducts =>
      isOwner ||
      (order.status == OrderStatus.inProgress && order.hasFeatureEditProducts);

  bool get canAddToCart => isOwner || order.status == OrderStatus.inProgress;

  bool get isSeeParticipantsVisible =>
      isOwner || order.hasFeatureSeeParticipants;

  bool get hasExtraEntries => isOwner;

  bool get canAddDiscount =>
      isOwner &&
      cart!.indexOfFirst((CartEntry entry) => entry is Discount) == -1;

  bool get canAddTransport =>
      isOwner &&
      cart!.indexOfFirst((CartEntry entry) => entry is Transport) == -1;

  static bool buildWhen(OrderState previous, OrderState current) {
    return previous.refreshState != current.refreshState ||
        previous.inProgressAction != current.inProgressAction ||
        previous.order != current.order ||
        previous.user != current.user ||
        previous.owner != current.owner ||
        previous.participants != current.participants ||
        previous.products != current.products ||
        previous.cart != current.cart;
  }

  static bool listenWhen(OrderState previous, OrderState current) {
    return current.effect != null;
  }
}
