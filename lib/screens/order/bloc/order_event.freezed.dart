// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'order_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$OrderEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderEventCopyWith<$Res> {
  factory $OrderEventCopyWith(
          OrderEvent value, $Res Function(OrderEvent) then) =
      _$OrderEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$OrderEventCopyWithImpl<$Res> implements $OrderEventCopyWith<$Res> {
  _$OrderEventCopyWithImpl(this._value, this._then);

  final OrderEvent _value;
  // ignore: unused_field
  final $Res Function(OrderEvent) _then;
}

/// @nodoc
abstract class _$$ScreenStartedCopyWith<$Res> {
  factory _$$ScreenStartedCopyWith(
          _$ScreenStarted value, $Res Function(_$ScreenStarted) then) =
      __$$ScreenStartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ScreenStartedCopyWithImpl<$Res> extends _$OrderEventCopyWithImpl<$Res>
    implements _$$ScreenStartedCopyWith<$Res> {
  __$$ScreenStartedCopyWithImpl(
      _$ScreenStarted _value, $Res Function(_$ScreenStarted) _then)
      : super(_value, (v) => _then(v as _$ScreenStarted));

  @override
  _$ScreenStarted get _value => super._value as _$ScreenStarted;
}

/// @nodoc

class _$ScreenStarted implements ScreenStarted {
  _$ScreenStarted();

  @override
  String toString() {
    return 'OrderEvent.screenStarted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ScreenStarted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return screenStarted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return screenStarted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (screenStarted != null) {
      return screenStarted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return screenStarted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return screenStarted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (screenStarted != null) {
      return screenStarted(this);
    }
    return orElse();
  }
}

abstract class ScreenStarted implements OrderEvent {
  factory ScreenStarted() = _$ScreenStarted;
}

/// @nodoc
abstract class _$$AppResumedCopyWith<$Res> {
  factory _$$AppResumedCopyWith(
          _$AppResumed value, $Res Function(_$AppResumed) then) =
      __$$AppResumedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AppResumedCopyWithImpl<$Res> extends _$OrderEventCopyWithImpl<$Res>
    implements _$$AppResumedCopyWith<$Res> {
  __$$AppResumedCopyWithImpl(
      _$AppResumed _value, $Res Function(_$AppResumed) _then)
      : super(_value, (v) => _then(v as _$AppResumed));

  @override
  _$AppResumed get _value => super._value as _$AppResumed;
}

/// @nodoc

class _$AppResumed implements AppResumed {
  _$AppResumed();

  @override
  String toString() {
    return 'OrderEvent.appResumed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AppResumed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return appResumed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return appResumed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (appResumed != null) {
      return appResumed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return appResumed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return appResumed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (appResumed != null) {
      return appResumed(this);
    }
    return orElse();
  }
}

abstract class AppResumed implements OrderEvent {
  factory AppResumed() = _$AppResumed;
}

/// @nodoc
abstract class _$$OrderUpdatedCopyWith<$Res> {
  factory _$$OrderUpdatedCopyWith(
          _$OrderUpdated value, $Res Function(_$OrderUpdated) then) =
      __$$OrderUpdatedCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$OrderUpdatedCopyWithImpl<$Res> extends _$OrderEventCopyWithImpl<$Res>
    implements _$$OrderUpdatedCopyWith<$Res> {
  __$$OrderUpdatedCopyWithImpl(
      _$OrderUpdated _value, $Res Function(_$OrderUpdated) _then)
      : super(_value, (v) => _then(v as _$OrderUpdated));

  @override
  _$OrderUpdated get _value => super._value as _$OrderUpdated;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$OrderUpdated(
      order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$OrderUpdated implements OrderUpdated {
  _$OrderUpdated(this.order);

  @override
  final Order order;

  @override
  String toString() {
    return 'OrderEvent.orderUpdated(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrderUpdated &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$OrderUpdatedCopyWith<_$OrderUpdated> get copyWith =>
      __$$OrderUpdatedCopyWithImpl<_$OrderUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return orderUpdated(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return orderUpdated?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (orderUpdated != null) {
      return orderUpdated(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return orderUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return orderUpdated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (orderUpdated != null) {
      return orderUpdated(this);
    }
    return orElse();
  }
}

abstract class OrderUpdated implements OrderEvent {
  factory OrderUpdated(final Order order) = _$OrderUpdated;

  Order get order;
  @JsonKey(ignore: true)
  _$$OrderUpdatedCopyWith<_$OrderUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ProductCreatedCopyWith<$Res> {
  factory _$$ProductCreatedCopyWith(
          _$ProductCreated value, $Res Function(_$ProductCreated) then) =
      __$$ProductCreatedCopyWithImpl<$Res>;
  $Res call({Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$ProductCreatedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$ProductCreatedCopyWith<$Res> {
  __$$ProductCreatedCopyWithImpl(
      _$ProductCreated _value, $Res Function(_$ProductCreated) _then)
      : super(_value, (v) => _then(v as _$ProductCreated));

  @override
  _$ProductCreated get _value => super._value as _$ProductCreated;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$ProductCreated(
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$ProductCreated implements ProductCreated {
  _$ProductCreated(this.product);

  @override
  final Product product;

  @override
  String toString() {
    return 'OrderEvent.productCreated(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductCreated &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$ProductCreatedCopyWith<_$ProductCreated> get copyWith =>
      __$$ProductCreatedCopyWithImpl<_$ProductCreated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return productCreated(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return productCreated?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (productCreated != null) {
      return productCreated(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return productCreated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return productCreated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (productCreated != null) {
      return productCreated(this);
    }
    return orElse();
  }
}

abstract class ProductCreated implements OrderEvent {
  factory ProductCreated(final Product product) = _$ProductCreated;

  Product get product;
  @JsonKey(ignore: true)
  _$$ProductCreatedCopyWith<_$ProductCreated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ProductUpdatedCopyWith<$Res> {
  factory _$$ProductUpdatedCopyWith(
          _$ProductUpdated value, $Res Function(_$ProductUpdated) then) =
      __$$ProductUpdatedCopyWithImpl<$Res>;
  $Res call({Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$ProductUpdatedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$ProductUpdatedCopyWith<$Res> {
  __$$ProductUpdatedCopyWithImpl(
      _$ProductUpdated _value, $Res Function(_$ProductUpdated) _then)
      : super(_value, (v) => _then(v as _$ProductUpdated));

  @override
  _$ProductUpdated get _value => super._value as _$ProductUpdated;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$ProductUpdated(
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$ProductUpdated implements ProductUpdated {
  _$ProductUpdated(this.product);

  @override
  final Product product;

  @override
  String toString() {
    return 'OrderEvent.productUpdated(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductUpdated &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$ProductUpdatedCopyWith<_$ProductUpdated> get copyWith =>
      __$$ProductUpdatedCopyWithImpl<_$ProductUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return productUpdated(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return productUpdated?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (productUpdated != null) {
      return productUpdated(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return productUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return productUpdated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (productUpdated != null) {
      return productUpdated(this);
    }
    return orElse();
  }
}

abstract class ProductUpdated implements OrderEvent {
  factory ProductUpdated(final Product product) = _$ProductUpdated;

  Product get product;
  @JsonKey(ignore: true)
  _$$ProductUpdatedCopyWith<_$ProductUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CartUpdatedCopyWith<$Res> {
  factory _$$CartUpdatedCopyWith(
          _$CartUpdated value, $Res Function(_$CartUpdated) then) =
      __$$CartUpdatedCopyWithImpl<$Res>;
  $Res call(
      {KtList<CartEntry> cart, String? modifiedProductId, int? numItemsToAdd});
}

/// @nodoc
class __$$CartUpdatedCopyWithImpl<$Res> extends _$OrderEventCopyWithImpl<$Res>
    implements _$$CartUpdatedCopyWith<$Res> {
  __$$CartUpdatedCopyWithImpl(
      _$CartUpdated _value, $Res Function(_$CartUpdated) _then)
      : super(_value, (v) => _then(v as _$CartUpdated));

  @override
  _$CartUpdated get _value => super._value as _$CartUpdated;

  @override
  $Res call({
    Object? cart = freezed,
    Object? modifiedProductId = freezed,
    Object? numItemsToAdd = freezed,
  }) {
    return _then(_$CartUpdated(
      cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>,
      modifiedProductId: modifiedProductId == freezed
          ? _value.modifiedProductId
          : modifiedProductId // ignore: cast_nullable_to_non_nullable
              as String?,
      numItemsToAdd: numItemsToAdd == freezed
          ? _value.numItemsToAdd
          : numItemsToAdd // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$CartUpdated implements CartUpdated {
  _$CartUpdated(this.cart, {this.modifiedProductId, this.numItemsToAdd});

  @override
  final KtList<CartEntry> cart;
  @override
  final String? modifiedProductId;
  @override
  final int? numItemsToAdd;

  @override
  String toString() {
    return 'OrderEvent.cartUpdated(cart: $cart, modifiedProductId: $modifiedProductId, numItemsToAdd: $numItemsToAdd)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CartUpdated &&
            const DeepCollectionEquality().equals(other.cart, cart) &&
            const DeepCollectionEquality()
                .equals(other.modifiedProductId, modifiedProductId) &&
            const DeepCollectionEquality()
                .equals(other.numItemsToAdd, numItemsToAdd));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(cart),
      const DeepCollectionEquality().hash(modifiedProductId),
      const DeepCollectionEquality().hash(numItemsToAdd));

  @JsonKey(ignore: true)
  @override
  _$$CartUpdatedCopyWith<_$CartUpdated> get copyWith =>
      __$$CartUpdatedCopyWithImpl<_$CartUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return cartUpdated(cart, modifiedProductId, numItemsToAdd);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return cartUpdated?.call(cart, modifiedProductId, numItemsToAdd);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (cartUpdated != null) {
      return cartUpdated(cart, modifiedProductId, numItemsToAdd);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return cartUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return cartUpdated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (cartUpdated != null) {
      return cartUpdated(this);
    }
    return orElse();
  }
}

abstract class CartUpdated implements OrderEvent {
  factory CartUpdated(final KtList<CartEntry> cart,
      {final String? modifiedProductId,
      final int? numItemsToAdd}) = _$CartUpdated;

  KtList<CartEntry> get cart;
  String? get modifiedProductId;
  int? get numItemsToAdd;
  @JsonKey(ignore: true)
  _$$CartUpdatedCopyWith<_$CartUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ParticipantChangedCopyWith<$Res> {
  factory _$$ParticipantChangedCopyWith(_$ParticipantChanged value,
          $Res Function(_$ParticipantChanged) then) =
      __$$ParticipantChangedCopyWithImpl<$Res>;
  $Res call({Participant participant});

  $ParticipantCopyWith<$Res> get participant;
}

/// @nodoc
class __$$ParticipantChangedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$ParticipantChangedCopyWith<$Res> {
  __$$ParticipantChangedCopyWithImpl(
      _$ParticipantChanged _value, $Res Function(_$ParticipantChanged) _then)
      : super(_value, (v) => _then(v as _$ParticipantChanged));

  @override
  _$ParticipantChanged get _value => super._value as _$ParticipantChanged;

  @override
  $Res call({
    Object? participant = freezed,
  }) {
    return _then(_$ParticipantChanged(
      participant: participant == freezed
          ? _value.participant
          : participant // ignore: cast_nullable_to_non_nullable
              as Participant,
    ));
  }

  @override
  $ParticipantCopyWith<$Res> get participant {
    return $ParticipantCopyWith<$Res>(_value.participant, (value) {
      return _then(_value.copyWith(participant: value));
    });
  }
}

/// @nodoc

class _$ParticipantChanged implements ParticipantChanged {
  _$ParticipantChanged({required this.participant});

  @override
  final Participant participant;

  @override
  String toString() {
    return 'OrderEvent.participantChanged(participant: $participant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ParticipantChanged &&
            const DeepCollectionEquality()
                .equals(other.participant, participant));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(participant));

  @JsonKey(ignore: true)
  @override
  _$$ParticipantChangedCopyWith<_$ParticipantChanged> get copyWith =>
      __$$ParticipantChangedCopyWithImpl<_$ParticipantChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return participantChanged(participant);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return participantChanged?.call(participant);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (participantChanged != null) {
      return participantChanged(participant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return participantChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return participantChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (participantChanged != null) {
      return participantChanged(this);
    }
    return orElse();
  }
}

abstract class ParticipantChanged implements OrderEvent {
  factory ParticipantChanged({required final Participant participant}) =
      _$ParticipantChanged;

  Participant get participant;
  @JsonKey(ignore: true)
  _$$ParticipantChangedCopyWith<_$ParticipantChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RefreshRequestedCopyWith<$Res> {
  factory _$$RefreshRequestedCopyWith(
          _$RefreshRequested value, $Res Function(_$RefreshRequested) then) =
      __$$RefreshRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RefreshRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$RefreshRequestedCopyWith<$Res> {
  __$$RefreshRequestedCopyWithImpl(
      _$RefreshRequested _value, $Res Function(_$RefreshRequested) _then)
      : super(_value, (v) => _then(v as _$RefreshRequested));

  @override
  _$RefreshRequested get _value => super._value as _$RefreshRequested;
}

/// @nodoc

class _$RefreshRequested implements RefreshRequested {
  _$RefreshRequested();

  @override
  String toString() {
    return 'OrderEvent.refreshRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RefreshRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return refreshRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return refreshRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (refreshRequested != null) {
      return refreshRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return refreshRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return refreshRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (refreshRequested != null) {
      return refreshRequested(this);
    }
    return orElse();
  }
}

abstract class RefreshRequested implements OrderEvent {
  factory RefreshRequested() = _$RefreshRequested;
}

/// @nodoc
abstract class _$$LinkRequestedCopyWith<$Res> {
  factory _$$LinkRequestedCopyWith(
          _$LinkRequested value, $Res Function(_$LinkRequested) then) =
      __$$LinkRequestedCopyWithImpl<$Res>;
  $Res call({String url});
}

/// @nodoc
class __$$LinkRequestedCopyWithImpl<$Res> extends _$OrderEventCopyWithImpl<$Res>
    implements _$$LinkRequestedCopyWith<$Res> {
  __$$LinkRequestedCopyWithImpl(
      _$LinkRequested _value, $Res Function(_$LinkRequested) _then)
      : super(_value, (v) => _then(v as _$LinkRequested));

  @override
  _$LinkRequested get _value => super._value as _$LinkRequested;

  @override
  $Res call({
    Object? url = freezed,
  }) {
    return _then(_$LinkRequested(
      url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LinkRequested implements LinkRequested {
  _$LinkRequested(this.url);

  @override
  final String url;

  @override
  String toString() {
    return 'OrderEvent.linkRequested(url: $url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LinkRequested &&
            const DeepCollectionEquality().equals(other.url, url));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(url));

  @JsonKey(ignore: true)
  @override
  _$$LinkRequestedCopyWith<_$LinkRequested> get copyWith =>
      __$$LinkRequestedCopyWithImpl<_$LinkRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return linkRequested(url);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return linkRequested?.call(url);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (linkRequested != null) {
      return linkRequested(url);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return linkRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return linkRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (linkRequested != null) {
      return linkRequested(this);
    }
    return orElse();
  }
}

abstract class LinkRequested implements OrderEvent {
  factory LinkRequested(final String url) = _$LinkRequested;

  String get url;
  @JsonKey(ignore: true)
  _$$LinkRequestedCopyWith<_$LinkRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InviteRequestedCopyWith<$Res> {
  factory _$$InviteRequestedCopyWith(
          _$InviteRequested value, $Res Function(_$InviteRequested) then) =
      __$$InviteRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InviteRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$InviteRequestedCopyWith<$Res> {
  __$$InviteRequestedCopyWithImpl(
      _$InviteRequested _value, $Res Function(_$InviteRequested) _then)
      : super(_value, (v) => _then(v as _$InviteRequested));

  @override
  _$InviteRequested get _value => super._value as _$InviteRequested;
}

/// @nodoc

class _$InviteRequested implements InviteRequested {
  _$InviteRequested();

  @override
  String toString() {
    return 'OrderEvent.inviteRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InviteRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return inviteRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return inviteRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (inviteRequested != null) {
      return inviteRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return inviteRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return inviteRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (inviteRequested != null) {
      return inviteRequested(this);
    }
    return orElse();
  }
}

abstract class InviteRequested implements OrderEvent {
  factory InviteRequested() = _$InviteRequested;
}

/// @nodoc
abstract class _$$ViewParticipantsRequestedCopyWith<$Res> {
  factory _$$ViewParticipantsRequestedCopyWith(
          _$ViewParticipantsRequested value,
          $Res Function(_$ViewParticipantsRequested) then) =
      __$$ViewParticipantsRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ViewParticipantsRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$ViewParticipantsRequestedCopyWith<$Res> {
  __$$ViewParticipantsRequestedCopyWithImpl(_$ViewParticipantsRequested _value,
      $Res Function(_$ViewParticipantsRequested) _then)
      : super(_value, (v) => _then(v as _$ViewParticipantsRequested));

  @override
  _$ViewParticipantsRequested get _value =>
      super._value as _$ViewParticipantsRequested;
}

/// @nodoc

class _$ViewParticipantsRequested implements ViewParticipantsRequested {
  _$ViewParticipantsRequested();

  @override
  String toString() {
    return 'OrderEvent.viewParticipantsRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ViewParticipantsRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return viewParticipantsRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return viewParticipantsRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (viewParticipantsRequested != null) {
      return viewParticipantsRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return viewParticipantsRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return viewParticipantsRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (viewParticipantsRequested != null) {
      return viewParticipantsRequested(this);
    }
    return orElse();
  }
}

abstract class ViewParticipantsRequested implements OrderEvent {
  factory ViewParticipantsRequested() = _$ViewParticipantsRequested;
}

/// @nodoc
abstract class _$$EditOrderRequestedCopyWith<$Res> {
  factory _$$EditOrderRequestedCopyWith(_$EditOrderRequested value,
          $Res Function(_$EditOrderRequested) then) =
      __$$EditOrderRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EditOrderRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$EditOrderRequestedCopyWith<$Res> {
  __$$EditOrderRequestedCopyWithImpl(
      _$EditOrderRequested _value, $Res Function(_$EditOrderRequested) _then)
      : super(_value, (v) => _then(v as _$EditOrderRequested));

  @override
  _$EditOrderRequested get _value => super._value as _$EditOrderRequested;
}

/// @nodoc

class _$EditOrderRequested implements EditOrderRequested {
  _$EditOrderRequested();

  @override
  String toString() {
    return 'OrderEvent.editOrderRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EditOrderRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return editOrderRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return editOrderRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (editOrderRequested != null) {
      return editOrderRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return editOrderRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return editOrderRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (editOrderRequested != null) {
      return editOrderRequested(this);
    }
    return orElse();
  }
}

abstract class EditOrderRequested implements OrderEvent {
  factory EditOrderRequested() = _$EditOrderRequested;
}

/// @nodoc
abstract class _$$StatusChangeRequestedCopyWith<$Res> {
  factory _$$StatusChangeRequestedCopyWith(_$StatusChangeRequested value,
          $Res Function(_$StatusChangeRequested) then) =
      __$$StatusChangeRequestedCopyWithImpl<$Res>;
  $Res call({OrderStatus status});
}

/// @nodoc
class __$$StatusChangeRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$StatusChangeRequestedCopyWith<$Res> {
  __$$StatusChangeRequestedCopyWithImpl(_$StatusChangeRequested _value,
      $Res Function(_$StatusChangeRequested) _then)
      : super(_value, (v) => _then(v as _$StatusChangeRequested));

  @override
  _$StatusChangeRequested get _value => super._value as _$StatusChangeRequested;

  @override
  $Res call({
    Object? status = freezed,
  }) {
    return _then(_$StatusChangeRequested(
      status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as OrderStatus,
    ));
  }
}

/// @nodoc

class _$StatusChangeRequested implements StatusChangeRequested {
  _$StatusChangeRequested(this.status);

  @override
  final OrderStatus status;

  @override
  String toString() {
    return 'OrderEvent.statusChangeRequested(status: $status)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StatusChangeRequested &&
            const DeepCollectionEquality().equals(other.status, status));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(status));

  @JsonKey(ignore: true)
  @override
  _$$StatusChangeRequestedCopyWith<_$StatusChangeRequested> get copyWith =>
      __$$StatusChangeRequestedCopyWithImpl<_$StatusChangeRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return statusChangeRequested(status);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return statusChangeRequested?.call(status);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (statusChangeRequested != null) {
      return statusChangeRequested(status);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return statusChangeRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return statusChangeRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (statusChangeRequested != null) {
      return statusChangeRequested(this);
    }
    return orElse();
  }
}

abstract class StatusChangeRequested implements OrderEvent {
  factory StatusChangeRequested(final OrderStatus status) =
      _$StatusChangeRequested;

  OrderStatus get status;
  @JsonKey(ignore: true)
  _$$StatusChangeRequestedCopyWith<_$StatusChangeRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DeleteOrderRequestedCopyWith<$Res> {
  factory _$$DeleteOrderRequestedCopyWith(_$DeleteOrderRequested value,
          $Res Function(_$DeleteOrderRequested) then) =
      __$$DeleteOrderRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DeleteOrderRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$DeleteOrderRequestedCopyWith<$Res> {
  __$$DeleteOrderRequestedCopyWithImpl(_$DeleteOrderRequested _value,
      $Res Function(_$DeleteOrderRequested) _then)
      : super(_value, (v) => _then(v as _$DeleteOrderRequested));

  @override
  _$DeleteOrderRequested get _value => super._value as _$DeleteOrderRequested;
}

/// @nodoc

class _$DeleteOrderRequested implements DeleteOrderRequested {
  _$DeleteOrderRequested();

  @override
  String toString() {
    return 'OrderEvent.deleteOrderRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DeleteOrderRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return deleteOrderRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return deleteOrderRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (deleteOrderRequested != null) {
      return deleteOrderRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return deleteOrderRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return deleteOrderRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (deleteOrderRequested != null) {
      return deleteOrderRequested(this);
    }
    return orElse();
  }
}

abstract class DeleteOrderRequested implements OrderEvent {
  factory DeleteOrderRequested() = _$DeleteOrderRequested;
}

/// @nodoc
abstract class _$$CartRequestedCopyWith<$Res> {
  factory _$$CartRequestedCopyWith(
          _$CartRequested value, $Res Function(_$CartRequested) then) =
      __$$CartRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CartRequestedCopyWithImpl<$Res> extends _$OrderEventCopyWithImpl<$Res>
    implements _$$CartRequestedCopyWith<$Res> {
  __$$CartRequestedCopyWithImpl(
      _$CartRequested _value, $Res Function(_$CartRequested) _then)
      : super(_value, (v) => _then(v as _$CartRequested));

  @override
  _$CartRequested get _value => super._value as _$CartRequested;
}

/// @nodoc

class _$CartRequested implements CartRequested {
  _$CartRequested();

  @override
  String toString() {
    return 'OrderEvent.cartRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CartRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return cartRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return cartRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (cartRequested != null) {
      return cartRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return cartRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return cartRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (cartRequested != null) {
      return cartRequested(this);
    }
    return orElse();
  }
}

abstract class CartRequested implements OrderEvent {
  factory CartRequested() = _$CartRequested;
}

/// @nodoc
abstract class _$$AddProductRequestedCopyWith<$Res> {
  factory _$$AddProductRequestedCopyWith(_$AddProductRequested value,
          $Res Function(_$AddProductRequested) then) =
      __$$AddProductRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AddProductRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$AddProductRequestedCopyWith<$Res> {
  __$$AddProductRequestedCopyWithImpl(
      _$AddProductRequested _value, $Res Function(_$AddProductRequested) _then)
      : super(_value, (v) => _then(v as _$AddProductRequested));

  @override
  _$AddProductRequested get _value => super._value as _$AddProductRequested;
}

/// @nodoc

class _$AddProductRequested implements AddProductRequested {
  _$AddProductRequested();

  @override
  String toString() {
    return 'OrderEvent.addProductRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AddProductRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return addProductRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return addProductRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (addProductRequested != null) {
      return addProductRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return addProductRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return addProductRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (addProductRequested != null) {
      return addProductRequested(this);
    }
    return orElse();
  }
}

abstract class AddProductRequested implements OrderEvent {
  factory AddProductRequested() = _$AddProductRequested;
}

/// @nodoc
abstract class _$$EditProductRequestedCopyWith<$Res> {
  factory _$$EditProductRequestedCopyWith(_$EditProductRequested value,
          $Res Function(_$EditProductRequested) then) =
      __$$EditProductRequestedCopyWithImpl<$Res>;
  $Res call({Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$EditProductRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$EditProductRequestedCopyWith<$Res> {
  __$$EditProductRequestedCopyWithImpl(_$EditProductRequested _value,
      $Res Function(_$EditProductRequested) _then)
      : super(_value, (v) => _then(v as _$EditProductRequested));

  @override
  _$EditProductRequested get _value => super._value as _$EditProductRequested;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$EditProductRequested(
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$EditProductRequested implements EditProductRequested {
  _$EditProductRequested(this.product);

  @override
  final Product product;

  @override
  String toString() {
    return 'OrderEvent.editProductRequested(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EditProductRequested &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$EditProductRequestedCopyWith<_$EditProductRequested> get copyWith =>
      __$$EditProductRequestedCopyWithImpl<_$EditProductRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return editProductRequested(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return editProductRequested?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (editProductRequested != null) {
      return editProductRequested(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return editProductRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return editProductRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (editProductRequested != null) {
      return editProductRequested(this);
    }
    return orElse();
  }
}

abstract class EditProductRequested implements OrderEvent {
  factory EditProductRequested(final Product product) = _$EditProductRequested;

  Product get product;
  @JsonKey(ignore: true)
  _$$EditProductRequestedCopyWith<_$EditProductRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DeleteProductRequestedCopyWith<$Res> {
  factory _$$DeleteProductRequestedCopyWith(_$DeleteProductRequested value,
          $Res Function(_$DeleteProductRequested) then) =
      __$$DeleteProductRequestedCopyWithImpl<$Res>;
  $Res call({Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$DeleteProductRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$DeleteProductRequestedCopyWith<$Res> {
  __$$DeleteProductRequestedCopyWithImpl(_$DeleteProductRequested _value,
      $Res Function(_$DeleteProductRequested) _then)
      : super(_value, (v) => _then(v as _$DeleteProductRequested));

  @override
  _$DeleteProductRequested get _value =>
      super._value as _$DeleteProductRequested;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$DeleteProductRequested(
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$DeleteProductRequested implements DeleteProductRequested {
  _$DeleteProductRequested(this.product);

  @override
  final Product product;

  @override
  String toString() {
    return 'OrderEvent.deleteProductRequested(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DeleteProductRequested &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$DeleteProductRequestedCopyWith<_$DeleteProductRequested> get copyWith =>
      __$$DeleteProductRequestedCopyWithImpl<_$DeleteProductRequested>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return deleteProductRequested(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return deleteProductRequested?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (deleteProductRequested != null) {
      return deleteProductRequested(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return deleteProductRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return deleteProductRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (deleteProductRequested != null) {
      return deleteProductRequested(this);
    }
    return orElse();
  }
}

abstract class DeleteProductRequested implements OrderEvent {
  factory DeleteProductRequested(final Product product) =
      _$DeleteProductRequested;

  Product get product;
  @JsonKey(ignore: true)
  _$$DeleteProductRequestedCopyWith<_$DeleteProductRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddProductToCartRequestedCopyWith<$Res> {
  factory _$$AddProductToCartRequestedCopyWith(
          _$AddProductToCartRequested value,
          $Res Function(_$AddProductToCartRequested) then) =
      __$$AddProductToCartRequestedCopyWithImpl<$Res>;
  $Res call({Product product, int numItems, String details});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$AddProductToCartRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$AddProductToCartRequestedCopyWith<$Res> {
  __$$AddProductToCartRequestedCopyWithImpl(_$AddProductToCartRequested _value,
      $Res Function(_$AddProductToCartRequested) _then)
      : super(_value, (v) => _then(v as _$AddProductToCartRequested));

  @override
  _$AddProductToCartRequested get _value =>
      super._value as _$AddProductToCartRequested;

  @override
  $Res call({
    Object? product = freezed,
    Object? numItems = freezed,
    Object? details = freezed,
  }) {
    return _then(_$AddProductToCartRequested(
      product: product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
      numItems: numItems == freezed
          ? _value.numItems
          : numItems // ignore: cast_nullable_to_non_nullable
              as int,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$AddProductToCartRequested implements AddProductToCartRequested {
  _$AddProductToCartRequested(
      {required this.product, required this.numItems, required this.details});

  @override
  final Product product;
  @override
  final int numItems;
  @override
  final String details;

  @override
  String toString() {
    return 'OrderEvent.addProductToCartRequested(product: $product, numItems: $numItems, details: $details)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddProductToCartRequested &&
            const DeepCollectionEquality().equals(other.product, product) &&
            const DeepCollectionEquality().equals(other.numItems, numItems) &&
            const DeepCollectionEquality().equals(other.details, details));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(product),
      const DeepCollectionEquality().hash(numItems),
      const DeepCollectionEquality().hash(details));

  @JsonKey(ignore: true)
  @override
  _$$AddProductToCartRequestedCopyWith<_$AddProductToCartRequested>
      get copyWith => __$$AddProductToCartRequestedCopyWithImpl<
          _$AddProductToCartRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return addProductToCartRequested(product, numItems, details);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return addProductToCartRequested?.call(product, numItems, details);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (addProductToCartRequested != null) {
      return addProductToCartRequested(product, numItems, details);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return addProductToCartRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return addProductToCartRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (addProductToCartRequested != null) {
      return addProductToCartRequested(this);
    }
    return orElse();
  }
}

abstract class AddProductToCartRequested implements OrderEvent {
  factory AddProductToCartRequested(
      {required final Product product,
      required final int numItems,
      required final String details}) = _$AddProductToCartRequested;

  Product get product;
  int get numItems;
  String get details;
  @JsonKey(ignore: true)
  _$$AddProductToCartRequestedCopyWith<_$AddProductToCartRequested>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddDiscountToCartRequestedCopyWith<$Res> {
  factory _$$AddDiscountToCartRequestedCopyWith(
          _$AddDiscountToCartRequested value,
          $Res Function(_$AddDiscountToCartRequested) then) =
      __$$AddDiscountToCartRequestedCopyWithImpl<$Res>;
  $Res call({double percent, String details});
}

/// @nodoc
class __$$AddDiscountToCartRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$AddDiscountToCartRequestedCopyWith<$Res> {
  __$$AddDiscountToCartRequestedCopyWithImpl(
      _$AddDiscountToCartRequested _value,
      $Res Function(_$AddDiscountToCartRequested) _then)
      : super(_value, (v) => _then(v as _$AddDiscountToCartRequested));

  @override
  _$AddDiscountToCartRequested get _value =>
      super._value as _$AddDiscountToCartRequested;

  @override
  $Res call({
    Object? percent = freezed,
    Object? details = freezed,
  }) {
    return _then(_$AddDiscountToCartRequested(
      percent: percent == freezed
          ? _value.percent
          : percent // ignore: cast_nullable_to_non_nullable
              as double,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AddDiscountToCartRequested implements AddDiscountToCartRequested {
  _$AddDiscountToCartRequested({required this.percent, required this.details});

  @override
  final double percent;
  @override
  final String details;

  @override
  String toString() {
    return 'OrderEvent.addDiscountToCartRequested(percent: $percent, details: $details)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddDiscountToCartRequested &&
            const DeepCollectionEquality().equals(other.percent, percent) &&
            const DeepCollectionEquality().equals(other.details, details));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(percent),
      const DeepCollectionEquality().hash(details));

  @JsonKey(ignore: true)
  @override
  _$$AddDiscountToCartRequestedCopyWith<_$AddDiscountToCartRequested>
      get copyWith => __$$AddDiscountToCartRequestedCopyWithImpl<
          _$AddDiscountToCartRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return addDiscountToCartRequested(percent, details);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return addDiscountToCartRequested?.call(percent, details);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (addDiscountToCartRequested != null) {
      return addDiscountToCartRequested(percent, details);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return addDiscountToCartRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return addDiscountToCartRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (addDiscountToCartRequested != null) {
      return addDiscountToCartRequested(this);
    }
    return orElse();
  }
}

abstract class AddDiscountToCartRequested implements OrderEvent {
  factory AddDiscountToCartRequested(
      {required final double percent,
      required final String details}) = _$AddDiscountToCartRequested;

  double get percent;
  String get details;
  @JsonKey(ignore: true)
  _$$AddDiscountToCartRequestedCopyWith<_$AddDiscountToCartRequested>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddTransportToCartRequestedCopyWith<$Res> {
  factory _$$AddTransportToCartRequestedCopyWith(
          _$AddTransportToCartRequested value,
          $Res Function(_$AddTransportToCartRequested) then) =
      __$$AddTransportToCartRequestedCopyWithImpl<$Res>;
  $Res call({double cost, String details});
}

/// @nodoc
class __$$AddTransportToCartRequestedCopyWithImpl<$Res>
    extends _$OrderEventCopyWithImpl<$Res>
    implements _$$AddTransportToCartRequestedCopyWith<$Res> {
  __$$AddTransportToCartRequestedCopyWithImpl(
      _$AddTransportToCartRequested _value,
      $Res Function(_$AddTransportToCartRequested) _then)
      : super(_value, (v) => _then(v as _$AddTransportToCartRequested));

  @override
  _$AddTransportToCartRequested get _value =>
      super._value as _$AddTransportToCartRequested;

  @override
  $Res call({
    Object? cost = freezed,
    Object? details = freezed,
  }) {
    return _then(_$AddTransportToCartRequested(
      cost: cost == freezed
          ? _value.cost
          : cost // ignore: cast_nullable_to_non_nullable
              as double,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AddTransportToCartRequested implements AddTransportToCartRequested {
  _$AddTransportToCartRequested({required this.cost, required this.details});

  @override
  final double cost;
  @override
  final String details;

  @override
  String toString() {
    return 'OrderEvent.addTransportToCartRequested(cost: $cost, details: $details)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddTransportToCartRequested &&
            const DeepCollectionEquality().equals(other.cost, cost) &&
            const DeepCollectionEquality().equals(other.details, details));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(cost),
      const DeepCollectionEquality().hash(details));

  @JsonKey(ignore: true)
  @override
  _$$AddTransportToCartRequestedCopyWith<_$AddTransportToCartRequested>
      get copyWith => __$$AddTransportToCartRequestedCopyWithImpl<
          _$AddTransportToCartRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() appResumed,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(Product product) productCreated,
    required TResult Function(Product product) productUpdated,
    required TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)
        cartUpdated,
    required TResult Function(Participant participant) participantChanged,
    required TResult Function() refreshRequested,
    required TResult Function(String url) linkRequested,
    required TResult Function() inviteRequested,
    required TResult Function() viewParticipantsRequested,
    required TResult Function() editOrderRequested,
    required TResult Function(OrderStatus status) statusChangeRequested,
    required TResult Function() deleteOrderRequested,
    required TResult Function() cartRequested,
    required TResult Function() addProductRequested,
    required TResult Function(Product product) editProductRequested,
    required TResult Function(Product product) deleteProductRequested,
    required TResult Function(Product product, int numItems, String details)
        addProductToCartRequested,
    required TResult Function(double percent, String details)
        addDiscountToCartRequested,
    required TResult Function(double cost, String details)
        addTransportToCartRequested,
  }) {
    return addTransportToCartRequested(cost, details);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
  }) {
    return addTransportToCartRequested?.call(cost, details);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? appResumed,
    TResult Function(Order order)? orderUpdated,
    TResult Function(Product product)? productCreated,
    TResult Function(Product product)? productUpdated,
    TResult Function(KtList<CartEntry> cart, String? modifiedProductId,
            int? numItemsToAdd)?
        cartUpdated,
    TResult Function(Participant participant)? participantChanged,
    TResult Function()? refreshRequested,
    TResult Function(String url)? linkRequested,
    TResult Function()? inviteRequested,
    TResult Function()? viewParticipantsRequested,
    TResult Function()? editOrderRequested,
    TResult Function(OrderStatus status)? statusChangeRequested,
    TResult Function()? deleteOrderRequested,
    TResult Function()? cartRequested,
    TResult Function()? addProductRequested,
    TResult Function(Product product)? editProductRequested,
    TResult Function(Product product)? deleteProductRequested,
    TResult Function(Product product, int numItems, String details)?
        addProductToCartRequested,
    TResult Function(double percent, String details)?
        addDiscountToCartRequested,
    TResult Function(double cost, String details)? addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (addTransportToCartRequested != null) {
      return addTransportToCartRequested(cost, details);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(AppResumed value) appResumed,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(ProductCreated value) productCreated,
    required TResult Function(ProductUpdated value) productUpdated,
    required TResult Function(CartUpdated value) cartUpdated,
    required TResult Function(ParticipantChanged value) participantChanged,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(LinkRequested value) linkRequested,
    required TResult Function(InviteRequested value) inviteRequested,
    required TResult Function(ViewParticipantsRequested value)
        viewParticipantsRequested,
    required TResult Function(EditOrderRequested value) editOrderRequested,
    required TResult Function(StatusChangeRequested value)
        statusChangeRequested,
    required TResult Function(DeleteOrderRequested value) deleteOrderRequested,
    required TResult Function(CartRequested value) cartRequested,
    required TResult Function(AddProductRequested value) addProductRequested,
    required TResult Function(EditProductRequested value) editProductRequested,
    required TResult Function(DeleteProductRequested value)
        deleteProductRequested,
    required TResult Function(AddProductToCartRequested value)
        addProductToCartRequested,
    required TResult Function(AddDiscountToCartRequested value)
        addDiscountToCartRequested,
    required TResult Function(AddTransportToCartRequested value)
        addTransportToCartRequested,
  }) {
    return addTransportToCartRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
  }) {
    return addTransportToCartRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(AppResumed value)? appResumed,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(ProductCreated value)? productCreated,
    TResult Function(ProductUpdated value)? productUpdated,
    TResult Function(CartUpdated value)? cartUpdated,
    TResult Function(ParticipantChanged value)? participantChanged,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(LinkRequested value)? linkRequested,
    TResult Function(InviteRequested value)? inviteRequested,
    TResult Function(ViewParticipantsRequested value)?
        viewParticipantsRequested,
    TResult Function(EditOrderRequested value)? editOrderRequested,
    TResult Function(StatusChangeRequested value)? statusChangeRequested,
    TResult Function(DeleteOrderRequested value)? deleteOrderRequested,
    TResult Function(CartRequested value)? cartRequested,
    TResult Function(AddProductRequested value)? addProductRequested,
    TResult Function(EditProductRequested value)? editProductRequested,
    TResult Function(DeleteProductRequested value)? deleteProductRequested,
    TResult Function(AddProductToCartRequested value)?
        addProductToCartRequested,
    TResult Function(AddDiscountToCartRequested value)?
        addDiscountToCartRequested,
    TResult Function(AddTransportToCartRequested value)?
        addTransportToCartRequested,
    required TResult orElse(),
  }) {
    if (addTransportToCartRequested != null) {
      return addTransportToCartRequested(this);
    }
    return orElse();
  }
}

abstract class AddTransportToCartRequested implements OrderEvent {
  factory AddTransportToCartRequested(
      {required final double cost,
      required final String details}) = _$AddTransportToCartRequested;

  double get cost;
  String get details;
  @JsonKey(ignore: true)
  _$$AddTransportToCartRequestedCopyWith<_$AddTransportToCartRequested>
      get copyWith => throw _privateConstructorUsedError;
}
