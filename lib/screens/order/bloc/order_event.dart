import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';

part 'order_event.freezed.dart';

@freezed
class OrderEvent with _$OrderEvent {
  factory OrderEvent.screenStarted() = ScreenStarted;

  factory OrderEvent.appResumed() = AppResumed;

  factory OrderEvent.orderUpdated(Order order) = OrderUpdated;

  factory OrderEvent.productCreated(Product product) = ProductCreated;

  factory OrderEvent.productUpdated(Product product) = ProductUpdated;

  factory OrderEvent.cartUpdated(
    KtList<CartEntry> cart, {
    String? modifiedProductId,
    int? numItemsToAdd,
  }) = CartUpdated;

  factory OrderEvent.participantChanged({
    required Participant participant,
  }) = ParticipantChanged;

  factory OrderEvent.refreshRequested() = RefreshRequested;

  factory OrderEvent.linkRequested(String url) = LinkRequested;

  factory OrderEvent.inviteRequested() = InviteRequested;

  factory OrderEvent.viewParticipantsRequested() = ViewParticipantsRequested;

  factory OrderEvent.editOrderRequested() = EditOrderRequested;

  factory OrderEvent.statusChangeRequested(OrderStatus status) =
      StatusChangeRequested;

  factory OrderEvent.deleteOrderRequested() = DeleteOrderRequested;

  factory OrderEvent.cartRequested() = CartRequested;

  factory OrderEvent.addProductRequested() = AddProductRequested;

  factory OrderEvent.editProductRequested(Product product) =
      EditProductRequested;

  factory OrderEvent.deleteProductRequested(Product product) =
      DeleteProductRequested;

  factory OrderEvent.addProductToCartRequested({
    required Product product,
    required int numItems,
    required String details,
  }) = AddProductToCartRequested;

  factory OrderEvent.addDiscountToCartRequested({
    required double percent,
    required String details,
  }) = AddDiscountToCartRequested;

  factory OrderEvent.addTransportToCartRequested({
    required double cost,
    required String details,
  }) = AddTransportToCartRequested;
}
