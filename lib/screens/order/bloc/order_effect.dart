import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';

part 'order_effect.freezed.dart';

@freezed
class OrderEffect with _$OrderEffect {
  factory OrderEffect.goToHome() = GoToHome;

  factory OrderEffect.goToCart() = GoToCart;

  factory OrderEffect.goToEditProduct({Product? product}) = GoToEditProduct;

  factory OrderEffect.goToEditOrder(Order order) = GoToEditOrder;

  factory OrderEffect.goToInvite({required Order order}) = GoToInvite;

  factory OrderEffect.goToParticipants({
    required Order order,
    required KtList<Participant> participants,
    required KtList<CartEntry> cart,
    required bool isOwner,
    required Participant owner,
  }) = GoToParticipants;

  factory OrderEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory OrderEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
