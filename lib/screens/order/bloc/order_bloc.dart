import 'package:bloc/bloc.dart';
import 'package:kt_dart/kt.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart' as data_event;
import 'package:orders/data/local/misc/launcher.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';
import 'package:orders/model/validators.dart';
import 'package:orders/screens/order/bloc/order_effect.dart';
import 'package:orders/screens/order/bloc/order_event.dart';
import 'package:orders/screens/order/bloc/order_state.dart';
import 'package:orders/screens/utils/refresh_state.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/add_entry_to_cart.dart';
import 'package:orders/usecases/add_product_to_cart.dart';
import 'package:orders/usecases/delete_product.dart';
import 'package:orders/usecases/load_order_data.dart';
import 'package:orders/usecases/update_order.dart';
import 'package:orders/usecases/use_case_results.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  OrderBloc(this.dataBloc, Order order)
      : super(OrderState.initial(order, dataBloc.user));

  final DataBloc dataBloc;

  final LoadOrderData _loadOrderData = getIt<LoadOrderData>();
  final AddProductToCart _addProductToCart = getIt<AddProductToCart>();
  final AddEntryToCart _addEntryToCart = getIt<AddEntryToCart>();
  final DeleteProduct _deleteProduct = getIt<DeleteProduct>();
  final UpdateOrder _updateOrder = getIt<UpdateOrder>();
  final Launcher _launcher = getIt<Launcher>();

  @override
  Stream<OrderState> mapEventToState(OrderEvent event) async* {
    if (event is ScreenStarted) {
      yield* _refresh(state);
    }
    if (event is AppResumed) {
      yield* _refresh(state);
    }
    if (event is OrderUpdated) {
      yield state.copyWith(order: event.order);
    }
    if (event is ProductCreated) {
      yield state.copyWith(
        products:
            state.products!.plusElement(event.product).sortedBy(_sortProducts),
      );
    }
    if (event is ProductUpdated) {
      yield state.copyWith(
          products: state.products!
              .map((Product it) =>
                  event.product.id == it.id ? event.product : it)
              .toList()
              .sortedBy(_sortProducts));
    }
    if (event is CartUpdated) {
      KtList<Product> products = state.products!;
      if (event.modifiedProductId != null) {
        products = state.products!
            .map((Product it) {
              final Stock stock = it.stock;
              return event.modifiedProductId == it.id && stock is Limited
                  ? it.copyWith(
                      stock: Limited(
                          numItemsAvailable: stock.numItemsAvailable +
                              (event.numItemsToAdd ?? 0)))
                  : it;
            })
            .toList()
            .sortedBy(_sortProducts);
      }
      yield state.copyWith(cart: event.cart, products: products);
    }
    if (event is ParticipantChanged) {
      final KtList<Participant> participants = state.participants!.map(
          (Participant it) =>
              it.userId == event.participant.userId ? event.participant : it);
      yield state.copyWith(participants: participants);
    }
    if (event is RefreshRequested) {
      yield* _refresh(state);
    }
    if (event is LinkRequested) {
      _launcher.openExternalUrl(event.url);
    }
    if (event is InviteRequested) {
      yield* _withEffect(state, GoToInvite(order: state.order));
    }
    if (event is ViewParticipantsRequested) {
      yield* _withEffect(
        state,
        GoToParticipants(
          order: state.order,
          participants: state.participants!,
          cart: state.cart!,
          isOwner: state.isOwner,
          owner: state.owner!,
        ),
      );
    }
    if (event is EditOrderRequested) {
      yield* _withEffect(state, GoToEditOrder(state.order));
    }
    if (event is StatusChangeRequested) {
      yield state.copyWith(inProgressAction: true);
      final UseCaseResult result =
          await _updateOrder(orderId: state.order.id, status: event.status);
      if (result is UseCaseSuccess) {
        final Order modifiedOrder = state.order.copyWith(status: event.status);
        yield state.copyWith(inProgressAction: false, order: modifiedOrder);
        dataBloc.add(data_event.OrderUpdated(order: modifiedOrder));
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.orderStatusErrorMsg));
      }
    }
    if (event is DeleteOrderRequested) {
      yield state.copyWith(inProgressAction: true);
      final UseCaseResult result =
          await _updateOrder(orderId: state.order.id, isDeleted: true);
      if (result is UseCaseSuccess) {
        yield* _withEffect(state.copyWith(inProgressAction: false), GoToHome());
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.orderDeletionErrorMsg));
      }
    }
    if (event is CartRequested) {
      yield* _withEffect(state, GoToCart());
    }
    if (event is AddProductRequested) {
      yield* _withEffect(state, GoToEditProduct());
    }
    if (event is EditProductRequested) {
      yield* _withEffect(state, GoToEditProduct(product: event.product));
    }
    if (event is DeleteProductRequested) {
      yield state.copyWith(inProgressAction: true);
      final UseCaseResult result = await _deleteProduct(
          orderId: state.order.id, productId: event.product.id);
      if (result is UseCaseSuccess) {
        yield* _withEffect(
            state.copyWith(
              inProgressAction: false,
              products: state.products!
                  .minusElement(event.product)
                  .toList()
                  .sortedBy(_sortProducts),
              cart: state.cart!.filterNot((CartEntry it) =>
                  it is CartProduct && it.productId == event.product.id),
            ),
            ShowInfoSnackBar(Strings.orderProductRemoveSuccess));
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.orderProductRemoveErrorMsg));
      }
    }
    if (event is AddProductToCartRequested) {
      yield state.copyWith(inProgressAction: true);
      final AddProductToCartResult result = await _addProductToCart(
        orderId: state.order.id,
        product: event.product,
        details: event.details,
        numItems: event.numItems,
      );
      if (result is AddProductToCartSuccess) {
        final Product product = event.product;
        final Stock stock = product.stock;
        KtList<Product> products = state.products!;
        if (stock is Limited) {
          products = products
              .map((Product it) => product.id == it.id
                  ? product.copyWith(
                      stock: Limited(
                          numItemsAvailable:
                              stock.numItemsAvailable - event.numItems))
                  : it)
              .toList()
              .sortedBy(_sortProducts);
        }
        yield* _withEffect(
            state.copyWith(
              inProgressAction: false,
              cart: state.cart!.plusElement(result.cartEntry),
              products: products,
            ),
            ShowInfoSnackBar(Strings.orderAddProductToCartSuccess));
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.orderAddProductToCartErrorMsg));
      }
    }
    if (event is AddDiscountToCartRequested) {
      yield state.copyWith(inProgressAction: true);
      final AddEntryToCartResult result = await _addEntryToCart(
        orderId: state.order.id,
        entry: Discount(details: event.details, percent: event.percent),
      );
      if (result is AddEntryToCartSuccess) {
        yield* _withEffect(
            state.copyWith(
              inProgressAction: false,
              cart: state.cart!.plusElement(result.cartEntry),
            ),
            ShowInfoSnackBar(Strings.orderAddEntryToCartSuccess));
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.orderAddEntryToCartErrorMsg));
      }
    }
    if (event is AddTransportToCartRequested) {
      yield state.copyWith(inProgressAction: true);
      final AddEntryToCartResult result = await _addEntryToCart(
        orderId: state.order.id,
        entry: Transport(details: event.details, cost: event.cost),
      );
      if (result is AddEntryToCartSuccess) {
        yield* _withEffect(
            state.copyWith(
              inProgressAction: false,
              cart: state.cart!.plusElement(result.cartEntry),
            ),
            ShowInfoSnackBar(Strings.orderAddEntryToCartSuccess));
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.orderAddEntryToCartErrorMsg));
      }
    }
  }

  Stream<OrderState> _refresh(OrderState state) async* {
    yield state.copyWith(refreshState: RefreshState.inProgress());
    final LoadOrderDataResult result =
        await _loadOrderData(orderId: state.order.id);
    if (result is LoadOrderDataSuccess) {
      yield state.copyWith(
        refreshState: RefreshState.success(),
        order: result.order,
        owner: result.owner,
        participants:
            result.participants.sortedBy((Participant it) => it.displayName),
        products: result.products.sortedBy(_sortProducts),
        cart: result.cart,
      );
    } else {
      yield state.copyWith(refreshState: RefreshState.error());
    }
  }

  String? percentValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    final double? val = double.tryParse(value!.trim().replaceFirst(',', '.'));
    if (val == null) {
      return Strings.orderPercentInvalid;
    }
    if (val < 1 || val > 99) {
      return Strings.orderPercentInvalid;
    }
    return null;
  }

  String? costValidator(String? value) {
    return Validators.price(
      value: value,
      invalidMsg: Strings.orderTransportInvalid,
    );
  }

  Stream<OrderState> _withEffect(OrderState state, OrderEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

Comparable<dynamic> _sortProducts(Product it) => it.title;

// ignore: unused_element
final Logger _logger = Logger('OrderBloc');
