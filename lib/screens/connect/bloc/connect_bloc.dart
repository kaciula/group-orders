import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/data/remote/connectors/apple_connector.dart';
import 'package:orders/data/remote/connectors/connector_results.dart';
import 'package:orders/data/remote/connectors/facebook_connector.dart';
import 'package:orders/data/remote/connectors/google_connector.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/connect/bloc/connect_effect.dart';
import 'package:orders/screens/connect/bloc/connect_event.dart';
import 'package:orders/screens/connect/bloc/connect_state.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/check_user_existing.dart';
import 'package:orders/usecases/set_up_after_sign_in.dart';
import 'package:orders/usecases/use_case_results.dart';
import 'package:universal_io/io.dart';

class ConnectBloc extends Bloc<ConnectEvent, ConnectState> {
  ConnectBloc()
      : super(ConnectState.initial(appleSignInSupported: Platform.isIOS));

  final GoogleConnector _googleConnector = getIt<GoogleConnector>();
  final AppleConnector _appleConnector = getIt<AppleConnector>();
  final FacebookConnector _facebookConnector = getIt<FacebookConnector>();
  final SetUpAfterSignIn _setUpAfterSignIn = getIt<SetUpAfterSignIn>();
  final CheckUserExisting _checkUserExisting = getIt<CheckUserExisting>();

  @override
  Stream<ConnectState> mapEventToState(ConnectEvent event) async* {
    if (event is ScreenStarted) {
      final bool isSupported = await _appleConnector.isSupported();
      yield state.copyWith(appleSignInSupported: isSupported);
    }
    if (event is ConnectGoogleRequested) {
      yield state.copyWith(inProgressAction: true);
      final SignInResult result = await _googleConnector.signIn();
      yield* _dealWithSignInResult(result);
    }
    if (event is ConnectAppleRequested) {
      yield state.copyWith(inProgressAction: true);
      final SignInResult result = await _appleConnector.signIn();
      yield* _dealWithSignInResult(result);
    }
    if (event is ConnectFacebookRequested) {
      yield state.copyWith(inProgressAction: true);
      final SignInResult result =
          await _facebookConnector.signIn(event.context);
      yield* _dealWithSignInResult(result);
    }
    if (event is ConnectEmailRequested) {
      yield* _withEffect(state, GoToSignUp());
    }
  }

  Stream<ConnectState> _dealWithSignInResult(SignInResult result) async* {
    if (result is SignInSuccess) {
      final User user = result.user;
      final CheckUserExistingResult existingResult =
          await _checkUserExisting(user);
      if (existingResult is CheckUserExistingSuccess) {
        if (existingResult.userExists) {
          final UseCaseResult useCaseResult =
              await _setUpAfterSignIn(user, checkHasAccount: true);
          if (useCaseResult is UseCaseSuccess) {
            yield* _withEffect(state, GoToHome());
          } else {
            yield* _withEffect(state.copyWith(inProgressAction: false),
                ShowErrorSnackBar(errorMsg: Strings.connectSignInErrorMsg));
          }
        } else {
          yield* _withEffect(state, GoToVerifyPhone(user));
        }
      } else {
        yield* _withEffect(
          state.copyWith(inProgressAction: false),
          ShowErrorSnackBar(
            errorMsg: '${Strings.connectSignInErrorMsg}.',
          ),
        );
      }
    } else {
      final SignInFailure failure = result as SignInFailure;
      if (failure.type != SignInFailureType.canceled) {
        yield* _withEffect(
          state.copyWith(inProgressAction: false),
          ShowErrorSnackBar(
            errorMsg:
                '${Strings.connectSignInErrorMsg}. ${failureMsg(failure.type)}',
          ),
        );
      } else {
        yield state.copyWith(inProgressAction: false);
      }
    }
  }

  Stream<ConnectState> _withEffect(
      ConnectState state, ConnectEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('ConnectBloc');
