// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'connect_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ConnectEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screensStarted,
    required TResult Function() connectEmailRequested,
    required TResult Function() connectGoogleRequested,
    required TResult Function() connectAppleRequested,
    required TResult Function(BuildContext context) connectFacebookRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screensStarted,
    required TResult Function(ConnectEmailRequested value)
        connectEmailRequested,
    required TResult Function(ConnectGoogleRequested value)
        connectGoogleRequested,
    required TResult Function(ConnectAppleRequested value)
        connectAppleRequested,
    required TResult Function(ConnectFacebookRequested value)
        connectFacebookRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ConnectEventCopyWith<$Res> {
  factory $ConnectEventCopyWith(
          ConnectEvent value, $Res Function(ConnectEvent) then) =
      _$ConnectEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ConnectEventCopyWithImpl<$Res> implements $ConnectEventCopyWith<$Res> {
  _$ConnectEventCopyWithImpl(this._value, this._then);

  final ConnectEvent _value;
  // ignore: unused_field
  final $Res Function(ConnectEvent) _then;
}

/// @nodoc
abstract class _$$ScreenStartedCopyWith<$Res> {
  factory _$$ScreenStartedCopyWith(
          _$ScreenStarted value, $Res Function(_$ScreenStarted) then) =
      __$$ScreenStartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ScreenStartedCopyWithImpl<$Res>
    extends _$ConnectEventCopyWithImpl<$Res>
    implements _$$ScreenStartedCopyWith<$Res> {
  __$$ScreenStartedCopyWithImpl(
      _$ScreenStarted _value, $Res Function(_$ScreenStarted) _then)
      : super(_value, (v) => _then(v as _$ScreenStarted));

  @override
  _$ScreenStarted get _value => super._value as _$ScreenStarted;
}

/// @nodoc

class _$ScreenStarted implements ScreenStarted {
  _$ScreenStarted();

  @override
  String toString() {
    return 'ConnectEvent.screensStarted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ScreenStarted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screensStarted,
    required TResult Function() connectEmailRequested,
    required TResult Function() connectGoogleRequested,
    required TResult Function() connectAppleRequested,
    required TResult Function(BuildContext context) connectFacebookRequested,
  }) {
    return screensStarted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
  }) {
    return screensStarted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (screensStarted != null) {
      return screensStarted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screensStarted,
    required TResult Function(ConnectEmailRequested value)
        connectEmailRequested,
    required TResult Function(ConnectGoogleRequested value)
        connectGoogleRequested,
    required TResult Function(ConnectAppleRequested value)
        connectAppleRequested,
    required TResult Function(ConnectFacebookRequested value)
        connectFacebookRequested,
  }) {
    return screensStarted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
  }) {
    return screensStarted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (screensStarted != null) {
      return screensStarted(this);
    }
    return orElse();
  }
}

abstract class ScreenStarted implements ConnectEvent {
  factory ScreenStarted() = _$ScreenStarted;
}

/// @nodoc
abstract class _$$ConnectEmailRequestedCopyWith<$Res> {
  factory _$$ConnectEmailRequestedCopyWith(_$ConnectEmailRequested value,
          $Res Function(_$ConnectEmailRequested) then) =
      __$$ConnectEmailRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ConnectEmailRequestedCopyWithImpl<$Res>
    extends _$ConnectEventCopyWithImpl<$Res>
    implements _$$ConnectEmailRequestedCopyWith<$Res> {
  __$$ConnectEmailRequestedCopyWithImpl(_$ConnectEmailRequested _value,
      $Res Function(_$ConnectEmailRequested) _then)
      : super(_value, (v) => _then(v as _$ConnectEmailRequested));

  @override
  _$ConnectEmailRequested get _value => super._value as _$ConnectEmailRequested;
}

/// @nodoc

class _$ConnectEmailRequested implements ConnectEmailRequested {
  _$ConnectEmailRequested();

  @override
  String toString() {
    return 'ConnectEvent.connectEmailRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ConnectEmailRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screensStarted,
    required TResult Function() connectEmailRequested,
    required TResult Function() connectGoogleRequested,
    required TResult Function() connectAppleRequested,
    required TResult Function(BuildContext context) connectFacebookRequested,
  }) {
    return connectEmailRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
  }) {
    return connectEmailRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (connectEmailRequested != null) {
      return connectEmailRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screensStarted,
    required TResult Function(ConnectEmailRequested value)
        connectEmailRequested,
    required TResult Function(ConnectGoogleRequested value)
        connectGoogleRequested,
    required TResult Function(ConnectAppleRequested value)
        connectAppleRequested,
    required TResult Function(ConnectFacebookRequested value)
        connectFacebookRequested,
  }) {
    return connectEmailRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
  }) {
    return connectEmailRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (connectEmailRequested != null) {
      return connectEmailRequested(this);
    }
    return orElse();
  }
}

abstract class ConnectEmailRequested implements ConnectEvent {
  factory ConnectEmailRequested() = _$ConnectEmailRequested;
}

/// @nodoc
abstract class _$$ConnectGoogleRequestedCopyWith<$Res> {
  factory _$$ConnectGoogleRequestedCopyWith(_$ConnectGoogleRequested value,
          $Res Function(_$ConnectGoogleRequested) then) =
      __$$ConnectGoogleRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ConnectGoogleRequestedCopyWithImpl<$Res>
    extends _$ConnectEventCopyWithImpl<$Res>
    implements _$$ConnectGoogleRequestedCopyWith<$Res> {
  __$$ConnectGoogleRequestedCopyWithImpl(_$ConnectGoogleRequested _value,
      $Res Function(_$ConnectGoogleRequested) _then)
      : super(_value, (v) => _then(v as _$ConnectGoogleRequested));

  @override
  _$ConnectGoogleRequested get _value =>
      super._value as _$ConnectGoogleRequested;
}

/// @nodoc

class _$ConnectGoogleRequested implements ConnectGoogleRequested {
  _$ConnectGoogleRequested();

  @override
  String toString() {
    return 'ConnectEvent.connectGoogleRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ConnectGoogleRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screensStarted,
    required TResult Function() connectEmailRequested,
    required TResult Function() connectGoogleRequested,
    required TResult Function() connectAppleRequested,
    required TResult Function(BuildContext context) connectFacebookRequested,
  }) {
    return connectGoogleRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
  }) {
    return connectGoogleRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (connectGoogleRequested != null) {
      return connectGoogleRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screensStarted,
    required TResult Function(ConnectEmailRequested value)
        connectEmailRequested,
    required TResult Function(ConnectGoogleRequested value)
        connectGoogleRequested,
    required TResult Function(ConnectAppleRequested value)
        connectAppleRequested,
    required TResult Function(ConnectFacebookRequested value)
        connectFacebookRequested,
  }) {
    return connectGoogleRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
  }) {
    return connectGoogleRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (connectGoogleRequested != null) {
      return connectGoogleRequested(this);
    }
    return orElse();
  }
}

abstract class ConnectGoogleRequested implements ConnectEvent {
  factory ConnectGoogleRequested() = _$ConnectGoogleRequested;
}

/// @nodoc
abstract class _$$ConnectAppleRequestedCopyWith<$Res> {
  factory _$$ConnectAppleRequestedCopyWith(_$ConnectAppleRequested value,
          $Res Function(_$ConnectAppleRequested) then) =
      __$$ConnectAppleRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ConnectAppleRequestedCopyWithImpl<$Res>
    extends _$ConnectEventCopyWithImpl<$Res>
    implements _$$ConnectAppleRequestedCopyWith<$Res> {
  __$$ConnectAppleRequestedCopyWithImpl(_$ConnectAppleRequested _value,
      $Res Function(_$ConnectAppleRequested) _then)
      : super(_value, (v) => _then(v as _$ConnectAppleRequested));

  @override
  _$ConnectAppleRequested get _value => super._value as _$ConnectAppleRequested;
}

/// @nodoc

class _$ConnectAppleRequested implements ConnectAppleRequested {
  _$ConnectAppleRequested();

  @override
  String toString() {
    return 'ConnectEvent.connectAppleRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ConnectAppleRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screensStarted,
    required TResult Function() connectEmailRequested,
    required TResult Function() connectGoogleRequested,
    required TResult Function() connectAppleRequested,
    required TResult Function(BuildContext context) connectFacebookRequested,
  }) {
    return connectAppleRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
  }) {
    return connectAppleRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (connectAppleRequested != null) {
      return connectAppleRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screensStarted,
    required TResult Function(ConnectEmailRequested value)
        connectEmailRequested,
    required TResult Function(ConnectGoogleRequested value)
        connectGoogleRequested,
    required TResult Function(ConnectAppleRequested value)
        connectAppleRequested,
    required TResult Function(ConnectFacebookRequested value)
        connectFacebookRequested,
  }) {
    return connectAppleRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
  }) {
    return connectAppleRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (connectAppleRequested != null) {
      return connectAppleRequested(this);
    }
    return orElse();
  }
}

abstract class ConnectAppleRequested implements ConnectEvent {
  factory ConnectAppleRequested() = _$ConnectAppleRequested;
}

/// @nodoc
abstract class _$$ConnectFacebookRequestedCopyWith<$Res> {
  factory _$$ConnectFacebookRequestedCopyWith(_$ConnectFacebookRequested value,
          $Res Function(_$ConnectFacebookRequested) then) =
      __$$ConnectFacebookRequestedCopyWithImpl<$Res>;
  $Res call({BuildContext context});
}

/// @nodoc
class __$$ConnectFacebookRequestedCopyWithImpl<$Res>
    extends _$ConnectEventCopyWithImpl<$Res>
    implements _$$ConnectFacebookRequestedCopyWith<$Res> {
  __$$ConnectFacebookRequestedCopyWithImpl(_$ConnectFacebookRequested _value,
      $Res Function(_$ConnectFacebookRequested) _then)
      : super(_value, (v) => _then(v as _$ConnectFacebookRequested));

  @override
  _$ConnectFacebookRequested get _value =>
      super._value as _$ConnectFacebookRequested;

  @override
  $Res call({
    Object? context = freezed,
  }) {
    return _then(_$ConnectFacebookRequested(
      context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
    ));
  }
}

/// @nodoc

class _$ConnectFacebookRequested implements ConnectFacebookRequested {
  _$ConnectFacebookRequested(this.context);

  @override
  final BuildContext context;

  @override
  String toString() {
    return 'ConnectEvent.connectFacebookRequested(context: $context)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ConnectFacebookRequested &&
            const DeepCollectionEquality().equals(other.context, context));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(context));

  @JsonKey(ignore: true)
  @override
  _$$ConnectFacebookRequestedCopyWith<_$ConnectFacebookRequested>
      get copyWith =>
          __$$ConnectFacebookRequestedCopyWithImpl<_$ConnectFacebookRequested>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screensStarted,
    required TResult Function() connectEmailRequested,
    required TResult Function() connectGoogleRequested,
    required TResult Function() connectAppleRequested,
    required TResult Function(BuildContext context) connectFacebookRequested,
  }) {
    return connectFacebookRequested(context);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
  }) {
    return connectFacebookRequested?.call(context);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screensStarted,
    TResult Function()? connectEmailRequested,
    TResult Function()? connectGoogleRequested,
    TResult Function()? connectAppleRequested,
    TResult Function(BuildContext context)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (connectFacebookRequested != null) {
      return connectFacebookRequested(context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screensStarted,
    required TResult Function(ConnectEmailRequested value)
        connectEmailRequested,
    required TResult Function(ConnectGoogleRequested value)
        connectGoogleRequested,
    required TResult Function(ConnectAppleRequested value)
        connectAppleRequested,
    required TResult Function(ConnectFacebookRequested value)
        connectFacebookRequested,
  }) {
    return connectFacebookRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
  }) {
    return connectFacebookRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screensStarted,
    TResult Function(ConnectEmailRequested value)? connectEmailRequested,
    TResult Function(ConnectGoogleRequested value)? connectGoogleRequested,
    TResult Function(ConnectAppleRequested value)? connectAppleRequested,
    TResult Function(ConnectFacebookRequested value)? connectFacebookRequested,
    required TResult orElse(),
  }) {
    if (connectFacebookRequested != null) {
      return connectFacebookRequested(this);
    }
    return orElse();
  }
}

abstract class ConnectFacebookRequested implements ConnectEvent {
  factory ConnectFacebookRequested(final BuildContext context) =
      _$ConnectFacebookRequested;

  BuildContext get context;
  @JsonKey(ignore: true)
  _$$ConnectFacebookRequestedCopyWith<_$ConnectFacebookRequested>
      get copyWith => throw _privateConstructorUsedError;
}
