import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'connect_event.freezed.dart';

@freezed
class ConnectEvent with _$ConnectEvent {
  factory ConnectEvent.screensStarted() = ScreenStarted;

  factory ConnectEvent.connectEmailRequested() = ConnectEmailRequested;

  factory ConnectEvent.connectGoogleRequested() = ConnectGoogleRequested;

  factory ConnectEvent.connectAppleRequested() = ConnectAppleRequested;

  factory ConnectEvent.connectFacebookRequested(BuildContext context) =
      ConnectFacebookRequested;
}
