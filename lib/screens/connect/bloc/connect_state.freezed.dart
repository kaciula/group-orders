// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'connect_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ConnectState {
  bool get inProgressAction => throw _privateConstructorUsedError;
  bool get appleSignInSupported => throw _privateConstructorUsedError;
  ConnectEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ConnectStateCopyWith<ConnectState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ConnectStateCopyWith<$Res> {
  factory $ConnectStateCopyWith(
          ConnectState value, $Res Function(ConnectState) then) =
      _$ConnectStateCopyWithImpl<$Res>;
  $Res call(
      {bool inProgressAction,
      bool appleSignInSupported,
      ConnectEffect? effect});

  $ConnectEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$ConnectStateCopyWithImpl<$Res> implements $ConnectStateCopyWith<$Res> {
  _$ConnectStateCopyWithImpl(this._value, this._then);

  final ConnectState _value;
  // ignore: unused_field
  final $Res Function(ConnectState) _then;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? appleSignInSupported = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      appleSignInSupported: appleSignInSupported == freezed
          ? _value.appleSignInSupported
          : appleSignInSupported // ignore: cast_nullable_to_non_nullable
              as bool,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as ConnectEffect?,
    ));
  }

  @override
  $ConnectEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $ConnectEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_ConnectStateCopyWith<$Res>
    implements $ConnectStateCopyWith<$Res> {
  factory _$$_ConnectStateCopyWith(
          _$_ConnectState value, $Res Function(_$_ConnectState) then) =
      __$$_ConnectStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool inProgressAction,
      bool appleSignInSupported,
      ConnectEffect? effect});

  @override
  $ConnectEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_ConnectStateCopyWithImpl<$Res>
    extends _$ConnectStateCopyWithImpl<$Res>
    implements _$$_ConnectStateCopyWith<$Res> {
  __$$_ConnectStateCopyWithImpl(
      _$_ConnectState _value, $Res Function(_$_ConnectState) _then)
      : super(_value, (v) => _then(v as _$_ConnectState));

  @override
  _$_ConnectState get _value => super._value as _$_ConnectState;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? appleSignInSupported = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_ConnectState(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      appleSignInSupported: appleSignInSupported == freezed
          ? _value.appleSignInSupported
          : appleSignInSupported // ignore: cast_nullable_to_non_nullable
              as bool,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as ConnectEffect?,
    ));
  }
}

/// @nodoc

class _$_ConnectState implements _ConnectState {
  _$_ConnectState(
      {required this.inProgressAction,
      required this.appleSignInSupported,
      this.effect});

  @override
  final bool inProgressAction;
  @override
  final bool appleSignInSupported;
  @override
  final ConnectEffect? effect;

  @override
  String toString() {
    return 'ConnectState(inProgressAction: $inProgressAction, appleSignInSupported: $appleSignInSupported, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ConnectState &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality()
                .equals(other.appleSignInSupported, appleSignInSupported) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(appleSignInSupported),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_ConnectStateCopyWith<_$_ConnectState> get copyWith =>
      __$$_ConnectStateCopyWithImpl<_$_ConnectState>(this, _$identity);
}

abstract class _ConnectState implements ConnectState {
  factory _ConnectState(
      {required final bool inProgressAction,
      required final bool appleSignInSupported,
      final ConnectEffect? effect}) = _$_ConnectState;

  @override
  bool get inProgressAction;
  @override
  bool get appleSignInSupported;
  @override
  ConnectEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_ConnectStateCopyWith<_$_ConnectState> get copyWith =>
      throw _privateConstructorUsedError;
}
