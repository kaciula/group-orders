import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/user.dart';

part 'connect_effect.freezed.dart';

@freezed
class ConnectEffect with _$ConnectEffect {
  factory ConnectEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory ConnectEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;

  factory ConnectEffect.goToSignUp() = GoToSignUp;

  factory ConnectEffect.goToHome() = GoToHome;

  factory ConnectEffect.goToVerifyPhone(User user) = GoToVerifyPhone;
}
