import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/screens/connect/bloc/connect_effect.dart';

part 'connect_state.freezed.dart';

@freezed
class ConnectState with _$ConnectState {
  factory ConnectState({
    required bool inProgressAction,
    required bool appleSignInSupported,
    ConnectEffect? effect,
  }) = _ConnectState;

  factory ConnectState.initial({required bool appleSignInSupported}) {
    return ConnectState(
      inProgressAction: false,
      appleSignInSupported: appleSignInSupported,
    );
  }

  static bool buildWhen(ConnectState previous, ConnectState current) {
    return previous.inProgressAction != current.inProgressAction ||
        previous.appleSignInSupported != current.appleSignInSupported;
  }

  static bool listenWhen(ConnectState previous, ConnectState current) {
    return current.effect != null;
  }
}
