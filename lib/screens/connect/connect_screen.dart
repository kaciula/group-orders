import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/app/app_styles.dart';
import 'package:orders/screens/connect/bloc/connect_bloc.dart';
import 'package:orders/screens/connect/bloc/connect_effect.dart';
import 'package:orders/screens/connect/bloc/connect_event.dart';
import 'package:orders/screens/connect/bloc/connect_state.dart';
import 'package:orders/screens/home/home_screen.dart';
import 'package:orders/screens/sign_up/sign_up_screen.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/verify_phone/verify_phone_screen.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class ConnectScreen extends StatefulWidget {
  static const String routeName = 'connect';

  @override
  State<ConnectScreen> createState() => _ConnectScreenState();
}

class _ConnectScreenState extends State<ConnectScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectBloc, ConnectState>(
      bloc: BlocProvider.of<ConnectBloc>(context),
      buildWhen: ConnectState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, ConnectState state) {
    final ConnectEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToSignUp) {
      Navigator.pushNamed(context, SignUpScreen.routeName);
    }
    if (effect is GoToHome) {
      Navigator.pushReplacementNamed(context, HomeScreen.routeName);
    }
    if (effect is GoToVerifyPhone) {
      Navigator.pushReplacementNamed(context, VerifyPhoneScreen.routeName,
          arguments: effect.user);
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, ConnectState state) {
    final ConnectBloc bloc = BlocProvider.of<ConnectBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(title: Text(Strings.connectTitle)),
      body: BlocListener<ConnectBloc, ConnectState>(
        bloc: bloc,
        listenWhen: ConnectState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, ConnectState state) {
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: ListView(
        padding: const EdgeInsets.all(16),
        children: <Widget>[
          Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: Strings.connectIntro1,
                  style: styleDirections.copyWith(
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                TextSpan(text: Strings.connectIntro2, style: styleDirections),
              ],
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 32),
          RaisedBtn(
            label: Strings.connectEmail,
            icon: const Icon(Icons.email),
            color: AppColors.connectEmail,
            textColor: AppColors.textWhite,
            onPressed: () => _bloc(context).add(ConnectEmailRequested()),
          ),
          const SizedBox(height: 16),
          RaisedBtn(
            label: Strings.connectGoogle,
            icon: SvgPicture.asset('assets/ic_google.svg'),
            color: AppColors.connectGoogle,
            textColor: AppColors.textWhite,
            onPressed: () => _bloc(context).add(ConnectGoogleRequested()),
          ),
          const SizedBox(height: 16),
          RaisedBtn(
            label: Strings.connectFacebook,
            icon: SvgPicture.asset('assets/ic_facebook.svg'),
            color: AppColors.connectFacebook,
            textColor: AppColors.textWhite,
            onPressed: () =>
                _bloc(context).add(ConnectFacebookRequested(context)),
          ),
          if (state.appleSignInSupported) ..._apple(context),
        ],
      ),
    );
  }

  List<Widget> _apple(BuildContext context) {
    return <Widget>[
      const SizedBox(height: 16),
      SizedBox(
        height: 44,
        child: SignInWithAppleButton(
          onPressed: () => _bloc(context).add(ConnectAppleRequested()),
        ),
      ),
    ];
  }

  ConnectBloc _bloc(BuildContext context) {
    return BlocProvider.of<ConnectBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('ConnectScreen');
