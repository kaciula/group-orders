import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_bloc.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_effect.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_event.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_state.dart';
import 'package:orders/screens/utils/formatter_utils.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/circular_progress.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';
import 'package:orders/screens/widgets/refresh_error_view.dart';

class AcceptInviteScreen extends StatefulWidget {
  static const String routeName = 'accept_invite';

  @override
  State<AcceptInviteScreen> createState() => _AcceptInviteScreenState();
}

class _AcceptInviteScreenState extends State<AcceptInviteScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AcceptInviteBloc, AcceptInviteState>(
      bloc: BlocProvider.of<AcceptInviteBloc>(context),
      buildWhen: AcceptInviteState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, AcceptInviteState state) {
    final AcceptInviteEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToHome) {
      Navigator.pop(context);
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, AcceptInviteState state) {
    final AcceptInviteBloc bloc = BlocProvider.of<AcceptInviteBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(title: Text(Strings.acceptInviteTitle)),
      body: BlocListener<AcceptInviteBloc, AcceptInviteState>(
        bloc: bloc,
        listenWhen: AcceptInviteState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, AcceptInviteState state) {
    return state.refreshState.when(
      inProgress: () => _progress(),
      success: () => _content(context, state),
      error: (bool isInternetConnected) =>
          _error(context, isInternetConnected: isInternetConnected),
    );
  }

  Widget _content(BuildContext context, AcceptInviteState state) {
    final Order order = state.order!;
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: ListView(
        padding: const EdgeInsets.all(16),
        children: <Widget>[
          Text(
            Strings.acceptInviteWelcome(order.ownerName),
            style: const TextStyle(fontSize: 16),
          ),
          const SizedBox(height: 32),
          Text(
            order.title,
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 16),
          Row(
            children: <Widget>[
              Text(Strings.genericDueDate),
              const SizedBox(width: 8),
              Text(
                FormatterUtils.dateTime(context, order.dueDate),
                style: const TextStyle(fontWeight: FontWeight.w300),
              )
            ],
          ),
          const SizedBox(height: 16),
          Text(order.details),
          const SizedBox(height: 32),
          RaisedBtn(
            label: Strings.acceptInviteAction,
            onPressed: () => _bloc(context).add(InvitationAccepted()),
          ),
        ],
      ),
    );
  }

  Widget _progress() {
    return const Center(child: CircularProgress());
  }

  Widget _error(BuildContext context, {required bool isInternetConnected}) {
    return RefreshErrorView(
      errorTitle: isInternetConnected
          ? Strings.genericErrorTitle
          : Strings.genericNoInternetTitle,
      errorMsg: isInternetConnected
          ? Strings.genericErrorMsg
          : Strings.genericNoInternetMsg,
      onRetry: () => _bloc(context).add(RefreshRequested()),
    );
  }

  AcceptInviteBloc _bloc(BuildContext context) {
    return BlocProvider.of<AcceptInviteBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('AcceptInviteScreen');
