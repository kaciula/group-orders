// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'accept_invite_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AcceptInviteState {
  RefreshState get refreshState => throw _privateConstructorUsedError;
  bool get inProgressAction => throw _privateConstructorUsedError;
  Order? get order => throw _privateConstructorUsedError;
  AcceptInviteEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AcceptInviteStateCopyWith<AcceptInviteState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AcceptInviteStateCopyWith<$Res> {
  factory $AcceptInviteStateCopyWith(
          AcceptInviteState value, $Res Function(AcceptInviteState) then) =
      _$AcceptInviteStateCopyWithImpl<$Res>;
  $Res call(
      {RefreshState refreshState,
      bool inProgressAction,
      Order? order,
      AcceptInviteEffect? effect});

  $RefreshStateCopyWith<$Res> get refreshState;
  $OrderCopyWith<$Res>? get order;
  $AcceptInviteEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$AcceptInviteStateCopyWithImpl<$Res>
    implements $AcceptInviteStateCopyWith<$Res> {
  _$AcceptInviteStateCopyWithImpl(this._value, this._then);

  final AcceptInviteState _value;
  // ignore: unused_field
  final $Res Function(AcceptInviteState) _then;

  @override
  $Res call({
    Object? refreshState = freezed,
    Object? inProgressAction = freezed,
    Object? order = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      refreshState: refreshState == freezed
          ? _value.refreshState
          : refreshState // ignore: cast_nullable_to_non_nullable
              as RefreshState,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order?,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as AcceptInviteEffect?,
    ));
  }

  @override
  $RefreshStateCopyWith<$Res> get refreshState {
    return $RefreshStateCopyWith<$Res>(_value.refreshState, (value) {
      return _then(_value.copyWith(refreshState: value));
    });
  }

  @override
  $OrderCopyWith<$Res>? get order {
    if (_value.order == null) {
      return null;
    }

    return $OrderCopyWith<$Res>(_value.order!, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $AcceptInviteEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $AcceptInviteEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_AcceptInviteStateCopyWith<$Res>
    implements $AcceptInviteStateCopyWith<$Res> {
  factory _$$_AcceptInviteStateCopyWith(_$_AcceptInviteState value,
          $Res Function(_$_AcceptInviteState) then) =
      __$$_AcceptInviteStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {RefreshState refreshState,
      bool inProgressAction,
      Order? order,
      AcceptInviteEffect? effect});

  @override
  $RefreshStateCopyWith<$Res> get refreshState;
  @override
  $OrderCopyWith<$Res>? get order;
  @override
  $AcceptInviteEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_AcceptInviteStateCopyWithImpl<$Res>
    extends _$AcceptInviteStateCopyWithImpl<$Res>
    implements _$$_AcceptInviteStateCopyWith<$Res> {
  __$$_AcceptInviteStateCopyWithImpl(
      _$_AcceptInviteState _value, $Res Function(_$_AcceptInviteState) _then)
      : super(_value, (v) => _then(v as _$_AcceptInviteState));

  @override
  _$_AcceptInviteState get _value => super._value as _$_AcceptInviteState;

  @override
  $Res call({
    Object? refreshState = freezed,
    Object? inProgressAction = freezed,
    Object? order = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_AcceptInviteState(
      refreshState: refreshState == freezed
          ? _value.refreshState
          : refreshState // ignore: cast_nullable_to_non_nullable
              as RefreshState,
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order?,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as AcceptInviteEffect?,
    ));
  }
}

/// @nodoc

class _$_AcceptInviteState implements _AcceptInviteState {
  _$_AcceptInviteState(
      {required this.refreshState,
      required this.inProgressAction,
      this.order,
      this.effect});

  @override
  final RefreshState refreshState;
  @override
  final bool inProgressAction;
  @override
  final Order? order;
  @override
  final AcceptInviteEffect? effect;

  @override
  String toString() {
    return 'AcceptInviteState(refreshState: $refreshState, inProgressAction: $inProgressAction, order: $order, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AcceptInviteState &&
            const DeepCollectionEquality()
                .equals(other.refreshState, refreshState) &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(refreshState),
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_AcceptInviteStateCopyWith<_$_AcceptInviteState> get copyWith =>
      __$$_AcceptInviteStateCopyWithImpl<_$_AcceptInviteState>(
          this, _$identity);
}

abstract class _AcceptInviteState implements AcceptInviteState {
  factory _AcceptInviteState(
      {required final RefreshState refreshState,
      required final bool inProgressAction,
      final Order? order,
      final AcceptInviteEffect? effect}) = _$_AcceptInviteState;

  @override
  RefreshState get refreshState;
  @override
  bool get inProgressAction;
  @override
  Order? get order;
  @override
  AcceptInviteEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_AcceptInviteStateCopyWith<_$_AcceptInviteState> get copyWith =>
      throw _privateConstructorUsedError;
}
