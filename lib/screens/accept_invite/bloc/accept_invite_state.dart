import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/order.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_effect.dart';
import 'package:orders/screens/utils/refresh_state.dart';

part 'accept_invite_state.freezed.dart';

@freezed
class AcceptInviteState with _$AcceptInviteState {
  factory AcceptInviteState({
    required RefreshState refreshState,
    required bool inProgressAction,
    Order? order,
    AcceptInviteEffect? effect,
  }) = _AcceptInviteState;

  factory AcceptInviteState.initial() {
    return AcceptInviteState(
      refreshState: RefreshState.inProgress(),
      inProgressAction: false,
    );
  }

  static bool buildWhen(AcceptInviteState previous, AcceptInviteState current) {
    return previous.refreshState != current.refreshState ||
        previous.inProgressAction != current.inProgressAction ||
        previous.order != current.order;
  }

  static bool listenWhen(
      AcceptInviteState previous, AcceptInviteState current) {
    return current.effect != null;
  }
}
