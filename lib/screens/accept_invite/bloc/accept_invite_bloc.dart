import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_effect.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_event.dart';
import 'package:orders/screens/accept_invite/bloc/accept_invite_state.dart';
import 'package:orders/screens/utils/refresh_state.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/usecases/accept_invite.dart';
import 'package:orders/usecases/use_case_results.dart';

class AcceptInviteBloc extends Bloc<AcceptInviteEvent, AcceptInviteState> {
  AcceptInviteBloc(this.dataBloc, this.orderId)
      : super(AcceptInviteState.initial());

  final DataBloc dataBloc;
  final String orderId;

  final DataRemoteStore _dataRemoteStore = getIt<DataRemoteStore>();
  final AcceptInvite _acceptInvite = getIt<AcceptInvite>();

  @override
  Stream<AcceptInviteState> mapEventToState(AcceptInviteEvent event) async* {
    if (event is ScreenStarted) {
      yield* _refresh(state);
    }
    if (event is RefreshRequested) {
      yield* _refresh(state);
    }
    if (event is InvitationAccepted) {
      yield state.copyWith(inProgressAction: true);
      final UseCaseResult result = await _acceptInvite(order: state.order!);
      if (result is UseCaseSuccess) {
        yield* _withEffect(state, GoToHome());
      } else {
        yield* _withEffect(state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.genericErrorSnackBarMsg));
      }
    }
  }

  Stream<AcceptInviteState> _refresh(AcceptInviteState state) async* {
    yield state.copyWith(refreshState: RefreshState.inProgress());
    final GetOrderResult result = await _dataRemoteStore.getOrder(orderId);
    if (result is GetOrderSuccess) {
      if (!result.order.participantIds.contains(dataBloc.user.id)) {
        yield state.copyWith(
            refreshState: RefreshState.success(), order: result.order);
      } else {
        yield* _withEffect(state, GoToHome());
      }
    } else {
      yield state.copyWith(refreshState: RefreshState.error());
    }
  }

  Stream<AcceptInviteState> _withEffect(
      AcceptInviteState state, AcceptInviteEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('AcceptInviteBloc');
