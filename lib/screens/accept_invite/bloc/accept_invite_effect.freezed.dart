// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'accept_invite_effect.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AcceptInviteEffect {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AcceptInviteEffectCopyWith<$Res> {
  factory $AcceptInviteEffectCopyWith(
          AcceptInviteEffect value, $Res Function(AcceptInviteEffect) then) =
      _$AcceptInviteEffectCopyWithImpl<$Res>;
}

/// @nodoc
class _$AcceptInviteEffectCopyWithImpl<$Res>
    implements $AcceptInviteEffectCopyWith<$Res> {
  _$AcceptInviteEffectCopyWithImpl(this._value, this._then);

  final AcceptInviteEffect _value;
  // ignore: unused_field
  final $Res Function(AcceptInviteEffect) _then;
}

/// @nodoc
abstract class _$$GoToHomeCopyWith<$Res> {
  factory _$$GoToHomeCopyWith(
          _$GoToHome value, $Res Function(_$GoToHome) then) =
      __$$GoToHomeCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GoToHomeCopyWithImpl<$Res>
    extends _$AcceptInviteEffectCopyWithImpl<$Res>
    implements _$$GoToHomeCopyWith<$Res> {
  __$$GoToHomeCopyWithImpl(_$GoToHome _value, $Res Function(_$GoToHome) _then)
      : super(_value, (v) => _then(v as _$GoToHome));

  @override
  _$GoToHome get _value => super._value as _$GoToHome;
}

/// @nodoc

class _$GoToHome implements GoToHome {
  _$GoToHome();

  @override
  String toString() {
    return 'AcceptInviteEffect.goToHome()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GoToHome);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToHome();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToHome?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToHome != null) {
      return goToHome();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToHome(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToHome?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToHome != null) {
      return goToHome(this);
    }
    return orElse();
  }
}

abstract class GoToHome implements AcceptInviteEffect {
  factory GoToHome() = _$GoToHome;
}

/// @nodoc
abstract class _$$ShowInfoSnackBarCopyWith<$Res> {
  factory _$$ShowInfoSnackBarCopyWith(
          _$ShowInfoSnackBar value, $Res Function(_$ShowInfoSnackBar) then) =
      __$$ShowInfoSnackBarCopyWithImpl<$Res>;
  $Res call({String msg});
}

/// @nodoc
class __$$ShowInfoSnackBarCopyWithImpl<$Res>
    extends _$AcceptInviteEffectCopyWithImpl<$Res>
    implements _$$ShowInfoSnackBarCopyWith<$Res> {
  __$$ShowInfoSnackBarCopyWithImpl(
      _$ShowInfoSnackBar _value, $Res Function(_$ShowInfoSnackBar) _then)
      : super(_value, (v) => _then(v as _$ShowInfoSnackBar));

  @override
  _$ShowInfoSnackBar get _value => super._value as _$ShowInfoSnackBar;

  @override
  $Res call({
    Object? msg = freezed,
  }) {
    return _then(_$ShowInfoSnackBar(
      msg == freezed
          ? _value.msg
          : msg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ShowInfoSnackBar implements ShowInfoSnackBar {
  _$ShowInfoSnackBar(this.msg);

  @override
  final String msg;

  @override
  String toString() {
    return 'AcceptInviteEffect.showInfoSnackBar(msg: $msg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShowInfoSnackBar &&
            const DeepCollectionEquality().equals(other.msg, msg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(msg));

  @JsonKey(ignore: true)
  @override
  _$$ShowInfoSnackBarCopyWith<_$ShowInfoSnackBar> get copyWith =>
      __$$ShowInfoSnackBarCopyWithImpl<_$ShowInfoSnackBar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return showInfoSnackBar(msg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return showInfoSnackBar?.call(msg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showInfoSnackBar != null) {
      return showInfoSnackBar(msg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return showInfoSnackBar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return showInfoSnackBar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showInfoSnackBar != null) {
      return showInfoSnackBar(this);
    }
    return orElse();
  }
}

abstract class ShowInfoSnackBar implements AcceptInviteEffect {
  factory ShowInfoSnackBar(final String msg) = _$ShowInfoSnackBar;

  String get msg;
  @JsonKey(ignore: true)
  _$$ShowInfoSnackBarCopyWith<_$ShowInfoSnackBar> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ShowErrorSnackBarCopyWith<$Res> {
  factory _$$ShowErrorSnackBarCopyWith(
          _$ShowErrorSnackBar value, $Res Function(_$ShowErrorSnackBar) then) =
      __$$ShowErrorSnackBarCopyWithImpl<$Res>;
  $Res call({String errorMsg});
}

/// @nodoc
class __$$ShowErrorSnackBarCopyWithImpl<$Res>
    extends _$AcceptInviteEffectCopyWithImpl<$Res>
    implements _$$ShowErrorSnackBarCopyWith<$Res> {
  __$$ShowErrorSnackBarCopyWithImpl(
      _$ShowErrorSnackBar _value, $Res Function(_$ShowErrorSnackBar) _then)
      : super(_value, (v) => _then(v as _$ShowErrorSnackBar));

  @override
  _$ShowErrorSnackBar get _value => super._value as _$ShowErrorSnackBar;

  @override
  $Res call({
    Object? errorMsg = freezed,
  }) {
    return _then(_$ShowErrorSnackBar(
      errorMsg: errorMsg == freezed
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ShowErrorSnackBar implements ShowErrorSnackBar {
  _$ShowErrorSnackBar({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'AcceptInviteEffect.showErrorSnackBar(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShowErrorSnackBar &&
            const DeepCollectionEquality().equals(other.errorMsg, errorMsg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(errorMsg));

  @JsonKey(ignore: true)
  @override
  _$$ShowErrorSnackBarCopyWith<_$ShowErrorSnackBar> get copyWith =>
      __$$ShowErrorSnackBarCopyWithImpl<_$ShowErrorSnackBar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToHome,
    required TResult Function(String msg) showInfoSnackBar,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return showErrorSnackBar(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return showErrorSnackBar?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToHome,
    TResult Function(String msg)? showInfoSnackBar,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showErrorSnackBar != null) {
      return showErrorSnackBar(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToHome value) goToHome,
    required TResult Function(ShowInfoSnackBar value) showInfoSnackBar,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return showErrorSnackBar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return showErrorSnackBar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToHome value)? goToHome,
    TResult Function(ShowInfoSnackBar value)? showInfoSnackBar,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showErrorSnackBar != null) {
      return showErrorSnackBar(this);
    }
    return orElse();
  }
}

abstract class ShowErrorSnackBar implements AcceptInviteEffect {
  factory ShowErrorSnackBar({required final String errorMsg}) =
      _$ShowErrorSnackBar;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$ShowErrorSnackBarCopyWith<_$ShowErrorSnackBar> get copyWith =>
      throw _privateConstructorUsedError;
}
