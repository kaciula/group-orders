import 'package:freezed_annotation/freezed_annotation.dart';

part 'accept_invite_event.freezed.dart';

@freezed
class AcceptInviteEvent with _$AcceptInviteEvent {
  factory AcceptInviteEvent.screenStarted() = ScreenStarted;

  factory AcceptInviteEvent.refreshRequested() = RefreshRequested;

  factory AcceptInviteEvent.invitationAccepted() = InvitationAccepted;
}
