// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'accept_invite_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AcceptInviteEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() invitationAccepted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? invitationAccepted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? invitationAccepted,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(InvitationAccepted value) invitationAccepted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(InvitationAccepted value)? invitationAccepted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AcceptInviteEventCopyWith<$Res> {
  factory $AcceptInviteEventCopyWith(
          AcceptInviteEvent value, $Res Function(AcceptInviteEvent) then) =
      _$AcceptInviteEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AcceptInviteEventCopyWithImpl<$Res>
    implements $AcceptInviteEventCopyWith<$Res> {
  _$AcceptInviteEventCopyWithImpl(this._value, this._then);

  final AcceptInviteEvent _value;
  // ignore: unused_field
  final $Res Function(AcceptInviteEvent) _then;
}

/// @nodoc
abstract class _$$ScreenStartedCopyWith<$Res> {
  factory _$$ScreenStartedCopyWith(
          _$ScreenStarted value, $Res Function(_$ScreenStarted) then) =
      __$$ScreenStartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ScreenStartedCopyWithImpl<$Res>
    extends _$AcceptInviteEventCopyWithImpl<$Res>
    implements _$$ScreenStartedCopyWith<$Res> {
  __$$ScreenStartedCopyWithImpl(
      _$ScreenStarted _value, $Res Function(_$ScreenStarted) _then)
      : super(_value, (v) => _then(v as _$ScreenStarted));

  @override
  _$ScreenStarted get _value => super._value as _$ScreenStarted;
}

/// @nodoc

class _$ScreenStarted implements ScreenStarted {
  _$ScreenStarted();

  @override
  String toString() {
    return 'AcceptInviteEvent.screenStarted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ScreenStarted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() invitationAccepted,
  }) {
    return screenStarted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? invitationAccepted,
  }) {
    return screenStarted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? invitationAccepted,
    required TResult orElse(),
  }) {
    if (screenStarted != null) {
      return screenStarted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(InvitationAccepted value) invitationAccepted,
  }) {
    return screenStarted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(InvitationAccepted value)? invitationAccepted,
  }) {
    return screenStarted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    required TResult orElse(),
  }) {
    if (screenStarted != null) {
      return screenStarted(this);
    }
    return orElse();
  }
}

abstract class ScreenStarted implements AcceptInviteEvent {
  factory ScreenStarted() = _$ScreenStarted;
}

/// @nodoc
abstract class _$$RefreshRequestedCopyWith<$Res> {
  factory _$$RefreshRequestedCopyWith(
          _$RefreshRequested value, $Res Function(_$RefreshRequested) then) =
      __$$RefreshRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RefreshRequestedCopyWithImpl<$Res>
    extends _$AcceptInviteEventCopyWithImpl<$Res>
    implements _$$RefreshRequestedCopyWith<$Res> {
  __$$RefreshRequestedCopyWithImpl(
      _$RefreshRequested _value, $Res Function(_$RefreshRequested) _then)
      : super(_value, (v) => _then(v as _$RefreshRequested));

  @override
  _$RefreshRequested get _value => super._value as _$RefreshRequested;
}

/// @nodoc

class _$RefreshRequested implements RefreshRequested {
  _$RefreshRequested();

  @override
  String toString() {
    return 'AcceptInviteEvent.refreshRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RefreshRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() invitationAccepted,
  }) {
    return refreshRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? invitationAccepted,
  }) {
    return refreshRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? invitationAccepted,
    required TResult orElse(),
  }) {
    if (refreshRequested != null) {
      return refreshRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(InvitationAccepted value) invitationAccepted,
  }) {
    return refreshRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(InvitationAccepted value)? invitationAccepted,
  }) {
    return refreshRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    required TResult orElse(),
  }) {
    if (refreshRequested != null) {
      return refreshRequested(this);
    }
    return orElse();
  }
}

abstract class RefreshRequested implements AcceptInviteEvent {
  factory RefreshRequested() = _$RefreshRequested;
}

/// @nodoc
abstract class _$$InvitationAcceptedCopyWith<$Res> {
  factory _$$InvitationAcceptedCopyWith(_$InvitationAccepted value,
          $Res Function(_$InvitationAccepted) then) =
      __$$InvitationAcceptedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvitationAcceptedCopyWithImpl<$Res>
    extends _$AcceptInviteEventCopyWithImpl<$Res>
    implements _$$InvitationAcceptedCopyWith<$Res> {
  __$$InvitationAcceptedCopyWithImpl(
      _$InvitationAccepted _value, $Res Function(_$InvitationAccepted) _then)
      : super(_value, (v) => _then(v as _$InvitationAccepted));

  @override
  _$InvitationAccepted get _value => super._value as _$InvitationAccepted;
}

/// @nodoc

class _$InvitationAccepted implements InvitationAccepted {
  _$InvitationAccepted();

  @override
  String toString() {
    return 'AcceptInviteEvent.invitationAccepted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InvitationAccepted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() screenStarted,
    required TResult Function() refreshRequested,
    required TResult Function() invitationAccepted,
  }) {
    return invitationAccepted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? invitationAccepted,
  }) {
    return invitationAccepted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? screenStarted,
    TResult Function()? refreshRequested,
    TResult Function()? invitationAccepted,
    required TResult orElse(),
  }) {
    if (invitationAccepted != null) {
      return invitationAccepted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScreenStarted value) screenStarted,
    required TResult Function(RefreshRequested value) refreshRequested,
    required TResult Function(InvitationAccepted value) invitationAccepted,
  }) {
    return invitationAccepted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(InvitationAccepted value)? invitationAccepted,
  }) {
    return invitationAccepted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScreenStarted value)? screenStarted,
    TResult Function(RefreshRequested value)? refreshRequested,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    required TResult orElse(),
  }) {
    if (invitationAccepted != null) {
      return invitationAccepted(this);
    }
    return orElse();
  }
}

abstract class InvitationAccepted implements AcceptInviteEvent {
  factory InvitationAccepted() = _$InvitationAccepted;
}
