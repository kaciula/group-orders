import 'package:freezed_annotation/freezed_annotation.dart';

part 'accept_invite_effect.freezed.dart';

@freezed
class AcceptInviteEffect with _$AcceptInviteEffect {
  factory AcceptInviteEffect.goToHome() = GoToHome;

  factory AcceptInviteEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory AcceptInviteEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
