import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_effect.dart';

part 'sign_up_state.freezed.dart';

@freezed
class SignUpState with _$SignUpState {
  factory SignUpState({
    required bool inProgressAction,
    SignUpEffect? effect,
  }) = _SignUpState;

  factory SignUpState.initial() {
    return SignUpState(inProgressAction: false);
  }

  static bool buildWhen(SignUpState previous, SignUpState current) {
    return previous.inProgressAction != current.inProgressAction;
  }

  static bool listenWhen(SignUpState previous, SignUpState current) {
    return current.effect != null;
  }
}
