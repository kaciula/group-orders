import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_up_event.freezed.dart';

@freezed
class SignUpEvent with _$SignUpEvent {
  factory SignUpEvent.signUpRequested({
    required String displayName,
    required String email,
    required String password,
    required String confirmPassword,
  }) = SignUpRequested;

  factory SignUpEvent.signInRequested() = SignInRequested;
}
