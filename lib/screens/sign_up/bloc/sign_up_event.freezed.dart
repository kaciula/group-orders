// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sign_up_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SignUpEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String displayName, String email, String password,
            String confirmPassword)
        signUpRequested,
    required TResult Function() signInRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String displayName, String email, String password,
            String confirmPassword)?
        signUpRequested,
    TResult Function()? signInRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String displayName, String email, String password,
            String confirmPassword)?
        signUpRequested,
    TResult Function()? signInRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpRequested value) signUpRequested,
    required TResult Function(SignInRequested value) signInRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignUpRequested value)? signUpRequested,
    TResult Function(SignInRequested value)? signInRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpRequested value)? signUpRequested,
    TResult Function(SignInRequested value)? signInRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpEventCopyWith<$Res> {
  factory $SignUpEventCopyWith(
          SignUpEvent value, $Res Function(SignUpEvent) then) =
      _$SignUpEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignUpEventCopyWithImpl<$Res> implements $SignUpEventCopyWith<$Res> {
  _$SignUpEventCopyWithImpl(this._value, this._then);

  final SignUpEvent _value;
  // ignore: unused_field
  final $Res Function(SignUpEvent) _then;
}

/// @nodoc
abstract class _$$SignUpRequestedCopyWith<$Res> {
  factory _$$SignUpRequestedCopyWith(
          _$SignUpRequested value, $Res Function(_$SignUpRequested) then) =
      __$$SignUpRequestedCopyWithImpl<$Res>;
  $Res call(
      {String displayName,
      String email,
      String password,
      String confirmPassword});
}

/// @nodoc
class __$$SignUpRequestedCopyWithImpl<$Res>
    extends _$SignUpEventCopyWithImpl<$Res>
    implements _$$SignUpRequestedCopyWith<$Res> {
  __$$SignUpRequestedCopyWithImpl(
      _$SignUpRequested _value, $Res Function(_$SignUpRequested) _then)
      : super(_value, (v) => _then(v as _$SignUpRequested));

  @override
  _$SignUpRequested get _value => super._value as _$SignUpRequested;

  @override
  $Res call({
    Object? displayName = freezed,
    Object? email = freezed,
    Object? password = freezed,
    Object? confirmPassword = freezed,
  }) {
    return _then(_$SignUpRequested(
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      confirmPassword: confirmPassword == freezed
          ? _value.confirmPassword
          : confirmPassword // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SignUpRequested implements SignUpRequested {
  _$SignUpRequested(
      {required this.displayName,
      required this.email,
      required this.password,
      required this.confirmPassword});

  @override
  final String displayName;
  @override
  final String email;
  @override
  final String password;
  @override
  final String confirmPassword;

  @override
  String toString() {
    return 'SignUpEvent.signUpRequested(displayName: $displayName, email: $email, password: $password, confirmPassword: $confirmPassword)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SignUpRequested &&
            const DeepCollectionEquality()
                .equals(other.displayName, displayName) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality().equals(other.password, password) &&
            const DeepCollectionEquality()
                .equals(other.confirmPassword, confirmPassword));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(displayName),
      const DeepCollectionEquality().hash(email),
      const DeepCollectionEquality().hash(password),
      const DeepCollectionEquality().hash(confirmPassword));

  @JsonKey(ignore: true)
  @override
  _$$SignUpRequestedCopyWith<_$SignUpRequested> get copyWith =>
      __$$SignUpRequestedCopyWithImpl<_$SignUpRequested>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String displayName, String email, String password,
            String confirmPassword)
        signUpRequested,
    required TResult Function() signInRequested,
  }) {
    return signUpRequested(displayName, email, password, confirmPassword);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String displayName, String email, String password,
            String confirmPassword)?
        signUpRequested,
    TResult Function()? signInRequested,
  }) {
    return signUpRequested?.call(displayName, email, password, confirmPassword);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String displayName, String email, String password,
            String confirmPassword)?
        signUpRequested,
    TResult Function()? signInRequested,
    required TResult orElse(),
  }) {
    if (signUpRequested != null) {
      return signUpRequested(displayName, email, password, confirmPassword);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpRequested value) signUpRequested,
    required TResult Function(SignInRequested value) signInRequested,
  }) {
    return signUpRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignUpRequested value)? signUpRequested,
    TResult Function(SignInRequested value)? signInRequested,
  }) {
    return signUpRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpRequested value)? signUpRequested,
    TResult Function(SignInRequested value)? signInRequested,
    required TResult orElse(),
  }) {
    if (signUpRequested != null) {
      return signUpRequested(this);
    }
    return orElse();
  }
}

abstract class SignUpRequested implements SignUpEvent {
  factory SignUpRequested(
      {required final String displayName,
      required final String email,
      required final String password,
      required final String confirmPassword}) = _$SignUpRequested;

  String get displayName;
  String get email;
  String get password;
  String get confirmPassword;
  @JsonKey(ignore: true)
  _$$SignUpRequestedCopyWith<_$SignUpRequested> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SignInRequestedCopyWith<$Res> {
  factory _$$SignInRequestedCopyWith(
          _$SignInRequested value, $Res Function(_$SignInRequested) then) =
      __$$SignInRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SignInRequestedCopyWithImpl<$Res>
    extends _$SignUpEventCopyWithImpl<$Res>
    implements _$$SignInRequestedCopyWith<$Res> {
  __$$SignInRequestedCopyWithImpl(
      _$SignInRequested _value, $Res Function(_$SignInRequested) _then)
      : super(_value, (v) => _then(v as _$SignInRequested));

  @override
  _$SignInRequested get _value => super._value as _$SignInRequested;
}

/// @nodoc

class _$SignInRequested implements SignInRequested {
  _$SignInRequested();

  @override
  String toString() {
    return 'SignUpEvent.signInRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SignInRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String displayName, String email, String password,
            String confirmPassword)
        signUpRequested,
    required TResult Function() signInRequested,
  }) {
    return signInRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String displayName, String email, String password,
            String confirmPassword)?
        signUpRequested,
    TResult Function()? signInRequested,
  }) {
    return signInRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String displayName, String email, String password,
            String confirmPassword)?
        signUpRequested,
    TResult Function()? signInRequested,
    required TResult orElse(),
  }) {
    if (signInRequested != null) {
      return signInRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpRequested value) signUpRequested,
    required TResult Function(SignInRequested value) signInRequested,
  }) {
    return signInRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignUpRequested value)? signUpRequested,
    TResult Function(SignInRequested value)? signInRequested,
  }) {
    return signInRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpRequested value)? signUpRequested,
    TResult Function(SignInRequested value)? signInRequested,
    required TResult orElse(),
  }) {
    if (signInRequested != null) {
      return signInRequested(this);
    }
    return orElse();
  }
}

abstract class SignInRequested implements SignUpEvent {
  factory SignInRequested() = _$SignInRequested;
}
