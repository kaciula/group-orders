import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/data/remote/connectors/connector_results.dart';
import 'package:orders/data/remote/connectors/email_connector.dart';
import 'package:orders/model/validators.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_effect.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_event.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_state.dart';
import 'package:orders/service_locator.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  SignUpBloc() : super(SignUpState.initial());

  final EmailConnector _emailConnector = getIt<EmailConnector>();

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is SignUpRequested) {
      if (event.password == event.confirmPassword) {
        yield state.copyWith(inProgressAction: true);
        final SignInResult result = await _emailConnector.signIn(
          event.email,
          event.password,
          isSignUp: true,
          displayName: event.displayName,
        );
        if (result is SignInSuccess) {
          yield* _withEffect(state, GoToVerifyPhone(result.user));
        } else {
          final SignInFailure failure = result as SignInFailure;
          yield* _withEffect(
              state.copyWith(inProgressAction: false),
              ShowErrorSnackBar(
                errorMsg:
                    '${Strings.signUpFailure}. ${failureMsg(failure.type)}',
              ));
        }
      } else {
        yield* _withEffect(
            state,
            ShowErrorSnackBar(
              errorMsg: Strings.signUpPasswordsMismatch,
            ));
      }
    }
    if (event is SignInRequested) {
      yield* _withEffect(state, GoToSignInEmail());
    }
  }

  String? displayNameValidator(String? value) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    return null;
  }

  String? emailValidator(String? value) {
    if (!Validators.isEmailValid(value?.trim())) {
      return Strings.genericInvalidEmail;
    }
    return null;
  }

  String? passwordValidator(String? value) {
    if (!Validators.isPasswordValid(value?.trim())) {
      return Strings.signUpInvalidPassword;
    }
    return null;
  }

  Stream<SignUpState> _withEffect(
      SignUpState state, SignUpEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }
}

// ignore: unused_element
final Logger _logger = Logger('SignUpBloc');
