import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/user.dart';

part 'sign_up_effect.freezed.dart';

@freezed
class SignUpEffect with _$SignUpEffect {
  factory SignUpEffect.goToSignInEmail() = GoToSignInEmail;

  factory SignUpEffect.goToHome() = GoToHome;

  factory SignUpEffect.goToVerifyPhone(User user) = GoToVerifyPhone;

  factory SignUpEffect.showInfoSnackBar(String msg) = ShowInfoSnackBar;

  factory SignUpEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
