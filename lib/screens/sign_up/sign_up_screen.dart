import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/home/home_screen.dart';
import 'package:orders/screens/sign_in_email/sign_in_email_screen.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_bloc.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_effect.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_event.dart';
import 'package:orders/screens/sign_up/bloc/sign_up_state.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/verify_phone/verify_phone_screen.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/modal_progress_hud.dart';

class SignUpScreen extends StatefulWidget {
  static const String routeName = 'sign_up';

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _displayNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();
  late FocusNode _emailFocusNode;
  late FocusNode _passwordFocusNode;
  late FocusNode _confirmPasswordFocusNode;

  @override
  void initState() {
    super.initState();
    _emailFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
    _confirmPasswordFocusNode = FocusNode();
  }

  @override
  void dispose() {
    _displayNameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    _confirmPasswordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpBloc, SignUpState>(
      bloc: BlocProvider.of<SignUpBloc>(context),
      buildWhen: SignUpState.buildWhen,
      builder: _builder,
    );
  }

  Future<void> _effectListener(BuildContext context, SignUpState state) async {
    final SignUpEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToSignInEmail) {
      Navigator.pushNamed(context, SignInEmailScreen.routeName);
    }
    if (effect is GoToHome) {
      Navigator.pushNamedAndRemoveUntil(
          context, HomeScreen.routeName, (Route<dynamic> route) => false);
    }
    if (effect is GoToVerifyPhone) {
      Navigator.pushNamedAndRemoveUntil(
          context, VerifyPhoneScreen.routeName, (Route<dynamic> route) => false,
          arguments: effect.user);
    }
    if (effect is ShowInfoSnackBar) {
      ScreenUtils.showSnackBar(context, text: effect.msg);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, SignUpState state) {
    final SignUpBloc bloc = BlocProvider.of<SignUpBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.signUpTitle),
        actions: <Widget>[
          FlatBtn(
            label: Strings.signUpSignIn,
            textColor: AppColors.textWhite,
            onPressed: () => bloc.add(SignInRequested()),
          )
        ],
      ),
      body: BlocListener<SignUpBloc, SignUpState>(
        bloc: bloc,
        listenWhen: SignUpState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, SignUpState state) {
    final SignUpBloc bloc = _bloc(context);
    return ModalProgressHUD(
      inAsyncCall: state.inProgressAction,
      child: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            TextFormField(
              controller: _displayNameController,
              autofocus: true,
              decoration: InputDecoration(hintText: Strings.signUpDisplayName),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              validator: bloc.displayNameValidator,
              onFieldSubmitted: (String value) =>
                  FocusScope.of(context).requestFocus(_emailFocusNode),
            ),
            const SizedBox(height: 16),
            TextFormField(
              controller: _emailController,
              decoration: InputDecoration(hintText: Strings.signUpInput),
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              validator: bloc.emailValidator,
              onFieldSubmitted: (String value) =>
                  FocusScope.of(context).requestFocus(_passwordFocusNode),
            ),
            const SizedBox(height: 16),
            TextFormField(
              controller: _passwordController,
              decoration: InputDecoration(hintText: Strings.signUpPassword),
              keyboardType: TextInputType.visiblePassword,
              textInputAction: TextInputAction.next,
              validator: bloc.passwordValidator,
              onFieldSubmitted: (String value) =>
                  FocusScope.of(context).requestFocus(_passwordFocusNode),
            ),
            const SizedBox(height: 16),
            TextFormField(
              controller: _confirmPasswordController,
              decoration:
                  InputDecoration(hintText: Strings.signUpConfirmPassword),
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.visiblePassword,
              validator: bloc.passwordValidator,
            ),
            const SizedBox(height: 16),
            RaisedBtn(
              label: Strings.signUpAction,
              onPressed: () {
                final FormState form = _formKey.currentState!;
                if (form.validate()) {
                  final String displayName = _displayNameController.text.trim();
                  final String email = _emailController.text.trim();
                  final String password = _passwordController.text.trim();
                  final String confirmPassword =
                      _confirmPasswordController.text.trim();
                  _bloc(context).add(SignUpRequested(
                    displayName: displayName,
                    email: email,
                    password: password,
                    confirmPassword: confirmPassword,
                  ));
                }
              },
            ),
            const SizedBox(height: 16),
            FlatBtn(
              label: Strings.signUpExisting,
              style: const TextStyle(fontSize: 16),
              onPressed: () => _bloc(context).add(SignInRequested()),
            ),
          ],
        ),
      ),
    );
  }

  SignUpBloc _bloc(BuildContext context) {
    return BlocProvider.of<SignUpBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('SignUpScreen');
