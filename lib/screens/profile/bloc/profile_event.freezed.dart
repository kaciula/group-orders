// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'profile_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) dataChanged,
    required TResult Function() signOutRequested,
    required TResult Function() contactRequested,
    required TResult Function() deleteAccountRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataChanged value) dataChanged,
    required TResult Function(SignOutRequested value) signOutRequested,
    required TResult Function(ContactRequested value) contactRequested,
    required TResult Function(DeleteAccountRequested value)
        deleteAccountRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileEventCopyWith<$Res> {
  factory $ProfileEventCopyWith(
          ProfileEvent value, $Res Function(ProfileEvent) then) =
      _$ProfileEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProfileEventCopyWithImpl<$Res> implements $ProfileEventCopyWith<$Res> {
  _$ProfileEventCopyWithImpl(this._value, this._then);

  final ProfileEvent _value;
  // ignore: unused_field
  final $Res Function(ProfileEvent) _then;
}

/// @nodoc
abstract class _$$DataChangedCopyWith<$Res> {
  factory _$$DataChangedCopyWith(
          _$DataChanged value, $Res Function(_$DataChanged) then) =
      __$$DataChangedCopyWithImpl<$Res>;
  $Res call({User user});

  $UserCopyWith<$Res> get user;
}

/// @nodoc
class __$$DataChangedCopyWithImpl<$Res> extends _$ProfileEventCopyWithImpl<$Res>
    implements _$$DataChangedCopyWith<$Res> {
  __$$DataChangedCopyWithImpl(
      _$DataChanged _value, $Res Function(_$DataChanged) _then)
      : super(_value, (v) => _then(v as _$DataChanged));

  @override
  _$DataChanged get _value => super._value as _$DataChanged;

  @override
  $Res call({
    Object? user = freezed,
  }) {
    return _then(_$DataChanged(
      user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc

class _$DataChanged implements DataChanged {
  _$DataChanged(this.user);

  @override
  final User user;

  @override
  String toString() {
    return 'ProfileEvent.dataChanged(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataChanged &&
            const DeepCollectionEquality().equals(other.user, user));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(user));

  @JsonKey(ignore: true)
  @override
  _$$DataChangedCopyWith<_$DataChanged> get copyWith =>
      __$$DataChangedCopyWithImpl<_$DataChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) dataChanged,
    required TResult Function() signOutRequested,
    required TResult Function() contactRequested,
    required TResult Function() deleteAccountRequested,
  }) {
    return dataChanged(user);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
  }) {
    return dataChanged?.call(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
    required TResult orElse(),
  }) {
    if (dataChanged != null) {
      return dataChanged(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataChanged value) dataChanged,
    required TResult Function(SignOutRequested value) signOutRequested,
    required TResult Function(ContactRequested value) contactRequested,
    required TResult Function(DeleteAccountRequested value)
        deleteAccountRequested,
  }) {
    return dataChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
  }) {
    return dataChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
    required TResult orElse(),
  }) {
    if (dataChanged != null) {
      return dataChanged(this);
    }
    return orElse();
  }
}

abstract class DataChanged implements ProfileEvent {
  factory DataChanged(final User user) = _$DataChanged;

  User get user;
  @JsonKey(ignore: true)
  _$$DataChangedCopyWith<_$DataChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SignOutRequestedCopyWith<$Res> {
  factory _$$SignOutRequestedCopyWith(
          _$SignOutRequested value, $Res Function(_$SignOutRequested) then) =
      __$$SignOutRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SignOutRequestedCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res>
    implements _$$SignOutRequestedCopyWith<$Res> {
  __$$SignOutRequestedCopyWithImpl(
      _$SignOutRequested _value, $Res Function(_$SignOutRequested) _then)
      : super(_value, (v) => _then(v as _$SignOutRequested));

  @override
  _$SignOutRequested get _value => super._value as _$SignOutRequested;
}

/// @nodoc

class _$SignOutRequested implements SignOutRequested {
  _$SignOutRequested();

  @override
  String toString() {
    return 'ProfileEvent.signOutRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SignOutRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) dataChanged,
    required TResult Function() signOutRequested,
    required TResult Function() contactRequested,
    required TResult Function() deleteAccountRequested,
  }) {
    return signOutRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
  }) {
    return signOutRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
    required TResult orElse(),
  }) {
    if (signOutRequested != null) {
      return signOutRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataChanged value) dataChanged,
    required TResult Function(SignOutRequested value) signOutRequested,
    required TResult Function(ContactRequested value) contactRequested,
    required TResult Function(DeleteAccountRequested value)
        deleteAccountRequested,
  }) {
    return signOutRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
  }) {
    return signOutRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
    required TResult orElse(),
  }) {
    if (signOutRequested != null) {
      return signOutRequested(this);
    }
    return orElse();
  }
}

abstract class SignOutRequested implements ProfileEvent {
  factory SignOutRequested() = _$SignOutRequested;
}

/// @nodoc
abstract class _$$ContactRequestedCopyWith<$Res> {
  factory _$$ContactRequestedCopyWith(
          _$ContactRequested value, $Res Function(_$ContactRequested) then) =
      __$$ContactRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ContactRequestedCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res>
    implements _$$ContactRequestedCopyWith<$Res> {
  __$$ContactRequestedCopyWithImpl(
      _$ContactRequested _value, $Res Function(_$ContactRequested) _then)
      : super(_value, (v) => _then(v as _$ContactRequested));

  @override
  _$ContactRequested get _value => super._value as _$ContactRequested;
}

/// @nodoc

class _$ContactRequested implements ContactRequested {
  _$ContactRequested();

  @override
  String toString() {
    return 'ProfileEvent.contactRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ContactRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) dataChanged,
    required TResult Function() signOutRequested,
    required TResult Function() contactRequested,
    required TResult Function() deleteAccountRequested,
  }) {
    return contactRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
  }) {
    return contactRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
    required TResult orElse(),
  }) {
    if (contactRequested != null) {
      return contactRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataChanged value) dataChanged,
    required TResult Function(SignOutRequested value) signOutRequested,
    required TResult Function(ContactRequested value) contactRequested,
    required TResult Function(DeleteAccountRequested value)
        deleteAccountRequested,
  }) {
    return contactRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
  }) {
    return contactRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
    required TResult orElse(),
  }) {
    if (contactRequested != null) {
      return contactRequested(this);
    }
    return orElse();
  }
}

abstract class ContactRequested implements ProfileEvent {
  factory ContactRequested() = _$ContactRequested;
}

/// @nodoc
abstract class _$$DeleteAccountRequestedCopyWith<$Res> {
  factory _$$DeleteAccountRequestedCopyWith(_$DeleteAccountRequested value,
          $Res Function(_$DeleteAccountRequested) then) =
      __$$DeleteAccountRequestedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DeleteAccountRequestedCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res>
    implements _$$DeleteAccountRequestedCopyWith<$Res> {
  __$$DeleteAccountRequestedCopyWithImpl(_$DeleteAccountRequested _value,
      $Res Function(_$DeleteAccountRequested) _then)
      : super(_value, (v) => _then(v as _$DeleteAccountRequested));

  @override
  _$DeleteAccountRequested get _value =>
      super._value as _$DeleteAccountRequested;
}

/// @nodoc

class _$DeleteAccountRequested implements DeleteAccountRequested {
  _$DeleteAccountRequested();

  @override
  String toString() {
    return 'ProfileEvent.deleteAccountRequested()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DeleteAccountRequested);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) dataChanged,
    required TResult Function() signOutRequested,
    required TResult Function() contactRequested,
    required TResult Function() deleteAccountRequested,
  }) {
    return deleteAccountRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
  }) {
    return deleteAccountRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? dataChanged,
    TResult Function()? signOutRequested,
    TResult Function()? contactRequested,
    TResult Function()? deleteAccountRequested,
    required TResult orElse(),
  }) {
    if (deleteAccountRequested != null) {
      return deleteAccountRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DataChanged value) dataChanged,
    required TResult Function(SignOutRequested value) signOutRequested,
    required TResult Function(ContactRequested value) contactRequested,
    required TResult Function(DeleteAccountRequested value)
        deleteAccountRequested,
  }) {
    return deleteAccountRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
  }) {
    return deleteAccountRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DataChanged value)? dataChanged,
    TResult Function(SignOutRequested value)? signOutRequested,
    TResult Function(ContactRequested value)? contactRequested,
    TResult Function(DeleteAccountRequested value)? deleteAccountRequested,
    required TResult orElse(),
  }) {
    if (deleteAccountRequested != null) {
      return deleteAccountRequested(this);
    }
    return orElse();
  }
}

abstract class DeleteAccountRequested implements ProfileEvent {
  factory DeleteAccountRequested() = _$DeleteAccountRequested;
}
