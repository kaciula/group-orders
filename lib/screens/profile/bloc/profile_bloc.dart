import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/blocs/data_bloc/data_state.dart';
import 'package:orders/data/local/misc/launcher.dart';
import 'package:orders/data/local/store/user_provider.dart';
import 'package:orders/data/remote/connectors/apple_connector.dart';
import 'package:orders/data/remote/connectors/email_connector.dart';
import 'package:orders/data/remote/connectors/facebook_connector.dart';
import 'package:orders/data/remote/connectors/google_connector.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/profile/bloc/profile_effect.dart';
import 'package:orders/screens/profile/bloc/profile_event.dart';
import 'package:orders/screens/profile/bloc/profile_state.dart';
import 'package:orders/service_locator.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc(this.dataBloc)
      : super(ProfileState.initial(
          dataBloc.user,
          versionName: dataBloc.versionName,
        )) {
    dataSub = dataBloc.stream
        .distinct(
            (DataState previous, DataState next) => previous.user == next.user)
        .listen((DataState state) {
      _logger.fine('Received new state from data bloc $state');
      add(DataChanged(state.user));
    });
  }

  final DataBloc dataBloc;
  late final StreamSubscription<DataState> dataSub;

  final UserProvider _userProvider = getIt<UserProvider>();
  final GoogleConnector _googleConnector = getIt<GoogleConnector>();
  final AppleConnector _appleConnector = getIt<AppleConnector>();
  final FacebookConnector _facebookConnector = getIt<FacebookConnector>();
  final EmailConnector _emailConnector = getIt<EmailConnector>();
  final Launcher _launcher = getIt<Launcher>();
  final DataRemoteStore _dataRemoteStore = getIt<DataRemoteStore>();

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is DataChanged) {
      yield state.copyWith(user: event.user);
    }
    if (event is DeleteAccountRequested) {
      yield state.copyWith(inProgressAction: true);
      final StoreResult result =
          await _dataRemoteStore.deleteAccount(userId: state.user.id);
      yield* result.when(
        success: () async* {
          yield* _signOut();
        },
        failure: () async* {
          yield* _withEffect(
            state.copyWith(inProgressAction: false),
            ShowErrorSnackBar(errorMsg: Strings.genericErrorMsg),
          );
        },
      );
    }
    if (event is SignOutRequested) {
      yield* _signOut();
    }
    if (event is ContactRequested) {
      _launcher.sendEmailTo(
        emailAddress: 'forgetfulsoulapps@gmail.com',
        subject: Strings.appName,
      );
    }
  }

  Stream<ProfileState> _signOut() async* {
    yield state.copyWith(inProgressAction: true);
    if (dataBloc.user.signInProvider == SignInProvider.google) {
      await _googleConnector.signOut();
    } else if (dataBloc.user.signInProvider == SignInProvider.facebook) {
      await _facebookConnector.signOut();
    } else if (dataBloc.user.signInProvider == SignInProvider.apple) {
      await _appleConnector.signOut();
    } else if (dataBloc.user.signInProvider == SignInProvider.email) {
      await _emailConnector.signOut();
    }
    await _userProvider.signedOut();
    dataBloc.add(UserSignedOut());

    yield* _withEffect(state, GoToConnect());
  }

  Stream<ProfileState> _withEffect(
      ProfileState state, ProfileEffect effect) async* {
    yield state.copyWith(effect: effect);
    yield state.copyWith(effect: null);
  }

  @override
  Future<void> close() async {
    dataSub.cancel();
    super.close();
  }
}

// ignore: unused_element
final Logger _logger = Logger('ProfileBloc');
