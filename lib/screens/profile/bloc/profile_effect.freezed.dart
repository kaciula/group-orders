// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'profile_effect.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileEffect {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToConnect,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToConnect value) goToConnect,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileEffectCopyWith<$Res> {
  factory $ProfileEffectCopyWith(
          ProfileEffect value, $Res Function(ProfileEffect) then) =
      _$ProfileEffectCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProfileEffectCopyWithImpl<$Res>
    implements $ProfileEffectCopyWith<$Res> {
  _$ProfileEffectCopyWithImpl(this._value, this._then);

  final ProfileEffect _value;
  // ignore: unused_field
  final $Res Function(ProfileEffect) _then;
}

/// @nodoc
abstract class _$$GoToConnectCopyWith<$Res> {
  factory _$$GoToConnectCopyWith(
          _$GoToConnect value, $Res Function(_$GoToConnect) then) =
      __$$GoToConnectCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GoToConnectCopyWithImpl<$Res>
    extends _$ProfileEffectCopyWithImpl<$Res>
    implements _$$GoToConnectCopyWith<$Res> {
  __$$GoToConnectCopyWithImpl(
      _$GoToConnect _value, $Res Function(_$GoToConnect) _then)
      : super(_value, (v) => _then(v as _$GoToConnect));

  @override
  _$GoToConnect get _value => super._value as _$GoToConnect;
}

/// @nodoc

class _$GoToConnect implements GoToConnect {
  _$GoToConnect();

  @override
  String toString() {
    return 'ProfileEffect.goToConnect()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GoToConnect);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToConnect,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return goToConnect();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return goToConnect?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToConnect != null) {
      return goToConnect();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToConnect value) goToConnect,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return goToConnect(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return goToConnect?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (goToConnect != null) {
      return goToConnect(this);
    }
    return orElse();
  }
}

abstract class GoToConnect implements ProfileEffect {
  factory GoToConnect() = _$GoToConnect;
}

/// @nodoc
abstract class _$$ShowErrorSnackBarCopyWith<$Res> {
  factory _$$ShowErrorSnackBarCopyWith(
          _$ShowErrorSnackBar value, $Res Function(_$ShowErrorSnackBar) then) =
      __$$ShowErrorSnackBarCopyWithImpl<$Res>;
  $Res call({String errorMsg});
}

/// @nodoc
class __$$ShowErrorSnackBarCopyWithImpl<$Res>
    extends _$ProfileEffectCopyWithImpl<$Res>
    implements _$$ShowErrorSnackBarCopyWith<$Res> {
  __$$ShowErrorSnackBarCopyWithImpl(
      _$ShowErrorSnackBar _value, $Res Function(_$ShowErrorSnackBar) _then)
      : super(_value, (v) => _then(v as _$ShowErrorSnackBar));

  @override
  _$ShowErrorSnackBar get _value => super._value as _$ShowErrorSnackBar;

  @override
  $Res call({
    Object? errorMsg = freezed,
  }) {
    return _then(_$ShowErrorSnackBar(
      errorMsg: errorMsg == freezed
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ShowErrorSnackBar implements ShowErrorSnackBar {
  _$ShowErrorSnackBar({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'ProfileEffect.showErrorSnackBar(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShowErrorSnackBar &&
            const DeepCollectionEquality().equals(other.errorMsg, errorMsg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(errorMsg));

  @JsonKey(ignore: true)
  @override
  _$$ShowErrorSnackBarCopyWith<_$ShowErrorSnackBar> get copyWith =>
      __$$ShowErrorSnackBarCopyWithImpl<_$ShowErrorSnackBar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() goToConnect,
    required TResult Function(String errorMsg) showErrorSnackBar,
  }) {
    return showErrorSnackBar(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function(String errorMsg)? showErrorSnackBar,
  }) {
    return showErrorSnackBar?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? goToConnect,
    TResult Function(String errorMsg)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showErrorSnackBar != null) {
      return showErrorSnackBar(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GoToConnect value) goToConnect,
    required TResult Function(ShowErrorSnackBar value) showErrorSnackBar,
  }) {
    return showErrorSnackBar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
  }) {
    return showErrorSnackBar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GoToConnect value)? goToConnect,
    TResult Function(ShowErrorSnackBar value)? showErrorSnackBar,
    required TResult orElse(),
  }) {
    if (showErrorSnackBar != null) {
      return showErrorSnackBar(this);
    }
    return orElse();
  }
}

abstract class ShowErrorSnackBar implements ProfileEffect {
  factory ShowErrorSnackBar({required final String errorMsg}) =
      _$ShowErrorSnackBar;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$ShowErrorSnackBarCopyWith<_$ShowErrorSnackBar> get copyWith =>
      throw _privateConstructorUsedError;
}
