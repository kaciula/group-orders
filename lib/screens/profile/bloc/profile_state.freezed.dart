// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'profile_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileState {
  bool get inProgressAction => throw _privateConstructorUsedError;
  User get user => throw _privateConstructorUsedError;
  String get versionName => throw _privateConstructorUsedError;
  ProfileEffect? get effect => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProfileStateCopyWith<ProfileState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileStateCopyWith<$Res> {
  factory $ProfileStateCopyWith(
          ProfileState value, $Res Function(ProfileState) then) =
      _$ProfileStateCopyWithImpl<$Res>;
  $Res call(
      {bool inProgressAction,
      User user,
      String versionName,
      ProfileEffect? effect});

  $UserCopyWith<$Res> get user;
  $ProfileEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class _$ProfileStateCopyWithImpl<$Res> implements $ProfileStateCopyWith<$Res> {
  _$ProfileStateCopyWithImpl(this._value, this._then);

  final ProfileState _value;
  // ignore: unused_field
  final $Res Function(ProfileState) _then;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? user = freezed,
    Object? versionName = freezed,
    Object? effect = freezed,
  }) {
    return _then(_value.copyWith(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      versionName: versionName == freezed
          ? _value.versionName
          : versionName // ignore: cast_nullable_to_non_nullable
              as String,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as ProfileEffect?,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }

  @override
  $ProfileEffectCopyWith<$Res>? get effect {
    if (_value.effect == null) {
      return null;
    }

    return $ProfileEffectCopyWith<$Res>(_value.effect!, (value) {
      return _then(_value.copyWith(effect: value));
    });
  }
}

/// @nodoc
abstract class _$$_ProfileStateCopyWith<$Res>
    implements $ProfileStateCopyWith<$Res> {
  factory _$$_ProfileStateCopyWith(
          _$_ProfileState value, $Res Function(_$_ProfileState) then) =
      __$$_ProfileStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool inProgressAction,
      User user,
      String versionName,
      ProfileEffect? effect});

  @override
  $UserCopyWith<$Res> get user;
  @override
  $ProfileEffectCopyWith<$Res>? get effect;
}

/// @nodoc
class __$$_ProfileStateCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$$_ProfileStateCopyWith<$Res> {
  __$$_ProfileStateCopyWithImpl(
      _$_ProfileState _value, $Res Function(_$_ProfileState) _then)
      : super(_value, (v) => _then(v as _$_ProfileState));

  @override
  _$_ProfileState get _value => super._value as _$_ProfileState;

  @override
  $Res call({
    Object? inProgressAction = freezed,
    Object? user = freezed,
    Object? versionName = freezed,
    Object? effect = freezed,
  }) {
    return _then(_$_ProfileState(
      inProgressAction: inProgressAction == freezed
          ? _value.inProgressAction
          : inProgressAction // ignore: cast_nullable_to_non_nullable
              as bool,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      versionName: versionName == freezed
          ? _value.versionName
          : versionName // ignore: cast_nullable_to_non_nullable
              as String,
      effect: effect == freezed
          ? _value.effect
          : effect // ignore: cast_nullable_to_non_nullable
              as ProfileEffect?,
    ));
  }
}

/// @nodoc

class _$_ProfileState implements _ProfileState {
  _$_ProfileState(
      {required this.inProgressAction,
      required this.user,
      required this.versionName,
      this.effect});

  @override
  final bool inProgressAction;
  @override
  final User user;
  @override
  final String versionName;
  @override
  final ProfileEffect? effect;

  @override
  String toString() {
    return 'ProfileState(inProgressAction: $inProgressAction, user: $user, versionName: $versionName, effect: $effect)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProfileState &&
            const DeepCollectionEquality()
                .equals(other.inProgressAction, inProgressAction) &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality()
                .equals(other.versionName, versionName) &&
            const DeepCollectionEquality().equals(other.effect, effect));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(inProgressAction),
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(versionName),
      const DeepCollectionEquality().hash(effect));

  @JsonKey(ignore: true)
  @override
  _$$_ProfileStateCopyWith<_$_ProfileState> get copyWith =>
      __$$_ProfileStateCopyWithImpl<_$_ProfileState>(this, _$identity);
}

abstract class _ProfileState implements ProfileState {
  factory _ProfileState(
      {required final bool inProgressAction,
      required final User user,
      required final String versionName,
      final ProfileEffect? effect}) = _$_ProfileState;

  @override
  bool get inProgressAction;
  @override
  User get user;
  @override
  String get versionName;
  @override
  ProfileEffect? get effect;
  @override
  @JsonKey(ignore: true)
  _$$_ProfileStateCopyWith<_$_ProfileState> get copyWith =>
      throw _privateConstructorUsedError;
}
