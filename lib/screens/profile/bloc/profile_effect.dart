import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_effect.freezed.dart';

@freezed
class ProfileEffect with _$ProfileEffect {
  factory ProfileEffect.goToConnect() = GoToConnect;

  factory ProfileEffect.showErrorSnackBar({required String errorMsg}) =
      ShowErrorSnackBar;
}
