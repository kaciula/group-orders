import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/user.dart';
import 'package:orders/screens/profile/bloc/profile_effect.dart';

part 'profile_state.freezed.dart';

@freezed
class ProfileState with _$ProfileState {
  factory ProfileState({
    required bool inProgressAction,
    required User user,
    required String versionName,
    ProfileEffect? effect,
  }) = _ProfileState;

  factory ProfileState.initial(User user, {required String versionName}) {
    return ProfileState(
      inProgressAction: false,
      user: user,
      versionName: versionName,
    );
  }

  static bool buildWhen(ProfileState previous, ProfileState current) {
    return previous.inProgressAction != current.inProgressAction ||
        previous.user != current.user ||
        previous.versionName != current.versionName;
  }

  static bool listenWhen(ProfileState previous, ProfileState current) {
    return current.effect != null;
  }
}
