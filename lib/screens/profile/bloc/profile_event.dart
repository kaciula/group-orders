import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/model/user.dart';

part 'profile_event.freezed.dart';

@freezed
class ProfileEvent with _$ProfileEvent {
  factory ProfileEvent.dataChanged(User user) = DataChanged;

  factory ProfileEvent.signOutRequested() = SignOutRequested;

  factory ProfileEvent.contactRequested() = ContactRequested;

  factory ProfileEvent.deleteAccountRequested() = DeleteAccountRequested;
}
