import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_colors.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/screens/connect/connect_screen.dart';
import 'package:orders/screens/edit_profile/edit_profile_screen.dart';
import 'package:orders/screens/profile/bloc/profile_bloc.dart';
import 'package:orders/screens/profile/bloc/profile_effect.dart';
import 'package:orders/screens/profile/bloc/profile_event.dart';
import 'package:orders/screens/profile/bloc/profile_state.dart';
import 'package:orders/screens/utils/screen_utils.dart';
import 'package:orders/screens/widgets/buttons.dart';
import 'package:orders/screens/widgets/sure_dialog.dart';

class ProfileScreen extends StatefulWidget {
  static const String routeName = 'profile';

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      bloc: BlocProvider.of<ProfileBloc>(context),
      buildWhen: ProfileState.buildWhen,
      builder: _builder,
    );
  }

  void _effectListener(BuildContext context, ProfileState state) {
    final ProfileEffect? effect = state.effect;
    _logger.info('effect=$effect');

    if (effect is GoToConnect) {
      Navigator.pushNamedAndRemoveUntil(
          context, ConnectScreen.routeName, (Route<dynamic> route) => false);
    }
    if (effect is ShowErrorSnackBar) {
      ScreenUtils.showErrorSnackBar(context, errorMsg: effect.errorMsg);
    }
  }

  Widget _builder(BuildContext context, ProfileState state) {
    final ProfileBloc bloc = BlocProvider.of<ProfileBloc>(context);
    _logger.info('rebuild state=$state');

    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.profileTitle),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.edit),
            onPressed: () => Navigator.pushNamed(
              context,
              EditProfileScreen.routeName,
              arguments: state.user,
            ),
          )
        ],
      ),
      body: BlocListener<ProfileBloc, ProfileState>(
        bloc: bloc,
        listenWhen: ProfileState.listenWhen,
        listener: _effectListener,
        child: _body(context, state),
      ),
    );
  }

  Widget _body(BuildContext context, ProfileState state) {
    return ListView(
      padding: const EdgeInsets.all(16),
      children: <Widget>[
        Text(
          Strings.profileSignedInAs(
              displayName: state.user.displayName,
              emailAddress: state.user.emailAddress),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 16),
        RaisedBtn(
          label: Strings.profileSignOut,
          onPressed: () => _bloc(context).add(SignOutRequested()),
        ),
        const SizedBox(height: 16),
        OutlinedButton.icon(
          icon: const Icon(Icons.email),
          onPressed: () => _bloc(context).add(ContactRequested()),
          label: Text(Strings.profileContact),
        ),
        const SizedBox(height: 16),
        _deleteAccountAction(),
        const SizedBox(height: 16),
        Center(child: Text('v${state.versionName}')),
      ],
    );
  }

  Widget _deleteAccountAction() {
    return UnconstrainedBox(
      child: OutlinedButton(
        style: TextButton.styleFrom(
          foregroundColor: AppColors.red,
          side: BorderSide(color: AppColors.red),
        ),
        onPressed: () => showDialog(
          context: context,
          builder: (BuildContext innerContext) => SureDialog(
            msg: Strings.profileSureDeleteAccount,
            onYesPressed: () => _bloc(context).add(DeleteAccountRequested()),
          ),
        ),
        child: Text(
          Strings.profileDeleteAccount,
          style: TextStyle(fontWeight: FontWeight.normal),
        ),
      ),
    );
  }

  ProfileBloc _bloc(BuildContext context) {
    return BlocProvider.of<ProfileBloc>(context);
  }
}

// ignore: unused_element
final Logger _logger = Logger('ProfileScreen');
