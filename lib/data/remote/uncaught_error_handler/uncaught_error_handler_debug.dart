import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:orders/data/remote/uncaught_error_handler/uncaught_error_handler.dart';

class UncaughtErrorHandlerDebug extends UncaughtErrorHandler {
  @override
  Future<void> handleFlutterError(FlutterErrorDetails details) async {
    _logger.severe('BOOOOM! Flutter error $details');
  }

  @override
  Future<void> handleZonedError(dynamic exception, dynamic stack) async {
    _logger.severe('BOOOOM! Zoned error $exception');
  }

  @override
  Future<void> reportUnexpectedError(
      Exception exception, StackTrace trace, String context,
      {bool? restartApp}) async {
    _logger.warning(
        'BOOOOM! Would have reported unexpected error if it was release mode');
  }
}

// ignore: unused_element
final Logger _logger = Logger('UncaughtErrorHandlerDebug');
