import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:orders/data/local/store/app_info_provider.dart';
import 'package:orders/data/remote/uncaught_error_handler/uncaught_error_handler.dart';
import 'package:orders/main_release.dart';
import 'package:universal_io/io.dart';

class UncaughtErrorHandlerRelease extends UncaughtErrorHandler {
  UncaughtErrorHandlerRelease(this.appInfoProvider);

  final AppInfoProvider appInfoProvider;

  @override
  Future<void> handleFlutterError(FlutterErrorDetails details) async {
    await FirebaseCrashlytics.instance.recordFlutterError(details);
    await _restartOrDie();
  }

  @override
  Future<void> handleZonedError(dynamic exception, dynamic stack) async {
    await FirebaseCrashlytics.instance.recordError(exception, stack);
    await _restartOrDie();
  }

  @override
  Future<void> reportUnexpectedError(
      Exception exception, StackTrace trace, String context,
      {bool? restartApp = false}) async {
    await FirebaseCrashlytics.instance
        .recordError(exception, trace, reason: context);
    if (restartApp ?? false) {
      // await _restartOrDie();
    }
  }

  Future<bool> _shouldRestartApp() async {
    final AppInfo appInfo = appInfoProvider.get();
    final int timestampNow = DateTime.now().millisecondsSinceEpoch;
    return (timestampNow - appInfo.timestampLastCrash) >
        20 * Duration.millisecondsPerSecond;
  }

  Future<void> _restartOrDie() async {
    final bool shouldRestart = await _shouldRestartApp();
    if (shouldRestart) {
      startApp();
      await appInfoProvider
          .saveTimestampLastCrash(DateTime.now().millisecondsSinceEpoch);
    } else {
      exit(10);
    }
  }
}
