import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:orders/model/product.dart';

class ProductDocument {
  ProductDocument(
    this.id,
    this.title,
    this.details,
    this.price,
    this.currency,
    this.numItemsAvailable,
  );

  factory ProductDocument.fromSnapshot(
      DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final Map<String, dynamic> data = snapshot.data()!;
    return ProductDocument(
      data['id'],
      data['title'],
      data['details'],
      data['price'],
      data['currency'] ?? 'RON',
      data['numItemsAvailable'],
    );
  }

  factory ProductDocument.fromProduct(Product product) {
    return ProductDocument(
      product.id,
      product.title,
      product.details,
      product.price,
      product.currency,
      _reverseMap(product.stock),
    );
  }

  final String id;
  final String title;
  final String details;
  final double price;
  final int? numItemsAvailable;

  // this field is kept just for backward compatibility
  final String currency;

  static const String collection = 'products';

  Product map() {
    return Product(
      id: id,
      title: title,
      details: details,
      price: price,
      currency: currency,
      stock: _map(numItemsAvailable),
    );
  }

  Map<String, dynamic> toMap() => <String, dynamic>{
        'id': id,
        'title': title,
        'details': details,
        'price': price,
        'currency': currency,
        'numItemsAvailable': numItemsAvailable,
      };

  Stock _map(int? numItemsAvailable) {
    if (numItemsAvailable == _unlimitedStock || numItemsAvailable == null) {
      return Stock.unlimited();
    } else {
      return Stock.limited(numItemsAvailable: numItemsAvailable);
    }
  }

  static int _reverseMap(Stock stock) {
    if (stock is Limited) {
      return stock.numItemsAvailable;
    } else {
      return _unlimitedStock;
    }
  }
}

const int _unlimitedStock = -10000;
