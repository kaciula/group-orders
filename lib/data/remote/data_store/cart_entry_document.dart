import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:orders/model/cart_entry.dart';

class CartEntryDocument {
  CartEntryDocument(
    this.id,
    this.productId,
    this.participantId,
    this.details,
    this.percent,
    this.cost,
    this.type,
    this.numItems,
    this.modifiedPrice,
  );

  factory CartEntryDocument.fromSnapshot(
      DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final Map<String, dynamic> data = snapshot.data()!;
    return CartEntryDocument(
      data['id'],
      data['productId'],
      data['participantId'],
      data['details'],
      data['percent'],
      data['cost'],
      data['type'] ?? _TYPE_PRODUCT,
      data['numItems'] ?? 1,
      // used for backward compatibility
      data['modifiedPrice'],
    );
  }

  factory CartEntryDocument.fromCartItem(CartEntry entry) {
    return entry.when(
      product: (String id, String productId, String participantId,
              String details, int numItems, double? modifiedPrice) =>
          CartEntryDocument(
        id,
        productId,
        participantId,
        details,
        null,
        null,
        _TYPE_PRODUCT,
        numItems,
        modifiedPrice,
      ),
      discount: (String id, String details, double percent) =>
          CartEntryDocument(
        id,
        null,
        null,
        details,
        percent,
        null,
        _TYPE_DISCOUNT,
        null,
        null,
      ),
      transport: (String id, String details, double cost) => CartEntryDocument(
        id,
        null,
        null,
        details,
        null,
        cost,
        _TYPE_TRANSPORT,
        null,
        null,
      ),
    );
  }

  final String id;
  final String type;
  final String details;
  final String? productId;
  final String? participantId;
  final int? numItems;
  final double? modifiedPrice;
  final double? percent;
  final double? cost;

  static const String collection = 'cart';
  static const String fieldProductId = 'productId';

  CartEntry map() {
    if (type == _TYPE_PRODUCT) {
      return CartProduct(
        id: id,
        productId: productId!,
        participantId: participantId!,
        details: details,
        numItems: numItems!,
        modifiedPrice: modifiedPrice,
      );
    } else if (type == _TYPE_DISCOUNT) {
      return Discount(id: id, details: details, percent: percent!);
    } else if (type == _TYPE_TRANSPORT) {
      return Transport(id: id, details: details, cost: cost!);
    }
    throw Exception('Unknown cart item type $type');
  }

  Map<String, dynamic> toMap() {
    if (type == _TYPE_PRODUCT) {
      return <String, dynamic>{
        'id': id,
        'productId': productId,
        'participantId': participantId,
        'details': details,
        'numItems': numItems,
        'modifiedPrice': modifiedPrice,
        'type': type,
      };
    } else if (type == _TYPE_DISCOUNT) {
      return <String, dynamic>{
        'id': id,
        'details': details,
        'percent': percent,
        'type': type,
      };
    } else if (type == _TYPE_TRANSPORT) {
      return <String, dynamic>{
        'id': id,
        'details': details,
        'cost': cost,
        'type': type,
      };
    }
    throw Exception('Unknown cart type $type');
  }

  static const String _TYPE_PRODUCT = 'Product';
  static const String _TYPE_DISCOUNT = 'Discount';
  static const String _TYPE_TRANSPORT = 'Transport';
}
