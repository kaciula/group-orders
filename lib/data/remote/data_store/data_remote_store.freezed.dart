// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'data_remote_store.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$StoreResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StoreSuccess value) success,
    required TResult Function(StoreFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StoreSuccess value)? success,
    TResult Function(StoreFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StoreSuccess value)? success,
    TResult Function(StoreFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StoreResultCopyWith<$Res> {
  factory $StoreResultCopyWith(
          StoreResult value, $Res Function(StoreResult) then) =
      _$StoreResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$StoreResultCopyWithImpl<$Res> implements $StoreResultCopyWith<$Res> {
  _$StoreResultCopyWithImpl(this._value, this._then);

  final StoreResult _value;
  // ignore: unused_field
  final $Res Function(StoreResult) _then;
}

/// @nodoc
abstract class _$$StoreSuccessCopyWith<$Res> {
  factory _$$StoreSuccessCopyWith(
          _$StoreSuccess value, $Res Function(_$StoreSuccess) then) =
      __$$StoreSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StoreSuccessCopyWithImpl<$Res> extends _$StoreResultCopyWithImpl<$Res>
    implements _$$StoreSuccessCopyWith<$Res> {
  __$$StoreSuccessCopyWithImpl(
      _$StoreSuccess _value, $Res Function(_$StoreSuccess) _then)
      : super(_value, (v) => _then(v as _$StoreSuccess));

  @override
  _$StoreSuccess get _value => super._value as _$StoreSuccess;
}

/// @nodoc

class _$StoreSuccess implements StoreSuccess {
  _$StoreSuccess();

  @override
  String toString() {
    return 'StoreResult.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StoreSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StoreSuccess value) success,
    required TResult Function(StoreFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StoreSuccess value)? success,
    TResult Function(StoreFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StoreSuccess value)? success,
    TResult Function(StoreFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class StoreSuccess implements StoreResult {
  factory StoreSuccess() = _$StoreSuccess;
}

/// @nodoc
abstract class _$$StoreFailureCopyWith<$Res> {
  factory _$$StoreFailureCopyWith(
          _$StoreFailure value, $Res Function(_$StoreFailure) then) =
      __$$StoreFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StoreFailureCopyWithImpl<$Res> extends _$StoreResultCopyWithImpl<$Res>
    implements _$$StoreFailureCopyWith<$Res> {
  __$$StoreFailureCopyWithImpl(
      _$StoreFailure _value, $Res Function(_$StoreFailure) _then)
      : super(_value, (v) => _then(v as _$StoreFailure));

  @override
  _$StoreFailure get _value => super._value as _$StoreFailure;
}

/// @nodoc

class _$StoreFailure implements StoreFailure {
  _$StoreFailure();

  @override
  String toString() {
    return 'StoreResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StoreFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StoreSuccess value) success,
    required TResult Function(StoreFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StoreSuccess value)? success,
    TResult Function(StoreFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StoreSuccess value)? success,
    TResult Function(StoreFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class StoreFailure implements StoreResult {
  factory StoreFailure() = _$StoreFailure;
}

/// @nodoc
mixin _$HasAccountResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool hasAccount, User? user) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool hasAccount, User? user)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool hasAccount, User? user)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HasAccountSuccess value) success,
    required TResult Function(HasAccountFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(HasAccountSuccess value)? success,
    TResult Function(HasAccountFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HasAccountSuccess value)? success,
    TResult Function(HasAccountFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HasAccountResultCopyWith<$Res> {
  factory $HasAccountResultCopyWith(
          HasAccountResult value, $Res Function(HasAccountResult) then) =
      _$HasAccountResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$HasAccountResultCopyWithImpl<$Res>
    implements $HasAccountResultCopyWith<$Res> {
  _$HasAccountResultCopyWithImpl(this._value, this._then);

  final HasAccountResult _value;
  // ignore: unused_field
  final $Res Function(HasAccountResult) _then;
}

/// @nodoc
abstract class _$$HasAccountSuccessCopyWith<$Res> {
  factory _$$HasAccountSuccessCopyWith(
          _$HasAccountSuccess value, $Res Function(_$HasAccountSuccess) then) =
      __$$HasAccountSuccessCopyWithImpl<$Res>;
  $Res call({bool hasAccount, User? user});

  $UserCopyWith<$Res>? get user;
}

/// @nodoc
class __$$HasAccountSuccessCopyWithImpl<$Res>
    extends _$HasAccountResultCopyWithImpl<$Res>
    implements _$$HasAccountSuccessCopyWith<$Res> {
  __$$HasAccountSuccessCopyWithImpl(
      _$HasAccountSuccess _value, $Res Function(_$HasAccountSuccess) _then)
      : super(_value, (v) => _then(v as _$HasAccountSuccess));

  @override
  _$HasAccountSuccess get _value => super._value as _$HasAccountSuccess;

  @override
  $Res call({
    Object? hasAccount = freezed,
    Object? user = freezed,
  }) {
    return _then(_$HasAccountSuccess(
      hasAccount: hasAccount == freezed
          ? _value.hasAccount
          : hasAccount // ignore: cast_nullable_to_non_nullable
              as bool,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User?,
    ));
  }

  @override
  $UserCopyWith<$Res>? get user {
    if (_value.user == null) {
      return null;
    }

    return $UserCopyWith<$Res>(_value.user!, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc

class _$HasAccountSuccess implements HasAccountSuccess {
  _$HasAccountSuccess({required this.hasAccount, this.user});

  @override
  final bool hasAccount;
  @override
  final User? user;

  @override
  String toString() {
    return 'HasAccountResult.success(hasAccount: $hasAccount, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HasAccountSuccess &&
            const DeepCollectionEquality()
                .equals(other.hasAccount, hasAccount) &&
            const DeepCollectionEquality().equals(other.user, user));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(hasAccount),
      const DeepCollectionEquality().hash(user));

  @JsonKey(ignore: true)
  @override
  _$$HasAccountSuccessCopyWith<_$HasAccountSuccess> get copyWith =>
      __$$HasAccountSuccessCopyWithImpl<_$HasAccountSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool hasAccount, User? user) success,
    required TResult Function() failure,
  }) {
    return success(hasAccount, user);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool hasAccount, User? user)? success,
    TResult Function()? failure,
  }) {
    return success?.call(hasAccount, user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool hasAccount, User? user)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(hasAccount, user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HasAccountSuccess value) success,
    required TResult Function(HasAccountFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(HasAccountSuccess value)? success,
    TResult Function(HasAccountFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HasAccountSuccess value)? success,
    TResult Function(HasAccountFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class HasAccountSuccess implements HasAccountResult {
  factory HasAccountSuccess(
      {required final bool hasAccount, final User? user}) = _$HasAccountSuccess;

  bool get hasAccount;
  User? get user;
  @JsonKey(ignore: true)
  _$$HasAccountSuccessCopyWith<_$HasAccountSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$HasAccountFailureCopyWith<$Res> {
  factory _$$HasAccountFailureCopyWith(
          _$HasAccountFailure value, $Res Function(_$HasAccountFailure) then) =
      __$$HasAccountFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$HasAccountFailureCopyWithImpl<$Res>
    extends _$HasAccountResultCopyWithImpl<$Res>
    implements _$$HasAccountFailureCopyWith<$Res> {
  __$$HasAccountFailureCopyWithImpl(
      _$HasAccountFailure _value, $Res Function(_$HasAccountFailure) _then)
      : super(_value, (v) => _then(v as _$HasAccountFailure));

  @override
  _$HasAccountFailure get _value => super._value as _$HasAccountFailure;
}

/// @nodoc

class _$HasAccountFailure implements HasAccountFailure {
  _$HasAccountFailure();

  @override
  String toString() {
    return 'HasAccountResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$HasAccountFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool hasAccount, User? user) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool hasAccount, User? user)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool hasAccount, User? user)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(HasAccountSuccess value) success,
    required TResult Function(HasAccountFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(HasAccountSuccess value)? success,
    TResult Function(HasAccountFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(HasAccountSuccess value)? success,
    TResult Function(HasAccountFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class HasAccountFailure implements HasAccountResult {
  factory HasAccountFailure() = _$HasAccountFailure;
}

/// @nodoc
mixin _$GetUserResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetUserSuccess value) success,
    required TResult Function(GetUserFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetUserSuccess value)? success,
    TResult Function(GetUserFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetUserSuccess value)? success,
    TResult Function(GetUserFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetUserResultCopyWith<$Res> {
  factory $GetUserResultCopyWith(
          GetUserResult value, $Res Function(GetUserResult) then) =
      _$GetUserResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$GetUserResultCopyWithImpl<$Res>
    implements $GetUserResultCopyWith<$Res> {
  _$GetUserResultCopyWithImpl(this._value, this._then);

  final GetUserResult _value;
  // ignore: unused_field
  final $Res Function(GetUserResult) _then;
}

/// @nodoc
abstract class _$$GetUserSuccessCopyWith<$Res> {
  factory _$$GetUserSuccessCopyWith(
          _$GetUserSuccess value, $Res Function(_$GetUserSuccess) then) =
      __$$GetUserSuccessCopyWithImpl<$Res>;
  $Res call({User user});

  $UserCopyWith<$Res> get user;
}

/// @nodoc
class __$$GetUserSuccessCopyWithImpl<$Res>
    extends _$GetUserResultCopyWithImpl<$Res>
    implements _$$GetUserSuccessCopyWith<$Res> {
  __$$GetUserSuccessCopyWithImpl(
      _$GetUserSuccess _value, $Res Function(_$GetUserSuccess) _then)
      : super(_value, (v) => _then(v as _$GetUserSuccess));

  @override
  _$GetUserSuccess get _value => super._value as _$GetUserSuccess;

  @override
  $Res call({
    Object? user = freezed,
  }) {
    return _then(_$GetUserSuccess(
      user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc

class _$GetUserSuccess implements GetUserSuccess {
  _$GetUserSuccess(this.user);

  @override
  final User user;

  @override
  String toString() {
    return 'GetUserResult.success(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetUserSuccess &&
            const DeepCollectionEquality().equals(other.user, user));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(user));

  @JsonKey(ignore: true)
  @override
  _$$GetUserSuccessCopyWith<_$GetUserSuccess> get copyWith =>
      __$$GetUserSuccessCopyWithImpl<_$GetUserSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) success,
    required TResult Function() failure,
  }) {
    return success(user);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function()? failure,
  }) {
    return success?.call(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetUserSuccess value) success,
    required TResult Function(GetUserFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetUserSuccess value)? success,
    TResult Function(GetUserFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetUserSuccess value)? success,
    TResult Function(GetUserFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class GetUserSuccess implements GetUserResult {
  factory GetUserSuccess(final User user) = _$GetUserSuccess;

  User get user;
  @JsonKey(ignore: true)
  _$$GetUserSuccessCopyWith<_$GetUserSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetUserFailureCopyWith<$Res> {
  factory _$$GetUserFailureCopyWith(
          _$GetUserFailure value, $Res Function(_$GetUserFailure) then) =
      __$$GetUserFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetUserFailureCopyWithImpl<$Res>
    extends _$GetUserResultCopyWithImpl<$Res>
    implements _$$GetUserFailureCopyWith<$Res> {
  __$$GetUserFailureCopyWithImpl(
      _$GetUserFailure _value, $Res Function(_$GetUserFailure) _then)
      : super(_value, (v) => _then(v as _$GetUserFailure));

  @override
  _$GetUserFailure get _value => super._value as _$GetUserFailure;
}

/// @nodoc

class _$GetUserFailure implements GetUserFailure {
  _$GetUserFailure();

  @override
  String toString() {
    return 'GetUserResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetUserFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetUserSuccess value) success,
    required TResult Function(GetUserFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetUserSuccess value)? success,
    TResult Function(GetUserFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetUserSuccess value)? success,
    TResult Function(GetUserFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class GetUserFailure implements GetUserResult {
  factory GetUserFailure() = _$GetUserFailure;
}

/// @nodoc
mixin _$GetOrderResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderSuccess value) success,
    required TResult Function(GetOrderFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderSuccess value)? success,
    TResult Function(GetOrderFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderSuccess value)? success,
    TResult Function(GetOrderFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetOrderResultCopyWith<$Res> {
  factory $GetOrderResultCopyWith(
          GetOrderResult value, $Res Function(GetOrderResult) then) =
      _$GetOrderResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$GetOrderResultCopyWithImpl<$Res>
    implements $GetOrderResultCopyWith<$Res> {
  _$GetOrderResultCopyWithImpl(this._value, this._then);

  final GetOrderResult _value;
  // ignore: unused_field
  final $Res Function(GetOrderResult) _then;
}

/// @nodoc
abstract class _$$GetOrderSuccessCopyWith<$Res> {
  factory _$$GetOrderSuccessCopyWith(
          _$GetOrderSuccess value, $Res Function(_$GetOrderSuccess) then) =
      __$$GetOrderSuccessCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$GetOrderSuccessCopyWithImpl<$Res>
    extends _$GetOrderResultCopyWithImpl<$Res>
    implements _$$GetOrderSuccessCopyWith<$Res> {
  __$$GetOrderSuccessCopyWithImpl(
      _$GetOrderSuccess _value, $Res Function(_$GetOrderSuccess) _then)
      : super(_value, (v) => _then(v as _$GetOrderSuccess));

  @override
  _$GetOrderSuccess get _value => super._value as _$GetOrderSuccess;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$GetOrderSuccess(
      order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$GetOrderSuccess implements GetOrderSuccess {
  _$GetOrderSuccess(this.order);

  @override
  final Order order;

  @override
  String toString() {
    return 'GetOrderResult.success(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetOrderSuccess &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$GetOrderSuccessCopyWith<_$GetOrderSuccess> get copyWith =>
      __$$GetOrderSuccessCopyWithImpl<_$GetOrderSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) {
    return success(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) {
    return success?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderSuccess value) success,
    required TResult Function(GetOrderFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderSuccess value)? success,
    TResult Function(GetOrderFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderSuccess value)? success,
    TResult Function(GetOrderFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class GetOrderSuccess implements GetOrderResult {
  factory GetOrderSuccess(final Order order) = _$GetOrderSuccess;

  Order get order;
  @JsonKey(ignore: true)
  _$$GetOrderSuccessCopyWith<_$GetOrderSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetOrderFailureCopyWith<$Res> {
  factory _$$GetOrderFailureCopyWith(
          _$GetOrderFailure value, $Res Function(_$GetOrderFailure) then) =
      __$$GetOrderFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetOrderFailureCopyWithImpl<$Res>
    extends _$GetOrderResultCopyWithImpl<$Res>
    implements _$$GetOrderFailureCopyWith<$Res> {
  __$$GetOrderFailureCopyWithImpl(
      _$GetOrderFailure _value, $Res Function(_$GetOrderFailure) _then)
      : super(_value, (v) => _then(v as _$GetOrderFailure));

  @override
  _$GetOrderFailure get _value => super._value as _$GetOrderFailure;
}

/// @nodoc

class _$GetOrderFailure implements GetOrderFailure {
  _$GetOrderFailure();

  @override
  String toString() {
    return 'GetOrderResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetOrderFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderSuccess value) success,
    required TResult Function(GetOrderFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderSuccess value)? success,
    TResult Function(GetOrderFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderSuccess value)? success,
    TResult Function(GetOrderFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class GetOrderFailure implements GetOrderResult {
  factory GetOrderFailure() = _$GetOrderFailure;
}

/// @nodoc
mixin _$GetOrderDataResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Participant> participants,
            KtList<Product> products, KtList<CartEntry> cart)
        success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Participant> participants, KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Participant> participants, KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderDataSuccess value) success,
    required TResult Function(GetOrderDataFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderDataSuccess value)? success,
    TResult Function(GetOrderDataFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderDataSuccess value)? success,
    TResult Function(GetOrderDataFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetOrderDataResultCopyWith<$Res> {
  factory $GetOrderDataResultCopyWith(
          GetOrderDataResult value, $Res Function(GetOrderDataResult) then) =
      _$GetOrderDataResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$GetOrderDataResultCopyWithImpl<$Res>
    implements $GetOrderDataResultCopyWith<$Res> {
  _$GetOrderDataResultCopyWithImpl(this._value, this._then);

  final GetOrderDataResult _value;
  // ignore: unused_field
  final $Res Function(GetOrderDataResult) _then;
}

/// @nodoc
abstract class _$$GetOrderDataSuccessCopyWith<$Res> {
  factory _$$GetOrderDataSuccessCopyWith(_$GetOrderDataSuccess value,
          $Res Function(_$GetOrderDataSuccess) then) =
      __$$GetOrderDataSuccessCopyWithImpl<$Res>;
  $Res call(
      {KtList<Participant> participants,
      KtList<Product> products,
      KtList<CartEntry> cart});
}

/// @nodoc
class __$$GetOrderDataSuccessCopyWithImpl<$Res>
    extends _$GetOrderDataResultCopyWithImpl<$Res>
    implements _$$GetOrderDataSuccessCopyWith<$Res> {
  __$$GetOrderDataSuccessCopyWithImpl(
      _$GetOrderDataSuccess _value, $Res Function(_$GetOrderDataSuccess) _then)
      : super(_value, (v) => _then(v as _$GetOrderDataSuccess));

  @override
  _$GetOrderDataSuccess get _value => super._value as _$GetOrderDataSuccess;

  @override
  $Res call({
    Object? participants = freezed,
    Object? products = freezed,
    Object? cart = freezed,
  }) {
    return _then(_$GetOrderDataSuccess(
      participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>,
      products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<Product>,
      cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>,
    ));
  }
}

/// @nodoc

class _$GetOrderDataSuccess implements GetOrderDataSuccess {
  _$GetOrderDataSuccess(this.participants, this.products, this.cart);

  @override
  final KtList<Participant> participants;
  @override
  final KtList<Product> products;
  @override
  final KtList<CartEntry> cart;

  @override
  String toString() {
    return 'GetOrderDataResult.success(participants: $participants, products: $products, cart: $cart)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetOrderDataSuccess &&
            const DeepCollectionEquality()
                .equals(other.participants, participants) &&
            const DeepCollectionEquality().equals(other.products, products) &&
            const DeepCollectionEquality().equals(other.cart, cart));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(participants),
      const DeepCollectionEquality().hash(products),
      const DeepCollectionEquality().hash(cart));

  @JsonKey(ignore: true)
  @override
  _$$GetOrderDataSuccessCopyWith<_$GetOrderDataSuccess> get copyWith =>
      __$$GetOrderDataSuccessCopyWithImpl<_$GetOrderDataSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Participant> participants,
            KtList<Product> products, KtList<CartEntry> cart)
        success,
    required TResult Function() failure,
  }) {
    return success(participants, products, cart);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Participant> participants, KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
  }) {
    return success?.call(participants, products, cart);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Participant> participants, KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(participants, products, cart);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderDataSuccess value) success,
    required TResult Function(GetOrderDataFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderDataSuccess value)? success,
    TResult Function(GetOrderDataFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderDataSuccess value)? success,
    TResult Function(GetOrderDataFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class GetOrderDataSuccess implements GetOrderDataResult {
  factory GetOrderDataSuccess(
      final KtList<Participant> participants,
      final KtList<Product> products,
      final KtList<CartEntry> cart) = _$GetOrderDataSuccess;

  KtList<Participant> get participants;
  KtList<Product> get products;
  KtList<CartEntry> get cart;
  @JsonKey(ignore: true)
  _$$GetOrderDataSuccessCopyWith<_$GetOrderDataSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetOrderDataFailureCopyWith<$Res> {
  factory _$$GetOrderDataFailureCopyWith(_$GetOrderDataFailure value,
          $Res Function(_$GetOrderDataFailure) then) =
      __$$GetOrderDataFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetOrderDataFailureCopyWithImpl<$Res>
    extends _$GetOrderDataResultCopyWithImpl<$Res>
    implements _$$GetOrderDataFailureCopyWith<$Res> {
  __$$GetOrderDataFailureCopyWithImpl(
      _$GetOrderDataFailure _value, $Res Function(_$GetOrderDataFailure) _then)
      : super(_value, (v) => _then(v as _$GetOrderDataFailure));

  @override
  _$GetOrderDataFailure get _value => super._value as _$GetOrderDataFailure;
}

/// @nodoc

class _$GetOrderDataFailure implements GetOrderDataFailure {
  _$GetOrderDataFailure();

  @override
  String toString() {
    return 'GetOrderDataResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetOrderDataFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Participant> participants,
            KtList<Product> products, KtList<CartEntry> cart)
        success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Participant> participants, KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Participant> participants, KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderDataSuccess value) success,
    required TResult Function(GetOrderDataFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderDataSuccess value)? success,
    TResult Function(GetOrderDataFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderDataSuccess value)? success,
    TResult Function(GetOrderDataFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class GetOrderDataFailure implements GetOrderDataResult {
  factory GetOrderDataFailure() = _$GetOrderDataFailure;
}

/// @nodoc
mixin _$GetOrderProductsResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Product> products) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Product> products)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Product> products)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderProductsSuccess value) success,
    required TResult Function(GetOrderProductsFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderProductsSuccess value)? success,
    TResult Function(GetOrderProductsFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderProductsSuccess value)? success,
    TResult Function(GetOrderProductsFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetOrderProductsResultCopyWith<$Res> {
  factory $GetOrderProductsResultCopyWith(GetOrderProductsResult value,
          $Res Function(GetOrderProductsResult) then) =
      _$GetOrderProductsResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$GetOrderProductsResultCopyWithImpl<$Res>
    implements $GetOrderProductsResultCopyWith<$Res> {
  _$GetOrderProductsResultCopyWithImpl(this._value, this._then);

  final GetOrderProductsResult _value;
  // ignore: unused_field
  final $Res Function(GetOrderProductsResult) _then;
}

/// @nodoc
abstract class _$$GetOrderProductsSuccessCopyWith<$Res> {
  factory _$$GetOrderProductsSuccessCopyWith(_$GetOrderProductsSuccess value,
          $Res Function(_$GetOrderProductsSuccess) then) =
      __$$GetOrderProductsSuccessCopyWithImpl<$Res>;
  $Res call({KtList<Product> products});
}

/// @nodoc
class __$$GetOrderProductsSuccessCopyWithImpl<$Res>
    extends _$GetOrderProductsResultCopyWithImpl<$Res>
    implements _$$GetOrderProductsSuccessCopyWith<$Res> {
  __$$GetOrderProductsSuccessCopyWithImpl(_$GetOrderProductsSuccess _value,
      $Res Function(_$GetOrderProductsSuccess) _then)
      : super(_value, (v) => _then(v as _$GetOrderProductsSuccess));

  @override
  _$GetOrderProductsSuccess get _value =>
      super._value as _$GetOrderProductsSuccess;

  @override
  $Res call({
    Object? products = freezed,
  }) {
    return _then(_$GetOrderProductsSuccess(
      products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<Product>,
    ));
  }
}

/// @nodoc

class _$GetOrderProductsSuccess implements GetOrderProductsSuccess {
  _$GetOrderProductsSuccess(this.products);

  @override
  final KtList<Product> products;

  @override
  String toString() {
    return 'GetOrderProductsResult.success(products: $products)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetOrderProductsSuccess &&
            const DeepCollectionEquality().equals(other.products, products));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(products));

  @JsonKey(ignore: true)
  @override
  _$$GetOrderProductsSuccessCopyWith<_$GetOrderProductsSuccess> get copyWith =>
      __$$GetOrderProductsSuccessCopyWithImpl<_$GetOrderProductsSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Product> products) success,
    required TResult Function() failure,
  }) {
    return success(products);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Product> products)? success,
    TResult Function()? failure,
  }) {
    return success?.call(products);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Product> products)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(products);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderProductsSuccess value) success,
    required TResult Function(GetOrderProductsFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderProductsSuccess value)? success,
    TResult Function(GetOrderProductsFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderProductsSuccess value)? success,
    TResult Function(GetOrderProductsFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class GetOrderProductsSuccess implements GetOrderProductsResult {
  factory GetOrderProductsSuccess(final KtList<Product> products) =
      _$GetOrderProductsSuccess;

  KtList<Product> get products;
  @JsonKey(ignore: true)
  _$$GetOrderProductsSuccessCopyWith<_$GetOrderProductsSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetOrderProductsFailureCopyWith<$Res> {
  factory _$$GetOrderProductsFailureCopyWith(_$GetOrderProductsFailure value,
          $Res Function(_$GetOrderProductsFailure) then) =
      __$$GetOrderProductsFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetOrderProductsFailureCopyWithImpl<$Res>
    extends _$GetOrderProductsResultCopyWithImpl<$Res>
    implements _$$GetOrderProductsFailureCopyWith<$Res> {
  __$$GetOrderProductsFailureCopyWithImpl(_$GetOrderProductsFailure _value,
      $Res Function(_$GetOrderProductsFailure) _then)
      : super(_value, (v) => _then(v as _$GetOrderProductsFailure));

  @override
  _$GetOrderProductsFailure get _value =>
      super._value as _$GetOrderProductsFailure;
}

/// @nodoc

class _$GetOrderProductsFailure implements GetOrderProductsFailure {
  _$GetOrderProductsFailure();

  @override
  String toString() {
    return 'GetOrderProductsResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetOrderProductsFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Product> products) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Product> products)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Product> products)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrderProductsSuccess value) success,
    required TResult Function(GetOrderProductsFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrderProductsSuccess value)? success,
    TResult Function(GetOrderProductsFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrderProductsSuccess value)? success,
    TResult Function(GetOrderProductsFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class GetOrderProductsFailure implements GetOrderProductsResult {
  factory GetOrderProductsFailure() = _$GetOrderProductsFailure;
}

/// @nodoc
mixin _$GetOrdersResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Order> orders) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Order> orders)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Order> orders)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrdersSuccess value) success,
    required TResult Function(GetOrdersFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrdersSuccess value)? success,
    TResult Function(GetOrdersFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrdersSuccess value)? success,
    TResult Function(GetOrdersFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetOrdersResultCopyWith<$Res> {
  factory $GetOrdersResultCopyWith(
          GetOrdersResult value, $Res Function(GetOrdersResult) then) =
      _$GetOrdersResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$GetOrdersResultCopyWithImpl<$Res>
    implements $GetOrdersResultCopyWith<$Res> {
  _$GetOrdersResultCopyWithImpl(this._value, this._then);

  final GetOrdersResult _value;
  // ignore: unused_field
  final $Res Function(GetOrdersResult) _then;
}

/// @nodoc
abstract class _$$GetOrdersSuccessCopyWith<$Res> {
  factory _$$GetOrdersSuccessCopyWith(
          _$GetOrdersSuccess value, $Res Function(_$GetOrdersSuccess) then) =
      __$$GetOrdersSuccessCopyWithImpl<$Res>;
  $Res call({KtList<Order> orders});
}

/// @nodoc
class __$$GetOrdersSuccessCopyWithImpl<$Res>
    extends _$GetOrdersResultCopyWithImpl<$Res>
    implements _$$GetOrdersSuccessCopyWith<$Res> {
  __$$GetOrdersSuccessCopyWithImpl(
      _$GetOrdersSuccess _value, $Res Function(_$GetOrdersSuccess) _then)
      : super(_value, (v) => _then(v as _$GetOrdersSuccess));

  @override
  _$GetOrdersSuccess get _value => super._value as _$GetOrdersSuccess;

  @override
  $Res call({
    Object? orders = freezed,
  }) {
    return _then(_$GetOrdersSuccess(
      orders == freezed
          ? _value.orders
          : orders // ignore: cast_nullable_to_non_nullable
              as KtList<Order>,
    ));
  }
}

/// @nodoc

class _$GetOrdersSuccess implements GetOrdersSuccess {
  _$GetOrdersSuccess(this.orders);

  @override
  final KtList<Order> orders;

  @override
  String toString() {
    return 'GetOrdersResult.success(orders: $orders)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetOrdersSuccess &&
            const DeepCollectionEquality().equals(other.orders, orders));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(orders));

  @JsonKey(ignore: true)
  @override
  _$$GetOrdersSuccessCopyWith<_$GetOrdersSuccess> get copyWith =>
      __$$GetOrdersSuccessCopyWithImpl<_$GetOrdersSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Order> orders) success,
    required TResult Function() failure,
  }) {
    return success(orders);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Order> orders)? success,
    TResult Function()? failure,
  }) {
    return success?.call(orders);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Order> orders)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(orders);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrdersSuccess value) success,
    required TResult Function(GetOrdersFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrdersSuccess value)? success,
    TResult Function(GetOrdersFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrdersSuccess value)? success,
    TResult Function(GetOrdersFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class GetOrdersSuccess implements GetOrdersResult {
  factory GetOrdersSuccess(final KtList<Order> orders) = _$GetOrdersSuccess;

  KtList<Order> get orders;
  @JsonKey(ignore: true)
  _$$GetOrdersSuccessCopyWith<_$GetOrdersSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetOrdersFailureCopyWith<$Res> {
  factory _$$GetOrdersFailureCopyWith(
          _$GetOrdersFailure value, $Res Function(_$GetOrdersFailure) then) =
      __$$GetOrdersFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetOrdersFailureCopyWithImpl<$Res>
    extends _$GetOrdersResultCopyWithImpl<$Res>
    implements _$$GetOrdersFailureCopyWith<$Res> {
  __$$GetOrdersFailureCopyWithImpl(
      _$GetOrdersFailure _value, $Res Function(_$GetOrdersFailure) _then)
      : super(_value, (v) => _then(v as _$GetOrdersFailure));

  @override
  _$GetOrdersFailure get _value => super._value as _$GetOrdersFailure;
}

/// @nodoc

class _$GetOrdersFailure implements GetOrdersFailure {
  _$GetOrdersFailure();

  @override
  String toString() {
    return 'GetOrdersResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetOrdersFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(KtList<Order> orders) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(KtList<Order> orders)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(KtList<Order> orders)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetOrdersSuccess value) success,
    required TResult Function(GetOrdersFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GetOrdersSuccess value)? success,
    TResult Function(GetOrdersFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetOrdersSuccess value)? success,
    TResult Function(GetOrdersFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class GetOrdersFailure implements GetOrdersResult {
  factory GetOrdersFailure() = _$GetOrdersFailure;
}
