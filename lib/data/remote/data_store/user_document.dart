import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:orders/model/user.dart';

class UserDocument {
  UserDocument(this.id, this.displayName, this.emailAddress, this.phoneNumber,
      this.signInProvider, this.languageCode, this.messagingToken);

  factory UserDocument.fromSnapshot(
      DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final Map<String, dynamic> data = snapshot.data()!;
    return UserDocument(
      data['id'],
      data['displayName'],
      data['emailAddress'],
      data['phoneNumber'],
      data['signInProvider'],
      data['languageCode'],
      data['messagingToken'],
    );
  }

  factory UserDocument.fromUser(User user) {
    return UserDocument(
      user.id,
      user.displayName,
      user.emailAddress,
      user.phoneNumber,
      _mapSignInProvider(user.signInProvider),
      user.languageCode,
      user.messagingToken,
    );
  }

  final String id;
  final String displayName;
  final String emailAddress;
  final String phoneNumber;
  final String signInProvider;
  final String languageCode;
  final String? messagingToken;

  static const String collection = 'users';
  static const String fieldDisplayName = 'displayName';
  static const String fieldMessagingToken = 'messagingToken';

  User map() {
    return User(
      id: id,
      displayName: displayName,
      emailAddress: emailAddress,
      phoneNumber: phoneNumber,
      signInProvider: _mapSignInProviderValue(signInProvider),
      languageCode: languageCode,
      messagingToken: messagingToken ?? '',
    );
  }

  Map<String, dynamic> toMap() => <String, dynamic>{
        'id': id,
        'displayName': displayName,
        'emailAddress': emailAddress,
        'phoneNumber': phoneNumber,
        'signInProvider': signInProvider,
        'languageCode': languageCode,
        'messagingToken': messagingToken,
      };

  static String _mapSignInProvider(SignInProvider signInProvider) {
    switch (signInProvider) {
      case SignInProvider.apple:
        return 'apple';
      case SignInProvider.google:
        return 'google';
      case SignInProvider.facebook:
        return 'facebook';
      case SignInProvider.email:
        return 'email';
    }
  }

  static SignInProvider _mapSignInProviderValue(String value) {
    if (value == 'apple') {
      return SignInProvider.apple;
    } else if (value == 'google') {
      return SignInProvider.google;
    } else if (value == 'facebook') {
      return SignInProvider.facebook;
    } else if (value == 'email') {
      return SignInProvider.email;
    } else {
      throw Exception('Unknown sign in provider $value');
    }
  }
}
