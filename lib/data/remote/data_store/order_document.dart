import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:orders/model/order.dart';

class OrderDocument {
  OrderDocument(
    this.id,
    this.ownerId,
    this.ownerName,
    this.title,
    this.details,
    this.currency,
    this.status,
    this.statusDetails,
    this.creationDate,
    this.deliveryDate,
    this.dueDate,
    this.participantIds,
    this.isDeleted,
    this.hasFeatureEditProducts,
    this.hasFeatureSeeParticipants,
    this.type,
    this.inviteLink,
  );

  factory OrderDocument.fromSnapshot(
      DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final Map<String, dynamic> data = snapshot.data()!;
    return OrderDocument(
      data['id'],
      data['ownerId'],
      data['ownerName'],
      data['title'],
      data['details'],
      data['currency'],
      data['status'],
      data['statusDetails'],
      data['creationDate'],
      data['deliveryDate'],
      data['dueDate'],
      List<String>.from(data['participantIds']),
      data['isDeleted'],
      data['hasFeatureEditProducts'],
      data['hasFeatureSeeParticipants'],
      data['type'],
      data['inviteLink'],
    );
  }

  factory OrderDocument.fromOrder(Order order) {
    return OrderDocument(
      order.id,
      order.ownerId,
      order.ownerName,
      order.title,
      order.details,
      order.currency,
      mapStatus(order.status),
      order.statusDetails,
      Timestamp.fromDate(order.creationDate),
      order.deliveryDate != null
          ? Timestamp.fromDate(order.deliveryDate!)
          : null,
      Timestamp.fromDate(order.dueDate),
      order.participantIds,
      order.isDeleted,
      order.hasFeatureEditProducts,
      order.hasFeatureSeeParticipants,
      mapType(order.type),
      order.inviteLink,
    );
  }

  final String id;
  final String ownerId;
  final String ownerName;
  final String title;
  final String details;
  final String? currency;
  final String status;
  final String statusDetails;
  final Timestamp? creationDate;
  final Timestamp? deliveryDate;
  final Timestamp? dueDate;
  final List<String> participantIds;
  final bool? isDeleted;
  final bool? hasFeatureEditProducts;
  final bool? hasFeatureSeeParticipants;
  final String? type;
  final String? inviteLink;

  static const String collection = 'orders';
  static const String fieldOwnerId = 'ownerId';
  static const String fieldStatus = 'status';
  static const String fieldTitle = 'title';
  static const String fieldDetails = 'details';
  static const String fieldCurrency = 'currency';
  static const String fieldDeliveryDate = 'deliveryDate';
  static const String fieldDueDate = 'dueDate';
  static const String fieldParticipantIds = 'participantIds';
  static const String fieldCreationDate = 'creationDate';
  static const String fieldIsDeleted = 'isDeleted';

  Order map() {
    return Order(
      id: id,
      ownerId: ownerId,
      ownerName: ownerName,
      title: title,
      details: details,
      // this ?? should be removed sometime later
      currency: currency ?? 'RON',
      status: _mapStatusValue(status),
      statusDetails: statusDetails,
      // this ? and ?? should be removed sometime later
      creationDate: creationDate?.toDate() ?? DateTime(2020),
      deliveryDate: deliveryDate?.toDate(),
      // this ? and ?? should be removed sometime later
      dueDate: dueDate?.toDate() ?? DateTime.now().add(const Duration(days: 1)),
      participantIds: participantIds,
      isDeleted: isDeleted ?? false,
      hasFeatureEditProducts: hasFeatureEditProducts ?? true,
      hasFeatureSeeParticipants: hasFeatureSeeParticipants ?? true,
      type: type != null ? _mapTypeValue(type!) : OrderType.proxy,
      // this ?? should be removed sometime later
      inviteLink: inviteLink ?? '',
    );
  }

  Map<String, dynamic> toMap() => <String, dynamic>{
        'id': id,
        'ownerId': ownerId,
        'ownerName': ownerName,
        'title': title,
        'details': details,
        'currency': currency,
        'status': status,
        'statusDetails': statusDetails,
        'creationDate': creationDate,
        'deliveryDate': deliveryDate,
        'dueDate': dueDate,
        'participantIds': participantIds,
        'isDeleted': isDeleted,
        'hasFeatureEditProducts': hasFeatureEditProducts,
        'hasFeatureSeeParticipants': hasFeatureSeeParticipants,
        'type': type,
        'inviteLink': inviteLink,
      };

  static String mapStatus(OrderStatus status) {
    switch (status) {
      case OrderStatus.inProgress:
        return 'inProgress';
      case OrderStatus.blocked:
        return 'blocked';
      case OrderStatus.madeUpstream:
        return 'madeUpstream';
      case OrderStatus.arrived:
        return 'arrived';
      case OrderStatus.completed:
        return 'completed';
      case OrderStatus.forcefullyClosed:
        return 'forcefullyClosed';
    }
  }

  static OrderStatus _mapStatusValue(String value) {
    if (value == 'inProgress') {
      return OrderStatus.inProgress;
    } else if (value == 'blocked') {
      return OrderStatus.blocked;
    } else if (value == 'madeUpstream') {
      return OrderStatus.madeUpstream;
    } else if (value == 'arrived') {
      return OrderStatus.arrived;
    } else if (value == 'completed') {
      return OrderStatus.completed;
    } else if (value == 'forcefullyClosed') {
      return OrderStatus.forcefullyClosed;
    } else {
      throw Exception('Unknown order status $value');
    }
  }

  static String mapType(OrderType type) {
    switch (type) {
      case OrderType.proxy:
        return 'proxy';
      case OrderType.provider:
        return 'provider';
    }
  }

  static OrderType _mapTypeValue(String value) {
    if (value == 'proxy') {
      return OrderType.proxy;
    } else if (value == 'provider') {
      return OrderType.provider;
    } else {
      throw Exception('Unknown order type $value');
    }
  }
}
