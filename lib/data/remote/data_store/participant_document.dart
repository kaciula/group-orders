import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:orders/model/participant.dart';

class ParticipantDocument {
  ParticipantDocument(
    this.userId,
    this.displayName,
    this.phoneNumber,
    this.delivered,
    this.confirmedDelivered,
    this.paid,
    this.confirmedPaid,
    this.paidDetails,
  );

  factory ParticipantDocument.fromSnapshot(
      DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final Map<String, dynamic> data = snapshot.data()!;
    return ParticipantDocument(
      data['userId'],
      data['displayName'],
      data['phoneNumber'],
      data['delivered'],
      data['confirmedDelivered'],
      data['paid'],
      data['confirmedPaid'],
      data['paidDetails'],
    );
  }

  factory ParticipantDocument.fromParticipant(Participant participant) {
    return ParticipantDocument(
      participant.userId,
      participant.displayName,
      participant.phoneNumber,
      participant.delivered,
      participant.confirmedDelivered,
      participant.paid,
      participant.confirmedPaid,
      participant.paidDetails,
    );
  }

  final String userId;
  final String displayName;
  final String? phoneNumber;
  final bool? delivered;
  final bool? confirmedDelivered;
  final bool? paid;
  final bool? confirmedPaid;
  final String? paidDetails;

  static const String collection = 'participants';
  static const String fieldDelivered = 'delivered';
  static const String fieldConfirmedDelivered = 'confirmedDelivered';
  static const String fieldPaid = 'paid';
  static const String fieldConfirmedPaid = 'confirmedPaid';
  static const String fieldPaidDetails = 'paidDetails';

  Participant map() {
    return Participant(
      userId: userId,
      displayName: displayName,
      phoneNumber: phoneNumber ?? '',
      delivered: delivered ?? false,
      confirmedDelivered: confirmedDelivered ?? false,
      paid: paid ?? false,
      confirmedPaid: confirmedPaid ?? false,
      paidDetails: paidDetails ?? '',
    );
  }

  Map<String, dynamic> toMap() => <String, dynamic>{
        'userId': userId,
        'displayName': displayName,
        'phoneNumber': phoneNumber,
        'delivered': delivered,
        'confirmedDelivered': confirmedDelivered,
        'paid': paid,
        'confirmedPaid': confirmedPaid,
        'paidDetails': paidDetails,
      };
}
