import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:logging/logging.dart';
import 'package:orders/data/remote/data_store/cart_entry_document.dart';
import 'package:orders/data/remote/data_store/order_document.dart';
import 'package:orders/data/remote/data_store/participant_document.dart';
import 'package:orders/data/remote/data_store/product_document.dart';
import 'package:orders/data/remote/data_store/user_document.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';
import 'package:orders/model/user.dart';

part 'data_remote_store.freezed.dart';

class DataRemoteStore {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseFunctions _functions =
      FirebaseFunctions.instanceFor(region: 'us-central1');

  Future<HasAccountResult> hasAccount(String userId) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> snapshot = await _firestore
          .collection(UserDocument.collection)
          .doc(userId)
          .get(const GetOptions(source: Source.server));
      if (snapshot.exists) {
        final UserDocument doc = UserDocument.fromSnapshot(snapshot);
        return HasAccountSuccess(hasAccount: true, user: doc.map());
      } else {
        return HasAccountSuccess(hasAccount: false);
      }
    } catch (error) {
      _logger.warning(error);
    }
    return HasAccountFailure();
  }

  Future<GetUserResult> getUser(String id) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> snapshot = await _firestore
          .collection(UserDocument.collection)
          .doc(id)
          .get(const GetOptions(source: Source.server));
      if (snapshot.exists) {
        final UserDocument doc = UserDocument.fromSnapshot(snapshot);
        return GetUserSuccess(doc.map());
      }
    } catch (error) {
      _logger.warning(error);
    }
    return GetUserFailure();
  }

  Future<StoreResult> saveUser(User user) async {
    try {
      await _firestore
          .collection(UserDocument.collection)
          .doc(user.id)
          .set(UserDocument.fromUser(user).toMap());
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> updateUserFields(
    String userId, {
    String? displayName,
    String? messagingToken,
  }) async {
    try {
      await _firestore.collection(UserDocument.collection).doc(userId).update(
        <String, dynamic>{
          if (displayName != null) UserDocument.fieldDisplayName: displayName,
          if (messagingToken != null)
            UserDocument.fieldMessagingToken: messagingToken,
        },
      );
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  String generateOrderId() {
    return _firestore.collection(OrderDocument.collection).doc().id;
  }

  String generateProductId({required String orderId}) {
    return _firestore
        .collection(OrderDocument.collection)
        .doc(orderId)
        .collection(ProductDocument.collection)
        .doc()
        .id;
  }

  String generateCartItemId({required String orderId}) {
    return _firestore
        .collection(OrderDocument.collection)
        .doc(orderId)
        .collection(CartEntryDocument.collection)
        .doc()
        .id;
  }

  Future<GetOrderResult> getOrder(String id) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> snapshot = await _firestore
          .collection(OrderDocument.collection)
          .doc(id)
          .get(const GetOptions(source: Source.server));
      if (snapshot.exists) {
        final OrderDocument doc = OrderDocument.fromSnapshot(snapshot);
        return GetOrderSuccess(doc.map());
      }
    } catch (error) {
      _logger.warning(error);
    }
    return GetOrderFailure();
  }

  Future<GetOrderDataResult> getOrderData({required String orderId}) async {
    try {
      final List<dynamic> results = await Future.wait(<Future<dynamic>>[
        _firestore
            .collection(OrderDocument.collection)
            .doc(orderId)
            .collection(ParticipantDocument.collection)
            .get(const GetOptions(source: Source.server)),
        _firestore
            .collection(OrderDocument.collection)
            .doc(orderId)
            .collection(ProductDocument.collection)
            .get(const GetOptions(source: Source.server)),
        _firestore
            .collection(OrderDocument.collection)
            .doc(orderId)
            .collection(CartEntryDocument.collection)
            .get(const GetOptions(source: Source.server)),
      ]);
      final QuerySnapshot<Map<String, dynamic>> querySnapshotParticipants =
          results[0];
      final QuerySnapshot<Map<String, dynamic>> querySnapshotProducts =
          results[1];
      final QuerySnapshot<Map<String, dynamic>> querySnapshotCart = results[2];

      final KtList<Participant> participantDocs = KtList<Participant>.from(
        querySnapshotParticipants.docs.map(
          (DocumentSnapshot<Map<String, dynamic>> snapshot) =>
              ParticipantDocument.fromSnapshot(snapshot).map(),
        ),
      );
      final KtList<Product> productDocs = KtList<Product>.from(
        querySnapshotProducts.docs.map(
          (DocumentSnapshot<Map<String, dynamic>> snapshot) =>
              ProductDocument.fromSnapshot(snapshot).map(),
        ),
      );
      final KtList<CartEntry> cartEntryDocs = KtList<CartEntry>.from(
        querySnapshotCart.docs.map(
          (DocumentSnapshot<Map<String, dynamic>> snapshot) =>
              CartEntryDocument.fromSnapshot(snapshot).map(),
        ),
      );
      return GetOrderDataSuccess(participantDocs, productDocs, cartEntryDocs);
    } catch (error) {
      _logger.warning(error);
    }
    return GetOrderDataFailure();
  }

  Future<GetOrderProductsResult> getOrderProducts(
      {required String orderId}) async {
    try {
      final QuerySnapshot<Map<String, dynamic>> querySnapshot = await _firestore
          .collection(OrderDocument.collection)
          .doc(orderId)
          .collection(ProductDocument.collection)
          .get(const GetOptions(source: Source.server));
      final KtList<Product> products = KtList<Product>.from(
        querySnapshot.docs.map(
          (DocumentSnapshot<Map<String, dynamic>> snapshot) =>
              ProductDocument.fromSnapshot(snapshot).map(),
        ),
      );
      return GetOrderProductsSuccess(products);
    } catch (error) {
      _logger.warning(error);
    }
    return GetOrderProductsFailure();
  }

  Future<StoreResult> createOrderProducts(
      {required String orderId, required KtList<Product> products}) async {
    try {
      final WriteBatch batch = _firestore.batch();
      for (Product product in products.iter) {
        batch.set(
          _firestore
              .collection(OrderDocument.collection)
              .doc(orderId)
              .collection(ProductDocument.collection)
              .doc(product.id),
          ProductDocument.fromProduct(product).toMap(),
        );
      }
      await batch.commit();
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
    }
    return StoreFailure();
  }

  Future<GetOrdersResult> getOrders({required String userId}) async {
    try {
      final List<dynamic> results = await Future.wait(<Future<dynamic>>[
        _firestore
            .collection(OrderDocument.collection)
            .where(OrderDocument.fieldOwnerId, isEqualTo: userId)
            .get(const GetOptions(source: Source.server)),
        _firestore
            .collection(OrderDocument.collection)
            .where(OrderDocument.fieldParticipantIds, arrayContains: userId)
            .get(const GetOptions(source: Source.server))
      ]);
      final QuerySnapshot<Map<String, dynamic>> querySnapshotOwner = results[0];
      final QuerySnapshot<Map<String, dynamic>> querySnapshotParticipant =
          results[1];

      final KtList<Order> ownerDocs = KtList<Order>.from(
        querySnapshotOwner.docs.map(
          (DocumentSnapshot<Map<String, dynamic>> snapshot) =>
              OrderDocument.fromSnapshot(snapshot).map(),
        ),
      );
      final KtList<Order> participantDocs = KtList<Order>.from(
        querySnapshotParticipant.docs.map(
          (DocumentSnapshot<Map<String, dynamic>> snapshot) =>
              OrderDocument.fromSnapshot(snapshot).map(),
        ),
      );
      return GetOrdersSuccess(ownerDocs.plus(participantDocs));
    } catch (error) {
      _logger.warning(error);
    }
    return GetOrdersFailure();
  }

  Future<StoreResult> createOrder(Order order, Participant participant) async {
    try {
      final DocumentReference<dynamic> orderRef =
          _firestore.collection(OrderDocument.collection).doc(order.id);
      final DocumentReference<dynamic> participantRef = orderRef
          .collection(ParticipantDocument.collection)
          .doc(participant.userId);

      await _firestore.runTransaction((Transaction tx) async {
        tx.set(
          orderRef,
          OrderDocument.fromOrder(order).toMap()
            ..addAll(<String, dynamic>{
              OrderDocument.fieldParticipantIds: <dynamic>[participant.userId]
            }),
        );
        tx.set(participantRef,
            ParticipantDocument.fromParticipant(participant).toMap());
      });
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> updateOrder(Order order) async {
    try {
      await _firestore
          .collection(OrderDocument.collection)
          .doc(order.id)
          .update(OrderDocument.fromOrder(order).toMap());
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> updateOrderFields({
    required String orderId,
    String? title,
    String? details,
    String? currency,
    DateTime? deliveryDate,
    DateTime? dueDate,
    bool? isDeleted,
    OrderStatus? status,
  }) async {
    try {
      await _firestore.collection(OrderDocument.collection).doc(orderId).update(
        <String, dynamic>{
          if (status != null)
            OrderDocument.fieldStatus: OrderDocument.mapStatus(status),
          if (title != null) OrderDocument.fieldTitle: title,
          if (details != null) OrderDocument.fieldDetails: details,
          if (currency != null) OrderDocument.fieldCurrency: currency,
          if (deliveryDate != null)
            OrderDocument.fieldDeliveryDate: Timestamp.fromDate(deliveryDate),
          if (dueDate != null)
            OrderDocument.fieldDueDate: Timestamp.fromDate(dueDate),
          if (isDeleted != null) OrderDocument.fieldIsDeleted: isDeleted,
        },
      );
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> addParticipantToOrder(
      {required String orderId, required Participant participant}) async {
    try {
      final DocumentReference<dynamic> orderRef =
          _firestore.collection(OrderDocument.collection).doc(orderId);
      final DocumentReference<dynamic> participantRef = orderRef
          .collection(ParticipantDocument.collection)
          .doc(participant.userId);

      await _firestore.runTransaction((Transaction tx) async {
        tx.update(
          orderRef,
          <String, dynamic>{
            OrderDocument.fieldParticipantIds:
                FieldValue.arrayUnion(<dynamic>[participant.userId])
          },
        );
        tx.set(participantRef,
            ParticipantDocument.fromParticipant(participant).toMap());
      });
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> updateParticipantFields({
    required String orderId,
    required String participantId,
    bool? delivered,
    bool? confirmedDelivered,
    bool? paid,
    bool? confirmedPaid,
    String? paidDetails,
  }) async {
    try {
      await _firestore
          .collection(OrderDocument.collection)
          .doc(orderId)
          .collection(ParticipantDocument.collection)
          .doc(participantId)
          .update(
        <String, dynamic>{
          if (delivered != null) ParticipantDocument.fieldDelivered: delivered,
          if (confirmedDelivered != null)
            ParticipantDocument.fieldConfirmedDelivered: confirmedDelivered,
          if (paid != null) ParticipantDocument.fieldPaid: paid,
          if (confirmedPaid != null)
            ParticipantDocument.fieldConfirmedPaid: confirmedPaid,
          if (paidDetails != null)
            ParticipantDocument.fieldPaidDetails: paidDetails,
        },
      );
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> saveProduct(Product product,
      {required String orderId}) async {
    try {
      await _firestore
          .collection(OrderDocument.collection)
          .doc(orderId)
          .collection(ProductDocument.collection)
          .doc(product.id)
          .set(ProductDocument.fromProduct(product).toMap());
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> deleteProduct(
      {required String orderId, required String productId}) async {
    try {
      final DocumentReference<dynamic> productRef = _firestore
          .collection(OrderDocument.collection)
          .doc(orderId)
          .collection(ProductDocument.collection)
          .doc(productId);
      final Query<dynamic> query = _firestore
          .collection(OrderDocument.collection)
          .doc(orderId)
          .collection(CartEntryDocument.collection)
          .where(CartEntryDocument.fieldProductId, isEqualTo: productId);

      await _firestore.runTransaction((Transaction tx) async {
        tx.delete(productRef);
        final QuerySnapshot<dynamic> querySnapshot =
            await query.get(const GetOptions(source: Source.server));
        final WriteBatch batch = _firestore.batch();

        for (DocumentSnapshot<dynamic> doc in querySnapshot.docs) {
          batch.delete(doc.reference);
        }
        await batch.commit();
      });
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> saveCartItem(CartEntry cartEntry,
      {required String orderId}) async {
    try {
      await _firestore
          .collection(OrderDocument.collection)
          .doc(orderId)
          .collection(CartEntryDocument.collection)
          .doc(cartEntry.id)
          .set(CartEntryDocument.fromCartItem(cartEntry).toMap());
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> deleteCartItem({
    required String orderId,
    required String cartEntryId,
  }) async {
    try {
      await _firestore
          .collection(OrderDocument.collection)
          .doc(orderId)
          .collection(CartEntryDocument.collection)
          .doc(cartEntryId)
          .delete();
      return StoreSuccess();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }

  Future<StoreResult> deleteAccount({required String userId}) async {
    try {
      final Map<String, dynamic> params = <String, dynamic>{
        'userId': userId,
      };
      final HttpsCallableResult<Map<String, dynamic>> callableResult =
          await _functions.httpsCallable('deleteAccount')(params);
      final Map<String, dynamic> result = callableResult.data;
      return result['success'] ? StoreSuccess() : StoreFailure();
    } catch (error) {
      _logger.warning(error);
      return StoreFailure();
    }
  }
}

@freezed
class StoreResult with _$StoreResult {
  factory StoreResult.success() = StoreSuccess;

  factory StoreResult.failure() = StoreFailure;
}

@freezed
class HasAccountResult with _$HasAccountResult {
  factory HasAccountResult.success({required bool hasAccount, User? user}) =
      HasAccountSuccess;

  factory HasAccountResult.failure() = HasAccountFailure;
}

@freezed
class GetUserResult with _$GetUserResult {
  factory GetUserResult.success(User user) = GetUserSuccess;

  factory GetUserResult.failure() = GetUserFailure;
}

@freezed
class GetOrderResult with _$GetOrderResult {
  factory GetOrderResult.success(Order order) = GetOrderSuccess;

  factory GetOrderResult.failure() = GetOrderFailure;
}

@freezed
class GetOrderDataResult with _$GetOrderDataResult {
  factory GetOrderDataResult.success(
    KtList<Participant> participants,
    KtList<Product> products,
    KtList<CartEntry> cart,
  ) = GetOrderDataSuccess;

  factory GetOrderDataResult.failure() = GetOrderDataFailure;
}

@freezed
class GetOrderProductsResult with _$GetOrderProductsResult {
  factory GetOrderProductsResult.success(KtList<Product> products) =
      GetOrderProductsSuccess;

  factory GetOrderProductsResult.failure() = GetOrderProductsFailure;
}

@freezed
class GetOrdersResult with _$GetOrdersResult {
  factory GetOrdersResult.success(KtList<Order> orders) = GetOrdersSuccess;

  factory GetOrdersResult.failure() = GetOrdersFailure;
}

// ignore: unused_element
final Logger _logger = Logger('DataRemoteStore');
