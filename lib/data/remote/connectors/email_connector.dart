import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:logging/logging.dart';
import 'package:orders/data/remote/connectors/connector_results.dart';
import 'package:orders/model/user.dart';

class EmailConnector {
  final auth.FirebaseAuth _firebaseAuth = auth.FirebaseAuth.instance;

  Future<SignInResult> signIn(String emailAddress, String password,
      {bool isSignUp = false, String? displayName}) async {
    try {
      auth.UserCredential userCredential;
      if (isSignUp) {
        userCredential = await _firebaseAuth.createUserWithEmailAndPassword(
            email: emailAddress, password: password);
      } else {
        userCredential = await _firebaseAuth.signInWithEmailAndPassword(
            email: emailAddress, password: password);
      }
      final auth.User? firebaseUser = userCredential.user;
      if (firebaseUser != null) {
        return SignInSuccess(
          User.basic(
            id: firebaseUser.uid,
            // if existing, this needs to be updated with the db display name
            displayName: displayName ?? '',
            emailAddress: firebaseUser.email!,
            signInProvider: SignInProvider.email,
          ),
        );
      }
    } catch (error) {
      _logger.warning(error);
      if (error is auth.FirebaseAuthException) {
        _logger.fine('code=[${error.code}]');
        if (error.code == 'weak-password') {
          return SignInFailure(type: SignInFailureType.weakPassword);
        }
        if (error.code == 'invalid-email') {
          return SignInFailure(type: SignInFailureType.invalidEmail);
        }
        if (error.code == 'email-already-in-use') {
          return SignInFailure(type: SignInFailureType.emailAlreadyInUse);
        }
        if (error.code == 'wrong-password') {
          return SignInFailure(type: SignInFailureType.wrongPassword);
        }
        if (error.code == 'user-not-found') {
          return SignInFailure(type: SignInFailureType.userNotFound);
        }
      }
    }
    return SignInFailure();
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }
}

// ignore: unused_element
final Logger _logger = Logger('EmailConnector');
