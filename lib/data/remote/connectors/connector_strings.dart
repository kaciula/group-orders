import 'package:orders/app/app_locale.dart';

// ignore: avoid_classes_with_only_static_members
class Strings {
  static String get facebookTitle {
    return AppLocale.isRo ? 'Conectare cu Facebook' : 'Connect with Facebook';
  }

  static String get failureSignInAccountWithDifferentProvider {
    return AppLocale.isRo
        ? 'Există deja un cont cu această adresă de email dar conectată printr-o altă metodă.'
        : 'There is already an account with this email address but signed in with a different provider.';
  }

  static String get failureSignInWeakPassword {
    return AppLocale.isRo
        ? 'Parola trebuie să aibă minim 6 caractere.'
        : 'Password should be at least 6 characters.';
  }

  static String get failureSignInInvalidEmail {
    return AppLocale.isRo
        ? 'Adresa de email este invalidă.'
        : 'The email address is malformed.';
  }

  static String get failureSignInEmailAlreadyInUse {
    return AppLocale.isRo
        ? 'Adresa de email este deja folosită de un alt cont.'
        : 'The email address is already in use by a different account.';
  }

  static String get failureSignInWrongPassword {
    return AppLocale.isRo ? 'Parola este greșită' : 'The password is wrong.';
  }

  static String get failureSignInUserNotFound {
    return AppLocale.isRo
        ? 'Niciun cont găsit cu această adresă de email.'
        : 'No user found with this email address.';
  }

  static String get failureSignInCanceled {
    return AppLocale.isRo ? '' : '';
  }

  static String get failureSignInOther {
    return AppLocale.isRo ? '' : '';
  }

  static String get failurePhoneVerifierDifferentEmail {
    return AppLocale.isRo
        ? 'Numărul de telefon este deja asociat cu o altă adresă de email.'
        : 'The phone number is already associated with a different email address.';
  }

  static String get failurePhoneVerifierWrongCode {
    return AppLocale.isRo
        ? 'Codul SMS de verificare este greșit.'
        : 'The SMS verification code is invalid.';
  }
}
