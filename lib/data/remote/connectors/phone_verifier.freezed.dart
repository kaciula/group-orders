// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'phone_verifier.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$VerifyPhoneNumberManuallyResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function(String errorMsg) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(VerifyPhoneNumberManuallySuccess value) success,
    required TResult Function(VerifyPhoneNumberManuallyFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(VerifyPhoneNumberManuallySuccess value)? success,
    TResult Function(VerifyPhoneNumberManuallyFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(VerifyPhoneNumberManuallySuccess value)? success,
    TResult Function(VerifyPhoneNumberManuallyFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VerifyPhoneNumberManuallyResultCopyWith<$Res> {
  factory $VerifyPhoneNumberManuallyResultCopyWith(
          VerifyPhoneNumberManuallyResult value,
          $Res Function(VerifyPhoneNumberManuallyResult) then) =
      _$VerifyPhoneNumberManuallyResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$VerifyPhoneNumberManuallyResultCopyWithImpl<$Res>
    implements $VerifyPhoneNumberManuallyResultCopyWith<$Res> {
  _$VerifyPhoneNumberManuallyResultCopyWithImpl(this._value, this._then);

  final VerifyPhoneNumberManuallyResult _value;
  // ignore: unused_field
  final $Res Function(VerifyPhoneNumberManuallyResult) _then;
}

/// @nodoc
abstract class _$$VerifyPhoneNumberManuallySuccessCopyWith<$Res> {
  factory _$$VerifyPhoneNumberManuallySuccessCopyWith(
          _$VerifyPhoneNumberManuallySuccess value,
          $Res Function(_$VerifyPhoneNumberManuallySuccess) then) =
      __$$VerifyPhoneNumberManuallySuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$VerifyPhoneNumberManuallySuccessCopyWithImpl<$Res>
    extends _$VerifyPhoneNumberManuallyResultCopyWithImpl<$Res>
    implements _$$VerifyPhoneNumberManuallySuccessCopyWith<$Res> {
  __$$VerifyPhoneNumberManuallySuccessCopyWithImpl(
      _$VerifyPhoneNumberManuallySuccess _value,
      $Res Function(_$VerifyPhoneNumberManuallySuccess) _then)
      : super(_value, (v) => _then(v as _$VerifyPhoneNumberManuallySuccess));

  @override
  _$VerifyPhoneNumberManuallySuccess get _value =>
      super._value as _$VerifyPhoneNumberManuallySuccess;
}

/// @nodoc

class _$VerifyPhoneNumberManuallySuccess
    implements VerifyPhoneNumberManuallySuccess {
  _$VerifyPhoneNumberManuallySuccess();

  @override
  String toString() {
    return 'VerifyPhoneNumberManuallyResult.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$VerifyPhoneNumberManuallySuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function(String errorMsg) failure,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(VerifyPhoneNumberManuallySuccess value) success,
    required TResult Function(VerifyPhoneNumberManuallyFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(VerifyPhoneNumberManuallySuccess value)? success,
    TResult Function(VerifyPhoneNumberManuallyFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(VerifyPhoneNumberManuallySuccess value)? success,
    TResult Function(VerifyPhoneNumberManuallyFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class VerifyPhoneNumberManuallySuccess
    implements VerifyPhoneNumberManuallyResult {
  factory VerifyPhoneNumberManuallySuccess() =
      _$VerifyPhoneNumberManuallySuccess;
}

/// @nodoc
abstract class _$$VerifyPhoneNumberManuallyFailureCopyWith<$Res> {
  factory _$$VerifyPhoneNumberManuallyFailureCopyWith(
          _$VerifyPhoneNumberManuallyFailure value,
          $Res Function(_$VerifyPhoneNumberManuallyFailure) then) =
      __$$VerifyPhoneNumberManuallyFailureCopyWithImpl<$Res>;
  $Res call({String errorMsg});
}

/// @nodoc
class __$$VerifyPhoneNumberManuallyFailureCopyWithImpl<$Res>
    extends _$VerifyPhoneNumberManuallyResultCopyWithImpl<$Res>
    implements _$$VerifyPhoneNumberManuallyFailureCopyWith<$Res> {
  __$$VerifyPhoneNumberManuallyFailureCopyWithImpl(
      _$VerifyPhoneNumberManuallyFailure _value,
      $Res Function(_$VerifyPhoneNumberManuallyFailure) _then)
      : super(_value, (v) => _then(v as _$VerifyPhoneNumberManuallyFailure));

  @override
  _$VerifyPhoneNumberManuallyFailure get _value =>
      super._value as _$VerifyPhoneNumberManuallyFailure;

  @override
  $Res call({
    Object? errorMsg = freezed,
  }) {
    return _then(_$VerifyPhoneNumberManuallyFailure(
      errorMsg: errorMsg == freezed
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$VerifyPhoneNumberManuallyFailure
    implements VerifyPhoneNumberManuallyFailure {
  _$VerifyPhoneNumberManuallyFailure({this.errorMsg = ''});

  @override
  @JsonKey()
  final String errorMsg;

  @override
  String toString() {
    return 'VerifyPhoneNumberManuallyResult.failure(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$VerifyPhoneNumberManuallyFailure &&
            const DeepCollectionEquality().equals(other.errorMsg, errorMsg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(errorMsg));

  @JsonKey(ignore: true)
  @override
  _$$VerifyPhoneNumberManuallyFailureCopyWith<
          _$VerifyPhoneNumberManuallyFailure>
      get copyWith => __$$VerifyPhoneNumberManuallyFailureCopyWithImpl<
          _$VerifyPhoneNumberManuallyFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function(String errorMsg) failure,
  }) {
    return failure(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
  }) {
    return failure?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(VerifyPhoneNumberManuallySuccess value) success,
    required TResult Function(VerifyPhoneNumberManuallyFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(VerifyPhoneNumberManuallySuccess value)? success,
    TResult Function(VerifyPhoneNumberManuallyFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(VerifyPhoneNumberManuallySuccess value)? success,
    TResult Function(VerifyPhoneNumberManuallyFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class VerifyPhoneNumberManuallyFailure
    implements VerifyPhoneNumberManuallyResult {
  factory VerifyPhoneNumberManuallyFailure({final String errorMsg}) =
      _$VerifyPhoneNumberManuallyFailure;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$VerifyPhoneNumberManuallyFailureCopyWith<
          _$VerifyPhoneNumberManuallyFailure>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$LinkWithCredentialResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function(String errorMsg) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkWithCredentialSuccess value) success,
    required TResult Function(LinkWithCredentialFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkWithCredentialSuccess value)? success,
    TResult Function(LinkWithCredentialFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkWithCredentialSuccess value)? success,
    TResult Function(LinkWithCredentialFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LinkWithCredentialResultCopyWith<$Res> {
  factory $LinkWithCredentialResultCopyWith(LinkWithCredentialResult value,
          $Res Function(LinkWithCredentialResult) then) =
      _$LinkWithCredentialResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$LinkWithCredentialResultCopyWithImpl<$Res>
    implements $LinkWithCredentialResultCopyWith<$Res> {
  _$LinkWithCredentialResultCopyWithImpl(this._value, this._then);

  final LinkWithCredentialResult _value;
  // ignore: unused_field
  final $Res Function(LinkWithCredentialResult) _then;
}

/// @nodoc
abstract class _$$LinkWithCredentialSuccessCopyWith<$Res> {
  factory _$$LinkWithCredentialSuccessCopyWith(
          _$LinkWithCredentialSuccess value,
          $Res Function(_$LinkWithCredentialSuccess) then) =
      __$$LinkWithCredentialSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LinkWithCredentialSuccessCopyWithImpl<$Res>
    extends _$LinkWithCredentialResultCopyWithImpl<$Res>
    implements _$$LinkWithCredentialSuccessCopyWith<$Res> {
  __$$LinkWithCredentialSuccessCopyWithImpl(_$LinkWithCredentialSuccess _value,
      $Res Function(_$LinkWithCredentialSuccess) _then)
      : super(_value, (v) => _then(v as _$LinkWithCredentialSuccess));

  @override
  _$LinkWithCredentialSuccess get _value =>
      super._value as _$LinkWithCredentialSuccess;
}

/// @nodoc

class _$LinkWithCredentialSuccess implements LinkWithCredentialSuccess {
  _$LinkWithCredentialSuccess();

  @override
  String toString() {
    return 'LinkWithCredentialResult.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LinkWithCredentialSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function(String errorMsg) failure,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkWithCredentialSuccess value) success,
    required TResult Function(LinkWithCredentialFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkWithCredentialSuccess value)? success,
    TResult Function(LinkWithCredentialFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkWithCredentialSuccess value)? success,
    TResult Function(LinkWithCredentialFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class LinkWithCredentialSuccess implements LinkWithCredentialResult {
  factory LinkWithCredentialSuccess() = _$LinkWithCredentialSuccess;
}

/// @nodoc
abstract class _$$LinkWithCredentialFailureCopyWith<$Res> {
  factory _$$LinkWithCredentialFailureCopyWith(
          _$LinkWithCredentialFailure value,
          $Res Function(_$LinkWithCredentialFailure) then) =
      __$$LinkWithCredentialFailureCopyWithImpl<$Res>;
  $Res call({String errorMsg});
}

/// @nodoc
class __$$LinkWithCredentialFailureCopyWithImpl<$Res>
    extends _$LinkWithCredentialResultCopyWithImpl<$Res>
    implements _$$LinkWithCredentialFailureCopyWith<$Res> {
  __$$LinkWithCredentialFailureCopyWithImpl(_$LinkWithCredentialFailure _value,
      $Res Function(_$LinkWithCredentialFailure) _then)
      : super(_value, (v) => _then(v as _$LinkWithCredentialFailure));

  @override
  _$LinkWithCredentialFailure get _value =>
      super._value as _$LinkWithCredentialFailure;

  @override
  $Res call({
    Object? errorMsg = freezed,
  }) {
    return _then(_$LinkWithCredentialFailure(
      errorMsg: errorMsg == freezed
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LinkWithCredentialFailure implements LinkWithCredentialFailure {
  _$LinkWithCredentialFailure({this.errorMsg = ''});

  @override
  @JsonKey()
  final String errorMsg;

  @override
  String toString() {
    return 'LinkWithCredentialResult.failure(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LinkWithCredentialFailure &&
            const DeepCollectionEquality().equals(other.errorMsg, errorMsg));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(errorMsg));

  @JsonKey(ignore: true)
  @override
  _$$LinkWithCredentialFailureCopyWith<_$LinkWithCredentialFailure>
      get copyWith => __$$LinkWithCredentialFailureCopyWithImpl<
          _$LinkWithCredentialFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function(String errorMsg) failure,
  }) {
    return failure(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
  }) {
    return failure?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function(String errorMsg)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LinkWithCredentialSuccess value) success,
    required TResult Function(LinkWithCredentialFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LinkWithCredentialSuccess value)? success,
    TResult Function(LinkWithCredentialFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LinkWithCredentialSuccess value)? success,
    TResult Function(LinkWithCredentialFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class LinkWithCredentialFailure implements LinkWithCredentialResult {
  factory LinkWithCredentialFailure({final String errorMsg}) =
      _$LinkWithCredentialFailure;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$LinkWithCredentialFailureCopyWith<_$LinkWithCredentialFailure>
      get copyWith => throw _privateConstructorUsedError;
}
