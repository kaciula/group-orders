import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orders/data/remote/connectors/connector_strings.dart';
import 'package:orders/model/user.dart';

part 'connector_results.freezed.dart';

@freezed
class SignInResult with _$SignInResult {
  factory SignInResult.success(User user) = SignInSuccess;

  factory SignInResult.failure(
          {@Default(SignInFailureType.other) SignInFailureType type}) =
      SignInFailure;
}

enum SignInFailureType {
  accountWithDifferentProvider,
  weakPassword,
  invalidEmail,
  emailAlreadyInUse,
  wrongPassword,
  userNotFound,
  canceled,
  other
}

String failureMsg(SignInFailureType type) {
  switch (type) {
    case SignInFailureType.accountWithDifferentProvider:
      return Strings.failureSignInAccountWithDifferentProvider;
    case SignInFailureType.weakPassword:
      return Strings.failureSignInWeakPassword;
    case SignInFailureType.invalidEmail:
      return Strings.failureSignInInvalidEmail;
    case SignInFailureType.emailAlreadyInUse:
      return Strings.failureSignInEmailAlreadyInUse;
    case SignInFailureType.wrongPassword:
      return Strings.failureSignInWrongPassword;
    case SignInFailureType.userNotFound:
      return Strings.failureSignInUserNotFound;
    case SignInFailureType.canceled:
      return Strings.failureSignInCanceled;
    case SignInFailureType.other:
      return Strings.failureSignInOther;
  }
}

@freezed
class SignOutResult with _$SignOutResult {
  factory SignOutResult.success() = SignOutSuccess;

  factory SignOutResult.failure() = SignOutFailure;
}
