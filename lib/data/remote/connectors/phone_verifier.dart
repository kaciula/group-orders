import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logging/logging.dart';
import 'package:orders/data/remote/connectors/connector_strings.dart';

part 'phone_verifier.freezed.dart';

class PhoneVerifier {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<void> verifyPhoneNumberAutomatically({
    required String phoneNumber,
    required VoidCallback successCallback,
    required VerifyFailureCallback failureCallback,
    required SmsCodeSentCallback smsCodeSentCallback,
  }) async {
    _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: const Duration(seconds: 30),
        verificationCompleted: (AuthCredential authCredential) async {
          final LinkWithCredentialResult result =
              await _linkWithCredential(authCredential);
          if (result is LinkWithCredentialSuccess) {
            successCallback();
          } else {
            final LinkWithCredentialFailure failure =
                result as LinkWithCredentialFailure;
            failureCallback(failure.errorMsg);
          }
        },
        verificationFailed: (FirebaseAuthException error) {
          _logger.fine('verificationFailed ${error.code} ${error.message}');
          failureCallback('');
        },
        codeSent: (String verificationId, [int? forceResendingToken]) {
          smsCodeSentCallback(verificationId);
        },
        codeAutoRetrievalTimeout: (String verificationId) {
          _logger.fine('codeAuthRetrievalTimeout');
        });
  }

  Future<VerifyPhoneNumberManuallyResult> verifyPhoneNumberManually(
      {required String verificationId, required String smsCode}) async {
    final AuthCredential authCredential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: smsCode);
    final LinkWithCredentialResult result =
        await _linkWithCredential(authCredential);
    if (result is LinkWithCredentialSuccess) {
      return VerifyPhoneNumberManuallySuccess();
    } else {
      final LinkWithCredentialFailure failure =
          result as LinkWithCredentialFailure;
      return VerifyPhoneNumberManuallyFailure(errorMsg: failure.errorMsg);
    }
  }

  Future<LinkWithCredentialResult> _linkWithCredential(
      AuthCredential authCredential) async {
    try {
      final User user = _firebaseAuth.currentUser!;
      final UserCredential userCredential =
          await user.linkWithCredential(authCredential);
      if (userCredential.user != null) {
        return LinkWithCredentialSuccess();
      }
    } catch (error) {
      _logger.warning(error);
      if (error is FirebaseAuthException) {
        String? errorMsg = error.message;
        if (error.code == 'invalid-verification-code') {
          errorMsg = Strings.failurePhoneVerifierWrongCode;
        }
        if (error.code == 'credential-already-in-use') {
          errorMsg = Strings.failurePhoneVerifierDifferentEmail;
        }
        return LinkWithCredentialFailure(
            errorMsg: errorMsg ?? Strings.failureSignInOther);
      }
    }
    return LinkWithCredentialFailure();
  }
}

typedef VerifyFailureCallback = void Function(String errorMsg);
typedef SmsCodeSentCallback = void Function(String verificationId);

@freezed
class VerifyPhoneNumberManuallyResult with _$VerifyPhoneNumberManuallyResult {
  factory VerifyPhoneNumberManuallyResult.success() =
      VerifyPhoneNumberManuallySuccess;

  factory VerifyPhoneNumberManuallyResult.failure(
      {@Default('') String errorMsg}) = VerifyPhoneNumberManuallyFailure;
}

@freezed
class LinkWithCredentialResult with _$LinkWithCredentialResult {
  factory LinkWithCredentialResult.success() = LinkWithCredentialSuccess;

  factory LinkWithCredentialResult.failure({@Default('') String errorMsg}) =
      LinkWithCredentialFailure;
}

// ignore: unused_element
final Logger _logger = Logger('PhoneVerifier');
