import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:logging/logging.dart';
import 'package:orders/data/remote/connectors/connector_results.dart';
import 'package:orders/model/user.dart';

class FirebaseConnector {
  final auth.FirebaseAuth _firebaseAuth = auth.FirebaseAuth.instance;

  Future<SignInResult> signIn(
      auth.AuthCredential credential, SignInProvider signInProvider,
      {String? displayName}) async {
    try {
      final auth.UserCredential userCredential =
          await _firebaseAuth.signInWithCredential(credential);
      final auth.User? firebaseUser = userCredential.user;
      if (firebaseUser != null) {
        final String theDisplayName = displayName != null
            ? displayName
            : (firebaseUser.displayName ?? '');
        return SignInSuccess(
          User.basic(
            id: firebaseUser.uid,
            displayName: theDisplayName,
            emailAddress: firebaseUser.email!,
            signInProvider: signInProvider,
          ),
        );
      }
    } catch (error) {
      _logger.warning(error);
      if (error is auth.FirebaseAuthException &&
          error.code == 'account-exists-with-different-credential') {
        return SignInFailure(
            type: SignInFailureType.accountWithDifferentProvider);
      }
    }
    return SignInFailure();
  }

  Future<SignOutResult> signOut() async {
    try {
      await _firebaseAuth.signOut();
      return SignOutSuccess();
    } catch (error) {
      _logger.warning(error);
      return SignOutFailure();
    }
  }
}

// ignore: unused_element
final Logger _logger = Logger('FirebaseConnector');
