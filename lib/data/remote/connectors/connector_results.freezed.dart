// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'connector_results.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SignInResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) success,
    required TResult Function(SignInFailureType type) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function(SignInFailureType type)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function(SignInFailureType type)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignInSuccess value) success,
    required TResult Function(SignInFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignInSuccess value)? success,
    TResult Function(SignInFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignInSuccess value)? success,
    TResult Function(SignInFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignInResultCopyWith<$Res> {
  factory $SignInResultCopyWith(
          SignInResult value, $Res Function(SignInResult) then) =
      _$SignInResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignInResultCopyWithImpl<$Res> implements $SignInResultCopyWith<$Res> {
  _$SignInResultCopyWithImpl(this._value, this._then);

  final SignInResult _value;
  // ignore: unused_field
  final $Res Function(SignInResult) _then;
}

/// @nodoc
abstract class _$$SignInSuccessCopyWith<$Res> {
  factory _$$SignInSuccessCopyWith(
          _$SignInSuccess value, $Res Function(_$SignInSuccess) then) =
      __$$SignInSuccessCopyWithImpl<$Res>;
  $Res call({User user});

  $UserCopyWith<$Res> get user;
}

/// @nodoc
class __$$SignInSuccessCopyWithImpl<$Res>
    extends _$SignInResultCopyWithImpl<$Res>
    implements _$$SignInSuccessCopyWith<$Res> {
  __$$SignInSuccessCopyWithImpl(
      _$SignInSuccess _value, $Res Function(_$SignInSuccess) _then)
      : super(_value, (v) => _then(v as _$SignInSuccess));

  @override
  _$SignInSuccess get _value => super._value as _$SignInSuccess;

  @override
  $Res call({
    Object? user = freezed,
  }) {
    return _then(_$SignInSuccess(
      user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc

class _$SignInSuccess implements SignInSuccess {
  _$SignInSuccess(this.user);

  @override
  final User user;

  @override
  String toString() {
    return 'SignInResult.success(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SignInSuccess &&
            const DeepCollectionEquality().equals(other.user, user));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(user));

  @JsonKey(ignore: true)
  @override
  _$$SignInSuccessCopyWith<_$SignInSuccess> get copyWith =>
      __$$SignInSuccessCopyWithImpl<_$SignInSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) success,
    required TResult Function(SignInFailureType type) failure,
  }) {
    return success(user);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function(SignInFailureType type)? failure,
  }) {
    return success?.call(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function(SignInFailureType type)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignInSuccess value) success,
    required TResult Function(SignInFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignInSuccess value)? success,
    TResult Function(SignInFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignInSuccess value)? success,
    TResult Function(SignInFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SignInSuccess implements SignInResult {
  factory SignInSuccess(final User user) = _$SignInSuccess;

  User get user;
  @JsonKey(ignore: true)
  _$$SignInSuccessCopyWith<_$SignInSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SignInFailureCopyWith<$Res> {
  factory _$$SignInFailureCopyWith(
          _$SignInFailure value, $Res Function(_$SignInFailure) then) =
      __$$SignInFailureCopyWithImpl<$Res>;
  $Res call({SignInFailureType type});
}

/// @nodoc
class __$$SignInFailureCopyWithImpl<$Res>
    extends _$SignInResultCopyWithImpl<$Res>
    implements _$$SignInFailureCopyWith<$Res> {
  __$$SignInFailureCopyWithImpl(
      _$SignInFailure _value, $Res Function(_$SignInFailure) _then)
      : super(_value, (v) => _then(v as _$SignInFailure));

  @override
  _$SignInFailure get _value => super._value as _$SignInFailure;

  @override
  $Res call({
    Object? type = freezed,
  }) {
    return _then(_$SignInFailure(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as SignInFailureType,
    ));
  }
}

/// @nodoc

class _$SignInFailure implements SignInFailure {
  _$SignInFailure({this.type = SignInFailureType.other});

  @override
  @JsonKey()
  final SignInFailureType type;

  @override
  String toString() {
    return 'SignInResult.failure(type: $type)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SignInFailure &&
            const DeepCollectionEquality().equals(other.type, type));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(type));

  @JsonKey(ignore: true)
  @override
  _$$SignInFailureCopyWith<_$SignInFailure> get copyWith =>
      __$$SignInFailureCopyWithImpl<_$SignInFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(User user) success,
    required TResult Function(SignInFailureType type) failure,
  }) {
    return failure(type);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function(SignInFailureType type)? failure,
  }) {
    return failure?.call(type);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(User user)? success,
    TResult Function(SignInFailureType type)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(type);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignInSuccess value) success,
    required TResult Function(SignInFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignInSuccess value)? success,
    TResult Function(SignInFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignInSuccess value)? success,
    TResult Function(SignInFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class SignInFailure implements SignInResult {
  factory SignInFailure({final SignInFailureType type}) = _$SignInFailure;

  SignInFailureType get type;
  @JsonKey(ignore: true)
  _$$SignInFailureCopyWith<_$SignInFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SignOutResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignOutSuccess value) success,
    required TResult Function(SignOutFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignOutSuccess value)? success,
    TResult Function(SignOutFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignOutSuccess value)? success,
    TResult Function(SignOutFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignOutResultCopyWith<$Res> {
  factory $SignOutResultCopyWith(
          SignOutResult value, $Res Function(SignOutResult) then) =
      _$SignOutResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignOutResultCopyWithImpl<$Res>
    implements $SignOutResultCopyWith<$Res> {
  _$SignOutResultCopyWithImpl(this._value, this._then);

  final SignOutResult _value;
  // ignore: unused_field
  final $Res Function(SignOutResult) _then;
}

/// @nodoc
abstract class _$$SignOutSuccessCopyWith<$Res> {
  factory _$$SignOutSuccessCopyWith(
          _$SignOutSuccess value, $Res Function(_$SignOutSuccess) then) =
      __$$SignOutSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SignOutSuccessCopyWithImpl<$Res>
    extends _$SignOutResultCopyWithImpl<$Res>
    implements _$$SignOutSuccessCopyWith<$Res> {
  __$$SignOutSuccessCopyWithImpl(
      _$SignOutSuccess _value, $Res Function(_$SignOutSuccess) _then)
      : super(_value, (v) => _then(v as _$SignOutSuccess));

  @override
  _$SignOutSuccess get _value => super._value as _$SignOutSuccess;
}

/// @nodoc

class _$SignOutSuccess implements SignOutSuccess {
  _$SignOutSuccess();

  @override
  String toString() {
    return 'SignOutResult.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SignOutSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignOutSuccess value) success,
    required TResult Function(SignOutFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignOutSuccess value)? success,
    TResult Function(SignOutFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignOutSuccess value)? success,
    TResult Function(SignOutFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SignOutSuccess implements SignOutResult {
  factory SignOutSuccess() = _$SignOutSuccess;
}

/// @nodoc
abstract class _$$SignOutFailureCopyWith<$Res> {
  factory _$$SignOutFailureCopyWith(
          _$SignOutFailure value, $Res Function(_$SignOutFailure) then) =
      __$$SignOutFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SignOutFailureCopyWithImpl<$Res>
    extends _$SignOutResultCopyWithImpl<$Res>
    implements _$$SignOutFailureCopyWith<$Res> {
  __$$SignOutFailureCopyWithImpl(
      _$SignOutFailure _value, $Res Function(_$SignOutFailure) _then)
      : super(_value, (v) => _then(v as _$SignOutFailure));

  @override
  _$SignOutFailure get _value => super._value as _$SignOutFailure;
}

/// @nodoc

class _$SignOutFailure implements SignOutFailure {
  _$SignOutFailure();

  @override
  String toString() {
    return 'SignOutResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SignOutFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignOutSuccess value) success,
    required TResult Function(SignOutFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SignOutSuccess value)? success,
    TResult Function(SignOutFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignOutSuccess value)? success,
    TResult Function(SignOutFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class SignOutFailure implements SignOutResult {
  factory SignOutFailure() = _$SignOutFailure;
}
