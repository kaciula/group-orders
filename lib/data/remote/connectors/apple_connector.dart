import 'package:firebase_auth/firebase_auth.dart';
import 'package:logging/logging.dart';
import 'package:orders/data/remote/connectors/connector_results.dart';
import 'package:orders/data/remote/connectors/firebase_connector.dart';
import 'package:orders/model/user.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class AppleConnector {
  AppleConnector(this.firebaseConnector);

  final FirebaseConnector firebaseConnector;

  Future<SignInResult> signIn() async {
    try {
      final AuthorizationCredentialAppleID result =
          await SignInWithApple.getAppleIDCredential(
        scopes: <AppleIDAuthorizationScopes>[
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      );
      final OAuthProvider oAuthProvider = OAuthProvider('apple.com');
      final AuthCredential credential = oAuthProvider.credential(
        idToken: result.identityToken,
        accessToken: result.authorizationCode,
      );
      String displayName;
      if (result.givenName != null && result.familyName != null) {
        displayName = '${result.givenName} ${result.familyName}';
      } else if (result.givenName != null) {
        displayName = '${result.givenName}';
      } else if (result.familyName != null) {
        displayName = '${result.familyName}';
      } else {
        displayName = '';
      }
      final SignInResult signInResult = await firebaseConnector.signIn(
        credential,
        SignInProvider.apple,
        displayName: displayName,
      );
      return signInResult;
    } catch (error) {
      _logger.warning(error);
      if (error is SignInWithAppleAuthorizationException &&
          error.code == AuthorizationErrorCode.canceled) {
        return SignInFailure(type: SignInFailureType.canceled);
      }
    }
    return SignInFailure();
  }

  Future<void> signOut() async {
    await firebaseConnector.signOut();
  }

  Future<bool> isSupported() async => SignInWithApple.isAvailable();
}

// ignore: unused_element
final Logger _logger = Logger('AppleConnector');
