import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:logging/logging.dart';
import 'package:orders/data/remote/connectors/connector_results.dart';
import 'package:orders/data/remote/connectors/firebase_connector.dart';
import 'package:orders/model/user.dart';

class FacebookConnector {
  FacebookConnector(this.firebaseConnector);

  final FirebaseConnector firebaseConnector;

  Future<SignInResult> signIn(BuildContext context) async {
    try {
      await signOut();

      final LoginResult result = await FacebookAuth.instance.login(
        permissions: const <String>['email', 'public_profile'],
        loginBehavior: LoginBehavior.dialogOnly,
      );
      if (result.status == LoginStatus.success) {
        final AccessToken accessToken = result.accessToken!;
        final AuthCredential credential =
            FacebookAuthProvider.credential(accessToken.token);
        final SignInResult signInResult =
            await firebaseConnector.signIn(credential, SignInProvider.facebook);
        return signInResult;
      } else {
        _logger.warning('facebook login status: failure');
      }
    } catch (error) {
      _logger.warning(error);
    }
    return SignInFailure();
  }

  Future<void> signOut() async {
    try {
      await firebaseConnector.signOut();
    } catch (e) {
      // Do nothing as the user may be already signed out
    }
  }
}

// ignore: unused_element
final Logger _logger = Logger('FacebookConnector');
