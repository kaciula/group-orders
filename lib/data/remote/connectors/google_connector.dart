import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logging/logging.dart';
import 'package:orders/data/remote/connectors/connector_results.dart';
import 'package:orders/data/remote/connectors/firebase_connector.dart';
import 'package:orders/model/user.dart';

class GoogleConnector {
  GoogleConnector(this.firebaseConnector);

  final GoogleSignIn _google =
      GoogleSignIn(scopes: <String>['email', 'profile']);
  final FirebaseConnector firebaseConnector;

  Future<SignInResult> signIn() async {
    try {
      await signOut();

      final GoogleSignInAccount? googleUser = await _google.signIn();
      if (googleUser != null) {
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;
        final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        final SignInResult signInResult =
            await firebaseConnector.signIn(credential, SignInProvider.google);
        return signInResult;
      } else {
        return SignInFailure(type: SignInFailureType.canceled);
      }
    } catch (error) {
      _logger.warning(error);
    }
    return SignInFailure();
  }

  Future<void> signOut() async {
    try {
      await _google.disconnect();
      await firebaseConnector.signOut();
    } catch (error) {
      _logger.warning(error);
    }
  }
}

// ignore: unused_element
final Logger _logger = Logger('GoogleConnector');
