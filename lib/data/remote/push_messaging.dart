import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/blocs/data_bloc/pending_push_notification.dart';
import 'package:orders/data/local/store/user_provider.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/user.dart';

class PushMessaging {
  PushMessaging(this.userProvider, this.dataRemoteStore, this.dataBloc);

  final FirebaseMessaging _messaging = FirebaseMessaging.instance;
  final UserProvider userProvider;
  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<void> init() async {
    // LATER: Check to see if permissions really granted or not
    await requestPermission();

    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) {
        final Map<String, dynamic> data = message.data;
        _logger.fine('data: $data');
        // LATER: message received while app is open
      },
    );
    FirebaseMessaging.onMessageOpenedApp.listen(
      (RemoteMessage message) {
        final Map<String, dynamic> data = message.data;
        final String orderId = data['orderId'];
        dataBloc.add(PendingPushNotificationReceived(
          pendingPushNotification: PendingPushNotification(orderId: orderId),
        ));
      },
    );

    _messaging.onTokenRefresh.listen((String token) async {
      _logger.fine('Token refresh: $token');
      final User user = userProvider.getUser() ?? User.anonymous();
      if (user.isSignedIn && token != user.messagingToken) {
        final StoreResult saveUserResult = await dataRemoteStore
            .updateUserFields(user.id, messagingToken: token);
        if (saveUserResult is StoreSuccess) {
          _logger.fine('Successfully updated user token');
          userProvider.persistUser(user.copyWith(messagingToken: token));
        } else {
          _logger.fine('Failure updating user token');
        }
      }
    });
  }

  Future<bool> requestPermission() async {
    try {
      final NotificationSettings notificationSettings = await _messaging
          .requestPermission(sound: true, badge: true, alert: true);
      return notificationSettings.authorizationStatus ==
          AuthorizationStatus.authorized;
    } catch (error) {
      _logger.warning(error);
      return false;
    }
  }

  Future<String?> getToken() async {
    return await _messaging.getToken();
  }
}

// ignore: unused_element
final Logger _logger = Logger('PushMessaging');
