import 'package:shared_preferences/shared_preferences.dart';

class AppInfoProvider {
  late SharedPreferences prefs;

  Future<void> init() async {
    prefs = await SharedPreferences.getInstance();
  }

  AppInfo get() {
    final bool isFirstTime = prefs.getBool(_KEY_FIRST_TIME) ?? true;
    final int versionCode = prefs.getInt(_KEY_VERSION_CODE) ?? 1;
    final int timestampLastCrash = prefs.getInt(_KEY_TIMESTAMP_LAST_CRASH) ?? 0;
    final int timestampLastSyncRemote =
        prefs.getInt(_KEY_TIMESTAMP_LAST_SYNC_REMOTE) ?? 0;
    final bool migrateMessagingToken =
        prefs.getBool(_KEY_MIGRATE_MESSAGING_TOKEN) ?? false;
    final bool refreshUser = prefs.getBool(_KEY_REFRESH_USER) ?? false;
    return AppInfo(isFirstTime, versionCode, timestampLastCrash,
        timestampLastSyncRemote, migrateMessagingToken, refreshUser);
  }

  Future<void> save(
      {required bool isFirstTime, required int versionCode}) async {
    await prefs.setBool(_KEY_FIRST_TIME, isFirstTime);
    await prefs.setInt(_KEY_VERSION_CODE, versionCode);
  }

  Future<void> saveTimestampLastCrash(int timestamp) async {
    await prefs.setInt(_KEY_TIMESTAMP_LAST_CRASH, timestamp);
  }

  Future<void> saveTimestampLastSyncRemote(int timestamp) async {
    await prefs.setInt(_KEY_TIMESTAMP_LAST_SYNC_REMOTE, timestamp);
  }

  Future<void> saveMigrateMessagingToken(bool migrateMessagingToken) async {
    await prefs.setBool(_KEY_MIGRATE_MESSAGING_TOKEN, migrateMessagingToken);
  }

  Future<void> saveRefreshUser(bool refreshUser) async {
    await prefs.setBool(_KEY_REFRESH_USER, refreshUser);
  }
}

const String _KEY_FIRST_TIME = 'isFirstTime';
const String _KEY_VERSION_CODE = 'versionCode';
const String _KEY_TIMESTAMP_LAST_CRASH = 'timestampLastCrash';
const String _KEY_TIMESTAMP_LAST_SYNC_REMOTE = 'timestampLastSyncRemote';
const String _KEY_MIGRATE_MESSAGING_TOKEN = 'migrateMessagingToken';
const String _KEY_REFRESH_USER = 'refreshUser';

class AppInfo {
  AppInfo(
      this.isFirstTime,
      this.versionCode,
      this.timestampLastCrash,
      this.timestampLastSyncRemote,
      this.migrateMessagingToken,
      this.refreshUser);

  final bool isFirstTime;
  final int versionCode;
  final int timestampLastCrash;
  final int timestampLastSyncRemote;
  final bool migrateMessagingToken;
  final bool refreshUser;
}
