import 'package:logging/logging.dart';
import 'package:orders/data/local/store/preferences.dart';
import 'package:orders/model/user.dart';

class UserProvider {
  UserProvider(this.preferences);

  final Preferences preferences;

  Future<void> signedIn({required User user}) async {
    await persistUser(user);
  }

  Future<void> signedOut() async {
    await _deleteUser();
  }

  User? getUser() => preferences.getUser();

  Future<void> persistUser(User user) async => preferences.setUser(user);

  Future<void> _deleteUser() async => await preferences.deleteUser();
}

// ignore: unused_element
final Logger _logger = Logger('UserProvider');
