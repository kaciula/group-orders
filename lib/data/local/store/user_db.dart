import 'package:hive/hive.dart';
import 'package:orders/model/user.dart';

part 'user_db.g.dart';

@HiveType(typeId: 2)
class UserDb {
  UserDb(
    this.id,
    this.displayName,
    this.emailAddress,
    this.signInProvider,
    this.messagingToken,
    this.phoneNumber,
  );

  factory UserDb.fromUser(User user) {
    return UserDb(
      user.id,
      user.displayName,
      user.emailAddress,
      _mapSignInProvider(user.signInProvider),
      user.messagingToken,
      user.phoneNumber,
    );
  }

  @HiveField(0)
  final String id;
  @HiveField(1)
  final String displayName;
  @HiveField(2)
  final String emailAddress;
  @HiveField(3)
  final String signInProvider;
  @HiveField(4)
  final String? messagingToken;
  @HiveField(5)
  final String? phoneNumber;

  User map() {
    return User(
      id: id,
      displayName: displayName,
      emailAddress: emailAddress,
      signInProvider: _mapSignInProviderValue(signInProvider),
      messagingToken: messagingToken ?? '',
      languageCode: '',
      phoneNumber: phoneNumber ?? '', // for backwards compatibility
    );
  }

  static String _mapSignInProvider(SignInProvider signInProvider) {
    switch (signInProvider) {
      case SignInProvider.apple:
        return 'apple';
      case SignInProvider.google:
        return 'google';
      case SignInProvider.facebook:
        return 'facebook';
      case SignInProvider.email:
        return 'email';
    }
  }

  static SignInProvider _mapSignInProviderValue(String value) {
    if (value == 'apple') {
      return SignInProvider.apple;
    } else if (value == 'google') {
      return SignInProvider.google;
    } else if (value == 'facebook') {
      return SignInProvider.facebook;
    } else if (value == 'email') {
      return SignInProvider.email;
    } else {
      throw Exception('Unknown sign in provider $value');
    }
  }
}
