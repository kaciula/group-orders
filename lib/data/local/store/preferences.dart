import 'package:hive/hive.dart';
import 'package:orders/data/local/store/user_db.dart';
import 'package:orders/model/user.dart';

class Preferences {
  static const String _preferencesBoxName = 'preferences';
  late Box<dynamic> _box;

  Future<void> init() async {
    Hive.registerAdapter(UserDbAdapter());
    _box = await Hive.openBox<dynamic>(_preferencesBoxName);
  }

  User? getUser() => _getValue<UserDb>(_KEY_USER)?.map();

  Future<void> setUser(User user) =>
      _setValue(_KEY_USER, UserDb.fromUser(user));

  Future<void> deleteUser() => _box.delete(_KEY_USER);

  T? _getValue<T>(dynamic key, {T? defaultValue}) =>
      _box.get(key, defaultValue: defaultValue);

  Future<void> _setValue<T>(dynamic key, T value) => _box.put(key, value);
}

const String _KEY_USER = 'key.user';
