import 'package:device_info_plus/device_info_plus.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logging/logging.dart';
import 'package:universal_io/io.dart';

part 'device_info_provider.freezed.dart';

class DeviceInfoProvider {
  final DeviceInfoPlugin plugin = DeviceInfoPlugin();

  Future<DeviceInfo> readDeviceInfo() async {
    if (Platform.isAndroid) {
      final AndroidDeviceInfo androidInfo = await plugin.androidInfo;
      return DeviceInfo(
        brand: androidInfo.brand ?? '',
        model: androidInfo.model ?? '',
        version: androidInfo.version.sdkInt.toString(),
      );
    } else {
      final IosDeviceInfo iosInfo = await plugin.iosInfo;
      return DeviceInfo(
        brand: 'Apple',
        model: iosInfo.utsname.machine ?? '',
        version: iosInfo.systemVersion ?? '',
      );
    }
  }
}

@freezed
class DeviceInfo with _$DeviceInfo {
  factory DeviceInfo({
    required String brand,
    required String model,
    required String version,
  }) = _DeviceInfo;
}

// ignore: unused_element
final Logger _logger = Logger('DeviceInfoProvider');
