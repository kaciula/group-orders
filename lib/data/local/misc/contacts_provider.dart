import 'package:contacts_service/contacts_service.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:logging/logging.dart';

part 'contacts_provider.freezed.dart';

class ContactsProvider {
  Future<KtList<DeviceContact>> getAllContacts() async {
    final Iterable<Contact> contacts =
        await ContactsService.getContacts(withThumbnails: false);
    return KtList<DeviceContact>.from(
      contacts.map(
        (Contact contact) => DeviceContact(
          displayName: contact.displayName ?? '',
          mainPhone: (contact.phones?.isNotEmpty ?? false)
              ? contact.phones!.first.value
              : null,
          mainEmail: (contact.emails?.isNotEmpty ?? false)
              ? contact.emails!.first.value
              : null,
        ),
      ),
    );
  }
}

@freezed
class DeviceContact with _$DeviceContact {
  factory DeviceContact({
    required String displayName,
    String? mainPhone,
    String? mainEmail,
  }) = _DeviceContact;
}

// ignore: unused_element
final Logger _logger = Logger('ContactsProvider');
