import 'package:logging/logging.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionHandler {
  Future<bool> hasContactsPermission() async {
    return await Permission.contacts.request().isGranted;
  }

  Future<bool> isContactsPermissionPermanentlyDenied() async {
    return await Permission.contacts.isPermanentlyDenied;
  }
}

// ignore: unused_element
final Logger _logger = Logger('PermissionHandler');
