import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/utils/app_constants.dart';

class DynamicLinks {
  DynamicLinks(this.dataBloc);

  final DataBloc dataBloc;

  bool processing = false;

  Future<String> generateLink({required String orderId}) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: DYNAMIC_LINK_PREFIX,
      link: Uri.parse(
          'https://grouporders.forgetfulsoulapps.com?$_queryParamOrderId=$orderId'),
      androidParameters: AndroidParameters(
        packageName: isProduction
            ? 'com.forgetfulsoul.group_orders'
            : 'com.forgetfulsoul.group_orders.debug',
        minimumVersion: 1,
      ),
      iosParameters: IOSParameters(
        bundleId: 'com.forgetfulsoul.groupOrders',
        minimumVersion: '1.0',
        appStoreId: '1523682397',
      ),
    );

    final ShortDynamicLink shortLink =
        await FirebaseDynamicLinks.instance.buildShortLink(parameters);
    return shortLink.shortUrl.toString();
  }

  Future<void> init() async {
    final PendingDynamicLinkData? data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri? deepLink = data?.link;

    if (deepLink != null) {
      _logger.fine('Got an initial link. Path=$deepLink');
      await handleLink(deepLink);
    }

    FirebaseDynamicLinks.instance.onLink.listen(
      (PendingDynamicLinkData dynamicLink) async {
        final Uri? deepLink = dynamicLink.link;
        if (deepLink != null) {
          // The method is called twice so we need to have a safeguard here
          if (!processing) {
            processing = true;
            _logger.fine('Listener triggered. Path=$deepLink');
            await handleLink(deepLink);
            processing = false;
          }
        }
      },
      onError: (Object error) async {
        _logger.fine('onLinkError $error');
      },
    );
  }

  Future<void> handleLink(Uri deepLink) async {
    final String orderId = deepLink.queryParameters[_queryParamOrderId]!;
    dataBloc.add(InvitationReceived(orderId: orderId));
  }
}

const String _queryParamOrderId = 'orderId';

// ignore: unused_element
final Logger _logger = Logger('DynamicLinks');
