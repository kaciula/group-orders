// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'contacts_provider.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DeviceContact {
  String get displayName => throw _privateConstructorUsedError;
  String? get mainPhone => throw _privateConstructorUsedError;
  String? get mainEmail => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DeviceContactCopyWith<DeviceContact> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeviceContactCopyWith<$Res> {
  factory $DeviceContactCopyWith(
          DeviceContact value, $Res Function(DeviceContact) then) =
      _$DeviceContactCopyWithImpl<$Res>;
  $Res call({String displayName, String? mainPhone, String? mainEmail});
}

/// @nodoc
class _$DeviceContactCopyWithImpl<$Res>
    implements $DeviceContactCopyWith<$Res> {
  _$DeviceContactCopyWithImpl(this._value, this._then);

  final DeviceContact _value;
  // ignore: unused_field
  final $Res Function(DeviceContact) _then;

  @override
  $Res call({
    Object? displayName = freezed,
    Object? mainPhone = freezed,
    Object? mainEmail = freezed,
  }) {
    return _then(_value.copyWith(
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      mainPhone: mainPhone == freezed
          ? _value.mainPhone
          : mainPhone // ignore: cast_nullable_to_non_nullable
              as String?,
      mainEmail: mainEmail == freezed
          ? _value.mainEmail
          : mainEmail // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_DeviceContactCopyWith<$Res>
    implements $DeviceContactCopyWith<$Res> {
  factory _$$_DeviceContactCopyWith(
          _$_DeviceContact value, $Res Function(_$_DeviceContact) then) =
      __$$_DeviceContactCopyWithImpl<$Res>;
  @override
  $Res call({String displayName, String? mainPhone, String? mainEmail});
}

/// @nodoc
class __$$_DeviceContactCopyWithImpl<$Res>
    extends _$DeviceContactCopyWithImpl<$Res>
    implements _$$_DeviceContactCopyWith<$Res> {
  __$$_DeviceContactCopyWithImpl(
      _$_DeviceContact _value, $Res Function(_$_DeviceContact) _then)
      : super(_value, (v) => _then(v as _$_DeviceContact));

  @override
  _$_DeviceContact get _value => super._value as _$_DeviceContact;

  @override
  $Res call({
    Object? displayName = freezed,
    Object? mainPhone = freezed,
    Object? mainEmail = freezed,
  }) {
    return _then(_$_DeviceContact(
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      mainPhone: mainPhone == freezed
          ? _value.mainPhone
          : mainPhone // ignore: cast_nullable_to_non_nullable
              as String?,
      mainEmail: mainEmail == freezed
          ? _value.mainEmail
          : mainEmail // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_DeviceContact implements _DeviceContact {
  _$_DeviceContact({required this.displayName, this.mainPhone, this.mainEmail});

  @override
  final String displayName;
  @override
  final String? mainPhone;
  @override
  final String? mainEmail;

  @override
  String toString() {
    return 'DeviceContact(displayName: $displayName, mainPhone: $mainPhone, mainEmail: $mainEmail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DeviceContact &&
            const DeepCollectionEquality()
                .equals(other.displayName, displayName) &&
            const DeepCollectionEquality().equals(other.mainPhone, mainPhone) &&
            const DeepCollectionEquality().equals(other.mainEmail, mainEmail));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(displayName),
      const DeepCollectionEquality().hash(mainPhone),
      const DeepCollectionEquality().hash(mainEmail));

  @JsonKey(ignore: true)
  @override
  _$$_DeviceContactCopyWith<_$_DeviceContact> get copyWith =>
      __$$_DeviceContactCopyWithImpl<_$_DeviceContact>(this, _$identity);
}

abstract class _DeviceContact implements DeviceContact {
  factory _DeviceContact(
      {required final String displayName,
      final String? mainPhone,
      final String? mainEmail}) = _$_DeviceContact;

  @override
  String get displayName;
  @override
  String? get mainPhone;
  @override
  String? get mainEmail;
  @override
  @JsonKey(ignore: true)
  _$$_DeviceContactCopyWith<_$_DeviceContact> get copyWith =>
      throw _privateConstructorUsedError;
}
