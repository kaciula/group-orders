import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:orders/app/app_strings.dart';
import 'package:orders/data/local/misc/device_info_provider.dart';
import 'package:orders/data/local/misc/launcher.dart';
import 'package:orders/data/remote/uncaught_error_handler/uncaught_error_handler.dart';
import 'package:orders/service_locator.dart';
import 'package:orders/utils/app_constants.dart';
import 'package:path_provider/path_provider.dart';
import 'package:universal_io/io.dart';

class LogHandler {
  LogHandler(this.launcher);

  final Launcher launcher;

  final List<String> memoryLogs = <String>[];
  bool _keepLogsInMemory = false;

  bool get trackLogsInMemory => _keepLogsInMemory;

  set trackLogsInMemory(bool value) {
    _keepLogsInMemory = value;
    _setLoggerLevel();
    memoryLogs.clear();
  }

  Future<void> init() async {
    _setLoggerLevel();
    Logger.root.onRecord.listen((LogRecord rec) {
      final String msg =
          '${rec.time.hour}:${rec.time.minute}:${rec.time.second}.${rec.time.millisecond} ${rec.loggerName}: ${rec.message}';
      if (isProduction) {
        FirebaseCrashlytics.instance.log(msg);
      } else {
        print(msg);
      }
      if (_keepLogsInMemory) {
        memoryLogs.add(msg);
      }
    });

    Bloc.observer = _LoggingBlocObserver();
  }

  void _setLoggerLevel() {
    Logger.root.level =
        (isProduction && !_keepLogsInMemory) ? Level.INFO : Level.ALL;
  }

  Future<void> sendLogsInMemory(
      {DeviceInfo? deviceInfo, String? versionName}) async {
    final String path = (await getTemporaryDirectory()).path +
        '/logs-${DateTime.now().toString()}.log';
    final File file = File(path);
    String logs = '';
    if (deviceInfo != null) {
      logs +=
          'DEVICE INFO: ${deviceInfo.brand} ${deviceInfo.model} API: ${deviceInfo.version} App version: $versionName\n\n';
    }
    for (String log in memoryLogs) {
      logs += '$log\n';
    }
    file.writeAsStringSync(logs);

    await launcher.sendEmailWithAttachment(
      emailAddress: 'catalin.morosan@gmail.com',
      subject: '${Strings.appName} Logs',
      body: 'I\'ve attached the logs.',
      attachmentPath: path,
    );
  }
}

class _LoggingBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc<dynamic, dynamic> bloc, Object? event) {
    super.onEvent(bloc, event);
    Logger(bloc.runtimeType.toString()).info(event);
  }

  @override
  void onError(BlocBase<dynamic> bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    if (isProduction) {
      super.onError(bloc, error, stackTrace);
      if (isProduction) {
        final UncaughtErrorHandler handler = getIt<UncaughtErrorHandler>();
        handler.reportUnexpectedError(
          Exception(error.toString()),
          stackTrace,
          '${bloc.runtimeType.toString()} exploded!',
          restartApp: true,
        );
      } else {
        Logger(bloc.runtimeType.toString()).info('BOOOOOOOOOM! $error');
      }
    } else {
      Logger(bloc.runtimeType.toString()).severe('BOOOOOOOOOM! $error');
    }
  }
}

// ignore: unused_element
final Logger _logger = Logger('LogHandler');
