import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logging/logging.dart';
import 'package:share_plus/share_plus.dart';
import 'package:universal_io/io.dart';
import 'package:url_launcher/url_launcher.dart';

part 'launcher.freezed.dart';

class Launcher {
  Future<ShareTextResult> shareText(
      {required String text, required String subject}) async {
    try {
      await Share.share(text, subject: subject);
      return ShareTextSuccess();
    } catch (error) {
      _logger.warning(error);
      return ShareTextFailure();
    }
  }

  Future<ShareTextResult> shareFile(File file, {String? subject}) async {
    try {
      await Share.shareFiles(<String>[file.path], subject: subject);
      return ShareTextSuccess();
    } catch (error) {
      _logger.warning(error);
      return ShareTextFailure();
    }
  }

  Future<void> openExternalUrl(String url, {bool forceWebView = false}) async {
    final Uri uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(
        uri,
        mode: forceWebView
            ? LaunchMode.inAppWebView
            : LaunchMode.externalApplication,
      );
    }
  }

  Future<void> sendEmailTo({
    required String emailAddress,
    String? subject,
  }) async {
    final Uri uri = Uri(
      scheme: 'mailto',
      path: emailAddress,
      queryParameters: <String, dynamic>{'subject': subject},
    );
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    }
  }

  Future<void> sendEmailWithAttachment({
    required String emailAddress,
    required String subject,
    required String body,
    required String attachmentPath,
  }) async {
    final Email email = Email(
      subject: subject,
      body: body,
      recipients: <String>[emailAddress],
      attachmentPaths: <String>[attachmentPath],
      isHTML: false,
    );
    await FlutterEmailSender.send(email);
  }

  Future<void> dial({required String phoneNum}) async {
    final Uri uri = Uri(scheme: 'tel', path: phoneNum);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    }
  }

  Future<void> launchWhatsApp({required String phoneNumber}) {
    final String url = 'https://wa.me/$phoneNumber';
    return openExternalUrl(url);
  }
}

@freezed
class ShareTextResult with _$ShareTextResult {
  factory ShareTextResult.success() = ShareTextSuccess;

  factory ShareTextResult.failure() = ShareTextFailure;
}

// ignore: unused_element
final Logger _logger = Logger('Launcher');
