import 'package:flutter/services.dart';
import 'package:orders/data/local/misc/log_handler.dart';
import 'package:orders/service_locator.dart';

Future<void> init() async {
  await SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
  ]);

  final LogHandler logHandler = getIt<LogHandler>();
  logHandler.init();
}
