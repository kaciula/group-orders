import 'package:flutter/material.dart';

// ignore: avoid_classes_with_only_static_members
class MiscUtils {
  static DateTime mergeDateTime(DateTime date, TimeOfDay time) {
    return DateTime(date.year, date.month, date.day, time.hour, time.minute);
  }
}
