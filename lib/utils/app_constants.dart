import 'package:flutter/foundation.dart';

const bool isProduction = kReleaseMode;
const String DYNAMIC_LINK_PREFIX =
    'https://grouporders.forgetfulsoulapps.com/invite';
