// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'data_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DataState {
  String get versionName => throw _privateConstructorUsedError;
  String get packageName => throw _privateConstructorUsedError;
  User get user => throw _privateConstructorUsedError;
  KtSet<Order> get orders => throw _privateConstructorUsedError;
  bool get isInternetConnected => throw _privateConstructorUsedError;
  String? get pendingInviteOrderId => throw _privateConstructorUsedError;
  PendingPushNotification? get pendingPushNotification =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DataStateCopyWith<DataState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DataStateCopyWith<$Res> {
  factory $DataStateCopyWith(DataState value, $Res Function(DataState) then) =
      _$DataStateCopyWithImpl<$Res>;
  $Res call(
      {String versionName,
      String packageName,
      User user,
      KtSet<Order> orders,
      bool isInternetConnected,
      String? pendingInviteOrderId,
      PendingPushNotification? pendingPushNotification});

  $UserCopyWith<$Res> get user;
  $PendingPushNotificationCopyWith<$Res>? get pendingPushNotification;
}

/// @nodoc
class _$DataStateCopyWithImpl<$Res> implements $DataStateCopyWith<$Res> {
  _$DataStateCopyWithImpl(this._value, this._then);

  final DataState _value;
  // ignore: unused_field
  final $Res Function(DataState) _then;

  @override
  $Res call({
    Object? versionName = freezed,
    Object? packageName = freezed,
    Object? user = freezed,
    Object? orders = freezed,
    Object? isInternetConnected = freezed,
    Object? pendingInviteOrderId = freezed,
    Object? pendingPushNotification = freezed,
  }) {
    return _then(_value.copyWith(
      versionName: versionName == freezed
          ? _value.versionName
          : versionName // ignore: cast_nullable_to_non_nullable
              as String,
      packageName: packageName == freezed
          ? _value.packageName
          : packageName // ignore: cast_nullable_to_non_nullable
              as String,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      orders: orders == freezed
          ? _value.orders
          : orders // ignore: cast_nullable_to_non_nullable
              as KtSet<Order>,
      isInternetConnected: isInternetConnected == freezed
          ? _value.isInternetConnected
          : isInternetConnected // ignore: cast_nullable_to_non_nullable
              as bool,
      pendingInviteOrderId: pendingInviteOrderId == freezed
          ? _value.pendingInviteOrderId
          : pendingInviteOrderId // ignore: cast_nullable_to_non_nullable
              as String?,
      pendingPushNotification: pendingPushNotification == freezed
          ? _value.pendingPushNotification
          : pendingPushNotification // ignore: cast_nullable_to_non_nullable
              as PendingPushNotification?,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }

  @override
  $PendingPushNotificationCopyWith<$Res>? get pendingPushNotification {
    if (_value.pendingPushNotification == null) {
      return null;
    }

    return $PendingPushNotificationCopyWith<$Res>(
        _value.pendingPushNotification!, (value) {
      return _then(_value.copyWith(pendingPushNotification: value));
    });
  }
}

/// @nodoc
abstract class _$$_DataStateCopyWith<$Res> implements $DataStateCopyWith<$Res> {
  factory _$$_DataStateCopyWith(
          _$_DataState value, $Res Function(_$_DataState) then) =
      __$$_DataStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {String versionName,
      String packageName,
      User user,
      KtSet<Order> orders,
      bool isInternetConnected,
      String? pendingInviteOrderId,
      PendingPushNotification? pendingPushNotification});

  @override
  $UserCopyWith<$Res> get user;
  @override
  $PendingPushNotificationCopyWith<$Res>? get pendingPushNotification;
}

/// @nodoc
class __$$_DataStateCopyWithImpl<$Res> extends _$DataStateCopyWithImpl<$Res>
    implements _$$_DataStateCopyWith<$Res> {
  __$$_DataStateCopyWithImpl(
      _$_DataState _value, $Res Function(_$_DataState) _then)
      : super(_value, (v) => _then(v as _$_DataState));

  @override
  _$_DataState get _value => super._value as _$_DataState;

  @override
  $Res call({
    Object? versionName = freezed,
    Object? packageName = freezed,
    Object? user = freezed,
    Object? orders = freezed,
    Object? isInternetConnected = freezed,
    Object? pendingInviteOrderId = freezed,
    Object? pendingPushNotification = freezed,
  }) {
    return _then(_$_DataState(
      versionName: versionName == freezed
          ? _value.versionName
          : versionName // ignore: cast_nullable_to_non_nullable
              as String,
      packageName: packageName == freezed
          ? _value.packageName
          : packageName // ignore: cast_nullable_to_non_nullable
              as String,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      orders: orders == freezed
          ? _value.orders
          : orders // ignore: cast_nullable_to_non_nullable
              as KtSet<Order>,
      isInternetConnected: isInternetConnected == freezed
          ? _value.isInternetConnected
          : isInternetConnected // ignore: cast_nullable_to_non_nullable
              as bool,
      pendingInviteOrderId: pendingInviteOrderId == freezed
          ? _value.pendingInviteOrderId
          : pendingInviteOrderId // ignore: cast_nullable_to_non_nullable
              as String?,
      pendingPushNotification: pendingPushNotification == freezed
          ? _value.pendingPushNotification
          : pendingPushNotification // ignore: cast_nullable_to_non_nullable
              as PendingPushNotification?,
    ));
  }
}

/// @nodoc

class _$_DataState implements _DataState {
  _$_DataState(
      {required this.versionName,
      required this.packageName,
      required this.user,
      required this.orders,
      required this.isInternetConnected,
      this.pendingInviteOrderId,
      this.pendingPushNotification});

  @override
  final String versionName;
  @override
  final String packageName;
  @override
  final User user;
  @override
  final KtSet<Order> orders;
  @override
  final bool isInternetConnected;
  @override
  final String? pendingInviteOrderId;
  @override
  final PendingPushNotification? pendingPushNotification;

  @override
  String toString() {
    return 'DataState(versionName: $versionName, packageName: $packageName, user: $user, orders: $orders, isInternetConnected: $isInternetConnected, pendingInviteOrderId: $pendingInviteOrderId, pendingPushNotification: $pendingPushNotification)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DataState &&
            const DeepCollectionEquality()
                .equals(other.versionName, versionName) &&
            const DeepCollectionEquality()
                .equals(other.packageName, packageName) &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality().equals(other.orders, orders) &&
            const DeepCollectionEquality()
                .equals(other.isInternetConnected, isInternetConnected) &&
            const DeepCollectionEquality()
                .equals(other.pendingInviteOrderId, pendingInviteOrderId) &&
            const DeepCollectionEquality().equals(
                other.pendingPushNotification, pendingPushNotification));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(versionName),
      const DeepCollectionEquality().hash(packageName),
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(orders),
      const DeepCollectionEquality().hash(isInternetConnected),
      const DeepCollectionEquality().hash(pendingInviteOrderId),
      const DeepCollectionEquality().hash(pendingPushNotification));

  @JsonKey(ignore: true)
  @override
  _$$_DataStateCopyWith<_$_DataState> get copyWith =>
      __$$_DataStateCopyWithImpl<_$_DataState>(this, _$identity);
}

abstract class _DataState implements DataState {
  factory _DataState(
      {required final String versionName,
      required final String packageName,
      required final User user,
      required final KtSet<Order> orders,
      required final bool isInternetConnected,
      final String? pendingInviteOrderId,
      final PendingPushNotification? pendingPushNotification}) = _$_DataState;

  @override
  String get versionName;
  @override
  String get packageName;
  @override
  User get user;
  @override
  KtSet<Order> get orders;
  @override
  bool get isInternetConnected;
  @override
  String? get pendingInviteOrderId;
  @override
  PendingPushNotification? get pendingPushNotification;
  @override
  @JsonKey(ignore: true)
  _$$_DataStateCopyWith<_$_DataState> get copyWith =>
      throw _privateConstructorUsedError;
}
