import 'package:freezed_annotation/freezed_annotation.dart';

part 'pending_push_notification.freezed.dart';

@freezed
class PendingPushNotification with _$PendingPushNotification {
  factory PendingPushNotification({
    required String orderId,
  }) = _PendingPushNotification;
}
