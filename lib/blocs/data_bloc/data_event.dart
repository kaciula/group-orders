import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:orders/blocs/data_bloc/pending_push_notification.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/user.dart';

part 'data_event.freezed.dart';

@freezed
class DataEvent with _$DataEvent {
  factory DataEvent.packageInfoRefreshed({
    required String versionName,
    required String packageName,
  }) = PackageInfoRefreshed;

  factory DataEvent.dataRefreshed({
    required User user,
    required KtList<Order> orders,
  }) = DataRefreshed;

  factory DataEvent.invitationReceived({required String orderId}) =
      InvitationReceived;

  factory DataEvent.invitationShown() = InvitationShown;

  factory DataEvent.orderCreated({required Order order}) = OrderCreated;

  factory DataEvent.orderUpdated({required Order order}) = OrderUpdated;

  factory DataEvent.userUpdated({required User user}) = UserUpdated;

  factory DataEvent.invitationAccepted({required Order order}) =
      InvitationAccepted;

  factory DataEvent.pendingPushNotificationReceived(
          {required PendingPushNotification pendingPushNotification}) =
      PendingPushNotificationReceived;

  factory DataEvent.pendingPushNotificationHandled() =
      PendingPushNotificationHandled;

  factory DataEvent.userSignedOut() = UserSignedOut;

  factory DataEvent.internetConnectionStatusChanged(
      {required bool isInternetConnected}) = InternetConnectionStatusChanged;
}
