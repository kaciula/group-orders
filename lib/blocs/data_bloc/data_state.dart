import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:orders/blocs/data_bloc/pending_push_notification.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/user.dart';

part 'data_state.freezed.dart';

@freezed
class DataState with _$DataState {
  factory DataState({
    required String versionName,
    required String packageName,
    required User user,
    required KtSet<Order> orders,
    required bool isInternetConnected,
    String? pendingInviteOrderId,
    PendingPushNotification? pendingPushNotification,
  }) = _DataState;

  factory DataState.initial() {
    return DataState(
      versionName: '',
      packageName: '',
      user: User.anonymous(),
      orders: const KtSet<Order>.empty(),
      isInternetConnected: true,
    );
  }
}
