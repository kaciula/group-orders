// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'pending_push_notification.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PendingPushNotification {
  String get orderId => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PendingPushNotificationCopyWith<PendingPushNotification> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PendingPushNotificationCopyWith<$Res> {
  factory $PendingPushNotificationCopyWith(PendingPushNotification value,
          $Res Function(PendingPushNotification) then) =
      _$PendingPushNotificationCopyWithImpl<$Res>;
  $Res call({String orderId});
}

/// @nodoc
class _$PendingPushNotificationCopyWithImpl<$Res>
    implements $PendingPushNotificationCopyWith<$Res> {
  _$PendingPushNotificationCopyWithImpl(this._value, this._then);

  final PendingPushNotification _value;
  // ignore: unused_field
  final $Res Function(PendingPushNotification) _then;

  @override
  $Res call({
    Object? orderId = freezed,
  }) {
    return _then(_value.copyWith(
      orderId: orderId == freezed
          ? _value.orderId
          : orderId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_PendingPushNotificationCopyWith<$Res>
    implements $PendingPushNotificationCopyWith<$Res> {
  factory _$$_PendingPushNotificationCopyWith(_$_PendingPushNotification value,
          $Res Function(_$_PendingPushNotification) then) =
      __$$_PendingPushNotificationCopyWithImpl<$Res>;
  @override
  $Res call({String orderId});
}

/// @nodoc
class __$$_PendingPushNotificationCopyWithImpl<$Res>
    extends _$PendingPushNotificationCopyWithImpl<$Res>
    implements _$$_PendingPushNotificationCopyWith<$Res> {
  __$$_PendingPushNotificationCopyWithImpl(_$_PendingPushNotification _value,
      $Res Function(_$_PendingPushNotification) _then)
      : super(_value, (v) => _then(v as _$_PendingPushNotification));

  @override
  _$_PendingPushNotification get _value =>
      super._value as _$_PendingPushNotification;

  @override
  $Res call({
    Object? orderId = freezed,
  }) {
    return _then(_$_PendingPushNotification(
      orderId: orderId == freezed
          ? _value.orderId
          : orderId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_PendingPushNotification implements _PendingPushNotification {
  _$_PendingPushNotification({required this.orderId});

  @override
  final String orderId;

  @override
  String toString() {
    return 'PendingPushNotification(orderId: $orderId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PendingPushNotification &&
            const DeepCollectionEquality().equals(other.orderId, orderId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(orderId));

  @JsonKey(ignore: true)
  @override
  _$$_PendingPushNotificationCopyWith<_$_PendingPushNotification>
      get copyWith =>
          __$$_PendingPushNotificationCopyWithImpl<_$_PendingPushNotification>(
              this, _$identity);
}

abstract class _PendingPushNotification implements PendingPushNotification {
  factory _PendingPushNotification({required final String orderId}) =
      _$_PendingPushNotification;

  @override
  String get orderId;
  @override
  @JsonKey(ignore: true)
  _$$_PendingPushNotificationCopyWith<_$_PendingPushNotification>
      get copyWith => throw _privateConstructorUsedError;
}
