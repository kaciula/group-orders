// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'data_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DataEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DataEventCopyWith<$Res> {
  factory $DataEventCopyWith(DataEvent value, $Res Function(DataEvent) then) =
      _$DataEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$DataEventCopyWithImpl<$Res> implements $DataEventCopyWith<$Res> {
  _$DataEventCopyWithImpl(this._value, this._then);

  final DataEvent _value;
  // ignore: unused_field
  final $Res Function(DataEvent) _then;
}

/// @nodoc
abstract class _$$PackageInfoRefreshedCopyWith<$Res> {
  factory _$$PackageInfoRefreshedCopyWith(_$PackageInfoRefreshed value,
          $Res Function(_$PackageInfoRefreshed) then) =
      __$$PackageInfoRefreshedCopyWithImpl<$Res>;
  $Res call({String versionName, String packageName});
}

/// @nodoc
class __$$PackageInfoRefreshedCopyWithImpl<$Res>
    extends _$DataEventCopyWithImpl<$Res>
    implements _$$PackageInfoRefreshedCopyWith<$Res> {
  __$$PackageInfoRefreshedCopyWithImpl(_$PackageInfoRefreshed _value,
      $Res Function(_$PackageInfoRefreshed) _then)
      : super(_value, (v) => _then(v as _$PackageInfoRefreshed));

  @override
  _$PackageInfoRefreshed get _value => super._value as _$PackageInfoRefreshed;

  @override
  $Res call({
    Object? versionName = freezed,
    Object? packageName = freezed,
  }) {
    return _then(_$PackageInfoRefreshed(
      versionName: versionName == freezed
          ? _value.versionName
          : versionName // ignore: cast_nullable_to_non_nullable
              as String,
      packageName: packageName == freezed
          ? _value.packageName
          : packageName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PackageInfoRefreshed implements PackageInfoRefreshed {
  _$PackageInfoRefreshed(
      {required this.versionName, required this.packageName});

  @override
  final String versionName;
  @override
  final String packageName;

  @override
  String toString() {
    return 'DataEvent.packageInfoRefreshed(versionName: $versionName, packageName: $packageName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PackageInfoRefreshed &&
            const DeepCollectionEquality()
                .equals(other.versionName, versionName) &&
            const DeepCollectionEquality()
                .equals(other.packageName, packageName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(versionName),
      const DeepCollectionEquality().hash(packageName));

  @JsonKey(ignore: true)
  @override
  _$$PackageInfoRefreshedCopyWith<_$PackageInfoRefreshed> get copyWith =>
      __$$PackageInfoRefreshedCopyWithImpl<_$PackageInfoRefreshed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return packageInfoRefreshed(versionName, packageName);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return packageInfoRefreshed?.call(versionName, packageName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (packageInfoRefreshed != null) {
      return packageInfoRefreshed(versionName, packageName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return packageInfoRefreshed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return packageInfoRefreshed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (packageInfoRefreshed != null) {
      return packageInfoRefreshed(this);
    }
    return orElse();
  }
}

abstract class PackageInfoRefreshed implements DataEvent {
  factory PackageInfoRefreshed(
      {required final String versionName,
      required final String packageName}) = _$PackageInfoRefreshed;

  String get versionName;
  String get packageName;
  @JsonKey(ignore: true)
  _$$PackageInfoRefreshedCopyWith<_$PackageInfoRefreshed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DataRefreshedCopyWith<$Res> {
  factory _$$DataRefreshedCopyWith(
          _$DataRefreshed value, $Res Function(_$DataRefreshed) then) =
      __$$DataRefreshedCopyWithImpl<$Res>;
  $Res call({User user, KtList<Order> orders});

  $UserCopyWith<$Res> get user;
}

/// @nodoc
class __$$DataRefreshedCopyWithImpl<$Res> extends _$DataEventCopyWithImpl<$Res>
    implements _$$DataRefreshedCopyWith<$Res> {
  __$$DataRefreshedCopyWithImpl(
      _$DataRefreshed _value, $Res Function(_$DataRefreshed) _then)
      : super(_value, (v) => _then(v as _$DataRefreshed));

  @override
  _$DataRefreshed get _value => super._value as _$DataRefreshed;

  @override
  $Res call({
    Object? user = freezed,
    Object? orders = freezed,
  }) {
    return _then(_$DataRefreshed(
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
      orders: orders == freezed
          ? _value.orders
          : orders // ignore: cast_nullable_to_non_nullable
              as KtList<Order>,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc

class _$DataRefreshed implements DataRefreshed {
  _$DataRefreshed({required this.user, required this.orders});

  @override
  final User user;
  @override
  final KtList<Order> orders;

  @override
  String toString() {
    return 'DataEvent.dataRefreshed(user: $user, orders: $orders)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataRefreshed &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality().equals(other.orders, orders));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(orders));

  @JsonKey(ignore: true)
  @override
  _$$DataRefreshedCopyWith<_$DataRefreshed> get copyWith =>
      __$$DataRefreshedCopyWithImpl<_$DataRefreshed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return dataRefreshed(user, orders);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return dataRefreshed?.call(user, orders);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (dataRefreshed != null) {
      return dataRefreshed(user, orders);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return dataRefreshed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return dataRefreshed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (dataRefreshed != null) {
      return dataRefreshed(this);
    }
    return orElse();
  }
}

abstract class DataRefreshed implements DataEvent {
  factory DataRefreshed(
      {required final User user,
      required final KtList<Order> orders}) = _$DataRefreshed;

  User get user;
  KtList<Order> get orders;
  @JsonKey(ignore: true)
  _$$DataRefreshedCopyWith<_$DataRefreshed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InvitationReceivedCopyWith<$Res> {
  factory _$$InvitationReceivedCopyWith(_$InvitationReceived value,
          $Res Function(_$InvitationReceived) then) =
      __$$InvitationReceivedCopyWithImpl<$Res>;
  $Res call({String orderId});
}

/// @nodoc
class __$$InvitationReceivedCopyWithImpl<$Res>
    extends _$DataEventCopyWithImpl<$Res>
    implements _$$InvitationReceivedCopyWith<$Res> {
  __$$InvitationReceivedCopyWithImpl(
      _$InvitationReceived _value, $Res Function(_$InvitationReceived) _then)
      : super(_value, (v) => _then(v as _$InvitationReceived));

  @override
  _$InvitationReceived get _value => super._value as _$InvitationReceived;

  @override
  $Res call({
    Object? orderId = freezed,
  }) {
    return _then(_$InvitationReceived(
      orderId: orderId == freezed
          ? _value.orderId
          : orderId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InvitationReceived implements InvitationReceived {
  _$InvitationReceived({required this.orderId});

  @override
  final String orderId;

  @override
  String toString() {
    return 'DataEvent.invitationReceived(orderId: $orderId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvitationReceived &&
            const DeepCollectionEquality().equals(other.orderId, orderId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(orderId));

  @JsonKey(ignore: true)
  @override
  _$$InvitationReceivedCopyWith<_$InvitationReceived> get copyWith =>
      __$$InvitationReceivedCopyWithImpl<_$InvitationReceived>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return invitationReceived(orderId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return invitationReceived?.call(orderId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (invitationReceived != null) {
      return invitationReceived(orderId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return invitationReceived(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return invitationReceived?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (invitationReceived != null) {
      return invitationReceived(this);
    }
    return orElse();
  }
}

abstract class InvitationReceived implements DataEvent {
  factory InvitationReceived({required final String orderId}) =
      _$InvitationReceived;

  String get orderId;
  @JsonKey(ignore: true)
  _$$InvitationReceivedCopyWith<_$InvitationReceived> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InvitationShownCopyWith<$Res> {
  factory _$$InvitationShownCopyWith(
          _$InvitationShown value, $Res Function(_$InvitationShown) then) =
      __$$InvitationShownCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvitationShownCopyWithImpl<$Res>
    extends _$DataEventCopyWithImpl<$Res>
    implements _$$InvitationShownCopyWith<$Res> {
  __$$InvitationShownCopyWithImpl(
      _$InvitationShown _value, $Res Function(_$InvitationShown) _then)
      : super(_value, (v) => _then(v as _$InvitationShown));

  @override
  _$InvitationShown get _value => super._value as _$InvitationShown;
}

/// @nodoc

class _$InvitationShown implements InvitationShown {
  _$InvitationShown();

  @override
  String toString() {
    return 'DataEvent.invitationShown()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InvitationShown);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return invitationShown();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return invitationShown?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (invitationShown != null) {
      return invitationShown();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return invitationShown(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return invitationShown?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (invitationShown != null) {
      return invitationShown(this);
    }
    return orElse();
  }
}

abstract class InvitationShown implements DataEvent {
  factory InvitationShown() = _$InvitationShown;
}

/// @nodoc
abstract class _$$OrderCreatedCopyWith<$Res> {
  factory _$$OrderCreatedCopyWith(
          _$OrderCreated value, $Res Function(_$OrderCreated) then) =
      __$$OrderCreatedCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$OrderCreatedCopyWithImpl<$Res> extends _$DataEventCopyWithImpl<$Res>
    implements _$$OrderCreatedCopyWith<$Res> {
  __$$OrderCreatedCopyWithImpl(
      _$OrderCreated _value, $Res Function(_$OrderCreated) _then)
      : super(_value, (v) => _then(v as _$OrderCreated));

  @override
  _$OrderCreated get _value => super._value as _$OrderCreated;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$OrderCreated(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$OrderCreated implements OrderCreated {
  _$OrderCreated({required this.order});

  @override
  final Order order;

  @override
  String toString() {
    return 'DataEvent.orderCreated(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrderCreated &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$OrderCreatedCopyWith<_$OrderCreated> get copyWith =>
      __$$OrderCreatedCopyWithImpl<_$OrderCreated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return orderCreated(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return orderCreated?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (orderCreated != null) {
      return orderCreated(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return orderCreated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return orderCreated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (orderCreated != null) {
      return orderCreated(this);
    }
    return orElse();
  }
}

abstract class OrderCreated implements DataEvent {
  factory OrderCreated({required final Order order}) = _$OrderCreated;

  Order get order;
  @JsonKey(ignore: true)
  _$$OrderCreatedCopyWith<_$OrderCreated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$OrderUpdatedCopyWith<$Res> {
  factory _$$OrderUpdatedCopyWith(
          _$OrderUpdated value, $Res Function(_$OrderUpdated) then) =
      __$$OrderUpdatedCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$OrderUpdatedCopyWithImpl<$Res> extends _$DataEventCopyWithImpl<$Res>
    implements _$$OrderUpdatedCopyWith<$Res> {
  __$$OrderUpdatedCopyWithImpl(
      _$OrderUpdated _value, $Res Function(_$OrderUpdated) _then)
      : super(_value, (v) => _then(v as _$OrderUpdated));

  @override
  _$OrderUpdated get _value => super._value as _$OrderUpdated;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$OrderUpdated(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$OrderUpdated implements OrderUpdated {
  _$OrderUpdated({required this.order});

  @override
  final Order order;

  @override
  String toString() {
    return 'DataEvent.orderUpdated(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrderUpdated &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$OrderUpdatedCopyWith<_$OrderUpdated> get copyWith =>
      __$$OrderUpdatedCopyWithImpl<_$OrderUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return orderUpdated(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return orderUpdated?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (orderUpdated != null) {
      return orderUpdated(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return orderUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return orderUpdated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (orderUpdated != null) {
      return orderUpdated(this);
    }
    return orElse();
  }
}

abstract class OrderUpdated implements DataEvent {
  factory OrderUpdated({required final Order order}) = _$OrderUpdated;

  Order get order;
  @JsonKey(ignore: true)
  _$$OrderUpdatedCopyWith<_$OrderUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UserUpdatedCopyWith<$Res> {
  factory _$$UserUpdatedCopyWith(
          _$UserUpdated value, $Res Function(_$UserUpdated) then) =
      __$$UserUpdatedCopyWithImpl<$Res>;
  $Res call({User user});

  $UserCopyWith<$Res> get user;
}

/// @nodoc
class __$$UserUpdatedCopyWithImpl<$Res> extends _$DataEventCopyWithImpl<$Res>
    implements _$$UserUpdatedCopyWith<$Res> {
  __$$UserUpdatedCopyWithImpl(
      _$UserUpdated _value, $Res Function(_$UserUpdated) _then)
      : super(_value, (v) => _then(v as _$UserUpdated));

  @override
  _$UserUpdated get _value => super._value as _$UserUpdated;

  @override
  $Res call({
    Object? user = freezed,
  }) {
    return _then(_$UserUpdated(
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc

class _$UserUpdated implements UserUpdated {
  _$UserUpdated({required this.user});

  @override
  final User user;

  @override
  String toString() {
    return 'DataEvent.userUpdated(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserUpdated &&
            const DeepCollectionEquality().equals(other.user, user));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(user));

  @JsonKey(ignore: true)
  @override
  _$$UserUpdatedCopyWith<_$UserUpdated> get copyWith =>
      __$$UserUpdatedCopyWithImpl<_$UserUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return userUpdated(user);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return userUpdated?.call(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (userUpdated != null) {
      return userUpdated(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return userUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return userUpdated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (userUpdated != null) {
      return userUpdated(this);
    }
    return orElse();
  }
}

abstract class UserUpdated implements DataEvent {
  factory UserUpdated({required final User user}) = _$UserUpdated;

  User get user;
  @JsonKey(ignore: true)
  _$$UserUpdatedCopyWith<_$UserUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InvitationAcceptedCopyWith<$Res> {
  factory _$$InvitationAcceptedCopyWith(_$InvitationAccepted value,
          $Res Function(_$InvitationAccepted) then) =
      __$$InvitationAcceptedCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$InvitationAcceptedCopyWithImpl<$Res>
    extends _$DataEventCopyWithImpl<$Res>
    implements _$$InvitationAcceptedCopyWith<$Res> {
  __$$InvitationAcceptedCopyWithImpl(
      _$InvitationAccepted _value, $Res Function(_$InvitationAccepted) _then)
      : super(_value, (v) => _then(v as _$InvitationAccepted));

  @override
  _$InvitationAccepted get _value => super._value as _$InvitationAccepted;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$InvitationAccepted(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$InvitationAccepted implements InvitationAccepted {
  _$InvitationAccepted({required this.order});

  @override
  final Order order;

  @override
  String toString() {
    return 'DataEvent.invitationAccepted(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvitationAccepted &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$InvitationAcceptedCopyWith<_$InvitationAccepted> get copyWith =>
      __$$InvitationAcceptedCopyWithImpl<_$InvitationAccepted>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return invitationAccepted(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return invitationAccepted?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (invitationAccepted != null) {
      return invitationAccepted(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return invitationAccepted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return invitationAccepted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (invitationAccepted != null) {
      return invitationAccepted(this);
    }
    return orElse();
  }
}

abstract class InvitationAccepted implements DataEvent {
  factory InvitationAccepted({required final Order order}) =
      _$InvitationAccepted;

  Order get order;
  @JsonKey(ignore: true)
  _$$InvitationAcceptedCopyWith<_$InvitationAccepted> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$PendingPushNotificationReceivedCopyWith<$Res> {
  factory _$$PendingPushNotificationReceivedCopyWith(
          _$PendingPushNotificationReceived value,
          $Res Function(_$PendingPushNotificationReceived) then) =
      __$$PendingPushNotificationReceivedCopyWithImpl<$Res>;
  $Res call({PendingPushNotification pendingPushNotification});

  $PendingPushNotificationCopyWith<$Res> get pendingPushNotification;
}

/// @nodoc
class __$$PendingPushNotificationReceivedCopyWithImpl<$Res>
    extends _$DataEventCopyWithImpl<$Res>
    implements _$$PendingPushNotificationReceivedCopyWith<$Res> {
  __$$PendingPushNotificationReceivedCopyWithImpl(
      _$PendingPushNotificationReceived _value,
      $Res Function(_$PendingPushNotificationReceived) _then)
      : super(_value, (v) => _then(v as _$PendingPushNotificationReceived));

  @override
  _$PendingPushNotificationReceived get _value =>
      super._value as _$PendingPushNotificationReceived;

  @override
  $Res call({
    Object? pendingPushNotification = freezed,
  }) {
    return _then(_$PendingPushNotificationReceived(
      pendingPushNotification: pendingPushNotification == freezed
          ? _value.pendingPushNotification
          : pendingPushNotification // ignore: cast_nullable_to_non_nullable
              as PendingPushNotification,
    ));
  }

  @override
  $PendingPushNotificationCopyWith<$Res> get pendingPushNotification {
    return $PendingPushNotificationCopyWith<$Res>(
        _value.pendingPushNotification, (value) {
      return _then(_value.copyWith(pendingPushNotification: value));
    });
  }
}

/// @nodoc

class _$PendingPushNotificationReceived
    implements PendingPushNotificationReceived {
  _$PendingPushNotificationReceived({required this.pendingPushNotification});

  @override
  final PendingPushNotification pendingPushNotification;

  @override
  String toString() {
    return 'DataEvent.pendingPushNotificationReceived(pendingPushNotification: $pendingPushNotification)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PendingPushNotificationReceived &&
            const DeepCollectionEquality().equals(
                other.pendingPushNotification, pendingPushNotification));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(pendingPushNotification));

  @JsonKey(ignore: true)
  @override
  _$$PendingPushNotificationReceivedCopyWith<_$PendingPushNotificationReceived>
      get copyWith => __$$PendingPushNotificationReceivedCopyWithImpl<
          _$PendingPushNotificationReceived>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return pendingPushNotificationReceived(pendingPushNotification);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return pendingPushNotificationReceived?.call(pendingPushNotification);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (pendingPushNotificationReceived != null) {
      return pendingPushNotificationReceived(pendingPushNotification);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return pendingPushNotificationReceived(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return pendingPushNotificationReceived?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (pendingPushNotificationReceived != null) {
      return pendingPushNotificationReceived(this);
    }
    return orElse();
  }
}

abstract class PendingPushNotificationReceived implements DataEvent {
  factory PendingPushNotificationReceived(
          {required final PendingPushNotification pendingPushNotification}) =
      _$PendingPushNotificationReceived;

  PendingPushNotification get pendingPushNotification;
  @JsonKey(ignore: true)
  _$$PendingPushNotificationReceivedCopyWith<_$PendingPushNotificationReceived>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$PendingPushNotificationHandledCopyWith<$Res> {
  factory _$$PendingPushNotificationHandledCopyWith(
          _$PendingPushNotificationHandled value,
          $Res Function(_$PendingPushNotificationHandled) then) =
      __$$PendingPushNotificationHandledCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PendingPushNotificationHandledCopyWithImpl<$Res>
    extends _$DataEventCopyWithImpl<$Res>
    implements _$$PendingPushNotificationHandledCopyWith<$Res> {
  __$$PendingPushNotificationHandledCopyWithImpl(
      _$PendingPushNotificationHandled _value,
      $Res Function(_$PendingPushNotificationHandled) _then)
      : super(_value, (v) => _then(v as _$PendingPushNotificationHandled));

  @override
  _$PendingPushNotificationHandled get _value =>
      super._value as _$PendingPushNotificationHandled;
}

/// @nodoc

class _$PendingPushNotificationHandled
    implements PendingPushNotificationHandled {
  _$PendingPushNotificationHandled();

  @override
  String toString() {
    return 'DataEvent.pendingPushNotificationHandled()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PendingPushNotificationHandled);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return pendingPushNotificationHandled();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return pendingPushNotificationHandled?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (pendingPushNotificationHandled != null) {
      return pendingPushNotificationHandled();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return pendingPushNotificationHandled(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return pendingPushNotificationHandled?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (pendingPushNotificationHandled != null) {
      return pendingPushNotificationHandled(this);
    }
    return orElse();
  }
}

abstract class PendingPushNotificationHandled implements DataEvent {
  factory PendingPushNotificationHandled() = _$PendingPushNotificationHandled;
}

/// @nodoc
abstract class _$$UserSignedOutCopyWith<$Res> {
  factory _$$UserSignedOutCopyWith(
          _$UserSignedOut value, $Res Function(_$UserSignedOut) then) =
      __$$UserSignedOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UserSignedOutCopyWithImpl<$Res> extends _$DataEventCopyWithImpl<$Res>
    implements _$$UserSignedOutCopyWith<$Res> {
  __$$UserSignedOutCopyWithImpl(
      _$UserSignedOut _value, $Res Function(_$UserSignedOut) _then)
      : super(_value, (v) => _then(v as _$UserSignedOut));

  @override
  _$UserSignedOut get _value => super._value as _$UserSignedOut;
}

/// @nodoc

class _$UserSignedOut implements UserSignedOut {
  _$UserSignedOut();

  @override
  String toString() {
    return 'DataEvent.userSignedOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UserSignedOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return userSignedOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return userSignedOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (userSignedOut != null) {
      return userSignedOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return userSignedOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return userSignedOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (userSignedOut != null) {
      return userSignedOut(this);
    }
    return orElse();
  }
}

abstract class UserSignedOut implements DataEvent {
  factory UserSignedOut() = _$UserSignedOut;
}

/// @nodoc
abstract class _$$InternetConnectionStatusChangedCopyWith<$Res> {
  factory _$$InternetConnectionStatusChangedCopyWith(
          _$InternetConnectionStatusChanged value,
          $Res Function(_$InternetConnectionStatusChanged) then) =
      __$$InternetConnectionStatusChangedCopyWithImpl<$Res>;
  $Res call({bool isInternetConnected});
}

/// @nodoc
class __$$InternetConnectionStatusChangedCopyWithImpl<$Res>
    extends _$DataEventCopyWithImpl<$Res>
    implements _$$InternetConnectionStatusChangedCopyWith<$Res> {
  __$$InternetConnectionStatusChangedCopyWithImpl(
      _$InternetConnectionStatusChanged _value,
      $Res Function(_$InternetConnectionStatusChanged) _then)
      : super(_value, (v) => _then(v as _$InternetConnectionStatusChanged));

  @override
  _$InternetConnectionStatusChanged get _value =>
      super._value as _$InternetConnectionStatusChanged;

  @override
  $Res call({
    Object? isInternetConnected = freezed,
  }) {
    return _then(_$InternetConnectionStatusChanged(
      isInternetConnected: isInternetConnected == freezed
          ? _value.isInternetConnected
          : isInternetConnected // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$InternetConnectionStatusChanged
    implements InternetConnectionStatusChanged {
  _$InternetConnectionStatusChanged({required this.isInternetConnected});

  @override
  final bool isInternetConnected;

  @override
  String toString() {
    return 'DataEvent.internetConnectionStatusChanged(isInternetConnected: $isInternetConnected)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InternetConnectionStatusChanged &&
            const DeepCollectionEquality()
                .equals(other.isInternetConnected, isInternetConnected));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(isInternetConnected));

  @JsonKey(ignore: true)
  @override
  _$$InternetConnectionStatusChangedCopyWith<_$InternetConnectionStatusChanged>
      get copyWith => __$$InternetConnectionStatusChangedCopyWithImpl<
          _$InternetConnectionStatusChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String versionName, String packageName)
        packageInfoRefreshed,
    required TResult Function(User user, KtList<Order> orders) dataRefreshed,
    required TResult Function(String orderId) invitationReceived,
    required TResult Function() invitationShown,
    required TResult Function(Order order) orderCreated,
    required TResult Function(Order order) orderUpdated,
    required TResult Function(User user) userUpdated,
    required TResult Function(Order order) invitationAccepted,
    required TResult Function(PendingPushNotification pendingPushNotification)
        pendingPushNotificationReceived,
    required TResult Function() pendingPushNotificationHandled,
    required TResult Function() userSignedOut,
    required TResult Function(bool isInternetConnected)
        internetConnectionStatusChanged,
  }) {
    return internetConnectionStatusChanged(isInternetConnected);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
  }) {
    return internetConnectionStatusChanged?.call(isInternetConnected);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String versionName, String packageName)?
        packageInfoRefreshed,
    TResult Function(User user, KtList<Order> orders)? dataRefreshed,
    TResult Function(String orderId)? invitationReceived,
    TResult Function()? invitationShown,
    TResult Function(Order order)? orderCreated,
    TResult Function(Order order)? orderUpdated,
    TResult Function(User user)? userUpdated,
    TResult Function(Order order)? invitationAccepted,
    TResult Function(PendingPushNotification pendingPushNotification)?
        pendingPushNotificationReceived,
    TResult Function()? pendingPushNotificationHandled,
    TResult Function()? userSignedOut,
    TResult Function(bool isInternetConnected)? internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (internetConnectionStatusChanged != null) {
      return internetConnectionStatusChanged(isInternetConnected);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PackageInfoRefreshed value) packageInfoRefreshed,
    required TResult Function(DataRefreshed value) dataRefreshed,
    required TResult Function(InvitationReceived value) invitationReceived,
    required TResult Function(InvitationShown value) invitationShown,
    required TResult Function(OrderCreated value) orderCreated,
    required TResult Function(OrderUpdated value) orderUpdated,
    required TResult Function(UserUpdated value) userUpdated,
    required TResult Function(InvitationAccepted value) invitationAccepted,
    required TResult Function(PendingPushNotificationReceived value)
        pendingPushNotificationReceived,
    required TResult Function(PendingPushNotificationHandled value)
        pendingPushNotificationHandled,
    required TResult Function(UserSignedOut value) userSignedOut,
    required TResult Function(InternetConnectionStatusChanged value)
        internetConnectionStatusChanged,
  }) {
    return internetConnectionStatusChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
  }) {
    return internetConnectionStatusChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PackageInfoRefreshed value)? packageInfoRefreshed,
    TResult Function(DataRefreshed value)? dataRefreshed,
    TResult Function(InvitationReceived value)? invitationReceived,
    TResult Function(InvitationShown value)? invitationShown,
    TResult Function(OrderCreated value)? orderCreated,
    TResult Function(OrderUpdated value)? orderUpdated,
    TResult Function(UserUpdated value)? userUpdated,
    TResult Function(InvitationAccepted value)? invitationAccepted,
    TResult Function(PendingPushNotificationReceived value)?
        pendingPushNotificationReceived,
    TResult Function(PendingPushNotificationHandled value)?
        pendingPushNotificationHandled,
    TResult Function(UserSignedOut value)? userSignedOut,
    TResult Function(InternetConnectionStatusChanged value)?
        internetConnectionStatusChanged,
    required TResult orElse(),
  }) {
    if (internetConnectionStatusChanged != null) {
      return internetConnectionStatusChanged(this);
    }
    return orElse();
  }
}

abstract class InternetConnectionStatusChanged implements DataEvent {
  factory InternetConnectionStatusChanged(
          {required final bool isInternetConnected}) =
      _$InternetConnectionStatusChanged;

  bool get isInternetConnected;
  @JsonKey(ignore: true)
  _$$InternetConnectionStatusChangedCopyWith<_$InternetConnectionStatusChanged>
      get copyWith => throw _privateConstructorUsedError;
}
