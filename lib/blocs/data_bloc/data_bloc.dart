import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:kt_dart/collection.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/blocs/data_bloc/data_state.dart';
import 'package:orders/blocs/data_bloc/pending_push_notification.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/user.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  DataBloc() : super(DataState.initial()) {
    connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .distinct()
        .listen((ConnectivityResult result) async {
      _logger.fine('connectivity change $result');
      bool isInternetConnected = result != ConnectivityResult.none;
      if (result != ConnectivityResult.none) {
        isInternetConnected = await InternetConnectionChecker().hasConnection;
      }
      add(InternetConnectionStatusChanged(
          isInternetConnected: isInternetConnected));
    });
  }

  late StreamSubscription<ConnectivityResult> connectivitySubscription;

  @override
  Stream<DataState> mapEventToState(DataEvent event) async* {
    if (event is PackageInfoRefreshed) {
      yield state.copyWith(
        versionName: event.versionName,
        packageName: event.packageName,
      );
    }
    if (event is DataRefreshed) {
      yield state.copyWith(user: event.user, orders: event.orders.toSet());
    }
    if (event is InvitationReceived) {
      yield state.copyWith(pendingInviteOrderId: event.orderId);
    }
    if (event is InvitationShown) {
      yield state.copyWith(pendingInviteOrderId: null);
    }
    if (event is PendingPushNotificationReceived) {
      yield state.copyWith(
          pendingPushNotification: event.pendingPushNotification);
    }
    if (event is PendingPushNotificationHandled) {
      yield state.copyWith(pendingPushNotification: null);
    }
    if (event is InvitationAccepted) {
      yield state.copyWith(
          orders: state.orders.plusElement(event.order).toSet());
    }
    if (event is InternetConnectionStatusChanged) {
      yield state.copyWith(isInternetConnected: event.isInternetConnected);
    }
    if (event is OrderCreated) {
      yield state.copyWith(
          orders: state.orders.plusElement(event.order).toSet());
    }
    if (event is OrderUpdated) {
      final KtSet<Order> modifiedOrders = state.orders
          .map((Order it) => it.id == event.order.id ? event.order : it)
          .toSet();
      yield state.copyWith(orders: modifiedOrders);
    }
    if (event is UserUpdated) {
      final KtSet<Order> modifiedOrders = state.orders
          .map((Order it) => it.ownerId == event.user.id
              ? it.copyWith(ownerName: event.user.displayName)
              : it)
          .toSet();
      yield state.copyWith(user: event.user, orders: modifiedOrders);
    }
    if (event is UserSignedOut) {
      yield state.copyWith(
          user: User.anonymous(), orders: const KtSet<Order>.empty());
    }
  }

  User get user => state.user;

  String get userId => user.id;

  bool get isSignedIn => user.isSignedIn;

  String? get pendingInviteOrderId => state.pendingInviteOrderId;

  PendingPushNotification? get pendingPushNotification =>
      state.pendingPushNotification;

  KtList<Order> get orders => state.orders.toList();

  bool get isInternetConnected => state.isInternetConnected;

  String get versionName => state.versionName;

  @override
  Future<void> close() async {
    connectivitySubscription.cancel();
    super.close();
  }
}

// ignore: unused_element
final Logger _logger = Logger('DataBloc');
