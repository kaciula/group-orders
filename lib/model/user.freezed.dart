// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$User {
  String get id => throw _privateConstructorUsedError;
  String get displayName => throw _privateConstructorUsedError;
  String get emailAddress => throw _privateConstructorUsedError;
  String get phoneNumber => throw _privateConstructorUsedError;
  SignInProvider get signInProvider => throw _privateConstructorUsedError;
  String get languageCode => throw _privateConstructorUsedError;
  String get messagingToken => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserCopyWith<User> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String displayName,
      String emailAddress,
      String phoneNumber,
      SignInProvider signInProvider,
      String languageCode,
      String messagingToken});
}

/// @nodoc
class _$UserCopyWithImpl<$Res> implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  final User _value;
  // ignore: unused_field
  final $Res Function(User) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? displayName = freezed,
    Object? emailAddress = freezed,
    Object? phoneNumber = freezed,
    Object? signInProvider = freezed,
    Object? languageCode = freezed,
    Object? messagingToken = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      signInProvider: signInProvider == freezed
          ? _value.signInProvider
          : signInProvider // ignore: cast_nullable_to_non_nullable
              as SignInProvider,
      languageCode: languageCode == freezed
          ? _value.languageCode
          : languageCode // ignore: cast_nullable_to_non_nullable
              as String,
      messagingToken: messagingToken == freezed
          ? _value.messagingToken
          : messagingToken // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_UserCopyWith<$Res> implements $UserCopyWith<$Res> {
  factory _$$_UserCopyWith(_$_User value, $Res Function(_$_User) then) =
      __$$_UserCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String displayName,
      String emailAddress,
      String phoneNumber,
      SignInProvider signInProvider,
      String languageCode,
      String messagingToken});
}

/// @nodoc
class __$$_UserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res>
    implements _$$_UserCopyWith<$Res> {
  __$$_UserCopyWithImpl(_$_User _value, $Res Function(_$_User) _then)
      : super(_value, (v) => _then(v as _$_User));

  @override
  _$_User get _value => super._value as _$_User;

  @override
  $Res call({
    Object? id = freezed,
    Object? displayName = freezed,
    Object? emailAddress = freezed,
    Object? phoneNumber = freezed,
    Object? signInProvider = freezed,
    Object? languageCode = freezed,
    Object? messagingToken = freezed,
  }) {
    return _then(_$_User(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      signInProvider: signInProvider == freezed
          ? _value.signInProvider
          : signInProvider // ignore: cast_nullable_to_non_nullable
              as SignInProvider,
      languageCode: languageCode == freezed
          ? _value.languageCode
          : languageCode // ignore: cast_nullable_to_non_nullable
              as String,
      messagingToken: messagingToken == freezed
          ? _value.messagingToken
          : messagingToken // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_User extends _User {
  _$_User(
      {required this.id,
      required this.displayName,
      required this.emailAddress,
      required this.phoneNumber,
      required this.signInProvider,
      required this.languageCode,
      required this.messagingToken})
      : super._();

  @override
  final String id;
  @override
  final String displayName;
  @override
  final String emailAddress;
  @override
  final String phoneNumber;
  @override
  final SignInProvider signInProvider;
  @override
  final String languageCode;
  @override
  final String messagingToken;

  @override
  String toString() {
    return 'User(id: $id, displayName: $displayName, emailAddress: $emailAddress, phoneNumber: $phoneNumber, signInProvider: $signInProvider, languageCode: $languageCode, messagingToken: $messagingToken)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_User &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality()
                .equals(other.displayName, displayName) &&
            const DeepCollectionEquality()
                .equals(other.emailAddress, emailAddress) &&
            const DeepCollectionEquality()
                .equals(other.phoneNumber, phoneNumber) &&
            const DeepCollectionEquality()
                .equals(other.signInProvider, signInProvider) &&
            const DeepCollectionEquality()
                .equals(other.languageCode, languageCode) &&
            const DeepCollectionEquality()
                .equals(other.messagingToken, messagingToken));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(displayName),
      const DeepCollectionEquality().hash(emailAddress),
      const DeepCollectionEquality().hash(phoneNumber),
      const DeepCollectionEquality().hash(signInProvider),
      const DeepCollectionEquality().hash(languageCode),
      const DeepCollectionEquality().hash(messagingToken));

  @JsonKey(ignore: true)
  @override
  _$$_UserCopyWith<_$_User> get copyWith =>
      __$$_UserCopyWithImpl<_$_User>(this, _$identity);
}

abstract class _User extends User {
  factory _User(
      {required final String id,
      required final String displayName,
      required final String emailAddress,
      required final String phoneNumber,
      required final SignInProvider signInProvider,
      required final String languageCode,
      required final String messagingToken}) = _$_User;
  _User._() : super._();

  @override
  String get id;
  @override
  String get displayName;
  @override
  String get emailAddress;
  @override
  String get phoneNumber;
  @override
  SignInProvider get signInProvider;
  @override
  String get languageCode;
  @override
  String get messagingToken;
  @override
  @JsonKey(ignore: true)
  _$$_UserCopyWith<_$_User> get copyWith => throw _privateConstructorUsedError;
}
