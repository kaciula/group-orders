// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cart_entry.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CartEntry {
  String get id => throw _privateConstructorUsedError;
  String get details => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)
        product,
    required TResult Function(String id, String details, double percent)
        discount,
    required TResult Function(String id, String details, double cost) transport,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)?
        product,
    TResult Function(String id, String details, double percent)? discount,
    TResult Function(String id, String details, double cost)? transport,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)?
        product,
    TResult Function(String id, String details, double percent)? discount,
    TResult Function(String id, String details, double cost)? transport,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CartProduct value) product,
    required TResult Function(Discount value) discount,
    required TResult Function(Transport value) transport,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CartProduct value)? product,
    TResult Function(Discount value)? discount,
    TResult Function(Transport value)? transport,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CartProduct value)? product,
    TResult Function(Discount value)? discount,
    TResult Function(Transport value)? transport,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CartEntryCopyWith<CartEntry> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CartEntryCopyWith<$Res> {
  factory $CartEntryCopyWith(CartEntry value, $Res Function(CartEntry) then) =
      _$CartEntryCopyWithImpl<$Res>;
  $Res call({String id, String details});
}

/// @nodoc
class _$CartEntryCopyWithImpl<$Res> implements $CartEntryCopyWith<$Res> {
  _$CartEntryCopyWithImpl(this._value, this._then);

  final CartEntry _value;
  // ignore: unused_field
  final $Res Function(CartEntry) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? details = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$CartProductCopyWith<$Res>
    implements $CartEntryCopyWith<$Res> {
  factory _$$CartProductCopyWith(
          _$CartProduct value, $Res Function(_$CartProduct) then) =
      __$$CartProductCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String productId,
      String participantId,
      String details,
      int numItems,
      double? modifiedPrice});
}

/// @nodoc
class __$$CartProductCopyWithImpl<$Res> extends _$CartEntryCopyWithImpl<$Res>
    implements _$$CartProductCopyWith<$Res> {
  __$$CartProductCopyWithImpl(
      _$CartProduct _value, $Res Function(_$CartProduct) _then)
      : super(_value, (v) => _then(v as _$CartProduct));

  @override
  _$CartProduct get _value => super._value as _$CartProduct;

  @override
  $Res call({
    Object? id = freezed,
    Object? productId = freezed,
    Object? participantId = freezed,
    Object? details = freezed,
    Object? numItems = freezed,
    Object? modifiedPrice = freezed,
  }) {
    return _then(_$CartProduct(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      productId: productId == freezed
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as String,
      participantId: participantId == freezed
          ? _value.participantId
          : participantId // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      numItems: numItems == freezed
          ? _value.numItems
          : numItems // ignore: cast_nullable_to_non_nullable
              as int,
      modifiedPrice: modifiedPrice == freezed
          ? _value.modifiedPrice
          : modifiedPrice // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc

class _$CartProduct implements CartProduct {
  _$CartProduct(
      {required this.id,
      required this.productId,
      required this.participantId,
      required this.details,
      required this.numItems,
      this.modifiedPrice});

  @override
  final String id;
  @override
  final String productId;
  @override
  final String participantId;
  @override
  final String details;
  @override
  final int numItems;
  @override
  final double? modifiedPrice;

  @override
  String toString() {
    return 'CartEntry.product(id: $id, productId: $productId, participantId: $participantId, details: $details, numItems: $numItems, modifiedPrice: $modifiedPrice)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CartProduct &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.productId, productId) &&
            const DeepCollectionEquality()
                .equals(other.participantId, participantId) &&
            const DeepCollectionEquality().equals(other.details, details) &&
            const DeepCollectionEquality().equals(other.numItems, numItems) &&
            const DeepCollectionEquality()
                .equals(other.modifiedPrice, modifiedPrice));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(productId),
      const DeepCollectionEquality().hash(participantId),
      const DeepCollectionEquality().hash(details),
      const DeepCollectionEquality().hash(numItems),
      const DeepCollectionEquality().hash(modifiedPrice));

  @JsonKey(ignore: true)
  @override
  _$$CartProductCopyWith<_$CartProduct> get copyWith =>
      __$$CartProductCopyWithImpl<_$CartProduct>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)
        product,
    required TResult Function(String id, String details, double percent)
        discount,
    required TResult Function(String id, String details, double cost) transport,
  }) {
    return product(
        id, productId, participantId, details, numItems, modifiedPrice);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)?
        product,
    TResult Function(String id, String details, double percent)? discount,
    TResult Function(String id, String details, double cost)? transport,
  }) {
    return product?.call(
        id, productId, participantId, details, numItems, modifiedPrice);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)?
        product,
    TResult Function(String id, String details, double percent)? discount,
    TResult Function(String id, String details, double cost)? transport,
    required TResult orElse(),
  }) {
    if (product != null) {
      return product(
          id, productId, participantId, details, numItems, modifiedPrice);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CartProduct value) product,
    required TResult Function(Discount value) discount,
    required TResult Function(Transport value) transport,
  }) {
    return product(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CartProduct value)? product,
    TResult Function(Discount value)? discount,
    TResult Function(Transport value)? transport,
  }) {
    return product?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CartProduct value)? product,
    TResult Function(Discount value)? discount,
    TResult Function(Transport value)? transport,
    required TResult orElse(),
  }) {
    if (product != null) {
      return product(this);
    }
    return orElse();
  }
}

abstract class CartProduct implements CartEntry {
  factory CartProduct(
      {required final String id,
      required final String productId,
      required final String participantId,
      required final String details,
      required final int numItems,
      final double? modifiedPrice}) = _$CartProduct;

  @override
  String get id;
  String get productId;
  String get participantId;
  @override
  String get details;
  int get numItems;
  double? get modifiedPrice;
  @override
  @JsonKey(ignore: true)
  _$$CartProductCopyWith<_$CartProduct> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DiscountCopyWith<$Res> implements $CartEntryCopyWith<$Res> {
  factory _$$DiscountCopyWith(
          _$Discount value, $Res Function(_$Discount) then) =
      __$$DiscountCopyWithImpl<$Res>;
  @override
  $Res call({String id, String details, double percent});
}

/// @nodoc
class __$$DiscountCopyWithImpl<$Res> extends _$CartEntryCopyWithImpl<$Res>
    implements _$$DiscountCopyWith<$Res> {
  __$$DiscountCopyWithImpl(_$Discount _value, $Res Function(_$Discount) _then)
      : super(_value, (v) => _then(v as _$Discount));

  @override
  _$Discount get _value => super._value as _$Discount;

  @override
  $Res call({
    Object? id = freezed,
    Object? details = freezed,
    Object? percent = freezed,
  }) {
    return _then(_$Discount(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      percent: percent == freezed
          ? _value.percent
          : percent // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$Discount implements Discount {
  _$Discount(
      {this.id = 'unused', required this.details, required this.percent});

  @override
  @JsonKey()
  final String id;
  @override
  final String details;
  @override
  final double percent;

  @override
  String toString() {
    return 'CartEntry.discount(id: $id, details: $details, percent: $percent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Discount &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.details, details) &&
            const DeepCollectionEquality().equals(other.percent, percent));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(details),
      const DeepCollectionEquality().hash(percent));

  @JsonKey(ignore: true)
  @override
  _$$DiscountCopyWith<_$Discount> get copyWith =>
      __$$DiscountCopyWithImpl<_$Discount>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)
        product,
    required TResult Function(String id, String details, double percent)
        discount,
    required TResult Function(String id, String details, double cost) transport,
  }) {
    return discount(id, details, percent);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)?
        product,
    TResult Function(String id, String details, double percent)? discount,
    TResult Function(String id, String details, double cost)? transport,
  }) {
    return discount?.call(id, details, percent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)?
        product,
    TResult Function(String id, String details, double percent)? discount,
    TResult Function(String id, String details, double cost)? transport,
    required TResult orElse(),
  }) {
    if (discount != null) {
      return discount(id, details, percent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CartProduct value) product,
    required TResult Function(Discount value) discount,
    required TResult Function(Transport value) transport,
  }) {
    return discount(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CartProduct value)? product,
    TResult Function(Discount value)? discount,
    TResult Function(Transport value)? transport,
  }) {
    return discount?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CartProduct value)? product,
    TResult Function(Discount value)? discount,
    TResult Function(Transport value)? transport,
    required TResult orElse(),
  }) {
    if (discount != null) {
      return discount(this);
    }
    return orElse();
  }
}

abstract class Discount implements CartEntry {
  factory Discount(
      {final String id,
      required final String details,
      required final double percent}) = _$Discount;

  @override
  String get id;
  @override
  String get details;
  double get percent;
  @override
  @JsonKey(ignore: true)
  _$$DiscountCopyWith<_$Discount> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$TransportCopyWith<$Res> implements $CartEntryCopyWith<$Res> {
  factory _$$TransportCopyWith(
          _$Transport value, $Res Function(_$Transport) then) =
      __$$TransportCopyWithImpl<$Res>;
  @override
  $Res call({String id, String details, double cost});
}

/// @nodoc
class __$$TransportCopyWithImpl<$Res> extends _$CartEntryCopyWithImpl<$Res>
    implements _$$TransportCopyWith<$Res> {
  __$$TransportCopyWithImpl(
      _$Transport _value, $Res Function(_$Transport) _then)
      : super(_value, (v) => _then(v as _$Transport));

  @override
  _$Transport get _value => super._value as _$Transport;

  @override
  $Res call({
    Object? id = freezed,
    Object? details = freezed,
    Object? cost = freezed,
  }) {
    return _then(_$Transport(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      cost: cost == freezed
          ? _value.cost
          : cost // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$Transport implements Transport {
  _$Transport({this.id = 'unused', required this.details, required this.cost});

  @override
  @JsonKey()
  final String id;
  @override
  final String details;
  @override
  final double cost;

  @override
  String toString() {
    return 'CartEntry.transport(id: $id, details: $details, cost: $cost)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Transport &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.details, details) &&
            const DeepCollectionEquality().equals(other.cost, cost));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(details),
      const DeepCollectionEquality().hash(cost));

  @JsonKey(ignore: true)
  @override
  _$$TransportCopyWith<_$Transport> get copyWith =>
      __$$TransportCopyWithImpl<_$Transport>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)
        product,
    required TResult Function(String id, String details, double percent)
        discount,
    required TResult Function(String id, String details, double cost) transport,
  }) {
    return transport(id, details, cost);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)?
        product,
    TResult Function(String id, String details, double percent)? discount,
    TResult Function(String id, String details, double cost)? transport,
  }) {
    return transport?.call(id, details, cost);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String productId, String participantId,
            String details, int numItems, double? modifiedPrice)?
        product,
    TResult Function(String id, String details, double percent)? discount,
    TResult Function(String id, String details, double cost)? transport,
    required TResult orElse(),
  }) {
    if (transport != null) {
      return transport(id, details, cost);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CartProduct value) product,
    required TResult Function(Discount value) discount,
    required TResult Function(Transport value) transport,
  }) {
    return transport(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CartProduct value)? product,
    TResult Function(Discount value)? discount,
    TResult Function(Transport value)? transport,
  }) {
    return transport?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CartProduct value)? product,
    TResult Function(Discount value)? discount,
    TResult Function(Transport value)? transport,
    required TResult orElse(),
  }) {
    if (transport != null) {
      return transport(this);
    }
    return orElse();
  }
}

abstract class Transport implements CartEntry {
  factory Transport(
      {final String id,
      required final String details,
      required final double cost}) = _$Transport;

  @override
  String get id;
  @override
  String get details;
  double get cost;
  @override
  @JsonKey(ignore: true)
  _$$TransportCopyWith<_$Transport> get copyWith =>
      throw _privateConstructorUsedError;
}
