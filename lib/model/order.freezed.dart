// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'order.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$Order {
  String get id => throw _privateConstructorUsedError;
  String get ownerId => throw _privateConstructorUsedError;
  String get ownerName => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get details => throw _privateConstructorUsedError;
  String get currency => throw _privateConstructorUsedError;
  OrderStatus get status => throw _privateConstructorUsedError;
  String get statusDetails => throw _privateConstructorUsedError;
  DateTime get creationDate => throw _privateConstructorUsedError;
  DateTime? get deliveryDate => throw _privateConstructorUsedError;
  DateTime get dueDate => throw _privateConstructorUsedError;
  List<String> get participantIds => throw _privateConstructorUsedError;
  bool get isDeleted => throw _privateConstructorUsedError;
  bool get hasFeatureEditProducts => throw _privateConstructorUsedError;
  bool get hasFeatureSeeParticipants => throw _privateConstructorUsedError;
  OrderType get type => throw _privateConstructorUsedError;
  String get inviteLink => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OrderCopyWith<Order> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderCopyWith<$Res> {
  factory $OrderCopyWith(Order value, $Res Function(Order) then) =
      _$OrderCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String ownerId,
      String ownerName,
      String title,
      String details,
      String currency,
      OrderStatus status,
      String statusDetails,
      DateTime creationDate,
      DateTime? deliveryDate,
      DateTime dueDate,
      List<String> participantIds,
      bool isDeleted,
      bool hasFeatureEditProducts,
      bool hasFeatureSeeParticipants,
      OrderType type,
      String inviteLink});
}

/// @nodoc
class _$OrderCopyWithImpl<$Res> implements $OrderCopyWith<$Res> {
  _$OrderCopyWithImpl(this._value, this._then);

  final Order _value;
  // ignore: unused_field
  final $Res Function(Order) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? ownerId = freezed,
    Object? ownerName = freezed,
    Object? title = freezed,
    Object? details = freezed,
    Object? currency = freezed,
    Object? status = freezed,
    Object? statusDetails = freezed,
    Object? creationDate = freezed,
    Object? deliveryDate = freezed,
    Object? dueDate = freezed,
    Object? participantIds = freezed,
    Object? isDeleted = freezed,
    Object? hasFeatureEditProducts = freezed,
    Object? hasFeatureSeeParticipants = freezed,
    Object? type = freezed,
    Object? inviteLink = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      ownerId: ownerId == freezed
          ? _value.ownerId
          : ownerId // ignore: cast_nullable_to_non_nullable
              as String,
      ownerName: ownerName == freezed
          ? _value.ownerName
          : ownerName // ignore: cast_nullable_to_non_nullable
              as String,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as OrderStatus,
      statusDetails: statusDetails == freezed
          ? _value.statusDetails
          : statusDetails // ignore: cast_nullable_to_non_nullable
              as String,
      creationDate: creationDate == freezed
          ? _value.creationDate
          : creationDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      deliveryDate: deliveryDate == freezed
          ? _value.deliveryDate
          : deliveryDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      dueDate: dueDate == freezed
          ? _value.dueDate
          : dueDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      participantIds: participantIds == freezed
          ? _value.participantIds
          : participantIds // ignore: cast_nullable_to_non_nullable
              as List<String>,
      isDeleted: isDeleted == freezed
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as bool,
      hasFeatureEditProducts: hasFeatureEditProducts == freezed
          ? _value.hasFeatureEditProducts
          : hasFeatureEditProducts // ignore: cast_nullable_to_non_nullable
              as bool,
      hasFeatureSeeParticipants: hasFeatureSeeParticipants == freezed
          ? _value.hasFeatureSeeParticipants
          : hasFeatureSeeParticipants // ignore: cast_nullable_to_non_nullable
              as bool,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as OrderType,
      inviteLink: inviteLink == freezed
          ? _value.inviteLink
          : inviteLink // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_OrderCopyWith<$Res> implements $OrderCopyWith<$Res> {
  factory _$$_OrderCopyWith(_$_Order value, $Res Function(_$_Order) then) =
      __$$_OrderCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String ownerId,
      String ownerName,
      String title,
      String details,
      String currency,
      OrderStatus status,
      String statusDetails,
      DateTime creationDate,
      DateTime? deliveryDate,
      DateTime dueDate,
      List<String> participantIds,
      bool isDeleted,
      bool hasFeatureEditProducts,
      bool hasFeatureSeeParticipants,
      OrderType type,
      String inviteLink});
}

/// @nodoc
class __$$_OrderCopyWithImpl<$Res> extends _$OrderCopyWithImpl<$Res>
    implements _$$_OrderCopyWith<$Res> {
  __$$_OrderCopyWithImpl(_$_Order _value, $Res Function(_$_Order) _then)
      : super(_value, (v) => _then(v as _$_Order));

  @override
  _$_Order get _value => super._value as _$_Order;

  @override
  $Res call({
    Object? id = freezed,
    Object? ownerId = freezed,
    Object? ownerName = freezed,
    Object? title = freezed,
    Object? details = freezed,
    Object? currency = freezed,
    Object? status = freezed,
    Object? statusDetails = freezed,
    Object? creationDate = freezed,
    Object? deliveryDate = freezed,
    Object? dueDate = freezed,
    Object? participantIds = freezed,
    Object? isDeleted = freezed,
    Object? hasFeatureEditProducts = freezed,
    Object? hasFeatureSeeParticipants = freezed,
    Object? type = freezed,
    Object? inviteLink = freezed,
  }) {
    return _then(_$_Order(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      ownerId: ownerId == freezed
          ? _value.ownerId
          : ownerId // ignore: cast_nullable_to_non_nullable
              as String,
      ownerName: ownerName == freezed
          ? _value.ownerName
          : ownerName // ignore: cast_nullable_to_non_nullable
              as String,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as OrderStatus,
      statusDetails: statusDetails == freezed
          ? _value.statusDetails
          : statusDetails // ignore: cast_nullable_to_non_nullable
              as String,
      creationDate: creationDate == freezed
          ? _value.creationDate
          : creationDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      deliveryDate: deliveryDate == freezed
          ? _value.deliveryDate
          : deliveryDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      dueDate: dueDate == freezed
          ? _value.dueDate
          : dueDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      participantIds: participantIds == freezed
          ? _value._participantIds
          : participantIds // ignore: cast_nullable_to_non_nullable
              as List<String>,
      isDeleted: isDeleted == freezed
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as bool,
      hasFeatureEditProducts: hasFeatureEditProducts == freezed
          ? _value.hasFeatureEditProducts
          : hasFeatureEditProducts // ignore: cast_nullable_to_non_nullable
              as bool,
      hasFeatureSeeParticipants: hasFeatureSeeParticipants == freezed
          ? _value.hasFeatureSeeParticipants
          : hasFeatureSeeParticipants // ignore: cast_nullable_to_non_nullable
              as bool,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as OrderType,
      inviteLink: inviteLink == freezed
          ? _value.inviteLink
          : inviteLink // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Order extends _Order {
  _$_Order(
      {required this.id,
      required this.ownerId,
      required this.ownerName,
      required this.title,
      required this.details,
      required this.currency,
      required this.status,
      required this.statusDetails,
      required this.creationDate,
      this.deliveryDate,
      required this.dueDate,
      required final List<String> participantIds,
      required this.isDeleted,
      required this.hasFeatureEditProducts,
      required this.hasFeatureSeeParticipants,
      required this.type,
      required this.inviteLink})
      : _participantIds = participantIds,
        super._();

  @override
  final String id;
  @override
  final String ownerId;
  @override
  final String ownerName;
  @override
  final String title;
  @override
  final String details;
  @override
  final String currency;
  @override
  final OrderStatus status;
  @override
  final String statusDetails;
  @override
  final DateTime creationDate;
  @override
  final DateTime? deliveryDate;
  @override
  final DateTime dueDate;
  final List<String> _participantIds;
  @override
  List<String> get participantIds {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_participantIds);
  }

  @override
  final bool isDeleted;
  @override
  final bool hasFeatureEditProducts;
  @override
  final bool hasFeatureSeeParticipants;
  @override
  final OrderType type;
  @override
  final String inviteLink;

  @override
  String toString() {
    return 'Order(id: $id, ownerId: $ownerId, ownerName: $ownerName, title: $title, details: $details, currency: $currency, status: $status, statusDetails: $statusDetails, creationDate: $creationDate, deliveryDate: $deliveryDate, dueDate: $dueDate, participantIds: $participantIds, isDeleted: $isDeleted, hasFeatureEditProducts: $hasFeatureEditProducts, hasFeatureSeeParticipants: $hasFeatureSeeParticipants, type: $type, inviteLink: $inviteLink)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Order &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.ownerId, ownerId) &&
            const DeepCollectionEquality().equals(other.ownerName, ownerName) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.details, details) &&
            const DeepCollectionEquality().equals(other.currency, currency) &&
            const DeepCollectionEquality().equals(other.status, status) &&
            const DeepCollectionEquality()
                .equals(other.statusDetails, statusDetails) &&
            const DeepCollectionEquality()
                .equals(other.creationDate, creationDate) &&
            const DeepCollectionEquality()
                .equals(other.deliveryDate, deliveryDate) &&
            const DeepCollectionEquality().equals(other.dueDate, dueDate) &&
            const DeepCollectionEquality()
                .equals(other._participantIds, _participantIds) &&
            const DeepCollectionEquality().equals(other.isDeleted, isDeleted) &&
            const DeepCollectionEquality()
                .equals(other.hasFeatureEditProducts, hasFeatureEditProducts) &&
            const DeepCollectionEquality().equals(
                other.hasFeatureSeeParticipants, hasFeatureSeeParticipants) &&
            const DeepCollectionEquality().equals(other.type, type) &&
            const DeepCollectionEquality()
                .equals(other.inviteLink, inviteLink));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(ownerId),
      const DeepCollectionEquality().hash(ownerName),
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(details),
      const DeepCollectionEquality().hash(currency),
      const DeepCollectionEquality().hash(status),
      const DeepCollectionEquality().hash(statusDetails),
      const DeepCollectionEquality().hash(creationDate),
      const DeepCollectionEquality().hash(deliveryDate),
      const DeepCollectionEquality().hash(dueDate),
      const DeepCollectionEquality().hash(_participantIds),
      const DeepCollectionEquality().hash(isDeleted),
      const DeepCollectionEquality().hash(hasFeatureEditProducts),
      const DeepCollectionEquality().hash(hasFeatureSeeParticipants),
      const DeepCollectionEquality().hash(type),
      const DeepCollectionEquality().hash(inviteLink));

  @JsonKey(ignore: true)
  @override
  _$$_OrderCopyWith<_$_Order> get copyWith =>
      __$$_OrderCopyWithImpl<_$_Order>(this, _$identity);
}

abstract class _Order extends Order {
  factory _Order(
      {required final String id,
      required final String ownerId,
      required final String ownerName,
      required final String title,
      required final String details,
      required final String currency,
      required final OrderStatus status,
      required final String statusDetails,
      required final DateTime creationDate,
      final DateTime? deliveryDate,
      required final DateTime dueDate,
      required final List<String> participantIds,
      required final bool isDeleted,
      required final bool hasFeatureEditProducts,
      required final bool hasFeatureSeeParticipants,
      required final OrderType type,
      required final String inviteLink}) = _$_Order;
  _Order._() : super._();

  @override
  String get id;
  @override
  String get ownerId;
  @override
  String get ownerName;
  @override
  String get title;
  @override
  String get details;
  @override
  String get currency;
  @override
  OrderStatus get status;
  @override
  String get statusDetails;
  @override
  DateTime get creationDate;
  @override
  DateTime? get deliveryDate;
  @override
  DateTime get dueDate;
  @override
  List<String> get participantIds;
  @override
  bool get isDeleted;
  @override
  bool get hasFeatureEditProducts;
  @override
  bool get hasFeatureSeeParticipants;
  @override
  OrderType get type;
  @override
  String get inviteLink;
  @override
  @JsonKey(ignore: true)
  _$$_OrderCopyWith<_$_Order> get copyWith =>
      throw _privateConstructorUsedError;
}
