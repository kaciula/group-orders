import 'package:freezed_annotation/freezed_annotation.dart';

part 'order.freezed.dart';

@freezed
class Order with _$Order {
  factory Order({
    required String id,
    required String ownerId,
    required String ownerName,
    required String title,
    required String details,
    required String currency,
    required OrderStatus status,
    required String statusDetails,
    required DateTime creationDate,
    DateTime? deliveryDate,
    required DateTime dueDate,
    required List<String> participantIds,
    required bool isDeleted,
    required bool hasFeatureEditProducts,
    required bool hasFeatureSeeParticipants,
    required OrderType type,
    required String inviteLink,
  }) = _Order;

  Order._();

  factory Order.empty(String id) {
    final DateTime now = DateTime.now();
    return Order(
      id: id,
      ownerId: '',
      ownerName: '',
      title: '',
      details: '',
      currency: '',
      status: OrderStatus.inProgress,
      statusDetails: '',
      creationDate: now,
      deliveryDate: null,
      dueDate: now.add(const Duration(days: 3)),
      participantIds: <String>[],
      isDeleted: false,
      hasFeatureEditProducts: false,
      hasFeatureSeeParticipants: false,
      type: OrderType.proxy,
      inviteLink: '',
    );
  }

  bool get isActive =>
      status != OrderStatus.completed && status != OrderStatus.forcefullyClosed;

  bool get isProxy => type == OrderType.proxy;

  bool get isProvider => type == OrderType.provider;
}

enum OrderStatus {
  inProgress,
  blocked,
  madeUpstream,
  arrived,
  completed,
  forcefullyClosed
}

enum OrderType { proxy, provider }
