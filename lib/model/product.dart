import 'package:freezed_annotation/freezed_annotation.dart';

part 'product.freezed.dart';

@freezed
class Product with _$Product {
  factory Product({
    required String id,
    required String title,
    required String details,
    required double price,
    required String currency,
    required Stock stock,
  }) = _Product;

  Product._();

  bool get hasItemsInStock =>
      stock is Unlimited ||
      (stock is Limited && (stock as Limited).numItemsAvailable > 0);
}

@freezed
class Stock with _$Stock {
  factory Stock.unlimited() = Unlimited;

  factory Stock.limited({required int numItemsAvailable}) = Limited;
}
