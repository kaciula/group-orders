// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'participant.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$Participant {
  String get userId => throw _privateConstructorUsedError;
  String get displayName => throw _privateConstructorUsedError;
  String get phoneNumber => throw _privateConstructorUsedError;
  bool get delivered => throw _privateConstructorUsedError;
  bool get confirmedDelivered => throw _privateConstructorUsedError;
  bool get paid => throw _privateConstructorUsedError;
  bool get confirmedPaid => throw _privateConstructorUsedError;
  String get paidDetails => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ParticipantCopyWith<Participant> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ParticipantCopyWith<$Res> {
  factory $ParticipantCopyWith(
          Participant value, $Res Function(Participant) then) =
      _$ParticipantCopyWithImpl<$Res>;
  $Res call(
      {String userId,
      String displayName,
      String phoneNumber,
      bool delivered,
      bool confirmedDelivered,
      bool paid,
      bool confirmedPaid,
      String paidDetails});
}

/// @nodoc
class _$ParticipantCopyWithImpl<$Res> implements $ParticipantCopyWith<$Res> {
  _$ParticipantCopyWithImpl(this._value, this._then);

  final Participant _value;
  // ignore: unused_field
  final $Res Function(Participant) _then;

  @override
  $Res call({
    Object? userId = freezed,
    Object? displayName = freezed,
    Object? phoneNumber = freezed,
    Object? delivered = freezed,
    Object? confirmedDelivered = freezed,
    Object? paid = freezed,
    Object? confirmedPaid = freezed,
    Object? paidDetails = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      delivered: delivered == freezed
          ? _value.delivered
          : delivered // ignore: cast_nullable_to_non_nullable
              as bool,
      confirmedDelivered: confirmedDelivered == freezed
          ? _value.confirmedDelivered
          : confirmedDelivered // ignore: cast_nullable_to_non_nullable
              as bool,
      paid: paid == freezed
          ? _value.paid
          : paid // ignore: cast_nullable_to_non_nullable
              as bool,
      confirmedPaid: confirmedPaid == freezed
          ? _value.confirmedPaid
          : confirmedPaid // ignore: cast_nullable_to_non_nullable
              as bool,
      paidDetails: paidDetails == freezed
          ? _value.paidDetails
          : paidDetails // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_ParticipantCopyWith<$Res>
    implements $ParticipantCopyWith<$Res> {
  factory _$$_ParticipantCopyWith(
          _$_Participant value, $Res Function(_$_Participant) then) =
      __$$_ParticipantCopyWithImpl<$Res>;
  @override
  $Res call(
      {String userId,
      String displayName,
      String phoneNumber,
      bool delivered,
      bool confirmedDelivered,
      bool paid,
      bool confirmedPaid,
      String paidDetails});
}

/// @nodoc
class __$$_ParticipantCopyWithImpl<$Res> extends _$ParticipantCopyWithImpl<$Res>
    implements _$$_ParticipantCopyWith<$Res> {
  __$$_ParticipantCopyWithImpl(
      _$_Participant _value, $Res Function(_$_Participant) _then)
      : super(_value, (v) => _then(v as _$_Participant));

  @override
  _$_Participant get _value => super._value as _$_Participant;

  @override
  $Res call({
    Object? userId = freezed,
    Object? displayName = freezed,
    Object? phoneNumber = freezed,
    Object? delivered = freezed,
    Object? confirmedDelivered = freezed,
    Object? paid = freezed,
    Object? confirmedPaid = freezed,
    Object? paidDetails = freezed,
  }) {
    return _then(_$_Participant(
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      displayName: displayName == freezed
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      delivered: delivered == freezed
          ? _value.delivered
          : delivered // ignore: cast_nullable_to_non_nullable
              as bool,
      confirmedDelivered: confirmedDelivered == freezed
          ? _value.confirmedDelivered
          : confirmedDelivered // ignore: cast_nullable_to_non_nullable
              as bool,
      paid: paid == freezed
          ? _value.paid
          : paid // ignore: cast_nullable_to_non_nullable
              as bool,
      confirmedPaid: confirmedPaid == freezed
          ? _value.confirmedPaid
          : confirmedPaid // ignore: cast_nullable_to_non_nullable
              as bool,
      paidDetails: paidDetails == freezed
          ? _value.paidDetails
          : paidDetails // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Participant extends _Participant {
  _$_Participant(
      {required this.userId,
      required this.displayName,
      required this.phoneNumber,
      required this.delivered,
      required this.confirmedDelivered,
      required this.paid,
      required this.confirmedPaid,
      required this.paidDetails})
      : super._();

  @override
  final String userId;
  @override
  final String displayName;
  @override
  final String phoneNumber;
  @override
  final bool delivered;
  @override
  final bool confirmedDelivered;
  @override
  final bool paid;
  @override
  final bool confirmedPaid;
  @override
  final String paidDetails;

  @override
  String toString() {
    return 'Participant(userId: $userId, displayName: $displayName, phoneNumber: $phoneNumber, delivered: $delivered, confirmedDelivered: $confirmedDelivered, paid: $paid, confirmedPaid: $confirmedPaid, paidDetails: $paidDetails)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Participant &&
            const DeepCollectionEquality().equals(other.userId, userId) &&
            const DeepCollectionEquality()
                .equals(other.displayName, displayName) &&
            const DeepCollectionEquality()
                .equals(other.phoneNumber, phoneNumber) &&
            const DeepCollectionEquality().equals(other.delivered, delivered) &&
            const DeepCollectionEquality()
                .equals(other.confirmedDelivered, confirmedDelivered) &&
            const DeepCollectionEquality().equals(other.paid, paid) &&
            const DeepCollectionEquality()
                .equals(other.confirmedPaid, confirmedPaid) &&
            const DeepCollectionEquality()
                .equals(other.paidDetails, paidDetails));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(userId),
      const DeepCollectionEquality().hash(displayName),
      const DeepCollectionEquality().hash(phoneNumber),
      const DeepCollectionEquality().hash(delivered),
      const DeepCollectionEquality().hash(confirmedDelivered),
      const DeepCollectionEquality().hash(paid),
      const DeepCollectionEquality().hash(confirmedPaid),
      const DeepCollectionEquality().hash(paidDetails));

  @JsonKey(ignore: true)
  @override
  _$$_ParticipantCopyWith<_$_Participant> get copyWith =>
      __$$_ParticipantCopyWithImpl<_$_Participant>(this, _$identity);
}

abstract class _Participant extends Participant {
  factory _Participant(
      {required final String userId,
      required final String displayName,
      required final String phoneNumber,
      required final bool delivered,
      required final bool confirmedDelivered,
      required final bool paid,
      required final bool confirmedPaid,
      required final String paidDetails}) = _$_Participant;
  _Participant._() : super._();

  @override
  String get userId;
  @override
  String get displayName;
  @override
  String get phoneNumber;
  @override
  bool get delivered;
  @override
  bool get confirmedDelivered;
  @override
  bool get paid;
  @override
  bool get confirmedPaid;
  @override
  String get paidDetails;
  @override
  @JsonKey(ignore: true)
  _$$_ParticipantCopyWith<_$_Participant> get copyWith =>
      throw _privateConstructorUsedError;
}
