import 'package:orders/app/app_strings.dart';

// ignore: avoid_classes_with_only_static_members
class Validators {
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  static bool isEmailValid(String? value) {
    if (value == null) {
      return false;
    }
    return _emailRegExp.hasMatch(value);
  }

  static bool isPasswordValid(String? value) {
    if (value == null) {
      return false;
    }
    return value.length >= 6;
  }

  static String? price({required String? value, required String invalidMsg}) {
    if (value?.trim().isEmpty ?? true) {
      return Strings.genericRequired;
    }
    final double? val =
        double.tryParse(value?.replaceFirst(',', '.').trim() ?? '');
    if (val == null) {
      return invalidMsg;
    }
    if (val <= 0) {
      return invalidMsg;
    }
    return null;
  }
}
