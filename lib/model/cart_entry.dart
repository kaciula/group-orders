import 'package:freezed_annotation/freezed_annotation.dart';

part 'cart_entry.freezed.dart';

@freezed
class CartEntry with _$CartEntry {
  factory CartEntry.product({
    required String id,
    required String productId,
    required String participantId,
    required String details,
    required int numItems,
    double? modifiedPrice,
  }) = CartProduct;

  factory CartEntry.discount({
    @Default('unused') String id,
    required String details,
    required double percent,
  }) = Discount;

  factory CartEntry.transport({
    @Default('unused') String id,
    required String details,
    required double cost,
  }) = Transport;
}
