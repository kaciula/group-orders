// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$Product {
  String get id => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get details => throw _privateConstructorUsedError;
  double get price => throw _privateConstructorUsedError;
  String get currency => throw _privateConstructorUsedError;
  Stock get stock => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProductCopyWith<Product> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductCopyWith<$Res> {
  factory $ProductCopyWith(Product value, $Res Function(Product) then) =
      _$ProductCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String title,
      String details,
      double price,
      String currency,
      Stock stock});

  $StockCopyWith<$Res> get stock;
}

/// @nodoc
class _$ProductCopyWithImpl<$Res> implements $ProductCopyWith<$Res> {
  _$ProductCopyWithImpl(this._value, this._then);

  final Product _value;
  // ignore: unused_field
  final $Res Function(Product) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? title = freezed,
    Object? details = freezed,
    Object? price = freezed,
    Object? currency = freezed,
    Object? stock = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      stock: stock == freezed
          ? _value.stock
          : stock // ignore: cast_nullable_to_non_nullable
              as Stock,
    ));
  }

  @override
  $StockCopyWith<$Res> get stock {
    return $StockCopyWith<$Res>(_value.stock, (value) {
      return _then(_value.copyWith(stock: value));
    });
  }
}

/// @nodoc
abstract class _$$_ProductCopyWith<$Res> implements $ProductCopyWith<$Res> {
  factory _$$_ProductCopyWith(
          _$_Product value, $Res Function(_$_Product) then) =
      __$$_ProductCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String title,
      String details,
      double price,
      String currency,
      Stock stock});

  @override
  $StockCopyWith<$Res> get stock;
}

/// @nodoc
class __$$_ProductCopyWithImpl<$Res> extends _$ProductCopyWithImpl<$Res>
    implements _$$_ProductCopyWith<$Res> {
  __$$_ProductCopyWithImpl(_$_Product _value, $Res Function(_$_Product) _then)
      : super(_value, (v) => _then(v as _$_Product));

  @override
  _$_Product get _value => super._value as _$_Product;

  @override
  $Res call({
    Object? id = freezed,
    Object? title = freezed,
    Object? details = freezed,
    Object? price = freezed,
    Object? currency = freezed,
    Object? stock = freezed,
  }) {
    return _then(_$_Product(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      details: details == freezed
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      stock: stock == freezed
          ? _value.stock
          : stock // ignore: cast_nullable_to_non_nullable
              as Stock,
    ));
  }
}

/// @nodoc

class _$_Product extends _Product {
  _$_Product(
      {required this.id,
      required this.title,
      required this.details,
      required this.price,
      required this.currency,
      required this.stock})
      : super._();

  @override
  final String id;
  @override
  final String title;
  @override
  final String details;
  @override
  final double price;
  @override
  final String currency;
  @override
  final Stock stock;

  @override
  String toString() {
    return 'Product(id: $id, title: $title, details: $details, price: $price, currency: $currency, stock: $stock)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Product &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.details, details) &&
            const DeepCollectionEquality().equals(other.price, price) &&
            const DeepCollectionEquality().equals(other.currency, currency) &&
            const DeepCollectionEquality().equals(other.stock, stock));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(details),
      const DeepCollectionEquality().hash(price),
      const DeepCollectionEquality().hash(currency),
      const DeepCollectionEquality().hash(stock));

  @JsonKey(ignore: true)
  @override
  _$$_ProductCopyWith<_$_Product> get copyWith =>
      __$$_ProductCopyWithImpl<_$_Product>(this, _$identity);
}

abstract class _Product extends Product {
  factory _Product(
      {required final String id,
      required final String title,
      required final String details,
      required final double price,
      required final String currency,
      required final Stock stock}) = _$_Product;
  _Product._() : super._();

  @override
  String get id;
  @override
  String get title;
  @override
  String get details;
  @override
  double get price;
  @override
  String get currency;
  @override
  Stock get stock;
  @override
  @JsonKey(ignore: true)
  _$$_ProductCopyWith<_$_Product> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$Stock {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unlimited,
    required TResult Function(int numItemsAvailable) limited,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unlimited,
    TResult Function(int numItemsAvailable)? limited,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unlimited,
    TResult Function(int numItemsAvailable)? limited,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Unlimited value) unlimited,
    required TResult Function(Limited value) limited,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Unlimited value)? unlimited,
    TResult Function(Limited value)? limited,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Unlimited value)? unlimited,
    TResult Function(Limited value)? limited,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StockCopyWith<$Res> {
  factory $StockCopyWith(Stock value, $Res Function(Stock) then) =
      _$StockCopyWithImpl<$Res>;
}

/// @nodoc
class _$StockCopyWithImpl<$Res> implements $StockCopyWith<$Res> {
  _$StockCopyWithImpl(this._value, this._then);

  final Stock _value;
  // ignore: unused_field
  final $Res Function(Stock) _then;
}

/// @nodoc
abstract class _$$UnlimitedCopyWith<$Res> {
  factory _$$UnlimitedCopyWith(
          _$Unlimited value, $Res Function(_$Unlimited) then) =
      __$$UnlimitedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UnlimitedCopyWithImpl<$Res> extends _$StockCopyWithImpl<$Res>
    implements _$$UnlimitedCopyWith<$Res> {
  __$$UnlimitedCopyWithImpl(
      _$Unlimited _value, $Res Function(_$Unlimited) _then)
      : super(_value, (v) => _then(v as _$Unlimited));

  @override
  _$Unlimited get _value => super._value as _$Unlimited;
}

/// @nodoc

class _$Unlimited implements Unlimited {
  _$Unlimited();

  @override
  String toString() {
    return 'Stock.unlimited()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Unlimited);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unlimited,
    required TResult Function(int numItemsAvailable) limited,
  }) {
    return unlimited();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unlimited,
    TResult Function(int numItemsAvailable)? limited,
  }) {
    return unlimited?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unlimited,
    TResult Function(int numItemsAvailable)? limited,
    required TResult orElse(),
  }) {
    if (unlimited != null) {
      return unlimited();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Unlimited value) unlimited,
    required TResult Function(Limited value) limited,
  }) {
    return unlimited(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Unlimited value)? unlimited,
    TResult Function(Limited value)? limited,
  }) {
    return unlimited?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Unlimited value)? unlimited,
    TResult Function(Limited value)? limited,
    required TResult orElse(),
  }) {
    if (unlimited != null) {
      return unlimited(this);
    }
    return orElse();
  }
}

abstract class Unlimited implements Stock {
  factory Unlimited() = _$Unlimited;
}

/// @nodoc
abstract class _$$LimitedCopyWith<$Res> {
  factory _$$LimitedCopyWith(_$Limited value, $Res Function(_$Limited) then) =
      __$$LimitedCopyWithImpl<$Res>;
  $Res call({int numItemsAvailable});
}

/// @nodoc
class __$$LimitedCopyWithImpl<$Res> extends _$StockCopyWithImpl<$Res>
    implements _$$LimitedCopyWith<$Res> {
  __$$LimitedCopyWithImpl(_$Limited _value, $Res Function(_$Limited) _then)
      : super(_value, (v) => _then(v as _$Limited));

  @override
  _$Limited get _value => super._value as _$Limited;

  @override
  $Res call({
    Object? numItemsAvailable = freezed,
  }) {
    return _then(_$Limited(
      numItemsAvailable: numItemsAvailable == freezed
          ? _value.numItemsAvailable
          : numItemsAvailable // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$Limited implements Limited {
  _$Limited({required this.numItemsAvailable});

  @override
  final int numItemsAvailable;

  @override
  String toString() {
    return 'Stock.limited(numItemsAvailable: $numItemsAvailable)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Limited &&
            const DeepCollectionEquality()
                .equals(other.numItemsAvailable, numItemsAvailable));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(numItemsAvailable));

  @JsonKey(ignore: true)
  @override
  _$$LimitedCopyWith<_$Limited> get copyWith =>
      __$$LimitedCopyWithImpl<_$Limited>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unlimited,
    required TResult Function(int numItemsAvailable) limited,
  }) {
    return limited(numItemsAvailable);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unlimited,
    TResult Function(int numItemsAvailable)? limited,
  }) {
    return limited?.call(numItemsAvailable);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unlimited,
    TResult Function(int numItemsAvailable)? limited,
    required TResult orElse(),
  }) {
    if (limited != null) {
      return limited(numItemsAvailable);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Unlimited value) unlimited,
    required TResult Function(Limited value) limited,
  }) {
    return limited(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Unlimited value)? unlimited,
    TResult Function(Limited value)? limited,
  }) {
    return limited?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Unlimited value)? unlimited,
    TResult Function(Limited value)? limited,
    required TResult orElse(),
  }) {
    if (limited != null) {
      return limited(this);
    }
    return orElse();
  }
}

abstract class Limited implements Stock {
  factory Limited({required final int numItemsAvailable}) = _$Limited;

  int get numItemsAvailable;
  @JsonKey(ignore: true)
  _$$LimitedCopyWith<_$Limited> get copyWith =>
      throw _privateConstructorUsedError;
}
