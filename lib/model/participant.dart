import 'package:freezed_annotation/freezed_annotation.dart';

part 'participant.freezed.dart';

@freezed
class Participant with _$Participant {
  factory Participant({
    required String userId,
    required String displayName,
    required String phoneNumber,
    required bool delivered,
    required bool confirmedDelivered,
    required bool paid,
    required bool confirmedPaid,
    required String paidDetails,
  }) = _Participant;

  Participant._();

  factory Participant.initial({
    required String id,
    required String displayName,
    required String phoneNumber,
  }) =>
      Participant(
        userId: id,
        displayName: displayName,
        phoneNumber: phoneNumber,
        delivered: false,
        confirmedDelivered: false,
        paid: false,
        confirmedPaid: false,
        paidDetails: '',
      );

  bool get hasPhoneNumber => phoneNumber.isNotEmpty;
}
