import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

@freezed
class User with _$User {
  factory User({
    required String id,
    required String displayName,
    required String emailAddress,
    required String phoneNumber,
    required SignInProvider signInProvider,
    required String languageCode,
    required String messagingToken,
  }) = _User;

  User._();

  factory User.basic({
    required String id,
    required String displayName,
    required String emailAddress,
    required SignInProvider signInProvider,
  }) =>
      User(
        id: id,
        displayName: displayName,
        emailAddress: emailAddress,
        signInProvider: signInProvider,
        phoneNumber: '',
        languageCode: '',
        messagingToken: '',
      );

  factory User.anonymous() => User(
        id: _anonId,
        displayName: 'Anonymous',
        emailAddress: '',
        phoneNumber: '',
        signInProvider: SignInProvider.email,
        languageCode: '',
        messagingToken: '',
      );

  bool get isSignedIn => id != _anonId;
}

const String _anonId = '';

enum SignInProvider { apple, google, facebook, email }
