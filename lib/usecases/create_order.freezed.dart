// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'create_order.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CreateOrderResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateOrderSuccess value) success,
    required TResult Function(CreateOrderFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CreateOrderSuccess value)? success,
    TResult Function(CreateOrderFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateOrderSuccess value)? success,
    TResult Function(CreateOrderFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateOrderResultCopyWith<$Res> {
  factory $CreateOrderResultCopyWith(
          CreateOrderResult value, $Res Function(CreateOrderResult) then) =
      _$CreateOrderResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$CreateOrderResultCopyWithImpl<$Res>
    implements $CreateOrderResultCopyWith<$Res> {
  _$CreateOrderResultCopyWithImpl(this._value, this._then);

  final CreateOrderResult _value;
  // ignore: unused_field
  final $Res Function(CreateOrderResult) _then;
}

/// @nodoc
abstract class _$$CreateOrderSuccessCopyWith<$Res> {
  factory _$$CreateOrderSuccessCopyWith(_$CreateOrderSuccess value,
          $Res Function(_$CreateOrderSuccess) then) =
      __$$CreateOrderSuccessCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$CreateOrderSuccessCopyWithImpl<$Res>
    extends _$CreateOrderResultCopyWithImpl<$Res>
    implements _$$CreateOrderSuccessCopyWith<$Res> {
  __$$CreateOrderSuccessCopyWithImpl(
      _$CreateOrderSuccess _value, $Res Function(_$CreateOrderSuccess) _then)
      : super(_value, (v) => _then(v as _$CreateOrderSuccess));

  @override
  _$CreateOrderSuccess get _value => super._value as _$CreateOrderSuccess;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$CreateOrderSuccess(
      order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$CreateOrderSuccess implements CreateOrderSuccess {
  _$CreateOrderSuccess(this.order);

  @override
  final Order order;

  @override
  String toString() {
    return 'CreateOrderResult.success(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateOrderSuccess &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$CreateOrderSuccessCopyWith<_$CreateOrderSuccess> get copyWith =>
      __$$CreateOrderSuccessCopyWithImpl<_$CreateOrderSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) {
    return success(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) {
    return success?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateOrderSuccess value) success,
    required TResult Function(CreateOrderFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CreateOrderSuccess value)? success,
    TResult Function(CreateOrderFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateOrderSuccess value)? success,
    TResult Function(CreateOrderFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class CreateOrderSuccess implements CreateOrderResult {
  factory CreateOrderSuccess(final Order order) = _$CreateOrderSuccess;

  Order get order;
  @JsonKey(ignore: true)
  _$$CreateOrderSuccessCopyWith<_$CreateOrderSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CreateOrderFailureCopyWith<$Res> {
  factory _$$CreateOrderFailureCopyWith(_$CreateOrderFailure value,
          $Res Function(_$CreateOrderFailure) then) =
      __$$CreateOrderFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CreateOrderFailureCopyWithImpl<$Res>
    extends _$CreateOrderResultCopyWithImpl<$Res>
    implements _$$CreateOrderFailureCopyWith<$Res> {
  __$$CreateOrderFailureCopyWithImpl(
      _$CreateOrderFailure _value, $Res Function(_$CreateOrderFailure) _then)
      : super(_value, (v) => _then(v as _$CreateOrderFailure));

  @override
  _$CreateOrderFailure get _value => super._value as _$CreateOrderFailure;
}

/// @nodoc

class _$CreateOrderFailure implements CreateOrderFailure {
  _$CreateOrderFailure();

  @override
  String toString() {
    return 'CreateOrderResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CreateOrderFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateOrderSuccess value) success,
    required TResult Function(CreateOrderFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CreateOrderSuccess value)? success,
    TResult Function(CreateOrderFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateOrderSuccess value)? success,
    TResult Function(CreateOrderFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class CreateOrderFailure implements CreateOrderResult {
  factory CreateOrderFailure() = _$CreateOrderFailure;
}
