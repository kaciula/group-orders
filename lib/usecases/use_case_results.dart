import 'package:freezed_annotation/freezed_annotation.dart';

part 'use_case_results.freezed.dart';

@freezed
class UseCaseResult with _$UseCaseResult {
  factory UseCaseResult.success() = UseCaseSuccess;

  factory UseCaseResult.failure() = UseCaseFailure;
}
