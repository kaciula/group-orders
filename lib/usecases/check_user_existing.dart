import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/user.dart';

part 'check_user_existing.freezed.dart';

class CheckUserExisting {
  CheckUserExisting(this.dataRemoteStore, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<CheckUserExistingResult> call(User user) async {
    if (dataBloc.isInternetConnected) {
      final HasAccountResult hasAccountResult =
          await dataRemoteStore.hasAccount(user.id);
      if (hasAccountResult is HasAccountSuccess) {
        return CheckUserExistingSuccess(
            userExists: hasAccountResult.hasAccount);
      }
    }
    return CheckUserExistingFailure();
  }
}

@freezed
class CheckUserExistingResult with _$CheckUserExistingResult {
  factory CheckUserExistingResult.success({required bool userExists}) =
      CheckUserExistingSuccess;

  factory CheckUserExistingResult.failure() = CheckUserExistingFailure;
}

// ignore: unused_element
final Logger _logger = Logger('CheckUserExisting');
