// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'load_order_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LoadOrderDataResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)
        success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadOrderDataSuccess value) success,
    required TResult Function(LoadOrderDataFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LoadOrderDataSuccess value)? success,
    TResult Function(LoadOrderDataFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadOrderDataSuccess value)? success,
    TResult Function(LoadOrderDataFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoadOrderDataResultCopyWith<$Res> {
  factory $LoadOrderDataResultCopyWith(
          LoadOrderDataResult value, $Res Function(LoadOrderDataResult) then) =
      _$LoadOrderDataResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadOrderDataResultCopyWithImpl<$Res>
    implements $LoadOrderDataResultCopyWith<$Res> {
  _$LoadOrderDataResultCopyWithImpl(this._value, this._then);

  final LoadOrderDataResult _value;
  // ignore: unused_field
  final $Res Function(LoadOrderDataResult) _then;
}

/// @nodoc
abstract class _$$LoadOrderDataSuccessCopyWith<$Res> {
  factory _$$LoadOrderDataSuccessCopyWith(_$LoadOrderDataSuccess value,
          $Res Function(_$LoadOrderDataSuccess) then) =
      __$$LoadOrderDataSuccessCopyWithImpl<$Res>;
  $Res call(
      {Order order,
      Participant owner,
      KtList<Participant> participants,
      KtList<Product> products,
      KtList<CartEntry> cart});

  $OrderCopyWith<$Res> get order;
  $ParticipantCopyWith<$Res> get owner;
}

/// @nodoc
class __$$LoadOrderDataSuccessCopyWithImpl<$Res>
    extends _$LoadOrderDataResultCopyWithImpl<$Res>
    implements _$$LoadOrderDataSuccessCopyWith<$Res> {
  __$$LoadOrderDataSuccessCopyWithImpl(_$LoadOrderDataSuccess _value,
      $Res Function(_$LoadOrderDataSuccess) _then)
      : super(_value, (v) => _then(v as _$LoadOrderDataSuccess));

  @override
  _$LoadOrderDataSuccess get _value => super._value as _$LoadOrderDataSuccess;

  @override
  $Res call({
    Object? order = freezed,
    Object? owner = freezed,
    Object? participants = freezed,
    Object? products = freezed,
    Object? cart = freezed,
  }) {
    return _then(_$LoadOrderDataSuccess(
      order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
      owner == freezed
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as Participant,
      participants == freezed
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as KtList<Participant>,
      products == freezed
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as KtList<Product>,
      cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as KtList<CartEntry>,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $ParticipantCopyWith<$Res> get owner {
    return $ParticipantCopyWith<$Res>(_value.owner, (value) {
      return _then(_value.copyWith(owner: value));
    });
  }
}

/// @nodoc

class _$LoadOrderDataSuccess implements LoadOrderDataSuccess {
  _$LoadOrderDataSuccess(
      this.order, this.owner, this.participants, this.products, this.cart);

  @override
  final Order order;
  @override
  final Participant owner;
  @override
  final KtList<Participant> participants;
  @override
  final KtList<Product> products;
  @override
  final KtList<CartEntry> cart;

  @override
  String toString() {
    return 'LoadOrderDataResult.success(order: $order, owner: $owner, participants: $participants, products: $products, cart: $cart)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadOrderDataSuccess &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality().equals(other.owner, owner) &&
            const DeepCollectionEquality()
                .equals(other.participants, participants) &&
            const DeepCollectionEquality().equals(other.products, products) &&
            const DeepCollectionEquality().equals(other.cart, cart));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(owner),
      const DeepCollectionEquality().hash(participants),
      const DeepCollectionEquality().hash(products),
      const DeepCollectionEquality().hash(cart));

  @JsonKey(ignore: true)
  @override
  _$$LoadOrderDataSuccessCopyWith<_$LoadOrderDataSuccess> get copyWith =>
      __$$LoadOrderDataSuccessCopyWithImpl<_$LoadOrderDataSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)
        success,
    required TResult Function() failure,
  }) {
    return success(order, owner, participants, products, cart);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
  }) {
    return success?.call(order, owner, participants, products, cart);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(order, owner, participants, products, cart);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadOrderDataSuccess value) success,
    required TResult Function(LoadOrderDataFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LoadOrderDataSuccess value)? success,
    TResult Function(LoadOrderDataFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadOrderDataSuccess value)? success,
    TResult Function(LoadOrderDataFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class LoadOrderDataSuccess implements LoadOrderDataResult {
  factory LoadOrderDataSuccess(
      final Order order,
      final Participant owner,
      final KtList<Participant> participants,
      final KtList<Product> products,
      final KtList<CartEntry> cart) = _$LoadOrderDataSuccess;

  Order get order;
  Participant get owner;
  KtList<Participant> get participants;
  KtList<Product> get products;
  KtList<CartEntry> get cart;
  @JsonKey(ignore: true)
  _$$LoadOrderDataSuccessCopyWith<_$LoadOrderDataSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoadOrderDataFailureCopyWith<$Res> {
  factory _$$LoadOrderDataFailureCopyWith(_$LoadOrderDataFailure value,
          $Res Function(_$LoadOrderDataFailure) then) =
      __$$LoadOrderDataFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadOrderDataFailureCopyWithImpl<$Res>
    extends _$LoadOrderDataResultCopyWithImpl<$Res>
    implements _$$LoadOrderDataFailureCopyWith<$Res> {
  __$$LoadOrderDataFailureCopyWithImpl(_$LoadOrderDataFailure _value,
      $Res Function(_$LoadOrderDataFailure) _then)
      : super(_value, (v) => _then(v as _$LoadOrderDataFailure));

  @override
  _$LoadOrderDataFailure get _value => super._value as _$LoadOrderDataFailure;
}

/// @nodoc

class _$LoadOrderDataFailure implements LoadOrderDataFailure {
  _$LoadOrderDataFailure();

  @override
  String toString() {
    return 'LoadOrderDataResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadOrderDataFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)
        success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Order order,
            Participant owner,
            KtList<Participant> participants,
            KtList<Product> products,
            KtList<CartEntry> cart)?
        success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadOrderDataSuccess value) success,
    required TResult Function(LoadOrderDataFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LoadOrderDataSuccess value)? success,
    TResult Function(LoadOrderDataFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadOrderDataSuccess value)? success,
    TResult Function(LoadOrderDataFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class LoadOrderDataFailure implements LoadOrderDataResult {
  factory LoadOrderDataFailure() = _$LoadOrderDataFailure;
}
