import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/product.dart';

part 'load_order_data.freezed.dart';

class LoadOrderData {
  LoadOrderData(this.dataRemoteStore, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<LoadOrderDataResult> call({required String orderId}) async {
    if (dataBloc.isInternetConnected) {
      final GetOrderResult orderResult =
          await dataRemoteStore.getOrder(orderId);
      if (orderResult is GetOrderSuccess) {
        final GetOrderDataResult dataResult =
            await dataRemoteStore.getOrderData(orderId: orderId);
        if (dataResult is GetOrderDataSuccess) {
          dataBloc.add(OrderUpdated(order: orderResult.order));
          final Participant owner = dataResult.participants.first(
              (Participant it) => it.userId == orderResult.order.ownerId);
          return LoadOrderDataSuccess(orderResult.order, owner,
              dataResult.participants, dataResult.products, dataResult.cart);
        }
      }
    }
    return LoadOrderDataFailure();
  }
}

@freezed
class LoadOrderDataResult with _$LoadOrderDataResult {
  factory LoadOrderDataResult.success(
    Order order,
    Participant owner,
    KtList<Participant> participants,
    KtList<Product> products,
    KtList<CartEntry> cart,
  ) = LoadOrderDataSuccess;

  factory LoadOrderDataResult.failure() = LoadOrderDataFailure;
}

// ignore: unused_element
final Logger _logger = Logger('LoadOrderData');
