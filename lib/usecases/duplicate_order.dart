import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/product.dart';
import 'package:orders/usecases/create_order.dart';

part 'duplicate_order.freezed.dart';

class DuplicateOrder {
  DuplicateOrder(this.createOrder, this.dataRemoteStore, this.dataBloc);

  final CreateOrder createOrder;
  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<DuplicateOrderResult> call({
    required String title,
    required String details,
    required String currency,
    required DateTime deliveryDate,
    required DateTime dueDate,
    required String duplicateOrderId,
    required OrderType orderType,
  }) async {
    if (dataBloc.isInternetConnected) {
      final GetOrderProductsResult productsResult =
          await dataRemoteStore.getOrderProducts(orderId: duplicateOrderId);
      if (productsResult is GetOrderProductsSuccess) {
        final CreateOrderResult result = await createOrder(
          title: title,
          details: details,
          currency: currency,
          deliveryDate: deliveryDate,
          dueDate: dueDate,
          orderType: orderType,
        );
        if (result is CreateOrderSuccess) {
          final KtList<Product> uniqueProducts = productsResult.products.map(
            (Product it) => it.copyWith(
              id: dataRemoteStore.generateProductId(orderId: result.order.id),
            ),
          );
          final StoreResult saveResult =
              await dataRemoteStore.createOrderProducts(
                  orderId: result.order.id, products: uniqueProducts);
          if (saveResult is StoreSuccess) {
            return DuplicateOrderSuccess(result.order);
          }
        }
      }
    }
    return DuplicateOrderFailure();
  }
}

@freezed
class DuplicateOrderResult with _$DuplicateOrderResult {
  factory DuplicateOrderResult.success(Order order) = DuplicateOrderSuccess;

  factory DuplicateOrderResult.failure() = DuplicateOrderFailure;
}

// ignore: unused_element
final Logger _logger = Logger('CreateOrder');
