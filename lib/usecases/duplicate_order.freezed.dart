// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'duplicate_order.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DuplicateOrderResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DuplicateOrderSuccess value) success,
    required TResult Function(DuplicateOrderFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DuplicateOrderSuccess value)? success,
    TResult Function(DuplicateOrderFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DuplicateOrderSuccess value)? success,
    TResult Function(DuplicateOrderFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DuplicateOrderResultCopyWith<$Res> {
  factory $DuplicateOrderResultCopyWith(DuplicateOrderResult value,
          $Res Function(DuplicateOrderResult) then) =
      _$DuplicateOrderResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$DuplicateOrderResultCopyWithImpl<$Res>
    implements $DuplicateOrderResultCopyWith<$Res> {
  _$DuplicateOrderResultCopyWithImpl(this._value, this._then);

  final DuplicateOrderResult _value;
  // ignore: unused_field
  final $Res Function(DuplicateOrderResult) _then;
}

/// @nodoc
abstract class _$$DuplicateOrderSuccessCopyWith<$Res> {
  factory _$$DuplicateOrderSuccessCopyWith(_$DuplicateOrderSuccess value,
          $Res Function(_$DuplicateOrderSuccess) then) =
      __$$DuplicateOrderSuccessCopyWithImpl<$Res>;
  $Res call({Order order});

  $OrderCopyWith<$Res> get order;
}

/// @nodoc
class __$$DuplicateOrderSuccessCopyWithImpl<$Res>
    extends _$DuplicateOrderResultCopyWithImpl<$Res>
    implements _$$DuplicateOrderSuccessCopyWith<$Res> {
  __$$DuplicateOrderSuccessCopyWithImpl(_$DuplicateOrderSuccess _value,
      $Res Function(_$DuplicateOrderSuccess) _then)
      : super(_value, (v) => _then(v as _$DuplicateOrderSuccess));

  @override
  _$DuplicateOrderSuccess get _value => super._value as _$DuplicateOrderSuccess;

  @override
  $Res call({
    Object? order = freezed,
  }) {
    return _then(_$DuplicateOrderSuccess(
      order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as Order,
    ));
  }

  @override
  $OrderCopyWith<$Res> get order {
    return $OrderCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$DuplicateOrderSuccess implements DuplicateOrderSuccess {
  _$DuplicateOrderSuccess(this.order);

  @override
  final Order order;

  @override
  String toString() {
    return 'DuplicateOrderResult.success(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DuplicateOrderSuccess &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$DuplicateOrderSuccessCopyWith<_$DuplicateOrderSuccess> get copyWith =>
      __$$DuplicateOrderSuccessCopyWithImpl<_$DuplicateOrderSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) {
    return success(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) {
    return success?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DuplicateOrderSuccess value) success,
    required TResult Function(DuplicateOrderFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DuplicateOrderSuccess value)? success,
    TResult Function(DuplicateOrderFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DuplicateOrderSuccess value)? success,
    TResult Function(DuplicateOrderFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class DuplicateOrderSuccess implements DuplicateOrderResult {
  factory DuplicateOrderSuccess(final Order order) = _$DuplicateOrderSuccess;

  Order get order;
  @JsonKey(ignore: true)
  _$$DuplicateOrderSuccessCopyWith<_$DuplicateOrderSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DuplicateOrderFailureCopyWith<$Res> {
  factory _$$DuplicateOrderFailureCopyWith(_$DuplicateOrderFailure value,
          $Res Function(_$DuplicateOrderFailure) then) =
      __$$DuplicateOrderFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DuplicateOrderFailureCopyWithImpl<$Res>
    extends _$DuplicateOrderResultCopyWithImpl<$Res>
    implements _$$DuplicateOrderFailureCopyWith<$Res> {
  __$$DuplicateOrderFailureCopyWithImpl(_$DuplicateOrderFailure _value,
      $Res Function(_$DuplicateOrderFailure) _then)
      : super(_value, (v) => _then(v as _$DuplicateOrderFailure));

  @override
  _$DuplicateOrderFailure get _value => super._value as _$DuplicateOrderFailure;
}

/// @nodoc

class _$DuplicateOrderFailure implements DuplicateOrderFailure {
  _$DuplicateOrderFailure();

  @override
  String toString() {
    return 'DuplicateOrderResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DuplicateOrderFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Order order) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Order order)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DuplicateOrderSuccess value) success,
    required TResult Function(DuplicateOrderFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DuplicateOrderSuccess value)? success,
    TResult Function(DuplicateOrderFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DuplicateOrderSuccess value)? success,
    TResult Function(DuplicateOrderFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class DuplicateOrderFailure implements DuplicateOrderResult {
  factory DuplicateOrderFailure() = _$DuplicateOrderFailure;
}
