import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/usecases/use_case_results.dart';

class RemoveFromCart {
  RemoveFromCart(this.dataRemoteStore, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<UseCaseResult> call({
    required String orderId,
    required CartEntry cartEntry,
  }) async {
    if (dataBloc.isInternetConnected) {
      final StoreResult result = await dataRemoteStore.deleteCartItem(
        orderId: orderId,
        cartEntryId: cartEntry.id,
      );
      if (result is StoreSuccess) {
        return UseCaseSuccess();
      }
    }
    return UseCaseFailure();
  }
}

// ignore: unused_element
final Logger _logger = Logger('RemoveFromCart');
