// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'check_user_existing.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CheckUserExistingResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool userExists) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool userExists)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool userExists)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CheckUserExistingSuccess value) success,
    required TResult Function(CheckUserExistingFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CheckUserExistingSuccess value)? success,
    TResult Function(CheckUserExistingFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CheckUserExistingSuccess value)? success,
    TResult Function(CheckUserExistingFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CheckUserExistingResultCopyWith<$Res> {
  factory $CheckUserExistingResultCopyWith(CheckUserExistingResult value,
          $Res Function(CheckUserExistingResult) then) =
      _$CheckUserExistingResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$CheckUserExistingResultCopyWithImpl<$Res>
    implements $CheckUserExistingResultCopyWith<$Res> {
  _$CheckUserExistingResultCopyWithImpl(this._value, this._then);

  final CheckUserExistingResult _value;
  // ignore: unused_field
  final $Res Function(CheckUserExistingResult) _then;
}

/// @nodoc
abstract class _$$CheckUserExistingSuccessCopyWith<$Res> {
  factory _$$CheckUserExistingSuccessCopyWith(_$CheckUserExistingSuccess value,
          $Res Function(_$CheckUserExistingSuccess) then) =
      __$$CheckUserExistingSuccessCopyWithImpl<$Res>;
  $Res call({bool userExists});
}

/// @nodoc
class __$$CheckUserExistingSuccessCopyWithImpl<$Res>
    extends _$CheckUserExistingResultCopyWithImpl<$Res>
    implements _$$CheckUserExistingSuccessCopyWith<$Res> {
  __$$CheckUserExistingSuccessCopyWithImpl(_$CheckUserExistingSuccess _value,
      $Res Function(_$CheckUserExistingSuccess) _then)
      : super(_value, (v) => _then(v as _$CheckUserExistingSuccess));

  @override
  _$CheckUserExistingSuccess get _value =>
      super._value as _$CheckUserExistingSuccess;

  @override
  $Res call({
    Object? userExists = freezed,
  }) {
    return _then(_$CheckUserExistingSuccess(
      userExists: userExists == freezed
          ? _value.userExists
          : userExists // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$CheckUserExistingSuccess implements CheckUserExistingSuccess {
  _$CheckUserExistingSuccess({required this.userExists});

  @override
  final bool userExists;

  @override
  String toString() {
    return 'CheckUserExistingResult.success(userExists: $userExists)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckUserExistingSuccess &&
            const DeepCollectionEquality()
                .equals(other.userExists, userExists));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userExists));

  @JsonKey(ignore: true)
  @override
  _$$CheckUserExistingSuccessCopyWith<_$CheckUserExistingSuccess>
      get copyWith =>
          __$$CheckUserExistingSuccessCopyWithImpl<_$CheckUserExistingSuccess>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool userExists) success,
    required TResult Function() failure,
  }) {
    return success(userExists);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool userExists)? success,
    TResult Function()? failure,
  }) {
    return success?.call(userExists);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool userExists)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(userExists);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CheckUserExistingSuccess value) success,
    required TResult Function(CheckUserExistingFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CheckUserExistingSuccess value)? success,
    TResult Function(CheckUserExistingFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CheckUserExistingSuccess value)? success,
    TResult Function(CheckUserExistingFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class CheckUserExistingSuccess implements CheckUserExistingResult {
  factory CheckUserExistingSuccess({required final bool userExists}) =
      _$CheckUserExistingSuccess;

  bool get userExists;
  @JsonKey(ignore: true)
  _$$CheckUserExistingSuccessCopyWith<_$CheckUserExistingSuccess>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CheckUserExistingFailureCopyWith<$Res> {
  factory _$$CheckUserExistingFailureCopyWith(_$CheckUserExistingFailure value,
          $Res Function(_$CheckUserExistingFailure) then) =
      __$$CheckUserExistingFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckUserExistingFailureCopyWithImpl<$Res>
    extends _$CheckUserExistingResultCopyWithImpl<$Res>
    implements _$$CheckUserExistingFailureCopyWith<$Res> {
  __$$CheckUserExistingFailureCopyWithImpl(_$CheckUserExistingFailure _value,
      $Res Function(_$CheckUserExistingFailure) _then)
      : super(_value, (v) => _then(v as _$CheckUserExistingFailure));

  @override
  _$CheckUserExistingFailure get _value =>
      super._value as _$CheckUserExistingFailure;
}

/// @nodoc

class _$CheckUserExistingFailure implements CheckUserExistingFailure {
  _$CheckUserExistingFailure();

  @override
  String toString() {
    return 'CheckUserExistingResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckUserExistingFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool userExists) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool userExists)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool userExists)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CheckUserExistingSuccess value) success,
    required TResult Function(CheckUserExistingFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CheckUserExistingSuccess value)? success,
    TResult Function(CheckUserExistingFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CheckUserExistingSuccess value)? success,
    TResult Function(CheckUserExistingFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class CheckUserExistingFailure implements CheckUserExistingResult {
  factory CheckUserExistingFailure() = _$CheckUserExistingFailure;
}
