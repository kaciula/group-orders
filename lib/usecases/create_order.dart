import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/data/local/misc/dynamic_links.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/user.dart';

part 'create_order.freezed.dart';

class CreateOrder {
  CreateOrder(this.dataRemoteStore, this.dynamicLinks, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DynamicLinks dynamicLinks;
  final DataBloc dataBloc;

  Future<CreateOrderResult> call({
    required String title,
    required String details,
    required String currency,
    required DateTime deliveryDate,
    required DateTime dueDate,
    required OrderType orderType,
  }) async {
    if (dataBloc.isInternetConnected) {
      final User user = dataBloc.user;
      final DateTime now = DateTime.now();
      final String orderId = dataRemoteStore.generateOrderId();

      final String inviteLink =
          await dynamicLinks.generateLink(orderId: orderId);

      final Order order = Order(
        id: orderId,
        ownerId: user.id,
        ownerName: user.displayName,
        title: title,
        details: details,
        currency: currency,
        status: OrderStatus.inProgress,
        statusDetails: '',
        creationDate: now,
        deliveryDate: deliveryDate,
        dueDate: dueDate,
        // this is overridden when creating the order
        participantIds: <String>[],
        isDeleted: false,
        hasFeatureEditProducts: orderType == OrderType.proxy,
        hasFeatureSeeParticipants: orderType == OrderType.proxy,
        type: orderType,
        inviteLink: inviteLink,
      );
      final StoreResult result = await dataRemoteStore.createOrder(
        order,
        Participant.initial(
          id: user.id,
          displayName: user.displayName,
          phoneNumber: user.phoneNumber,
        ),
      );
      if (result is StoreSuccess) {
        dataBloc.add(OrderCreated(order: order));
        return CreateOrderSuccess(order);
      }
    }
    return CreateOrderFailure();
  }
}

@freezed
class CreateOrderResult with _$CreateOrderResult {
  factory CreateOrderResult.success(Order order) = CreateOrderSuccess;

  factory CreateOrderResult.failure() = CreateOrderFailure;
}

// ignore: unused_element
final Logger _logger = Logger('CreateOrder');
