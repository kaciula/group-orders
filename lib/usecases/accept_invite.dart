import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/participant.dart';
import 'package:orders/model/user.dart';
import 'package:orders/usecases/use_case_results.dart';

class AcceptInvite {
  AcceptInvite(this.dataRemoteStore, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<UseCaseResult> call({required Order order}) async {
    if (dataBloc.isInternetConnected) {
      final User user = dataBloc.user;
      final StoreResult result = await dataRemoteStore.addParticipantToOrder(
        orderId: order.id,
        participant: Participant.initial(
          id: user.id,
          displayName: user.displayName,
          phoneNumber: user.phoneNumber,
        ),
      );
      if (result is StoreSuccess) {
        dataBloc.add(InvitationAccepted(order: order));
        return UseCaseSuccess();
      }
    }
    return UseCaseFailure();
  }
}

// ignore: unused_element
final Logger _logger = Logger('AcceptInvite');
