// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'save_product.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SaveProductResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Product product) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Product product)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Product product)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveProductSuccess value) success,
    required TResult Function(SaveProductFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveProductSuccess value)? success,
    TResult Function(SaveProductFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveProductSuccess value)? success,
    TResult Function(SaveProductFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SaveProductResultCopyWith<$Res> {
  factory $SaveProductResultCopyWith(
          SaveProductResult value, $Res Function(SaveProductResult) then) =
      _$SaveProductResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$SaveProductResultCopyWithImpl<$Res>
    implements $SaveProductResultCopyWith<$Res> {
  _$SaveProductResultCopyWithImpl(this._value, this._then);

  final SaveProductResult _value;
  // ignore: unused_field
  final $Res Function(SaveProductResult) _then;
}

/// @nodoc
abstract class _$$SaveProductSuccessCopyWith<$Res> {
  factory _$$SaveProductSuccessCopyWith(_$SaveProductSuccess value,
          $Res Function(_$SaveProductSuccess) then) =
      __$$SaveProductSuccessCopyWithImpl<$Res>;
  $Res call({Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$SaveProductSuccessCopyWithImpl<$Res>
    extends _$SaveProductResultCopyWithImpl<$Res>
    implements _$$SaveProductSuccessCopyWith<$Res> {
  __$$SaveProductSuccessCopyWithImpl(
      _$SaveProductSuccess _value, $Res Function(_$SaveProductSuccess) _then)
      : super(_value, (v) => _then(v as _$SaveProductSuccess));

  @override
  _$SaveProductSuccess get _value => super._value as _$SaveProductSuccess;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$SaveProductSuccess(
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$SaveProductSuccess implements SaveProductSuccess {
  _$SaveProductSuccess(this.product);

  @override
  final Product product;

  @override
  String toString() {
    return 'SaveProductResult.success(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SaveProductSuccess &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$SaveProductSuccessCopyWith<_$SaveProductSuccess> get copyWith =>
      __$$SaveProductSuccessCopyWithImpl<_$SaveProductSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Product product) success,
    required TResult Function() failure,
  }) {
    return success(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Product product)? success,
    TResult Function()? failure,
  }) {
    return success?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Product product)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveProductSuccess value) success,
    required TResult Function(SaveProductFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveProductSuccess value)? success,
    TResult Function(SaveProductFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveProductSuccess value)? success,
    TResult Function(SaveProductFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SaveProductSuccess implements SaveProductResult {
  factory SaveProductSuccess(final Product product) = _$SaveProductSuccess;

  Product get product;
  @JsonKey(ignore: true)
  _$$SaveProductSuccessCopyWith<_$SaveProductSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SaveProductFailureCopyWith<$Res> {
  factory _$$SaveProductFailureCopyWith(_$SaveProductFailure value,
          $Res Function(_$SaveProductFailure) then) =
      __$$SaveProductFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SaveProductFailureCopyWithImpl<$Res>
    extends _$SaveProductResultCopyWithImpl<$Res>
    implements _$$SaveProductFailureCopyWith<$Res> {
  __$$SaveProductFailureCopyWithImpl(
      _$SaveProductFailure _value, $Res Function(_$SaveProductFailure) _then)
      : super(_value, (v) => _then(v as _$SaveProductFailure));

  @override
  _$SaveProductFailure get _value => super._value as _$SaveProductFailure;
}

/// @nodoc

class _$SaveProductFailure implements SaveProductFailure {
  _$SaveProductFailure();

  @override
  String toString() {
    return 'SaveProductResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SaveProductFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Product product) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Product product)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Product product)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SaveProductSuccess value) success,
    required TResult Function(SaveProductFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SaveProductSuccess value)? success,
    TResult Function(SaveProductFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SaveProductSuccess value)? success,
    TResult Function(SaveProductFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class SaveProductFailure implements SaveProductResult {
  factory SaveProductFailure() = _$SaveProductFailure;
}
