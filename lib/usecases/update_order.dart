import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/order.dart';
import 'package:orders/usecases/use_case_results.dart';

class UpdateOrder {
  UpdateOrder(this.dataRemoteStore, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<UseCaseResult> call({
    required String orderId,
    String? title,
    String? details,
    String? currency,
    DateTime? deliveryDate,
    DateTime? dueDate,
    bool? isDeleted,
    OrderStatus? status,
  }) async {
    if (dataBloc.isInternetConnected) {
      final StoreResult result = await dataRemoteStore.updateOrderFields(
        orderId: orderId,
        title: title,
        details: details,
        currency: currency,
        deliveryDate: deliveryDate,
        dueDate: dueDate,
        isDeleted: isDeleted,
        status: status,
      );
      if (result is StoreSuccess) {
        return UseCaseSuccess();
      }
    }
    return UseCaseFailure();
  }
}

// ignore: unused_element
final Logger _logger = Logger('UpdateOrder');
