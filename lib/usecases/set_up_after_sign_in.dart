import 'package:logging/logging.dart';
import 'package:orders/app/app_locale.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/data/local/store/user_provider.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/data/remote/push_messaging.dart';
import 'package:orders/model/user.dart';
import 'package:orders/usecases/use_case_results.dart';

class SetUpAfterSignIn {
  SetUpAfterSignIn(this.dataRemoteStore, this.userProvider, this.pushMessaging,
      this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final UserProvider userProvider;
  final PushMessaging pushMessaging;
  final DataBloc dataBloc;

  Future<UseCaseResult> call(User user, {bool checkHasAccount = false}) async {
    if (dataBloc.isInternetConnected) {
      User updatedUser = user;
      if (checkHasAccount) {
        final HasAccountResult hasAccountResult =
            await dataRemoteStore.hasAccount(updatedUser.id);
        if (hasAccountResult is HasAccountSuccess) {
          if (hasAccountResult.hasAccount) {
            updatedUser = hasAccountResult.user!
                .copyWith(signInProvider: updatedUser.signInProvider);
          }
        } else {
          return UseCaseFailure();
        }
      }

      final String? messagingToken = await pushMessaging.getToken();
      updatedUser = updatedUser.copyWith(
        languageCode: AppLocale.languageCode,
        messagingToken: messagingToken ?? '',
      );

      final StoreResult userResult =
          await dataRemoteStore.saveUser(updatedUser);
      final GetOrdersResult ordersResult =
          await dataRemoteStore.getOrders(userId: updatedUser.id);
      if (userResult is StoreSuccess && ordersResult is GetOrdersSuccess) {
        await userProvider.signedIn(user: updatedUser);
        dataBloc.add(DataRefreshed(
          user: updatedUser,
          orders: ordersResult.orders,
        ));
        return UseCaseSuccess();
      }
    }
    return UseCaseFailure();
  }
}

// ignore: unused_element
final Logger _logger = Logger('SetUpAfterSignIn');
