// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'add_product_to_cart.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AddProductToCartResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CartEntry cartEntry) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddProductToCartSuccess value) success,
    required TResult Function(AddProductToCartFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddProductToCartSuccess value)? success,
    TResult Function(AddProductToCartFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddProductToCartSuccess value)? success,
    TResult Function(AddProductToCartFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddProductToCartResultCopyWith<$Res> {
  factory $AddProductToCartResultCopyWith(AddProductToCartResult value,
          $Res Function(AddProductToCartResult) then) =
      _$AddProductToCartResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$AddProductToCartResultCopyWithImpl<$Res>
    implements $AddProductToCartResultCopyWith<$Res> {
  _$AddProductToCartResultCopyWithImpl(this._value, this._then);

  final AddProductToCartResult _value;
  // ignore: unused_field
  final $Res Function(AddProductToCartResult) _then;
}

/// @nodoc
abstract class _$$AddProductToCartSuccessCopyWith<$Res> {
  factory _$$AddProductToCartSuccessCopyWith(_$AddProductToCartSuccess value,
          $Res Function(_$AddProductToCartSuccess) then) =
      __$$AddProductToCartSuccessCopyWithImpl<$Res>;
  $Res call({CartEntry cartEntry});

  $CartEntryCopyWith<$Res> get cartEntry;
}

/// @nodoc
class __$$AddProductToCartSuccessCopyWithImpl<$Res>
    extends _$AddProductToCartResultCopyWithImpl<$Res>
    implements _$$AddProductToCartSuccessCopyWith<$Res> {
  __$$AddProductToCartSuccessCopyWithImpl(_$AddProductToCartSuccess _value,
      $Res Function(_$AddProductToCartSuccess) _then)
      : super(_value, (v) => _then(v as _$AddProductToCartSuccess));

  @override
  _$AddProductToCartSuccess get _value =>
      super._value as _$AddProductToCartSuccess;

  @override
  $Res call({
    Object? cartEntry = freezed,
  }) {
    return _then(_$AddProductToCartSuccess(
      cartEntry == freezed
          ? _value.cartEntry
          : cartEntry // ignore: cast_nullable_to_non_nullable
              as CartEntry,
    ));
  }

  @override
  $CartEntryCopyWith<$Res> get cartEntry {
    return $CartEntryCopyWith<$Res>(_value.cartEntry, (value) {
      return _then(_value.copyWith(cartEntry: value));
    });
  }
}

/// @nodoc

class _$AddProductToCartSuccess implements AddProductToCartSuccess {
  _$AddProductToCartSuccess(this.cartEntry);

  @override
  final CartEntry cartEntry;

  @override
  String toString() {
    return 'AddProductToCartResult.success(cartEntry: $cartEntry)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddProductToCartSuccess &&
            const DeepCollectionEquality().equals(other.cartEntry, cartEntry));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(cartEntry));

  @JsonKey(ignore: true)
  @override
  _$$AddProductToCartSuccessCopyWith<_$AddProductToCartSuccess> get copyWith =>
      __$$AddProductToCartSuccessCopyWithImpl<_$AddProductToCartSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CartEntry cartEntry) success,
    required TResult Function() failure,
  }) {
    return success(cartEntry);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
  }) {
    return success?.call(cartEntry);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(cartEntry);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddProductToCartSuccess value) success,
    required TResult Function(AddProductToCartFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddProductToCartSuccess value)? success,
    TResult Function(AddProductToCartFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddProductToCartSuccess value)? success,
    TResult Function(AddProductToCartFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class AddProductToCartSuccess implements AddProductToCartResult {
  factory AddProductToCartSuccess(final CartEntry cartEntry) =
      _$AddProductToCartSuccess;

  CartEntry get cartEntry;
  @JsonKey(ignore: true)
  _$$AddProductToCartSuccessCopyWith<_$AddProductToCartSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddProductToCartFailureCopyWith<$Res> {
  factory _$$AddProductToCartFailureCopyWith(_$AddProductToCartFailure value,
          $Res Function(_$AddProductToCartFailure) then) =
      __$$AddProductToCartFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AddProductToCartFailureCopyWithImpl<$Res>
    extends _$AddProductToCartResultCopyWithImpl<$Res>
    implements _$$AddProductToCartFailureCopyWith<$Res> {
  __$$AddProductToCartFailureCopyWithImpl(_$AddProductToCartFailure _value,
      $Res Function(_$AddProductToCartFailure) _then)
      : super(_value, (v) => _then(v as _$AddProductToCartFailure));

  @override
  _$AddProductToCartFailure get _value =>
      super._value as _$AddProductToCartFailure;
}

/// @nodoc

class _$AddProductToCartFailure implements AddProductToCartFailure {
  _$AddProductToCartFailure();

  @override
  String toString() {
    return 'AddProductToCartResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddProductToCartFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CartEntry cartEntry) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddProductToCartSuccess value) success,
    required TResult Function(AddProductToCartFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddProductToCartSuccess value)? success,
    TResult Function(AddProductToCartFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddProductToCartSuccess value)? success,
    TResult Function(AddProductToCartFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class AddProductToCartFailure implements AddProductToCartResult {
  factory AddProductToCartFailure() = _$AddProductToCartFailure;
}
