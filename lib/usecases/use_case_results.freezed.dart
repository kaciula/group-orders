// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'use_case_results.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UseCaseResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UseCaseSuccess value) success,
    required TResult Function(UseCaseFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(UseCaseSuccess value)? success,
    TResult Function(UseCaseFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UseCaseSuccess value)? success,
    TResult Function(UseCaseFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UseCaseResultCopyWith<$Res> {
  factory $UseCaseResultCopyWith(
          UseCaseResult value, $Res Function(UseCaseResult) then) =
      _$UseCaseResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$UseCaseResultCopyWithImpl<$Res>
    implements $UseCaseResultCopyWith<$Res> {
  _$UseCaseResultCopyWithImpl(this._value, this._then);

  final UseCaseResult _value;
  // ignore: unused_field
  final $Res Function(UseCaseResult) _then;
}

/// @nodoc
abstract class _$$UseCaseSuccessCopyWith<$Res> {
  factory _$$UseCaseSuccessCopyWith(
          _$UseCaseSuccess value, $Res Function(_$UseCaseSuccess) then) =
      __$$UseCaseSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UseCaseSuccessCopyWithImpl<$Res>
    extends _$UseCaseResultCopyWithImpl<$Res>
    implements _$$UseCaseSuccessCopyWith<$Res> {
  __$$UseCaseSuccessCopyWithImpl(
      _$UseCaseSuccess _value, $Res Function(_$UseCaseSuccess) _then)
      : super(_value, (v) => _then(v as _$UseCaseSuccess));

  @override
  _$UseCaseSuccess get _value => super._value as _$UseCaseSuccess;
}

/// @nodoc

class _$UseCaseSuccess implements UseCaseSuccess {
  _$UseCaseSuccess();

  @override
  String toString() {
    return 'UseCaseResult.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UseCaseSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UseCaseSuccess value) success,
    required TResult Function(UseCaseFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(UseCaseSuccess value)? success,
    TResult Function(UseCaseFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UseCaseSuccess value)? success,
    TResult Function(UseCaseFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class UseCaseSuccess implements UseCaseResult {
  factory UseCaseSuccess() = _$UseCaseSuccess;
}

/// @nodoc
abstract class _$$UseCaseFailureCopyWith<$Res> {
  factory _$$UseCaseFailureCopyWith(
          _$UseCaseFailure value, $Res Function(_$UseCaseFailure) then) =
      __$$UseCaseFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UseCaseFailureCopyWithImpl<$Res>
    extends _$UseCaseResultCopyWithImpl<$Res>
    implements _$$UseCaseFailureCopyWith<$Res> {
  __$$UseCaseFailureCopyWithImpl(
      _$UseCaseFailure _value, $Res Function(_$UseCaseFailure) _then)
      : super(_value, (v) => _then(v as _$UseCaseFailure));

  @override
  _$UseCaseFailure get _value => super._value as _$UseCaseFailure;
}

/// @nodoc

class _$UseCaseFailure implements UseCaseFailure {
  _$UseCaseFailure();

  @override
  String toString() {
    return 'UseCaseResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UseCaseFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UseCaseSuccess value) success,
    required TResult Function(UseCaseFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(UseCaseSuccess value)? success,
    TResult Function(UseCaseFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UseCaseSuccess value)? success,
    TResult Function(UseCaseFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class UseCaseFailure implements UseCaseResult {
  factory UseCaseFailure() = _$UseCaseFailure;
}
