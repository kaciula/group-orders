// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'set_up_on_start.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SetUpOnStartResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isSignedIn) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool isSignedIn)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isSignedIn)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SetUpOnStartSuccess value) success,
    required TResult Function(SetUpOnStartFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SetUpOnStartSuccess value)? success,
    TResult Function(SetUpOnStartFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SetUpOnStartSuccess value)? success,
    TResult Function(SetUpOnStartFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SetUpOnStartResultCopyWith<$Res> {
  factory $SetUpOnStartResultCopyWith(
          SetUpOnStartResult value, $Res Function(SetUpOnStartResult) then) =
      _$SetUpOnStartResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$SetUpOnStartResultCopyWithImpl<$Res>
    implements $SetUpOnStartResultCopyWith<$Res> {
  _$SetUpOnStartResultCopyWithImpl(this._value, this._then);

  final SetUpOnStartResult _value;
  // ignore: unused_field
  final $Res Function(SetUpOnStartResult) _then;
}

/// @nodoc
abstract class _$$SetUpOnStartSuccessCopyWith<$Res> {
  factory _$$SetUpOnStartSuccessCopyWith(_$SetUpOnStartSuccess value,
          $Res Function(_$SetUpOnStartSuccess) then) =
      __$$SetUpOnStartSuccessCopyWithImpl<$Res>;
  $Res call({bool isSignedIn});
}

/// @nodoc
class __$$SetUpOnStartSuccessCopyWithImpl<$Res>
    extends _$SetUpOnStartResultCopyWithImpl<$Res>
    implements _$$SetUpOnStartSuccessCopyWith<$Res> {
  __$$SetUpOnStartSuccessCopyWithImpl(
      _$SetUpOnStartSuccess _value, $Res Function(_$SetUpOnStartSuccess) _then)
      : super(_value, (v) => _then(v as _$SetUpOnStartSuccess));

  @override
  _$SetUpOnStartSuccess get _value => super._value as _$SetUpOnStartSuccess;

  @override
  $Res call({
    Object? isSignedIn = freezed,
  }) {
    return _then(_$SetUpOnStartSuccess(
      isSignedIn: isSignedIn == freezed
          ? _value.isSignedIn
          : isSignedIn // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$SetUpOnStartSuccess implements SetUpOnStartSuccess {
  _$SetUpOnStartSuccess({required this.isSignedIn});

  @override
  final bool isSignedIn;

  @override
  String toString() {
    return 'SetUpOnStartResult.success(isSignedIn: $isSignedIn)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SetUpOnStartSuccess &&
            const DeepCollectionEquality()
                .equals(other.isSignedIn, isSignedIn));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(isSignedIn));

  @JsonKey(ignore: true)
  @override
  _$$SetUpOnStartSuccessCopyWith<_$SetUpOnStartSuccess> get copyWith =>
      __$$SetUpOnStartSuccessCopyWithImpl<_$SetUpOnStartSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isSignedIn) success,
    required TResult Function() failure,
  }) {
    return success(isSignedIn);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool isSignedIn)? success,
    TResult Function()? failure,
  }) {
    return success?.call(isSignedIn);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isSignedIn)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(isSignedIn);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SetUpOnStartSuccess value) success,
    required TResult Function(SetUpOnStartFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SetUpOnStartSuccess value)? success,
    TResult Function(SetUpOnStartFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SetUpOnStartSuccess value)? success,
    TResult Function(SetUpOnStartFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SetUpOnStartSuccess implements SetUpOnStartResult {
  factory SetUpOnStartSuccess({required final bool isSignedIn}) =
      _$SetUpOnStartSuccess;

  bool get isSignedIn;
  @JsonKey(ignore: true)
  _$$SetUpOnStartSuccessCopyWith<_$SetUpOnStartSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SetUpOnStartFailureCopyWith<$Res> {
  factory _$$SetUpOnStartFailureCopyWith(_$SetUpOnStartFailure value,
          $Res Function(_$SetUpOnStartFailure) then) =
      __$$SetUpOnStartFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SetUpOnStartFailureCopyWithImpl<$Res>
    extends _$SetUpOnStartResultCopyWithImpl<$Res>
    implements _$$SetUpOnStartFailureCopyWith<$Res> {
  __$$SetUpOnStartFailureCopyWithImpl(
      _$SetUpOnStartFailure _value, $Res Function(_$SetUpOnStartFailure) _then)
      : super(_value, (v) => _then(v as _$SetUpOnStartFailure));

  @override
  _$SetUpOnStartFailure get _value => super._value as _$SetUpOnStartFailure;
}

/// @nodoc

class _$SetUpOnStartFailure implements SetUpOnStartFailure {
  _$SetUpOnStartFailure();

  @override
  String toString() {
    return 'SetUpOnStartResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SetUpOnStartFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isSignedIn) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool isSignedIn)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isSignedIn)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SetUpOnStartSuccess value) success,
    required TResult Function(SetUpOnStartFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SetUpOnStartSuccess value)? success,
    TResult Function(SetUpOnStartFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SetUpOnStartSuccess value)? success,
    TResult Function(SetUpOnStartFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class SetUpOnStartFailure implements SetUpOnStartResult {
  factory SetUpOnStartFailure() = _$SetUpOnStartFailure;
}
