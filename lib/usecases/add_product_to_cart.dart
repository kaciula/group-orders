import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/cart_entry.dart';
import 'package:orders/model/product.dart';

part 'add_product_to_cart.freezed.dart';

class AddProductToCart {
  AddProductToCart(this.dataRemoteStore, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<AddProductToCartResult> call({
    required String orderId,
    required Product product,
    required String details,
    required int numItems,
  }) async {
    if (dataBloc.isInternetConnected) {
      final CartEntry cartEntry = CartProduct(
        id: dataRemoteStore.generateCartItemId(orderId: orderId),
        productId: product.id,
        participantId: dataBloc.userId,
        details: details,
        numItems: numItems,
      );
      final StoreResult result =
          await dataRemoteStore.saveCartItem(cartEntry, orderId: orderId);
      if (result is StoreSuccess) {
        return AddProductToCartSuccess(cartEntry);
      }
    }
    return AddProductToCartFailure();
  }
}

@freezed
class AddProductToCartResult with _$AddProductToCartResult {
  factory AddProductToCartResult.success(CartEntry cartEntry) =
      AddProductToCartSuccess;

  factory AddProductToCartResult.failure() = AddProductToCartFailure;
}

// ignore: unused_element
final Logger _logger = Logger('AddToCart');
