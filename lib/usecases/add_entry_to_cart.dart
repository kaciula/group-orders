import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/cart_entry.dart';

part 'add_entry_to_cart.freezed.dart';

class AddEntryToCart {
  AddEntryToCart(this.dataRemoteStore, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<AddEntryToCartResult> call({
    required String orderId,
    required CartEntry entry,
  }) async {
    if (dataBloc.isInternetConnected) {
      final CartEntry cartEntry = entry.copyWith(
          id: dataRemoteStore.generateCartItemId(orderId: orderId));
      final StoreResult result =
          await dataRemoteStore.saveCartItem(cartEntry, orderId: orderId);
      if (result is StoreSuccess) {
        return AddEntryToCartSuccess(cartEntry);
      }
    }
    return AddEntryToCartFailure();
  }
}

@freezed
class AddEntryToCartResult with _$AddEntryToCartResult {
  factory AddEntryToCartResult.success(CartEntry cartEntry) =
      AddEntryToCartSuccess;

  factory AddEntryToCartResult.failure() = AddEntryToCartFailure;
}

// ignore: unused_element
final Logger _logger = Logger('AddToCart');
