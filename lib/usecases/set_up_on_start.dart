import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/blocs/data_bloc/data_event.dart';
import 'package:orders/data/local/store/app_info_provider.dart';
import 'package:orders/data/local/store/user_provider.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/data/remote/push_messaging.dart';
import 'package:orders/model/order.dart';
import 'package:orders/model/user.dart';

part 'set_up_on_start.freezed.dart';

class SetUpOnStart {
  SetUpOnStart(this.userProvider, this.appInfoProvider, this.dataBloc,
      this.dataRemoteStore, this.pushMessaging);

  final UserProvider userProvider;
  final AppInfoProvider appInfoProvider;
  final DataBloc dataBloc;
  final DataRemoteStore dataRemoteStore;
  final PushMessaging pushMessaging;

  Future<SetUpOnStartResult> call() async {
    _logger.fine('Set up on start');
    if (dataBloc.isInternetConnected) {
      User user = userProvider.getUser() ?? User.anonymous();
      if (user.isSignedIn) {
        if (appInfoProvider.get().migrateMessagingToken) {
          final String? token = await pushMessaging.getToken();
          final StoreResult saveUserResult = await dataRemoteStore
              .updateUserFields(user.id, messagingToken: token);
          if (saveUserResult is StoreSuccess) {
            _logger.fine('Successfully updated user token');
            user = user.copyWith(messagingToken: token ?? '');
            await userProvider.persistUser(user);
            await appInfoProvider.saveMigrateMessagingToken(false);
          } else {
            _logger.fine('Failure updating user token');
            return SetUpOnStartFailure();
          }
        }

        if (appInfoProvider.get().refreshUser) {
          final GetUserResult result = await dataRemoteStore.getUser(user.id);
          if (result is GetUserSuccess) {
            user = result.user;
            await userProvider.persistUser(user);
            await appInfoProvider.saveRefreshUser(false);
          } else {
            _logger.fine('Failure refreshing user');
            return SetUpOnStartFailure();
          }
        }

        final GetOrdersResult ordersResult =
            await dataRemoteStore.getOrders(userId: user.id);
        if (ordersResult is GetOrdersSuccess) {
          dataBloc.add(DataRefreshed(user: user, orders: ordersResult.orders));
        } else {
          return SetUpOnStartFailure();
        }
      } else {
        dataBloc.add(
            DataRefreshed(user: user, orders: const KtList<Order>.empty()));
      }
      return SetUpOnStartSuccess(isSignedIn: user.isSignedIn);
    }
    return SetUpOnStartFailure();
  }
}

@freezed
class SetUpOnStartResult with _$SetUpOnStartResult {
  factory SetUpOnStartResult.success({required bool isSignedIn}) =
      SetUpOnStartSuccess;

  factory SetUpOnStartResult.failure() = SetUpOnStartFailure;
}

// ignore: unused_element
final Logger _logger = Logger('SetUpOnStart');
