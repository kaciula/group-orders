// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'add_entry_to_cart.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AddEntryToCartResult {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CartEntry cartEntry) success,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddEntryToCartSuccess value) success,
    required TResult Function(AddEntryToCartFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddEntryToCartSuccess value)? success,
    TResult Function(AddEntryToCartFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddEntryToCartSuccess value)? success,
    TResult Function(AddEntryToCartFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddEntryToCartResultCopyWith<$Res> {
  factory $AddEntryToCartResultCopyWith(AddEntryToCartResult value,
          $Res Function(AddEntryToCartResult) then) =
      _$AddEntryToCartResultCopyWithImpl<$Res>;
}

/// @nodoc
class _$AddEntryToCartResultCopyWithImpl<$Res>
    implements $AddEntryToCartResultCopyWith<$Res> {
  _$AddEntryToCartResultCopyWithImpl(this._value, this._then);

  final AddEntryToCartResult _value;
  // ignore: unused_field
  final $Res Function(AddEntryToCartResult) _then;
}

/// @nodoc
abstract class _$$AddEntryToCartSuccessCopyWith<$Res> {
  factory _$$AddEntryToCartSuccessCopyWith(_$AddEntryToCartSuccess value,
          $Res Function(_$AddEntryToCartSuccess) then) =
      __$$AddEntryToCartSuccessCopyWithImpl<$Res>;
  $Res call({CartEntry cartEntry});

  $CartEntryCopyWith<$Res> get cartEntry;
}

/// @nodoc
class __$$AddEntryToCartSuccessCopyWithImpl<$Res>
    extends _$AddEntryToCartResultCopyWithImpl<$Res>
    implements _$$AddEntryToCartSuccessCopyWith<$Res> {
  __$$AddEntryToCartSuccessCopyWithImpl(_$AddEntryToCartSuccess _value,
      $Res Function(_$AddEntryToCartSuccess) _then)
      : super(_value, (v) => _then(v as _$AddEntryToCartSuccess));

  @override
  _$AddEntryToCartSuccess get _value => super._value as _$AddEntryToCartSuccess;

  @override
  $Res call({
    Object? cartEntry = freezed,
  }) {
    return _then(_$AddEntryToCartSuccess(
      cartEntry == freezed
          ? _value.cartEntry
          : cartEntry // ignore: cast_nullable_to_non_nullable
              as CartEntry,
    ));
  }

  @override
  $CartEntryCopyWith<$Res> get cartEntry {
    return $CartEntryCopyWith<$Res>(_value.cartEntry, (value) {
      return _then(_value.copyWith(cartEntry: value));
    });
  }
}

/// @nodoc

class _$AddEntryToCartSuccess implements AddEntryToCartSuccess {
  _$AddEntryToCartSuccess(this.cartEntry);

  @override
  final CartEntry cartEntry;

  @override
  String toString() {
    return 'AddEntryToCartResult.success(cartEntry: $cartEntry)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddEntryToCartSuccess &&
            const DeepCollectionEquality().equals(other.cartEntry, cartEntry));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(cartEntry));

  @JsonKey(ignore: true)
  @override
  _$$AddEntryToCartSuccessCopyWith<_$AddEntryToCartSuccess> get copyWith =>
      __$$AddEntryToCartSuccessCopyWithImpl<_$AddEntryToCartSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CartEntry cartEntry) success,
    required TResult Function() failure,
  }) {
    return success(cartEntry);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
  }) {
    return success?.call(cartEntry);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(cartEntry);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddEntryToCartSuccess value) success,
    required TResult Function(AddEntryToCartFailure value) failure,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddEntryToCartSuccess value)? success,
    TResult Function(AddEntryToCartFailure value)? failure,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddEntryToCartSuccess value)? success,
    TResult Function(AddEntryToCartFailure value)? failure,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class AddEntryToCartSuccess implements AddEntryToCartResult {
  factory AddEntryToCartSuccess(final CartEntry cartEntry) =
      _$AddEntryToCartSuccess;

  CartEntry get cartEntry;
  @JsonKey(ignore: true)
  _$$AddEntryToCartSuccessCopyWith<_$AddEntryToCartSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddEntryToCartFailureCopyWith<$Res> {
  factory _$$AddEntryToCartFailureCopyWith(_$AddEntryToCartFailure value,
          $Res Function(_$AddEntryToCartFailure) then) =
      __$$AddEntryToCartFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AddEntryToCartFailureCopyWithImpl<$Res>
    extends _$AddEntryToCartResultCopyWithImpl<$Res>
    implements _$$AddEntryToCartFailureCopyWith<$Res> {
  __$$AddEntryToCartFailureCopyWithImpl(_$AddEntryToCartFailure _value,
      $Res Function(_$AddEntryToCartFailure) _then)
      : super(_value, (v) => _then(v as _$AddEntryToCartFailure));

  @override
  _$AddEntryToCartFailure get _value => super._value as _$AddEntryToCartFailure;
}

/// @nodoc

class _$AddEntryToCartFailure implements AddEntryToCartFailure {
  _$AddEntryToCartFailure();

  @override
  String toString() {
    return 'AddEntryToCartResult.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AddEntryToCartFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CartEntry cartEntry) success,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CartEntry cartEntry)? success,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddEntryToCartSuccess value) success,
    required TResult Function(AddEntryToCartFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddEntryToCartSuccess value)? success,
    TResult Function(AddEntryToCartFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddEntryToCartSuccess value)? success,
    TResult Function(AddEntryToCartFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class AddEntryToCartFailure implements AddEntryToCartResult {
  factory AddEntryToCartFailure() = _$AddEntryToCartFailure;
}
