import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logging/logging.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/model/product.dart';

part 'save_product.freezed.dart';

class SaveProduct {
  SaveProduct(this.dataRemoteStore, this.dataBloc);

  final DataRemoteStore dataRemoteStore;
  final DataBloc dataBloc;

  Future<SaveProductResult> call({
    required String orderId,
    required String title,
    required String details,
    required double price,
    required String currency,
    required Stock stock,
    String? productId,
  }) async {
    if (dataBloc.isInternetConnected) {
      final Product product = Product(
        id: productId ?? dataRemoteStore.generateProductId(orderId: orderId),
        title: title,
        details: details,
        price: price,
        currency: currency,
        stock: stock,
      );
      final StoreResult result =
          await dataRemoteStore.saveProduct(product, orderId: orderId);
      if (result is StoreSuccess) {
        return SaveProductSuccess(product);
      }
    }
    return SaveProductFailure();
  }
}

@freezed
class SaveProductResult with _$SaveProductResult {
  factory SaveProductResult.success(Product product) = SaveProductSuccess;

  factory SaveProductResult.failure() = SaveProductFailure;
}

// ignore: unused_element
final Logger _logger = Logger('CreateProduct');
