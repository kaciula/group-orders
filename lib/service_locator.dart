import 'package:get_it/get_it.dart';
import 'package:orders/blocs/data_bloc/data_bloc.dart';
import 'package:orders/data/local/misc/contacts_provider.dart';
import 'package:orders/data/local/misc/device_info_provider.dart';
import 'package:orders/data/local/misc/dynamic_links.dart';
import 'package:orders/data/local/misc/file_storage_service.dart';
import 'package:orders/data/local/misc/launcher.dart';
import 'package:orders/data/local/misc/log_handler.dart';
import 'package:orders/data/local/misc/permission_handler.dart';
import 'package:orders/data/local/store/app_info_provider.dart';
import 'package:orders/data/local/store/preferences.dart';
import 'package:orders/data/local/store/user_provider.dart';
import 'package:orders/data/remote/analytics.dart';
import 'package:orders/data/remote/connectors/apple_connector.dart';
import 'package:orders/data/remote/connectors/email_connector.dart';
import 'package:orders/data/remote/connectors/facebook_connector.dart';
import 'package:orders/data/remote/connectors/firebase_connector.dart';
import 'package:orders/data/remote/connectors/google_connector.dart';
import 'package:orders/data/remote/connectors/phone_verifier.dart';
import 'package:orders/data/remote/data_store/data_remote_store.dart';
import 'package:orders/data/remote/push_messaging.dart';
import 'package:orders/data/remote/uncaught_error_handler/uncaught_error_handler.dart';
import 'package:orders/data/remote/uncaught_error_handler/uncaught_error_handler_debug.dart';
import 'package:orders/data/remote/uncaught_error_handler/uncaught_error_handler_release.dart';
import 'package:orders/usecases/accept_invite.dart';
import 'package:orders/usecases/add_entry_to_cart.dart';
import 'package:orders/usecases/add_product_to_cart.dart';
import 'package:orders/usecases/check_user_existing.dart';
import 'package:orders/usecases/create_order.dart';
import 'package:orders/usecases/delete_product.dart';
import 'package:orders/usecases/duplicate_order.dart';
import 'package:orders/usecases/load_order_data.dart';
import 'package:orders/usecases/remove_from_cart.dart';
import 'package:orders/usecases/save_product.dart';
import 'package:orders/usecases/set_up_after_sign_in.dart';
import 'package:orders/usecases/set_up_on_start.dart';
import 'package:orders/usecases/update_order.dart';
import 'package:orders/utils/app_constants.dart';

GetIt getIt = GetIt.instance;

void registerInstances() {
  final DataBloc dataBloc = DataBloc();
  getIt.registerSingleton(dataBloc);

  final AppInfoProvider appInfoProvider = AppInfoProvider();
  getIt.registerSingleton<AppInfoProvider>(appInfoProvider);
  getIt.registerSingleton<UncaughtErrorHandler>(isProduction
      ? UncaughtErrorHandlerRelease(appInfoProvider)
      : UncaughtErrorHandlerDebug());
  final Preferences preferences = Preferences();
  getIt.registerSingleton(preferences);
  final UserProvider userProvider = UserProvider(preferences);
  getIt.registerSingleton(userProvider);
  getIt.registerSingleton(DeviceInfoProvider());
  getIt.registerSingleton(ContactsProvider());
  final Launcher launcher = Launcher();
  getIt.registerSingleton(launcher);
  getIt.registerSingleton(LogHandler(launcher));
  getIt.registerSingleton(PermissionHandler());
  getIt.registerSingleton(FileStorageService());

  final FirebaseConnector firebaseConnector = FirebaseConnector();
  getIt.registerSingleton(firebaseConnector);
  getIt.registerSingleton(GoogleConnector(firebaseConnector));
  getIt.registerSingleton(AppleConnector(firebaseConnector));
  getIt.registerSingleton(FacebookConnector(firebaseConnector));
  getIt.registerSingleton(EmailConnector());
  getIt.registerSingleton(PhoneVerifier());

  getIt.registerSingleton(Analytics());
  final DataRemoteStore dataRemoteStore = DataRemoteStore();
  getIt.registerSingleton<DataRemoteStore>(dataRemoteStore);
  final PushMessaging pushMessaging =
      PushMessaging(userProvider, dataRemoteStore, dataBloc);
  getIt.registerSingleton(pushMessaging);

  final DynamicLinks dynamicLinks = DynamicLinks(dataBloc);
  getIt.registerSingleton(dynamicLinks);

  getIt.registerSingleton(SetUpOnStart(
      userProvider, appInfoProvider, dataBloc, dataRemoteStore, pushMessaging));
  getIt.registerSingleton(
      SetUpAfterSignIn(dataRemoteStore, userProvider, pushMessaging, dataBloc));
  getIt.registerSingleton(CheckUserExisting(dataRemoteStore, dataBloc));
  final CreateOrder createOrder =
      CreateOrder(dataRemoteStore, dynamicLinks, dataBloc);
  getIt.registerSingleton(createOrder);
  getIt.registerSingleton(
      DuplicateOrder(createOrder, dataRemoteStore, dataBloc));
  getIt.registerSingleton(UpdateOrder(dataRemoteStore, dataBloc));
  getIt.registerSingleton(AcceptInvite(dataRemoteStore, dataBloc));
  getIt.registerSingleton(SaveProduct(dataRemoteStore, dataBloc));
  getIt.registerSingleton(DeleteProduct(dataRemoteStore, dataBloc));
  getIt.registerSingleton(LoadOrderData(dataRemoteStore, dataBloc));
  getIt.registerSingleton(AddProductToCart(dataRemoteStore, dataBloc));
  getIt.registerSingleton(AddEntryToCart(dataRemoteStore, dataBloc));
  getIt.registerSingleton(RemoveFromCart(dataRemoteStore, dataBloc));
}
