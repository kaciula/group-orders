import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:orders/app/app.dart';
import 'package:orders/data/remote/uncaught_error_handler/uncaught_error_handler.dart';
import 'package:orders/service_locator.dart';

import 'main_utils.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  registerInstances();

  FlutterError.onError = (FlutterErrorDetails details) async {
    await getIt<UncaughtErrorHandler>().handleFlutterError(details);
  };

  await init();

  startApp();
}

void startApp() {
  runZonedGuarded<Future<void>>(() async {
    runApp(ThisApp());
  }, (Object error, StackTrace stack) async {
    await getIt<UncaughtErrorHandler>().handleZonedError(error, stack);
  });
}
